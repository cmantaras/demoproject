﻿using GestorClientes.Dominio.Models;
using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;


namespace GestorClientes.Dominio.DAL
{
    public class GestorContext : DbContext
    {
        public GestorContext() : base("GestorContext")
        {
        }

  
        public virtual DbSet<Auditoria> Auditorias { get; set; }
        public virtual DbSet<AvisoPago> AvisosPago { get; set; }
        public virtual DbSet<Ciudad> Ciudades { get; set; }
        public virtual DbSet<Usuario> Usuarios { get; set; }
        public virtual DbSet<EstadoCliente> EstadosCliente { get; set; }
        public virtual DbSet<Zona> Zonas { get; set; }
        public virtual DbSet<Impuesto> Impuestos { get; set; }
        public virtual DbSet<Marca> Marcas { get; set; }
        public virtual DbSet<Item> Items { get; set; }

        public virtual DbSet<Categoria> Categorias { get; set; }
        public virtual DbSet<Proveedor> Proveedores { get; set; }
        public virtual DbSet<ProveedorBanco> ProveedorBanco { get; set; }
        public virtual DbSet<CuentaTipo> CuentaTipo { get; set; }
        public virtual DbSet<MetodoPago> MetodosPago { get; set; }
        public virtual DbSet<MoraCuota> MorasCuota { get; set; }

        public virtual DbSet<CierreMes> Cierres { get; set; }
        public virtual DbSet<Cliente> Clientes { get; set; }
        public virtual DbSet<PagoCliente> PagosCliente { get; set; }
        public virtual DbSet<Parametro> Parametros { get; set; }
        public virtual DbSet<Producto> Productos { get; set; }
        public virtual DbSet<ProductoPrecio> ProductoPrecios { get; set; }
        public virtual DbSet<ClienteProducto> ClienteProductos { get; set; }
        public virtual DbSet<ClienteProductoItem> ClienteProductoItems { get; set; }
        public virtual DbSet<ClienteProductoDeuda> ClienteProductoDeudas { get; set; }
        public virtual DbSet<Egreso> Egresos  { get; set; }



        public virtual DbSet<Rol> Roles { get; set; }
        public virtual DbSet<Permiso> Permisos { get; set; }
        public virtual DbSet<PermisoGrupo> PermisosGrupos { get; set; }





        public virtual DbSet<Moneda> Monedas { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            modelBuilder.Entity<ProductoPrecio>().HasMany<MetodoPago>(m => m.Metodos).WithMany(p => p.Productos).Map(pm =>
            {
                pm.MapLeftKey("ProductoPrecioId");
                pm.MapRightKey("MetodoPagoId");
                pm.ToTable("producto_precio_metodo_pago");
            });




            base.OnModelCreating(modelBuilder);
        }
    }
}
