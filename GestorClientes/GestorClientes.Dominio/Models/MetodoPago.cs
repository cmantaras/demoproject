﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Dominio.Models
{
    [Table("metodo_pago")]
    public class MetodoPago
    {
        public MetodoPago() { }
        public int Id { get; set; }
        public string Descripcion { get; set; }
        public int Cantidad { get; set; }
        public bool Activo { get; set; }

        public virtual List<ProductoPrecio> Productos { get; set; }
    }
}
