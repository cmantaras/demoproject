﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Dominio.Models
{
    [DataContract]
    [Table("rol")]
    public class Rol
    {
        public Rol() { }

        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Nombre { get; set; }
        [DataMember]
        public bool Activo { get; set; }

        public virtual ICollection<Permiso> Permisos { get; set; }
    }
}
