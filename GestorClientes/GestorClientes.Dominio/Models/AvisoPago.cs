﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Dominio.Models
{

    [Table("aviso_pago")]
    public class AvisoPago
    {
        public AvisoPago() { }
        public int Id { get; set; }
        public DateTime FechaImpresion { get; set; }

        public virtual List<ClienteProductoDeuda> ClienteProductoDeudas { get; set; }


    }
}
