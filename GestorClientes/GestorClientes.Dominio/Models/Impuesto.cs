﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Dominio.Models
{
    [Table("impuesto")]
    public class Impuesto
    {
        public Impuesto() { }
        public int Id { get; set; }
        public string Descripcion { get; set; }
        public double Porcentaje { get; set; }
        public bool Activo { get; set; }
    }
}
