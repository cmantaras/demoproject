﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Dominio.Models
{
    [Table("producto_precio")]
    public class ProductoPrecio
    {
        public ProductoPrecio() { }
        public int Id { get; set; }
        public int ProductoId { get; set; }
        public double Precio { get; set; }
        public DateTime Desde { get; set; }
        public int ImpuestoId { get; set; }
        public int MonedaId { get; set; }
        
        public bool Activo { get; set; }
        

        public virtual Producto Producto { get; set; }
        public virtual Impuesto Impuesto { get; set; }
        public virtual Moneda Moneda { get; set; }
        public virtual List<MetodoPago> Metodos { get; set; }
        
    }
}
