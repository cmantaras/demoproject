﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Dominio.Models
{
    [Table("usuario")]
    public class Usuario
    {

        public Usuario() { }
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string ChangePasswordToken { get; set; }
        public DateTime? TokenDate { get; set; }
        public DateTime? LastPasswordChange { get; set; }
        public DateTime? LastLogin { get; set; }
        public string Email { get; set; }
        public bool IsAdmin { get; set; }
        public int? RolId { get; set; }
        public bool Activo { get; set; }

        public virtual Rol Rol { get; set; }

    }
}
