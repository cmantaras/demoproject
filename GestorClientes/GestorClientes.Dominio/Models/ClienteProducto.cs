﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Dominio.Models
{
    [Table("cliente_producto")]
    public class ClienteProducto
    {
        public ClienteProducto() { }
        public int Id { get; set; }
        public int ClienteId { get; set; }
        public int ProductoPrecioId { get; set; }
      

        public int MetodoPagoId { get; set; }
        public DateTime Desde { get; set; }
        public DateTime Hasta { get; set; }
        public bool Activo { get; set; }

        public virtual MetodoPago MetodoPago { get; set; }
        public virtual Cliente Cliente { get; set; }
        public virtual ProductoPrecio ProductoPrecio { get; set; }

        public virtual List<ClienteProductoDeuda> Cuotas { get; set; }
        public virtual List<ClienteProductoItem> Items { get; set; }
    }
}
