﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace GestorClientes.Dominio.Models
{
    [Table("egreso")]
    public class Egreso
    {
        public int Id { get; set; }
        public DateTime? FechaEgreso { get; set; }
        public DateTime? FechaCheque { get; set; }
        public double Importe { get; set; }
        public string Descripcion { get; set; }
        public int MonedaId { get; set; }
        public int CategoriaId { get; set; }
        public int ProveedorId { get; set; }
        public bool EsCheque { get; set; }
        public bool Activo { get; set; }

        public virtual Moneda Moneda {get; set;}
        public virtual Categoria Categoria { get; set; }
        public virtual Proveedor Proveedor { get; set; }
    }
}
