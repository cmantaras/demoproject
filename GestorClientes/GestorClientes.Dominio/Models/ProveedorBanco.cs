﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Dominio.Models
{
    [Table("proveedor_banco")]
    public class ProveedorBanco
    {
        public ProveedorBanco() { }
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Cuenta { get; set; }

        public int? ProveedorId { get; set; }
        public int? CuentaTipoId { get; set; }
        public int? MonedaId { get; set; }
        public virtual Proveedor Proveedor { get; set; }
        public virtual CuentaTipo CuentaTipo { get; set; }
        public virtual Moneda Moneda{ get; set; }



    }
}
