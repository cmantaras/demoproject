﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Dominio.Models
{
    [Table("permisos_grupos")]
    public class PermisoGrupo
    {
        public PermisoGrupo() { }

        public int Id { get; set; }

        public string Nombre { get; set; }

        public virtual ICollection<Permiso> Permisos { get; set; }
    }
}
