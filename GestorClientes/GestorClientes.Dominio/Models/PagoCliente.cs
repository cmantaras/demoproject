﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Dominio.Models
{
    [Table("pago_cliente")]
    public class PagoCliente
    {

        public PagoCliente() { }
        public int Id { get; set; }
        public int ClienteId { get; set; }
        public double MontoTotal { get; set; }
        public DateTime Fecha { get; set; }
        public virtual Cliente Cliente { get; set; }
        public virtual List<ClienteProductoDeuda> Cuotas { get; set; }
    }
}
