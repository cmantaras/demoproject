﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Dominio.Models
{

    [Table("cliente_producto_item")]
    public class ClienteProductoItem
    {
        public ClienteProductoItem() { }
        public int Id { get; set; }
        public int ClienteProductoId { get; set; }
        public int ItemId { get; set; }
        public string Valor { get; set; }
        public bool Activo { get; set; }

        public virtual ClienteProducto ClienteProducto { get; set; }
        public virtual Item Item { get; set; }
    }
}
