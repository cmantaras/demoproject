﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Dominio.Models
{
    [Table("cuenta_tipo")]
    public class CuentaTipo
    {
        public CuentaTipo() { }
        public int Id { get; set; }
        public string Descripcion { get; set; }
        


    }
}
