﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Dominio.Models
{
    [Table("cliente_estado")]
    public class ClienteEstado
    {
        public ClienteEstado() { }
        public int Id { get; set; }
        public int ClienteId { get; set; }
        public int EstadoClienteId { get; set; }
        public DateTime Desde { get; set; }
        public bool Activo { get; set; }
    
        public virtual EstadoCliente EstadoCliente { get; set; }
    }

}
