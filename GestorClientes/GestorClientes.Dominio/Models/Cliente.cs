﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Dominio.Models
{
    [Table("cliente")]
    public class Cliente
    {
        public Cliente() { }
        public int Id { get; set; }
        public string Identificador { get; set; }
        public string Hte { get; set; }
        public string Nombre { get; set; }
        public string Apellido{ get; set; }
        public string Telefono { get; set; }
        public string Direccion { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public DateTime FechaHasta { get; set; }
        public int CiudadId { get; set; }
        public int ZonaId { get; set; }

        public bool ClienteViejo { get; set; }

        public int EstadoId { get; set; }

        public bool Activo { get; set; }
        public virtual Ciudad Ciudad { get; set; }
        public virtual Zona Zona { get; set; }
        public virtual EstadoCliente Estado { get; set; }
        public virtual ICollection<ClienteEstado> Estados {get; set;}
        public virtual ICollection<ClienteProducto> ClienteProductos { get; set; }
        public virtual ICollection<PagoCliente> Pagos { get; set; }
    }
}
