﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Dominio.Models
{
    [Table("permisos")]
    public class Permiso
    {
        public Permiso() { }

        public int Id { get; set; }
        public int GrupoId { get; set; }
        public string Key { get; set; }
        public string Descripcion { get; set; }

        public virtual ICollection<Rol> Roles { get; set; }
        public virtual PermisoGrupo Grupo { get; set; }
    }
}
