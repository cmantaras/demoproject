﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Dominio.Models
{
    [Table("cierre_mes")]
    public class CierreMes
    {
        public CierreMes() { }
        public int Id { get; set; }
        public DateTime Fecha { get; set; }


    }
}
