﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Dominio.Models
{
    [Table("proveedor")]
    public class Proveedor
    {
        public Proveedor() { }
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string RazonSocial { get; set; }
        public string RUT { get; set; }
        public string Direccion { get; set; }
        public string Ciudad { get; set; }
        public string Telefono { get; set; }
        public string Correo { get; set; }
        public string Contacto { get; set; }
        public string ContactoTelefono { get; set; }
        public string ContactoCorreo { get; set; }

        public bool Activo { get; set; }
        /*public int CiudadId { get; set; }
        public virtual Ciudad Ciudad { get; set; }*/
  
    }
}
