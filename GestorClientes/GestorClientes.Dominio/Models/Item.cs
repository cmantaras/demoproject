﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Dominio.Models
{
    [Table("item")]
    public class Item
    {
        public Item() { }
        public int Id { get; set; }
        public string Descripcion { get; set; }
        //public string Color { get; set; }
        public bool Activo { get; set; }
    }
}
