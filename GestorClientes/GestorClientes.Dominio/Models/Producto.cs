﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Dominio.Models
{
    [Table("producto")]
   public class Producto
    {
        public Producto() { }
        public int Id { get; set; }
        public string Codigo { get; set; }
        public string Descripcion { get; set; }

        public int? MarcaId { get; set; }
        public bool Activo { get; set; }
        public bool Servicio { get; set; }
        public virtual Marca Marca { get; set; }
        public virtual List<ProductoPrecio> Precios { get; set; }
    }
}
