﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Dominio.Models
{
    [Table("mora_cuota")]
    public class MoraCuota
    {
        public MoraCuota() { }
        public int Id { get; set; }
        public string Descripcion { get; set; }
        public int Porcentaje { get; set; }
    }
}
