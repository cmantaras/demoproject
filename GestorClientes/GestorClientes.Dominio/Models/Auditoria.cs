﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Dominio.Models
{
    [Table("auditoria")]
    public class Auditoria
    {

        public int Id { get; set; }
        public DateTime? Fecha { get; set; }
        public int? UsuarioId { get; set; }
        public string Accion { get; set; }
        public string Detalle { get; set; }
        public string DatoTipo { get; set; }
        public int DatoId { get; set; }
        public string Origen { get; set; }

        public virtual Usuario Usuario { get; set; }
    }
}
