﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Dominio.Models
{
    [Table("cliente_producto_deuda")]
    public class ClienteProductoDeuda
    {
        public ClienteProductoDeuda() { }
        public int Id { get; set; }
        public int ClienteProductoId { get; set; }
        public int? CierreMesId { get; set; }
        public int Cuota { get; set; }
        public double Importe { get; set; }
        public int? MoraCuotaId { get; set; }
        public DateTime? FechaPago { get; set; }
        public DateTime? BajaManual { get; set; }
        public int? PagoClienteId { get; set; }
        public bool Activo { get; set; }
        public int? AvisoPagoId { get; set; }
        public virtual ClienteProducto ClienteProducto { get; set; }
        public virtual CierreMes Cierre { get; set; }

        public virtual MoraCuota MoraCuota { get; set; }
    }



}

