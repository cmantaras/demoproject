﻿using GestorClientes.Dominio.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto
{
    public class MonedaDto
    {
        public MonedaDto() { }

        public int Id { get; set; }
        public string Descripcion { get; set; }
       

        public MonedaDto(Moneda moneda)
        {
            this.Id = moneda.Id;
            this.Descripcion = moneda.Descripcion;
            
        }
    }
}
