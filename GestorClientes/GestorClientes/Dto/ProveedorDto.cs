﻿using GestorClientes.Dominio.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto
{
    public class ProveedorDto
    {
        public ProveedorDto() { }

        public int Id { get; set; }
        public string Nombre { get; set; }
        public string RazonSocial { get; set; }
        public string RUT { get; set; }
        public string Direccion { get; set; }
        public string Ciudad { get; set; }
        public string Telefono { get; set; }
        public string Correo { get; set; }
        public string Contacto { get; set; }
        public string ContactoTelefono { get; set; }
        public string ContactoCorreo { get; set; }
        public bool Activo { get; set; }
        /*public int CiudadId { get; set; }
        public string CiudadDescripcion { get; set; }*/
        public int ProveedorCategoriaId { get; set; }
        public ChequeDto ChequePorVencer { get; set; }
       // public CiudadDto Ciudad { get; set; }

        public ProveedorDto(Proveedor proveedor)
        {
            this.Id = proveedor.Id;
            this.Nombre= proveedor.Nombre;
            this.RazonSocial = proveedor.RazonSocial;
            this.RUT = proveedor.RUT;
            this.Direccion = proveedor.Direccion;
            this.Ciudad = proveedor.Ciudad;
            this.Telefono = proveedor.Telefono;
            this.Correo = proveedor.Correo;
            this.Contacto = proveedor.Contacto;
            this.ContactoCorreo = proveedor.ContactoCorreo;
            this.ContactoTelefono = proveedor.ContactoTelefono;
            this.Activo = proveedor.Activo;
           // this.CiudadId = proveedor.CiudadId;
            //this.Ciudad = (proveedor.Ciudad == null) ? null : new CiudadDto(proveedor.Ciudad);
        }
        
    }
}
