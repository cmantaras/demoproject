﻿using GestorClientes.Dominio.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto
{
    public class MarcaDto
    {
        public MarcaDto() { }

        public int Id { get; set; }
        public string Descripcion { get; set; }
        public bool Activo { get; set; }

        public MarcaDto(Marca marca)
        {
            this.Id = marca.Id;
            this.Descripcion = marca.Descripcion;
            this.Activo = marca.Activo;
        }
    }
}
