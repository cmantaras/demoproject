﻿using GestorClientes.Dominio.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto
{
    public class EgresoDto
    {
        public EgresoDto() { }

        public int Id { get; set; }
        public double Importe { get; set; }
        public bool Activo { get; set; }
        public bool EsCheque { get; set; }
        public DateTime? FechaEgreso { get; set; }

        public string Descripcion { get; set; }
        public DateTime? FechaCheque { get; set; }

        public CategoriaDto Categoria { get; set; }
        public ProveedorDto Proveedor { get; set; }
        public MonedaDto Moneda { get; set; }




    public EgresoDto(Egreso egreso)
        {
            this.Id = egreso.Id;
            this.Importe = egreso.Importe;
            this.Categoria = egreso.Categoria != null ? new CategoriaDto(egreso.Categoria) : null;
            this.Moneda = egreso.Moneda != null ? new MonedaDto(egreso.Moneda) : null;
            this.FechaEgreso = egreso.FechaEgreso != default ? egreso.FechaEgreso : default;
            this.Proveedor = egreso.Proveedor != null ? new ProveedorDto(egreso.Proveedor) : null;
            this.EsCheque = egreso.EsCheque;
            this.FechaEgreso = egreso.FechaEgreso;
            this.FechaCheque = egreso.FechaCheque != default ? egreso.FechaCheque : default;
            this.Descripcion = egreso.Descripcion;
        }

       
    }
}
