﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto
{
    public class UsuarioPermisoDto
    {
        public List<PermisoDto> Permisos { get; set; }
        public bool IsAdmin { get; set; }

        public UsuarioPermisoDto(UsuarioDto usuarioDto)
        {
            IsAdmin = usuarioDto.IsAdmin;
            Permisos = usuarioDto.Rol != null ? usuarioDto.Rol.Permisos : new List<PermisoDto>();
        }
    }
}
