﻿using GestorClientes.Dominio.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto
{
    public class ProveedorBancoDto
    {
        public ProveedorBancoDto() { }

        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Cuenta { get; set; }

        public int? ProveedorId { get; set; }
        public int? CuentaTipoId { get; set; }
        public int? MonedaId { get; set; }

        public ProveedorBancoDto(ProveedorBanco proveedorBanco)
        {
            this.Id = proveedorBanco.Id;
            this.Nombre = proveedorBanco.Nombre;
            this.Cuenta = proveedorBanco.Cuenta;
            this.ProveedorId = proveedorBanco.ProveedorId;
            this.CuentaTipoId = proveedorBanco.CuentaTipoId;
            this.MonedaId = proveedorBanco.MonedaId;

        }

    }
}
