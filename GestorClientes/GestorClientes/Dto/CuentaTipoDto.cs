﻿using GestorClientes.Dominio.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto
{
    public class CuentaTipoDto
    {
        public CuentaTipoDto() { }

        public int Id { get; set; }
        public string Descripcion { get; set; }

        public CuentaTipoDto(CuentaTipo cuenta)
        {
            this.Id = cuenta.Id;
            this.Descripcion = cuenta.Descripcion;


        }

    }
}
