﻿using GestorClientes.Dominio.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto
{
    public class ProductoDto
    {
        public ProductoDto() { }

        public int Id { get; set; }
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
        public int? MarcaId { get; set; }

        public MarcaDto Marca { get; set; }
        public bool Activo { get; set; }
        public bool Servicio { get; set; }
        public ProductoPrecioDto PrecioActual { get; set; }

        public ProductoDto(Producto producto)
        {           
            this.Id = producto.Id;
            this.Codigo = producto.Codigo;
            this.MarcaId = producto.MarcaId;
            this.Descripcion = producto.Descripcion;
            this.Marca = (producto.Marca == null) ? null : new MarcaDto(producto.Marca);
            this.Activo = producto.Activo;
            this.PrecioActual = (producto.Precios.Where(x => x.Activo && x.Desde <= DateTime.Now).OrderByDescending(y => y.Desde).FirstOrDefault() != null)
                                    ? new ProductoPrecioDto(producto.Precios.Where(x => x.Activo && x.Desde <= DateTime.Now).OrderByDescending(y => y.Desde).FirstOrDefault())
                                    : new ProductoPrecioDto(producto.Precios.Where(x => x.Activo).OrderByDescending(y => y.Desde).FirstOrDefault()) ;
            this.Servicio = producto.Servicio;
        }

        public ProductoDto(Producto producto, bool getPrecioActual = false)
        {
            this.Id = producto.Id;
            this.Codigo = producto.Codigo;
            this.MarcaId = producto.MarcaId;
            this.Descripcion = producto.Descripcion;
            this.Marca = (producto.Marca == null) ? null : new MarcaDto(producto.Marca);
            this.Activo = producto.Activo;
            this.Servicio = producto.Servicio;

        }


    }
}
