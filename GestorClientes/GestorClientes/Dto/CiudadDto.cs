﻿using GestorClientes.Dominio.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto
{
    public class CiudadDto
    {
        public CiudadDto() { }

        public int Id { get; set; }
        public string Descripcion { get; set; }
        public bool Activo { get; set; }

        public CiudadDto(Ciudad ciudad)
        {
            this.Id = ciudad.Id;
            this.Descripcion = ciudad.Descripcion;
            this.Activo = ciudad.Activo;
        }
    }
}
