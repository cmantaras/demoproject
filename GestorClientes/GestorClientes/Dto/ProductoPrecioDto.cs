﻿using GestorClientes.Dominio.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto
{
    public class ProductoPrecioDto
    {
        public ProductoPrecioDto() { }
        public int Id { get; set; }
        public int ProductoId { get; set; }
        public double Precio { get; set; }
        public DateTime Desde { get; set; }
        public int ImpuestoId { get; set; }
        public int MonedaId { get; set; }
        public int[] MetodosIds { get; set; }

        public ProductoDto Producto { get; set; }
        public MonedaDto Moneda { get; set; }
        public ImpuestoDto Impuesto { get; set; }
        public List<MetodoPagoDto> Metodos { get; set; }
        public ProductoPrecioDto(ProductoPrecio productoPrecio)
        {
            this.Id = productoPrecio.Id;
            this.ProductoId = productoPrecio.ProductoId;
            this.Precio = productoPrecio.Precio;
            this.Desde = productoPrecio.Desde;
            this.ImpuestoId = productoPrecio.ImpuestoId;
            this.Producto = (productoPrecio.Producto == null) ? null : new ProductoDto(productoPrecio.Producto,false);
            this.Impuesto = (productoPrecio.Impuesto == null) ? null : new ImpuestoDto(productoPrecio.Impuesto);
            this.Metodos = productoPrecio.Metodos.Where(x => x.Activo).ToList().Select(x => new MetodoPagoDto(x)).ToList();
            this.MetodosIds = this.Metodos.Select(m => m.Id).ToArray();
            this.MonedaId = productoPrecio.MonedaId;
            this.Moneda = (productoPrecio.Moneda == null) ? null : new MonedaDto(productoPrecio.Moneda);

        }

        public ProductoPrecioDto(ProductoPrecio productoPrecio, bool getProducto= false)
        {
            this.Id = productoPrecio.Id;
            this.ProductoId = productoPrecio.ProductoId;
            this.Precio = productoPrecio.Precio;
            this.Desde = productoPrecio.Desde;
            this.ImpuestoId = productoPrecio.ImpuestoId;

            this.Impuesto = (productoPrecio.Impuesto == null) ? null : new ImpuestoDto(productoPrecio.Impuesto);
            this.Metodos = productoPrecio.Metodos.Where(x => x.Activo).ToList().Select(x => new MetodoPagoDto(x)).ToList();
            this.MonedaId = productoPrecio.MonedaId;
            this.Moneda = (productoPrecio.Moneda == null) ? null : new MonedaDto(productoPrecio.Moneda);

        }


    }
}
