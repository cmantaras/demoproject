﻿using GestorClientes.Dominio.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto
{
    public class ItemDto
    {
        public ItemDto() { }

        public int Id { get; set; }
        public string Descripcion { get; set; }
        //public string Color { get; set; }
        public bool Activo { get; set; }

        public ItemDto(Item item)
        {
            this.Id = item.Id;
            this.Descripcion = item.Descripcion;
            //this.Color = item.Color;
            this.Activo = item.Activo;
        }
    }
}
