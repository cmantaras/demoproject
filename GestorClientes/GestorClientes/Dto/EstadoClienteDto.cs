﻿using GestorClientes.Dominio.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto
{
    public class EstadoClienteDto
    {
        public EstadoClienteDto() { }

        public int Id { get; set; }
        public string Descripcion { get; set; }
        public bool Tipo { get; set; }
        public bool Activo { get; set; }

        public EstadoClienteDto(EstadoCliente estadoCliente)
        {
            this.Id = estadoCliente.Id;
            this.Descripcion = estadoCliente.Descripcion;
            this.Tipo = estadoCliente.Tipo;
            this.Activo = estadoCliente.Activo;
        }
    }
}
