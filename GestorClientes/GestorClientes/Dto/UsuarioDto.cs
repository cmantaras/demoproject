﻿using GestorClientes.Dominio.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto
{
    public class UsuarioDto
    {
        public UsuarioDto() { }

        public int Id { get; set; }
        public string Username { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Email { get; set; }
        public bool IsAdmin { get; set; }
        public int? RolId { get; set; }

        public RolDto Rol { get; set; }

        public UsuarioDto(Usuario usuario)
        {
            this.Id = usuario.Id;
            this.Username = usuario.Username;
            this.Nombre = usuario.Nombre;
            this.Apellido = usuario.Apellido;
            this.Email = usuario.Email;
            this.IsAdmin = usuario.IsAdmin;
            this.RolId = usuario.RolId;
            this.Rol = usuario.Rol != null ? new RolDto(usuario.Rol) : null;
        }
    }
}
