﻿using GestorClientes.Dominio.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto
{
    public class CategoriaDto
    {
        public CategoriaDto() { }

        public int Id { get; set; }
        public string Descripcion { get; set; }
        public bool Activo { get; set; }

        public CategoriaDto(Categoria Categoria)
        {
            this.Id = Categoria.Id;
            this.Descripcion = Categoria.Descripcion;
            this.Activo = Categoria.Activo;
        }
    }
}
