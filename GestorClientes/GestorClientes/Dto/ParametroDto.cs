﻿using GestorClientes.Dominio.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto
{
    public class ParametroDto
    {
        public ParametroDto() { }

        public string Codigo { get; set; }
        public string Valor { get; set; }

        public ParametroDto(Parametro parametro)
        {
            this.Codigo = parametro.Codigo;
            this.Valor = parametro.Valor;
        }
    }
}
