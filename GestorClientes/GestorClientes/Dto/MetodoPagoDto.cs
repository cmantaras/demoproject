﻿using GestorClientes.Dominio.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto
{
    public class MetodoPagoDto
    {
        public MetodoPagoDto() { }

        public int Id { get; set; }
        public string Descripcion { get; set; }
        public int Cantidad { get; set; }
        public bool Activo { get; set; }

        public MetodoPagoDto(MetodoPago metodoPago)
        {
            this.Id = metodoPago.Id;
            this.Descripcion = metodoPago.Descripcion;
            this.Cantidad = metodoPago.Cantidad;
            this.Activo = metodoPago.Activo;
        }
    }
}
