﻿using GestorClientes.Dominio.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto
{
    public class PagoClienteDto
    {
        public PagoClienteDto() { }

        public int Id { get; set; }
        public ClienteDto Cliente { get; set; }
        public int ClienteId { get; set; }
        public double MontoTotal { get; set; }
        public DateTime Fecha { get; set; }
        public string Detalle { get; set; }

        public bool CanDelete { get; set; }


        public PagoClienteDto(PagoCliente pagoCliente)
        {
            this.Id = pagoCliente.Id;
            this.ClienteId = pagoCliente.Cliente.Id;
            this.Cliente = new ClienteDto(pagoCliente.Cliente);
            this.MontoTotal = pagoCliente.MontoTotal;
            this.Fecha = pagoCliente.Fecha;
            this.Detalle = String.Join("-", pagoCliente.Cuotas.Select(p => p.ClienteProducto.ProductoPrecio.Producto.Descripcion));
            this.CanDelete = !pagoCliente.Cuotas.Any(x => x.CierreMesId != null);
        }
    }
}
