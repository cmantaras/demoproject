﻿using GestorClientes.Dominio.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto
{
    public class ClienteProductoDto
    {
        public int Id { get; set; }
        public ProductoPrecioDto ProductoPrecio { get; set; }
        public ClienteDto Cliente { get; set; }
        public MetodoPagoDto MetodoPago { get; set; }
        public DateTime Desde { get; set; }
        public DateTime Hasta { get; set; }
        public bool Activo { get; set; }
        public List<ClienteProductoDeudaDto> Cuotas { get; set; }
        public List<ClienteProductoItemDto> Items { get; set; }

        public ClienteProductoDto() { }
        public ClienteProductoDto(ClienteProducto clienteProducto)
        {   
          
            this.Id = clienteProducto.Id;
            this.Desde = clienteProducto.Desde;
            this.Hasta = clienteProducto.Hasta;
            this.Activo = clienteProducto.Activo;
            this.MetodoPago = (clienteProducto.MetodoPago == null) ? null : new MetodoPagoDto(clienteProducto.MetodoPago);
            this.ProductoPrecio = (clienteProducto.ProductoPrecio == null) ? null : new ProductoPrecioDto(clienteProducto.ProductoPrecio);
           // this.Cuotas = clienteProducto.Cuotas == null ? null : clienteProducto.Cuotas.Where(i => i.Activo).ToList().Select(x => new ClienteProductoDeudaDto(x)).ToList();
            this.Items = (clienteProducto.Items == null) ? null : clienteProducto.Items.Where(i => i.Activo).ToList().Select(x => new ClienteProductoItemDto(x)).ToList(); 

        }

        public static ClienteProductoDto ProductoWithCuotas(ClienteProducto clienteProducto)
        {
            ClienteProductoDto clienteProductoDto = new ClienteProductoDto();
         

            clienteProductoDto.Id = clienteProducto.Id;
            clienteProductoDto.Desde = clienteProducto.Desde;
            clienteProductoDto.Hasta = clienteProducto.Hasta;
            clienteProductoDto.MetodoPago = (clienteProducto.MetodoPago == null) ? null : new MetodoPagoDto(clienteProducto.MetodoPago);
            clienteProductoDto.ProductoPrecio = (clienteProducto.ProductoPrecio == null) ? null : new ProductoPrecioDto(clienteProducto.ProductoPrecio);
            clienteProductoDto.Cuotas = clienteProducto.Cuotas == null ? null : clienteProducto.Cuotas.Where(i => i.Activo).ToList().Select(x => ClienteProductoDeudaDto.CuotaProd(x)).ToList();

            return clienteProductoDto;
        }
    }
}
