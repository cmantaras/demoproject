﻿using GestorClientes.Dominio.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto
{
    public class AuditoriaDto
    {

        public AuditoriaDto() { }

        public int Id { get; set; }
        public DateTime? Fecha { get; set; }
        public UsuarioDto Usuario { get; set; }
        public string Accion { get; set; }
        public string Detalle { get; set; }
        public string DatoTipo { get; set; }
        public int DatoId { get; set; }
        public object DetalleObj { get; set; }

        public string Origen { get; set; }

        public AuditoriaDto(Auditoria auditoria)
        {
            this.Id = auditoria.Id;
            this.Fecha = auditoria.Fecha;
            this.Usuario = (auditoria.Usuario == null) ? null : new UsuarioDto(auditoria.Usuario);
            this.Accion = auditoria.Accion;
            this.Detalle = auditoria.Detalle;
            this.DatoTipo = auditoria.DatoTipo;
            this.DatoId = auditoria.DatoId;
            this.DetalleObj = Newtonsoft.Json.JsonConvert.DeserializeObject(auditoria.Detalle);
            this.Origen = auditoria.Origen;
        }
    }
}
