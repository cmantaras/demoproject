﻿using GestorClientes.Dominio.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto
{
    public class ClienteEstadoDto
    {
        public int Id { get; set; }
        public int ClienteId { get; set; }
        public int EstadoClienteId { get; set; }
        public DateTime Desde { get; set; } 
        public EstadoClienteDto EstadoCliente { get; set; }

        public ClienteEstadoDto(ClienteEstado clienteEstado)
        {
            if (clienteEstado == null)
                return;
            this.Id = clienteEstado.Id;
            this.ClienteId = clienteEstado.ClienteId;
            this.EstadoClienteId = clienteEstado.EstadoClienteId;
            this.Desde = clienteEstado.Desde;
            this.EstadoCliente = (clienteEstado.EstadoCliente == null) ? null : new EstadoClienteDto(clienteEstado.EstadoCliente);
        }
    }
}
