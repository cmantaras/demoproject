﻿using GestorClientes.Dominio.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto
{
    public class ClienteProductoDeudaDto
    {
        public int Id { get; set; }
        public int ClienteProductoId { get; set; }
        public int Cuota { get; set; }
        public double Importe { get; set; }
        public DateTime? FechaPago { get; set; }
        public DateTime? CierreMes { get; set; }

        public ClienteProductoDto ClienteProducto { get; set; }
        public MoraCuotaDto MoraCuota { get; set; }

        ClienteProductoDeudaDto() { }
        public ClienteProductoDeudaDto(ClienteProductoDeuda clienteProductoDeuda, bool withProducto = true)
        {
            this.CierreMes = (clienteProductoDeuda.Cierre != null) ? clienteProductoDeuda.Cierre.Fecha : default;
            this.Id = clienteProductoDeuda.Id != default ? clienteProductoDeuda.Id : default;
            this.ClienteProductoId = clienteProductoDeuda.ClienteProductoId != default ? clienteProductoDeuda.ClienteProductoId : default;
            this.Cuota = clienteProductoDeuda.Cuota != default ? clienteProductoDeuda.Cuota : default;
            this.Importe = clienteProductoDeuda.Id != default ? clienteProductoDeuda.Importe : default;
            this.FechaPago = clienteProductoDeuda.FechaPago.HasValue ? clienteProductoDeuda.FechaPago : null;
            this.MoraCuota = clienteProductoDeuda.MoraCuota != default ? new MoraCuotaDto(clienteProductoDeuda.MoraCuota) : default;
            if (withProducto)
            {
                this.ClienteProducto = clienteProductoDeuda != null ? new ClienteProductoDto(clienteProductoDeuda.ClienteProducto) : null;
            }
       
        }


        public static ClienteProductoDeudaDto CuotaProd(ClienteProductoDeuda clienteproductodeuda)
        {
            ClienteProductoDeudaDto clienteProductoDeudaDto = new ClienteProductoDeudaDto();
            clienteProductoDeudaDto.Id = clienteproductodeuda.Id != default ? clienteproductodeuda.Id : default;
            clienteProductoDeudaDto.Cuota = clienteproductodeuda.Cuota != default ? clienteproductodeuda.Cuota : default;
            clienteProductoDeudaDto.Importe = clienteproductodeuda.Id != default ? clienteproductodeuda.Importe : default;

            clienteProductoDeudaDto.MoraCuota = clienteproductodeuda.MoraCuota != default ? new MoraCuotaDto(clienteproductodeuda.MoraCuota) : default;

            return clienteProductoDeudaDto;

        }
    }
}
