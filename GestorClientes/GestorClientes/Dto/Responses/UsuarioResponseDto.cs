﻿using GestorClientes.Logic.Dto.Requests;
using GestorClientes.Logic.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto.Responses
{
    public class UsuarioResponseDto : PageResult
    {
        public UsuarioResponseDto()
        {
        }

        public List<UsuarioDto> Usuarios { get; set; }

        public UsuarioResponseDto(int count, List<UsuarioDto> usuarios)
        {
            this.Count = count;
            this.Usuarios = usuarios;
        }
    }
}
