﻿using GestorClientes.Logic.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto.Responses
{

    public class MarcaResponseDto : PageResult
    {
        public MarcaResponseDto()
        {
        }

        public List<MarcaDto> Marcas { get; set; }

        public MarcaResponseDto(int count, List<MarcaDto> Marcas)
        {
            this.Count = count;
            this.Marcas = Marcas;
        }
    }
}
