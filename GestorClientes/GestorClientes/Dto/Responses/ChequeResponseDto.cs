﻿using GestorClientes.Logic.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto.Responses
{

    public class ChequeResponseDto : PageResult
    {
        public ChequeResponseDto()
        {
        }

        public List<ChequeDto> Cheques { get; set; }

        public ChequeResponseDto(int count, List<ChequeDto> cheques)
        {
            this.Count = count;
            this.Cheques = cheques;
        }
    }
}
