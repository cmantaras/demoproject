﻿using GestorClientes.Logic.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto.Responses
{

    public class ProductoResponseDto : PageResult
    {
        public ProductoResponseDto()
        {
        }

        public List<ProductoDto> Productos { get; set; }

        public ProductoResponseDto(int count, List<ProductoDto> producto)
        {
            this.Count = count;
            this.Productos = producto;
        }
    }
}
