﻿using GestorClientes.Logic.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto.Responses
{
    public class ClienteResponseDto : PageResult
    {
        public ClienteResponseDto()
        {
        }

        public List<ClienteDto> Clientes { get; set; }

        public ClienteResponseDto(int count, List<ClienteDto> clientes)
        {
            this.Count = count;
            this.Clientes = clientes;
        }
    }
}
