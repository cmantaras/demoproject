﻿using GestorClientes.Logic.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto.Responses
{

    public class ItemResponseDto : PageResult
    {
        public ItemResponseDto()
        {
        }

        public List<ItemDto> Items { get; set; }

        public ItemResponseDto(int count, List<ItemDto> items)
        {
            this.Count = count;
            this.Items = items;
        }
    }
}
