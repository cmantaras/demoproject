﻿using GestorClientes.Dominio.Models;
using GestorClientes.Logic.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto.Responses
{
    public class EstadoClienteResponseDto : PageResult
    {
        public EstadoClienteResponseDto()
        {
        }

        public List<EstadoClienteDto> EstadosCliente { get; set; }

        public EstadoClienteResponseDto(int count, List<EstadoClienteDto> estadosCliente)
        {
            this.Count = count;
            this.EstadosCliente = estadosCliente;
        }

    }
}
