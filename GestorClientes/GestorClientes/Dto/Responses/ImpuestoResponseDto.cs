﻿using GestorClientes.Logic.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto.Responses
{

    public class ImpuestoResponseDto : PageResult
    {
        public ImpuestoResponseDto()
        {
        }

        public List<ImpuestoDto> Impuestos { get; set; }

        public ImpuestoResponseDto(int count, List<ImpuestoDto> impuestos)
        {
            this.Count = count;
            this.Impuestos = impuestos;
        }
    }
}
