﻿using GestorClientes.Logic.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto.Responses
{
    public class AuditoriaResponseDto : PageResult
    {
        public AuditoriaResponseDto()
        {
        }

        public List<AuditoriaDto> Auditorias { get; set; }

        public AuditoriaResponseDto(int count, List<AuditoriaDto> auditorias)
        {
            this.Count = count;
            this.Auditorias = auditorias;
        }

    }
}
