﻿using GestorClientes.Logic.Dto;
using GestorClientes.Logic.Dto.Requests;
using GestorClientes.Logic.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto.Responses
{
    public class RolResponseDto : PageResult
    {
        public RolResponseDto()
        {
        }

        public List<RolDto> Roles { get; set; }

        public RolResponseDto(int count, List<RolDto> roles)
        {
            this.Count = count;
            this.Roles = roles;
        }
    }
}
