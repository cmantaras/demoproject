﻿using GestorClientes.Logic.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto.Responses
{
    public class ProveedorResponseDto : PageResult
    {
        public ProveedorResponseDto()
        {
        }

        public List<ProveedorDto> Proveedores { get; set; }

        public ProveedorResponseDto(int count, List<ProveedorDto> proveedores)
        {
            this.Count = count;
            this.Proveedores = proveedores;
        }
    }
}
