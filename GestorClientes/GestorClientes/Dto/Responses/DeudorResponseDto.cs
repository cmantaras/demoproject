﻿using GestorClientes.Logic.Dto.Requests;
using GestorClientes.Logic.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto.Responses
{
    public class DeudorResponseDto : PageResult
    {
        public DeudorResponseDto()
        {
        }

        public List<DeudorDto> Deudores { get; set; }

        public DeudorResponseDto(int count, List<DeudorDto> deudores)
        {
            this.Count = count;
            this.Deudores = deudores;
        }
    }
}
