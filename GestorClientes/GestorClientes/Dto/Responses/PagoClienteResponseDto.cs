﻿using GestorClientes.Logic.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto.Responses
{

    public class PagoClienteResponseDto : PageResult
    {
        public PagoClienteResponseDto()
        {
        }

        public List<PagoClienteDto> Pagos { get; set; }
        public double? TotalIngreso { get; set; }

        public PagoClienteResponseDto(int count, List<PagoClienteDto> pagosCliente, double? total = 0)
        {
            this.TotalIngreso = total;
            this.Count = count;
            this.Pagos = pagosCliente;
        }
    }
}
