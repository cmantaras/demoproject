﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto.Responses
{
    public class AvisoPagoResponseDto
    {
        public List<AvisoPagoDto> AvisosPago;

        public AvisoPagoResponseDto(List<AvisoPagoDto> avisos)
        {

            this.AvisosPago = avisos;
        }

    }
}
