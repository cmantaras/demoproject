﻿using GestorClientes.Logic.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto.Responses
{

    public class MetodoPagoResponseDto : PageResult
    {
        public MetodoPagoResponseDto()
        {
        }

        public List<MetodoPagoDto> Metodos { get; set; }

        public MetodoPagoResponseDto(int count, List<MetodoPagoDto> metodos)
        {
            this.Count = count;
            this.Metodos = metodos;
        }
    }
}