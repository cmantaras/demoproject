﻿using GestorClientes.Logic.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto.Responses
{

    public class ProductoPrecioResponseDto : PageResult
    {
        public ProductoPrecioResponseDto()
        {
        }

        public List<ProductoPrecioDto> ProductoPrecios { get; set; }

        public ProductoPrecioResponseDto(int count, List<ProductoPrecioDto> productoPrecios)
        {
            this.Count = count;
            this.ProductoPrecios = productoPrecios;
        }
    }
}
