﻿
using GestorClientes.Logic.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto.Responses
{
    public class ParametroResponseDto : PageResult
    {
        public ParametroResponseDto()
        {
        }

        public List<ParametroDto> Parametros { get; set; }

        public ParametroResponseDto(int count, List<ParametroDto> parametros)
        {
            this.Count = count;
            this.Parametros = parametros;
        }
    }
}
