﻿using GestorClientes.Logic.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto.Responses
{
    public class ClienteProdDeudaResponseDto : PageResult
    {

        public ClienteProdDeudaResponseDto () { }


        public Dictionary<int, List<ClienteProductoDeudaDto>> Productos;
        public ClienteDto Cliente;
        public int? IdAvisoPago;
        public bool? AvisoCorte;

        public ClienteProdDeudaResponseDto (Dictionary<int, List<ClienteProductoDeudaDto>> productos, ClienteDto cliente, int count, int? idAvisoPago, bool avisoCorte)
        {
            this.Cliente = cliente;
            this.Count = count;
            this.Productos = productos;
            this.IdAvisoPago = idAvisoPago.HasValue ? idAvisoPago : null;
            this.AvisoCorte = avisoCorte;

        
        }
        public ClienteProdDeudaResponseDto(Dictionary<int, List<ClienteProductoDeudaDto>> productos, ClienteDto cliente, int count, int? idAvisoPago)
        {
            this.Cliente = cliente;
            this.Count = count;
            this.Productos = productos;
            this.IdAvisoPago = idAvisoPago.HasValue ? idAvisoPago : null;



        }
    }
}
