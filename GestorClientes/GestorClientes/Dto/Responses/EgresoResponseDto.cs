﻿using GestorClientes.Logic.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto.Responses
{

    public class EgresoResponseDto : PageResult
    {
        public EgresoResponseDto()
        {
        }

        public List<EgresoDto> Egresos { get; set; }
        public double? TotalEgreso { get; set; }

        public EgresoResponseDto(int count,  List<EgresoDto> egresos, double? total = 0)
        {
            this.TotalEgreso = total;
            this.Count = count;
            this.Egresos = egresos;
        }

    }
}
