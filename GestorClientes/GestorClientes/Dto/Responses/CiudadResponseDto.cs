﻿using GestorClientes.Logic.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto.Responses
{
    
    public class CiudadResponseDto : PageResult
    {
        public CiudadResponseDto()
        {
        }

        public List<CiudadDto> Ciudades { get; set; }

        public CiudadResponseDto(int count, List<CiudadDto> ciudades)
        {
            this.Count = count;
            this.Ciudades = ciudades;
        }
    }
}
