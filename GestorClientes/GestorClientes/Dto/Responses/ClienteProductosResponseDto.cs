﻿using GestorClientes.Logic.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto.Responses
{
    
    public class ClienteProductosResponseDto : PageResult
    {
        public ClienteProductosResponseDto()
        {
        }

        public List<ClienteProductoDto> Productos { get; set; }
        public ClienteDto Cliente { get; set; }

        public ClienteProductosResponseDto(int count, List<ClienteProductoDto> productos, ClienteDto cliente)
        {
            this.Count = count;
            this.Productos = productos;
            this.Cliente = cliente;
        }

        public ClienteProductosResponseDto(int count, List<ClienteProductoDto> productos)
        {
            this.Count = count;
            this.Productos = productos;

        }

    }
}
