﻿using GestorClientes.Logic.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto.Responses
{
    public class CuotaPagaResponseDto : PageResult
    {
        public CuotaPagaResponseDto()
        {
        }

        public List<ClienteProductoDeudaDto> Cuotas { get; set; }

        public CuotaPagaResponseDto(int count, List<ClienteProductoDeudaDto> cuotas)
        {
            this.Count = count;
            this.Cuotas = cuotas;
        }
    }
}
