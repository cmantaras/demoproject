﻿using GestorClientes.Logic.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto.Responses
{

    public class MonedaResponseDto : PageResult
    {
        public MonedaResponseDto()
        {
        }

        public List<MonedaDto> Monedas { get; set; }

        public MonedaResponseDto(int count, List<MonedaDto> monedas)
        {
            this.Count = count;
            this.Monedas = monedas;
        }
    }
}
