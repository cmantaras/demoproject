﻿using GestorClientes.Logic.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto.Responses
{

    public class ZonaResponseDto : PageResult
    {
        public ZonaResponseDto()
        {
        }

        public List<ZonaDto> Zonas { get; set; }

        public ZonaResponseDto(int count, List<ZonaDto> zonas)
        {
            this.Count = count;
            this.Zonas = zonas;
        }
    }
}
