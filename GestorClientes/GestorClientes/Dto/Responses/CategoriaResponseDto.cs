﻿using GestorClientes.Logic.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto.Responses
{

    public class CategoriaResponseDto : PageResult
    {
        public CategoriaResponseDto()
        {
        }

        public List<CategoriaDto> Categorias { get; set; }

        public CategoriaResponseDto(int count, List<CategoriaDto> categorias)
        {
            this.Count = count;
            this.Categorias = categorias;
        }
    }
}
