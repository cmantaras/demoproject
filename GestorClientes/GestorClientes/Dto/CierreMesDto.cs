﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto
{
    public class CierreMesDto
    {

        public int Id {get; set; }
        public DateTime Fecha { get; set; }

        CierreMesDto(CierreMesDto mesFinalizado)
        {
            this.Id = mesFinalizado.Id;
            this.Fecha = mesFinalizado.Fecha;
        }


    }
}
