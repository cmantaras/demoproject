﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto
{
    public class ItemValorDto
    {
        public int Id { get; set; }
        public int ItemId { get; set; }
        public string Valor { get; set; }
    }
}
