﻿using GestorClientes.Dominio.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto
{
    public class PermisoGrupoDto
    {
        public PermisoGrupoDto() { }

        public int Id { get; set; }
        public string Nombre { get; set; }

        public List<PermisoDto> Permisos { get; set; }

        public PermisoGrupoDto(PermisoGrupo permisoGrupo)
        {
            this.Id = permisoGrupo.Id;
            this.Nombre = permisoGrupo.Nombre;
            this.Permisos = new List<PermisoDto>();
            foreach(Permiso permiso in permisoGrupo.Permisos)
            {
                this.Permisos.Add(new PermisoDto(permiso));
            }
        }
    }
}
