﻿using GestorClientes.Dominio.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto
{
    public class ChequeDto
    {
        public ChequeDto() { }

        public int Id { get; set; }
        public int ProveedorId { get; set; }
        public int CategoriaId { get; set; }
        public ProveedorDto Proveedor { get; set; }
        public string Descripcion { get; set; }
        public DateTime Fecha { get; set; }
        public DateTime FechaPago { get; set; }
        public double Importe { get; set; }
        public int MonedaId { get; set; }
        public MonedaDto Moneda {get; set;}

        public CategoriaDto Categoria { get; set; }



    }
}
