﻿using GestorClientes.Dominio.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto
{
    public class ImpuestoDto
    {
        public ImpuestoDto() { }

        public int Id { get; set; }
        public string Descripcion { get; set; }
        public double Porcentaje { get; set; }
        public bool Activo { get; set; }

        public ImpuestoDto(Impuesto impuesto)
        {
            this.Id = impuesto.Id;
            this.Descripcion = impuesto.Descripcion;
            this.Porcentaje = impuesto.Porcentaje;
            this.Activo = impuesto.Activo;
        }
    }
}
