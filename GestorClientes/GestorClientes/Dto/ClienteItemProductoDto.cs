﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto.Requests
{
    public class ClienteItemProductoDto : PageRequest
    {
        public int Id { get; set; }
        public int ClienteProductoId { get; set; }
        public ItemValorDto ItemValor { get; set; }
    }
}