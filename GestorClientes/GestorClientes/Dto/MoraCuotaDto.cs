﻿using GestorClientes.Dominio.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto
{
    public class MoraCuotaDto
    {
        public MoraCuotaDto() { }

        public int Id { get; set; }
        public string Descripcion { get; set; }
        public int Porcentaje{ get; set; }

        public MoraCuotaDto(MoraCuota moraCuota)
        {
            this.Id = moraCuota.Id;
            this.Descripcion = moraCuota.Descripcion;
            this.Porcentaje = moraCuota.Porcentaje;
        }
    }
}
