﻿using GestorClientes.Dominio.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto
{
    public class PermisoDto
    {
        public PermisoDto() { }

        public int Id { get; set; }
        public int GrupoId { get; set; }
        public string Key { get; set; }
        public string Descripcion { get; set; }

        public PermisoDto(Permiso permiso)
        {
            this.Id = permiso.Id;
            this.GrupoId = permiso.GrupoId;
            this.Key = permiso.Key;
            this.Descripcion = permiso.Descripcion;
        }
    }
}
