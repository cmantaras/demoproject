﻿using GestorClientes.Dominio.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto
{
    public class ZonaDto
    {
        public ZonaDto() { }

        public int Id { get; set; }
        public string Descripcion { get; set; }
        public string Codigo { get; set; }
        public bool Activo { get; set; }

        public ZonaDto(Zona zona)
        {
            this.Id = zona.Id;
            this.Descripcion = zona.Descripcion;
            this.Codigo = zona.Codigo;
            this.Activo = zona.Activo;
        }
    }
}
