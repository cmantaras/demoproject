﻿using GestorClientes.Dominio.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto
{
    public class DeudorDto
    {

        public ClienteDto Cliente { get; set; }
        public DateTime FechaUltimoPago { get; set; }
        public int DiasDesdePago { get; set; }
        public double TotalDeuda { get; set; }
        //&& d.Desde.AddMonths(c.Cuota) <= DateTime.Now
        public DeudorDto(Cliente cliente)
        {
            this.Cliente = new ClienteDto(cliente);
            this.FechaUltimoPago = (cliente.Pagos.Count > 0) ? cliente.Pagos.OrderBy(p => p.Fecha).LastOrDefault().Fecha : default;
            this.DiasDesdePago = (cliente.Pagos.Count > 0) ? (DateTime.Now - cliente.Pagos.OrderBy(p => p.Fecha).LastOrDefault().Fecha).Days : 0;

            this.TotalDeuda = cliente.ClienteProductos.Select(p => p.Cuotas.Where(d =>  d.PagoClienteId == null))
                                                         .Sum(i => i.Sum(d => d.Importe));
                                              
        }

    }
}
