﻿using GestorClientes.Dominio.Models;
using GestorClientes.Logic.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto
{
    public class RolDto
    {
        public RolDto() { }

        public int Id { get; set; }
        public string Nombre { get; set; }
        public List<PermisoDto> Permisos { get; set; }

        public RolDto(Rol rol)
        {
            this.Id = rol.Id;
            this.Nombre = rol.Nombre;
            this.Permisos = new List<PermisoDto>();
            foreach (Permiso permiso in rol.Permisos)
            {
                this.Permisos.Add(new PermisoDto(permiso));
            }
        }

        public void ValidatePost()
        {
            if (string.IsNullOrEmpty(this.Nombre))
                throw new BadRequestException("El campo nombre no puede ser vacio o null");
        }

        public void ValidatePut()
        {
            if (string.IsNullOrEmpty(this.Nombre))
                throw new BadRequestException("El campo nombre no puede ser vacio o null");
            if (this.Id == 0)
                throw new BadRequestException("El campo id no puede ser 0 o null");
        }
    }
}
