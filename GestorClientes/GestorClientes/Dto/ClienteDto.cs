﻿using GestorClientes.Dominio.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto
{
    public class ClienteDto 
    {
        public ClienteDto() { }

        public int Id { get; set; }
        public string Identificador { get; set; }
        public string Hte { get; set; }
        public string Nombre { get; set; }

        public string Apellido { get; set; }
        public string Telefono { get; set; }
        public string Direccion { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public DateTime FechaHasta { get; set; }
        public int CiudadId { get; set; }
        public int ZonaId { get; set; }
        public int EstadoActualId { get; set; }
        public string Servicios { get; set; }

        public bool ClienteViejo { get; set; }
        public ClienteEstadoDto EstadoActual { get; set; }
        public EstadoClienteDto Estado { get; set; }

        public virtual CiudadDto Ciudad { get; set; }
        public virtual ZonaDto Zona { get; set; }
        public virtual List<ClienteEstadoDto> Estados { get; set; }
        public ClienteDto(Cliente cliente)
        {
            this.Hte = cliente.Hte;
            this.Id = cliente.Id;
            this.Identificador = cliente.Identificador;
            this.Nombre = cliente.Nombre;
            this.Telefono = cliente.Telefono;
            this.Direccion = cliente.Direccion;
            this.FechaNacimiento = cliente.FechaNacimiento;
            this.FechaHasta = cliente.FechaHasta;
            this.CiudadId = cliente.CiudadId;
            this.ZonaId = cliente.ZonaId;
            this.Apellido = cliente.Apellido;
            this.Ciudad = (cliente.Ciudad == null) ? null : new CiudadDto(cliente.Ciudad);
            this.Zona = (cliente.Zona == null) ? null : new ZonaDto(cliente.Zona);
            this.EstadoActualId = cliente.EstadoId;
            this.Estados = (cliente.Estados == null) ? null : cliente.Estados.Select(x => new ClienteEstadoDto(x)).ToList();
            this.EstadoActual = (cliente.Estados == null) ? null : this.Estados.OrderByDescending(x => x.Desde).FirstOrDefault();
            this.ClienteViejo = cliente.ClienteViejo;
        }


        public static ClienteDto ReporteClienteDto(Cliente cliente)
        {
            ClienteDto clienteDto = new ClienteDto();

            clienteDto.Id = cliente.Id;

            clienteDto.Nombre = cliente.Nombre ;
            clienteDto.Apellido = cliente.Apellido;
            clienteDto.Telefono = cliente.Telefono;
            clienteDto.Direccion = cliente.Direccion;
            clienteDto.Ciudad = (cliente.Ciudad == null) ? null : new CiudadDto(cliente.Ciudad);
            clienteDto.EstadoActualId = cliente.EstadoId;
            clienteDto.Estado = (cliente.Estado == null) ? null : new EstadoClienteDto(cliente.Estado);

            string ServiciosClienteDto ="";
            foreach (ClienteProducto p in cliente.ClienteProductos)
            {

                ClienteProductoDto productoDto = new ClienteProductoDto(p);

                if (productoDto.ProductoPrecio.Producto.Servicio && p.Activo)
                {
                    string cadena = productoDto.ProductoPrecio.Producto.Descripcion + "  Código: " + productoDto.ProductoPrecio.Producto.Codigo+"\n";
                    ServiciosClienteDto += cadena;

                }
                
            }
            clienteDto.Servicios = ServiciosClienteDto;

            return clienteDto;
        }
    }
}
