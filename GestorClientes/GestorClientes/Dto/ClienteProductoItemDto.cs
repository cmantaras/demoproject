﻿using GestorClientes.Dominio.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto
{
    public class ClienteProductoItemDto
    {
        public ItemDto Item { get; set; }

        public string Valor { get; set; }
        public int Id { get; set; }


        public ClienteProductoItemDto(ClienteProductoItem clienteProductoItem) 
        {
            this.Id = clienteProductoItem.Id;
            this.Item = new ItemDto(clienteProductoItem.Item);
            this.Valor = clienteProductoItem.Valor;
        
        }
    }
}
