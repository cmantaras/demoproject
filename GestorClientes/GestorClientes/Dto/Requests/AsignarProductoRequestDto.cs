﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto.Requests
{
    public class AsignarProductoRequestDto : PageRequest
    {
        public int ClienteId { get; set; }
        public int ProductoId { get; set; }
        public int MetodoPagoId { get; set; }
        public DateTime Desde { get; set; }
        public DateTime Hasta { get; set; }
        public List<ItemValorDto> Items { get; set; }
    }
}
