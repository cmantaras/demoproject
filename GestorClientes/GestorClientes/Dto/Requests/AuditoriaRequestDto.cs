﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto.Requests
{
    public class AuditoriaRequestDto : PageRequest
    {
        public AuditoriaRequestDto()
        {
        }

        public int? Id { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime FechaFin { get; set; }
        public int? Usuario { get; set; }
        public string Accion { get; set; }
        public string Detalle { get; set; }
        public string DatoTipo { get; set; }
        public int? DatoId { get; set; }
    }
}
