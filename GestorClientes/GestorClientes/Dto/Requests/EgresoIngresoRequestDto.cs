﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto.Requests
{
    public class EgresoIngresoRequestDto : PageRequest
    {
        EgresoIngresoRequestDto() { }

        public DateTime FechaDesde { get; set; }
        public DateTime FechaHasta { get; set; }

        public int CiudadId { get; set; }
        public int CategoriaId { get; set; }
    }
}
