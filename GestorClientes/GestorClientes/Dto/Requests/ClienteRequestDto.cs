﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto.Requests
{
    public class ClienteRequestDto : PageRequest
    {
        public ClienteRequestDto()
        {
        }

        public int Id { get; set; }
        public string Identificador { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Telefono { get; set; }
        public string Direccion { get; set; }
        public int CiudadId { get; set; }
        public int ZonaId { get; set; }

        public int ClienteEstadoId { get; set; }

    }
}
