﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto.Requests
{

    public class AvisoPagoRequestDto : PageRequest
    {
        public AvisoPagoRequestDto()
        {
        }

        public List<int> ClientesIds { get; set; }
        public bool GenerarAvisoPago { get; set; }




    }
}
