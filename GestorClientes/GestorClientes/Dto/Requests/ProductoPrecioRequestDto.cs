﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto.Requests
{

    public class ProductoPrecioRequestDto : PageRequest
    {
        public ProductoPrecioRequestDto()
        {
        }

        public int ProductoId { get; set; }
        public DateTime Inicio { get; set; }
        public DateTime Final { get; set; }


    }
}
