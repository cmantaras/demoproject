﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto.Requests
{
    public class ClienteReporteRequestDto : PageRequest
    {
        public int CiudadId { get; set; }
        public int ZonaId { get; set; }
        public int Estado { get; set; }



    }
}
