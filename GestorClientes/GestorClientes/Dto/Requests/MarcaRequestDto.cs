﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto.Requests
{

    public class MarcaRequestDto : PageRequest
    {
        public MarcaRequestDto()
        {
        }

        public string Descripcion { get; set; }


    }
}
