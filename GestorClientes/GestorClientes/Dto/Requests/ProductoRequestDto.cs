﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto.Requests
{

    public class ProductoRequestDto : PageRequest
    {
        public ProductoRequestDto()
        {
        }

        public string Codigo { get; set; }
        public string Descripcion { get; set; }
        public string Marca { get; set; }

    }
}
