﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto.Requests
{

    public class ImpuestoRequestDto : PageRequest
    {
        public ImpuestoRequestDto()
        {
        }

        public string Descripcion { get; set; }
        public double Porcentaje { get; set; }


    }
}
