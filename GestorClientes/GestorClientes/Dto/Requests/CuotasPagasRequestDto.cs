﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto.Requests
{
    public class CuotasPagasRequestDto
    {
        CuotasPagasRequestDto() { }
        public List<ClienteProductoCuotaRequestDto> Cuotas { get; set; }
        public int ClienteId { get; set; }
        public DateTime Fecha { get; set; }
    }
}
