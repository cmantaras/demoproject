﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto.Requests
{
    public class EstadoClienteRequestDto : PageRequest
    {
        public EstadoClienteRequestDto()
        {
        }

        public string Descripcion { get; set; }
        public bool? Tipo { get; set; }
      

    }
}
