﻿using GestorClientes.Logic.Dto.Requests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto.Requests
{
    public class ParametroRequestDto : PageRequest
    {
        public ParametroRequestDto()
        {
        }

        public string Codigo { get; set; }
    }
}
