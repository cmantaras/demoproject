﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto.Requests
{

    public class EgresoRequestDto : PageRequest
    {
        public EgresoRequestDto()
        {
        }

        public int MonedaId { get; set; }
        public int CategoriaId { get; set; }
        public int ProveedorId { get; set; }
        public int Id { get; set; }
        public DateTime? FechaCheque { get; set; }
        public DateTime? FechaEgreso { get; set; }
        public bool EsCheque { get; set; }
        public string Descripcion{ get; set; }
     
        public double Importe { get; set; }



    }
}
