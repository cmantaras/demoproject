﻿using GestorClientes.Logic.Dto.Requests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto.Requests
{
    public class UsuarioRequestDto : PageRequest
    {
        public UsuarioRequestDto()
        {
        }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Username { get; set; }

    }
}
