﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto.Requests
{
    public class ClienteProductosRequestDto : PageRequest
    {
        public ClienteProductosRequestDto()
        {
        }

        public int ClienteId { get; set; }
        public int ProductoId { get; set; }

    }
}
