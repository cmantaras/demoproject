﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto.Requests
{

    public class CategoriaRequestDto : PageRequest
    {
        public CategoriaRequestDto()
        {
        }

        public string Descripcion { get; set; }
        public int Id { get; set; }


    }
}
