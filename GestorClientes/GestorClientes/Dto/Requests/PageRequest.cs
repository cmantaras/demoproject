﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto.Requests
{
    public class PageRequest
    {
        public PageRequest()
        {
        }

        public int Limit { get; set; }
        public int Offset { get; set; }

        public int LimitResult { get { return Limit == 0 ? 1000 : Limit; } }
    }
}
