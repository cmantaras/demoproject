﻿using GestorClientes.Logic.Dto.Requests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Dto.Requests
{
    public class RolRequestDto : PageRequest
    {
        public RolRequestDto()
        {
        }
        public string Nombre { get; set; }

    }
}
