﻿
using GestorClientes.Dominio.DAL;
using GestorClientes.Dominio.Models;
using GestorClientes.Logic.Dto;
using GestorClientes.Logic.Dto.Requests;
using GestorClientes.Logic.Dto.Responses;
using GestorClientes.Logic.Exceptions.Pago;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Services
{
    public class PagoClienteService : BaseService
    {


        private ClienteEstadoService clienteEstadoService;
        private ProductoService productoService;

        public PagoClienteService(GestorContext context, IEnumerable<Claim> claims) : base(context, claims)
        {
            clienteEstadoService = new ClienteEstadoService(context, claims);
            productoService = new ProductoService(context, claims);
        }


        public bool DeletePagoCliente(int pagoId)
        {
            try
            {

                IEnumerable<ClienteProductoDeuda> Cuotas = db.ClienteProductoDeudas.Where(c => c.PagoClienteId == pagoId).ToList();
                foreach (ClienteProductoDeuda cuota in Cuotas)
                {
                    cuota.PagoClienteId = null;
                    cuota.FechaPago= null;
                }
                var pago = db.PagosCliente.Where(x => x.Id == pagoId).FirstOrDefault();
                
                if (pago == null) {
                    throw new PagoNotFoundException(string.Format("El pago con id "+pagoId+" no existe."));
                }
                    
                db.PagosCliente.Remove(pago);
                db.SaveChanges();
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Baja, pago);
                return true;

            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }


        public PagoClienteResponseDto GetPagosCliente(int clienteId)
        {
            try
            {
                var pagos = db.PagosCliente.Where(x => x.ClienteId == clienteId).ToList();
                var response = pagos.Select(p => new PagoClienteDto(p)).OrderBy(d => d.Fecha).ToList();


                return new PagoClienteResponseDto(response.Count, response);
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }

        public CuotaPagaResponseDto GetCuotasPago(int pagoId)
        {
            try
            {
                var cuotas = db.ClienteProductoDeudas.Where(x => x.PagoClienteId == pagoId).ToList();


                var response = cuotas.Select(p => new ClienteProductoDeudaDto(p, true)).ToList();

                return new CuotaPagaResponseDto(response.Count, response);
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }

        public int AddPagoCliente(PagoClienteDto pagoClienteDto)
        {
            try
            {
                PagoCliente pagoCliente = new PagoCliente()
                    {
                        ClienteId = pagoClienteDto.ClienteId,
                        MontoTotal = pagoClienteDto.MontoTotal,
                        Fecha = pagoClienteDto.Fecha
                    };
                    db.PagosCliente.Add(pagoCliente);
                

                db.SaveChanges();
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Alta, pagoCliente);
                return pagoCliente.Id;
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }

        public int RealizarPago(CuotasPagasRequestDto request)
        {
            try
            {
                List<int> cuotasId = request.Cuotas.Select(c => c.CuotaId).ToList();
                List<ClienteProductoDeuda> cuotasProducto = db.ClienteProductoDeudas
                                        .Where(x => cuotasId.Contains(x.Id) ).ToList();

                Cliente cliente = db.Clientes.Where(x => x.Id == request.ClienteId).FirstOrDefault();

                var totalCuotas = cuotasProducto.Sum(d => (d.MoraCuota != null ) ? d.Importe * d.MoraCuota.Porcentaje /100 + d.Importe : d.Importe);

                PagoClienteDto pagoClienteDto = new PagoClienteDto()
                {
                    ClienteId = request.ClienteId,
                    MontoTotal = totalCuotas,
                    Fecha = request.Fecha
                };

                var idPago = AddPagoCliente(pagoClienteDto);

                foreach (var cuota in cuotasProducto)
                {
                    cuota.FechaPago = request.Fecha;
                    cuota.PagoClienteId = idPago;
                }

                db.SaveChanges();

                ChequearSiClienteEsDeudor(cliente);

                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Alta, cuotasProducto);
                return idPago;
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }

        public void ChequearSiClienteEsDeudor(Cliente cliente)
        {
            try
            {
                //Si el cliente es deudor se obtiene cliente Id para chequear si existen productos impagos 
                //Debera pagar la totalidad de la deuda para eliminar estado deudor
                //  var clienteProductoDeuda = db.ClienteProductos.Include(d => d.Cliente).Where(p => p.Id == clienteProductoId).FirstOrDefault();

                if (cliente.EstadoId == (int)Enum.EnumUtils.EstadoCliente.Deudor)
                {
                    var listProd = db.ClienteProductoDeudas.Where(p => p.ClienteProducto.ClienteId == cliente.Id && p.Cierre != default && p.FechaPago == null).ToList();
                    //si el cliente no tiene cuotas impagas deja de ser deudor
                    if (listProd.Count == 0)
                    {
                        clienteEstadoService.Add(cliente, (int)Enum.EnumUtils.EstadoCliente.Activo);
                        cliente.EstadoId = (int)Enum.EnumUtils.EstadoCliente.Activo;
                    }
                }

                db.SaveChanges();

            }
            catch(Exception e)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, e);
                throw e;
            }
        }

        //public bool Edit(ItemDto itemDto)
        //{
        //    try
        //    {
        //        Item item = db.Items.Where(x => x.Id == itemDto.Id && x.Activo).FirstOrDefault();
        //        if (item == null) throw new ItemNotFoundException(string.Format("El item con id {0} no existe.", itemDto.Id));
        //        Item exists = ExistsWithDifferentId(itemDto);
        //        if (exists != null)
        //        {
        //            if (exists.Descripcion == itemDto.Descripcion)
        //                throw new ItemDescripcionAlreadyExistsException(string.Format("El item {0} ya se encuentra registrado.", itemDto.Descripcion));
        //        }
        //        else
        //        {
        //            item.Descripcion = itemDto.Descripcion;
        //            //item.Color = itemDto.Color;
        //            db.SaveChanges();
        //            auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Modificacion, item);
        //            return true;
        //        }
        //        return false;
        //    }
        //    catch (Exception ex)
        //    {
        //        auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
        //        throw ex;
        //    }
        //}

        //public bool Delete(ItemDto itemDto)
        //{
        //    try
        //    {
        //        Item item = db.Items.Where(x => x.Id == itemDto.Id && x.Activo).FirstOrDefault();
        //        if (item == null)
        //            throw new ItemNotFoundException(string.Format("La item con id {0} no existe.", itemDto.Id));
        //        item.Activo = false;
        //        db.SaveChanges();
        //        auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Baja, item);
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
        //        throw ex;
        //    }
        //}

        //public ItemResponseDto GetAll(ItemRequestDto request)
        //{
        //    IEnumerable<Item> itemes = db.Items.Where(x => x.Activo);
        //    if (!string.IsNullOrEmpty(request.Descripcion))
        //    {
        //        itemes = itemes.Where(x => x.Descripcion.Contains(request.Descripcion));
        //    }


        //    IEnumerable<Item> itemCount = itemes;
        //    int count = itemCount.Count();
        //    List<ItemDto> itemesDto = itemes.Skip(request.Offset).Take(request.LimitResult).ToList().Select(x => new ItemDto(x)).ToList();
        //    return new ItemResponseDto(count, itemesDto);
        //}



        //private Item Exists(ItemDto itemDto)
        //{
        //    return db.Items.Where(x => x.Id == itemDto.Id || x.Descripcion == itemDto.Descripcion).FirstOrDefault();
        //}

        //private Item ExistsWithDifferentId(ItemDto itemDto)
        //{
        //    return db.Items.Where(x => x.Descripcion == itemDto.Descripcion && x.Id != itemDto.Id && x.Activo).FirstOrDefault();
        //}
    }
}
