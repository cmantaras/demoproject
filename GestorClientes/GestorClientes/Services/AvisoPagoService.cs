﻿using GestorClientes.Dominio.DAL;
using GestorClientes.Dominio.Models;
using GestorClientes.Logic.Dto;
using GestorClientes.Logic.Dto.Requests;
using GestorClientes.Logic.Dto.Responses;
using GestorClientes.Logic.Exceptions.Zona;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Services
{
    public class AvisoPagoService : BaseService
    {
        public AvisoPagoService(GestorContext context, IEnumerable<Claim> claims) : base(context, claims) { }

        public int Add()
        {
            try
            {
                    AvisoPago aviso = new AvisoPago()
                    {
                        FechaImpresion = DateTime.Now,
                    };
                    db.AvisosPago.Add(aviso);

                db.SaveChanges();
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Alta, aviso);
                return aviso.Id;
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }
     
    }
}
