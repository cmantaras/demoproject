﻿using GestorClientes.Dominio.DAL;
using GestorClientes.Dominio.Models;
using GestorClientes.Logic.Dto;
using GestorClientes.Logic.Dto.Requests;
using GestorClientes.Logic.Dto.Responses;
using GestorClientes.Logic.Exceptions.Zona;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Services
{
    public class ReportesService : BaseService
    {
        public ReportesService(GestorContext context, IEnumerable<Claim> claims) : base(context, claims) { }

        public EgresoResponseDto GetAllEgresos(EgresoIngresoRequestDto request)
        {
            IEnumerable<Egreso> egresos = db.Egresos.Where(e => e.Activo == true);
     
            List<EgresoDto> chequesAndEgresos = new List<EgresoDto>();

            if (request.FechaDesde != DateTime.MinValue)
            {
              
                egresos = egresos.Where(e => e.FechaEgreso >= request.FechaDesde).ToList();
                
            }
            if (request.FechaHasta != DateTime.MinValue)
            {
                egresos = egresos.Where(e => e.FechaEgreso <= request.FechaHasta.AddDays(1)).ToList();
               
            }
            if(request.CategoriaId != 0)
            {
                egresos = egresos.Where(e => e.CategoriaId == request.CategoriaId).ToList();
     
            }

            chequesAndEgresos.AddRange(egresos.Select(x => new EgresoDto(x)).ToList());
            

       

            var totalEgresos = egresos.Sum(e => e.Importe);

            IEnumerable<Egreso> egresosCount = egresos;
            int count = egresosCount.Count();
            List<EgresoDto> egresosDto = chequesAndEgresos.Skip((request.Offset - 1) * request.LimitResult).Take(request.LimitResult).ToList();
            return new EgresoResponseDto(count, egresosDto, totalEgresos);
        }

        public PagoClienteResponseDto GetAllPagos(EgresoIngresoRequestDto request)
        {
            try
            {
                var ingresos = db.PagosCliente.Include("Cuotas").ToList();

                if (request.CiudadId != 0)
                {
                    ingresos = ingresos.Where(e => e.Cliente.CiudadId == request.CiudadId).ToList();
                }
                if (request.FechaDesde != DateTime.MinValue)
                {
                    ingresos = ingresos.Where(e => e.Fecha.Date >= request.FechaDesde.Date).ToList();
                }
                if (request.FechaHasta != DateTime.MinValue)
                {
                    ingresos = ingresos.Where(e => e.Fecha.Date <= request.FechaHasta.Date).ToList();
                }
                var totalIngresos = ingresos.Sum(d => d.MontoTotal);
                var response = ingresos.Skip((request.Offset - 1) * request.LimitResult).Take(request.LimitResult).Select(p => new PagoClienteDto(p)).OrderBy(d => d.Fecha).ToList();


                return new PagoClienteResponseDto(ingresos.Count, response, totalIngresos);
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }

        public Dictionary<DateTime, double> GetEgresosByCierre()
        {
            try
            {
                var listCierres = db.Cierres.OrderBy(f => f.Fecha).ToList();
                var egresos = db.Egresos.ToList();

                Dictionary<DateTime, double> dictVentas = new Dictionary<DateTime, double>();

                foreach (var cierre in listCierres)
                {
                    var i = listCierres.IndexOf(cierre);
                    if (!(i == 0))
                    {
                        var pagos = egresos.Where(d => d.FechaEgreso <= cierre.Fecha && d.FechaEgreso >= listCierres[listCierres.IndexOf(cierre) - 1].Fecha).ToList();
                        var suma = pagos.Sum(x => x.Importe);

                        dictVentas.Add(cierre.Fecha, suma);
                    }
                }
                db.SaveChanges();

                return dictVentas;
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }
        public Dictionary<DateTime, double> GetIngresosByCierre()
        {
            try
            {
                var listCierres = db.Cierres.OrderBy(f => f.Fecha).ToList();
                var listPagos = db.PagosCliente.ToList();

                Dictionary<DateTime, double> dictVentas = new Dictionary<DateTime, double>();

                foreach (var cierre in listCierres)
                {
                    //var i = listCierres[listCierres.IndexOf(cierre) - 1];
                    var i = listCierres.IndexOf(cierre);
                    if (!(i == 0))
                    {
                        var pagos = listPagos.Where(d => d.Fecha <= cierre.Fecha && d.Fecha >= listCierres[listCierres.IndexOf(cierre) - 1].Fecha).ToList();
                        var suma = pagos.Sum(x => x.MontoTotal);
                        dictVentas.Add(cierre.Fecha, suma);
                    }

                }
                db.SaveChanges();

                return dictVentas;
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }
        public DeudorResponseDto GetDeudores(DeudorRequestDto request)

        {
            List<Cliente> deudores = db.Clientes.Where(c => c.Activo == true
                                                    && c.EstadoId == ((int)Enum.EnumUtils.EstadoCliente.Deudor) ).ToList();
            if (request.idCliente != 0) { 
                deudores = deudores.Where(c => c.Activo == true && c.Id == (request.idCliente)).ToList();
            }
           

            int count = deudores.Count();

            List<DeudorDto> deudoresDto = deudores.Skip((request.Offset - 1) * request.LimitResult).Take(request.LimitResult).Select(x => new DeudorDto(x)).ToList();
            return new DeudorResponseDto(count, deudoresDto);
        }
        public ClienteResponseDto GetBirthdays()
        {
            DateTime now = DateTime.Now;
           
            List<Cliente> clientes = db.Clientes.Where(c => c.Activo == true
                                                    && c.FechaNacimiento.Day == DateTime.Now.Day && c.FechaNacimiento.Month == now.Month).ToList();

            int count = clientes.Count();
            List<ClienteDto> deudoresDto = clientes.Select(x => new ClienteDto(x)).ToList();
            return new ClienteResponseDto(count, deudoresDto);
        }
        public ClienteResponseDto GetContratosPorVencer()
        {
            var fechaSemAtras = DateTime.Now.AddDays(7);
            List<Cliente> clientes = db.Clientes.Where(c => c.Activo == true
                                                    && c.FechaHasta >= DateTime.Now && c.FechaHasta <= fechaSemAtras).ToList();

            int count = clientes.Count();
            List<ClienteDto> deudoresDto = clientes.Select(x => new ClienteDto(x)).ToList();
            return new ClienteResponseDto(count, deudoresDto);
        }

        public ClienteResponseDto GetClientes(ClienteReporteRequestDto request)
        {
            IEnumerable<Cliente> clientes = db.Clientes.Include("Estado").Include("Ciudad").Where(c => c.Activo == true);

            //Obtener clientes de la BD
            if(request.ZonaId != 0)
            {
                clientes = clientes.Where(c => c.ZonaId == request.ZonaId);
            }
            if (request.CiudadId != 0)
            {
                clientes = clientes.Where(c => c.CiudadId == request.CiudadId);
            }
            if (request.Estado != 2)
            {
                if(request.Estado == 1)
                {
                    clientes = clientes.Where(c => c.EstadoId == 8);
                }
                if (request.Estado == 0)
                {
                    clientes = clientes.Where(c => c.EstadoId != 8);
                }

            }
            var clientesList = clientes.ToList();
            int count = clientes.Count();

            List<ClienteDto> clienteesDto = clientesList.Skip((request.Offset - 1) * request.Limit).Take(request.LimitResult).Select(x => ClienteDto.ReporteClienteDto(x)).OrderByDescending(o => o.Id).ToList();
            return new ClienteResponseDto(count, clienteesDto);

        }

        public EgresoResponseDto GetCheques()
        {
            var datAdd  = DateTime.Now.Date.AddDays(15);
            var now = DateTime.Now.Date;
            IEnumerable<Egreso> egresos = db.Egresos.Include("Categoria").Include("Moneda")
                                                    .Include("Proveedor").Where(c => c.Activo == true && 
                                                                    c.EsCheque && 
                                                                    c.FechaCheque <= datAdd
                                                                    && c.FechaCheque >= now);
            //.Include("Proveedor.Ciudad")

            var egresosDto = egresos.Select(x => new EgresoDto(x)).ToList();

            return new EgresoResponseDto(egresos.Count(), egresosDto);
        }
    }
}
