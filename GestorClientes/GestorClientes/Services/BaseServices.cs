﻿using GestorClientes.Dominio.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Services
{
    public class BaseService
    {
        public GestorContext db;
        public int userId;
        public AuditoriaService auditoria;

        public BaseService(GestorContext context)
        {
            db = context;
        }

        public BaseService(GestorContext context, IEnumerable<Claim> claims)
        {
            db = context;
            userId = Convert.ToInt32(claims.Where(c => c.Type == "UID").Single().Value);
            auditoria = new AuditoriaService(context, userId);
        }

        public BaseService(GestorContext context, int userId)
        {
            db = context;
            this.userId = userId;
        }

        public BaseService(GestorContext context, int userId, AuditoriaService auditoria)
        {
            db = context;
            this.userId = userId;
            this.auditoria = auditoria;
        }
    }
}
