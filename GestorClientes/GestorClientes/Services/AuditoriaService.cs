﻿using GestorClientes.Dominio.DAL;
using GestorClientes.Dominio.Models;
using GestorClientes.Logic;
using GestorClientes.Logic.Dto;
using GestorClientes.Logic.Dto.Requests;
using GestorClientes.Logic.Dto.Responses;
using GestorClientes.Logic.Logic;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Services
{
    public class AuditoriaService : BaseService
    {
        public AuditoriaService(GestorContext context, IEnumerable<Claim> claims) : base(context, claims) { }

        public AuditoriaService(GestorContext context, int userId) : base(context, userId) { }

        public int Add(Enum.EnumUtils.AuditoriaAccion accion, object dato)
        {

            try
            {
                int id_dato = 0;
                if (dato.GetType().GetProperty("Id") != null)
                    Int32.TryParse(dato.GetType().GetProperty("Id").GetValue(dato).ToString(), out id_dato);

                int? user = null;
                if (base.userId != 0)
                    user = base.userId;

                string detalle = "";
                string origen = "";
                try
                {
                   // detalle = JsonConvert.SerializeObject(dato);
                }
                catch (Exception ex)
                {
                    detalle = "Error al capturar.";
                    throw ex;
                    
                }

                try
                {
                    origen = Auxiliares.GetCurrentMethod();
                }
                catch (Exception ex)
                {
                    origen = "Error al capturar.";
                    throw ex;

                }

                Auditoria auditoria = new Auditoria()
                {
                    Accion = accion.ToString(),
                    Detalle = detalle,
                    DatoTipo = dato.GetType().Name,
                    DatoId = id_dato,
                    Fecha = DateTime.Now,
                    UsuarioId = user,
                    Origen = origen
                };

                db.Auditorias.Add(auditoria);
                db.SaveChanges();
                return auditoria.Id;
            }
            catch (Exception ex)
            {
                Auditoria auditoria2 = new Auditoria()
                {
                    Accion = "",
                    Detalle = "prueba",
                    DatoTipo = "",
                    DatoId = 2,
                    Fecha =  DateTime.Now,
                    UsuarioId = 1,
                    Origen = ""
                };
      
                db.Auditorias.Add(auditoria2);
                db.SaveChanges();
                throw ex;
            }
        }

       

        public AuditoriaResponseDto GetAll(AuditoriaRequestDto request)
        {
            IEnumerable<Auditoria> auditorias = db.Auditorias;
            if (request.Id.HasValue)
            {
                auditorias = auditorias.Where(x => x.Id == request.Id);
            }
            if (request.Usuario.HasValue)
            {
                auditorias = auditorias.Where(x => x.Usuario.Id == request.Usuario);
            }
            if (!string.IsNullOrEmpty(request.Accion))
            {
                auditorias = auditorias.Where(x => x.Accion.Contains(request.Accion));
            }
            if (request.FechaInicio != DateTime.MinValue && request.FechaFin != DateTime.MinValue)
            {
                auditorias = auditorias.Where(x => x.Fecha >= request.FechaInicio && x.Fecha <= request.FechaFin);
            }
            if (!string.IsNullOrEmpty(request.Detalle))
            {
                auditorias = auditorias.Where(x => x.Accion.Contains(request.Detalle));
            }
            if (!string.IsNullOrEmpty(request.DatoTipo))
            {
                auditorias = auditorias.Where(x => x.Accion.Contains(request.DatoTipo));
            }
            if (request.DatoId.HasValue)
            {
                auditorias = auditorias.Where(x => x.DatoId == request.DatoId);
            }

            IEnumerable<Auditoria> auditoriasCount = auditorias;
            int count = auditoriasCount.Count();
            List<AuditoriaDto> auditoriasDto = auditorias.Skip(request.Offset).Take(request.LimitResult).ToList().Select(x => new AuditoriaDto(x)).ToList();
            return new AuditoriaResponseDto(count, auditoriasDto);

        }
    }
}
