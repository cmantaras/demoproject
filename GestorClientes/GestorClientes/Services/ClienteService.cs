﻿
using GestorClientes.Dominio.DAL;
using GestorClientes.Dominio.Models;
using GestorClientes.Logic.Dto;
using GestorClientes.Logic.Dto.Requests;
using GestorClientes.Logic.Dto.Responses;
using GestorClientes.Logic.Exceptions.Cliente;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace GestorClientes.Logic.Services
{
    public class ClienteService : BaseService
    {
        private ClienteEstadoService clienteEstadoService;
        private ProductoService productoService;
        private AvisoPagoService avisoPagoService;

        public ClienteService(GestorContext context, IEnumerable<Claim> claims) : base(context, claims) {
            clienteEstadoService = new ClienteEstadoService(context, claims);
            productoService = new ProductoService(context, claims);
            avisoPagoService = new AvisoPagoService(context, claims);
        }


        public int Add(ClienteDto clienteDto)
        {
            try
            {
                Cliente cliente = Exists(clienteDto);


                if (cliente == null)
                {
                    var estadoActivo = db.EstadosCliente.Where(e => e.Descripcion == "ACTIVO").FirstOrDefault();
                    cliente = new Cliente()
                    {
                        Identificador = clienteDto.Identificador,
                        Nombre = clienteDto.Nombre,
                        Apellido = clienteDto.Apellido,
                        Telefono = clienteDto.Telefono,
                        Direccion = clienteDto.Direccion,
                        FechaNacimiento = clienteDto.FechaNacimiento,
                        FechaHasta = (clienteDto.FechaHasta) != DateTime.MinValue ? clienteDto.FechaHasta : DateTime.Now.AddYears(2),
                        CiudadId = clienteDto.CiudadId,
                        ZonaId = clienteDto.ZonaId,
                        Hte = clienteDto.Hte,
                        Activo = true,
                        EstadoId = (int)Enum.EnumUtils.EstadoCliente.Activo,
                        Ciudad = db.Ciudades.Where(x => x.Id == clienteDto.CiudadId).FirstOrDefault(),
                        Zona = db.Zonas.Where(x => x.Id == clienteDto.ZonaId).FirstOrDefault(),
                        ClienteViejo = clienteDto.ClienteViejo,
                    };
                    db.Clientes.Add(cliente);

                    clienteEstadoService.Add(cliente, (int)Enum.EnumUtils.EstadoCliente.Activo);
                }
                else if (cliente.Activo)
                {
                    if (cliente.Identificador == clienteDto.Identificador)
                        throw new ClienteAlreadyExistsException(string.Format("El cliente {0} ya se encuentra creado.", cliente.Identificador));
                }
                else
                {
                    cliente.Activo = true;
                    cliente.Identificador = clienteDto.Identificador;
                    cliente.Nombre = clienteDto.Nombre;
                    cliente.Apellido = clienteDto.Apellido;
                    cliente.Telefono = clienteDto.Telefono;
                    cliente.Direccion = clienteDto.Direccion;
                    cliente.FechaNacimiento = clienteDto.FechaNacimiento;
                    cliente.FechaHasta = (clienteDto.FechaHasta) != DateTime.MinValue ? clienteDto.FechaHasta : DateTime.Now.AddYears(2);
                    cliente.CiudadId = clienteDto.CiudadId;
                    cliente.ZonaId = clienteDto.ZonaId;
                    cliente.Hte = clienteDto.Hte;

                    cliente.EstadoId = (int)Enum.EnumUtils.EstadoCliente.Activo;
                    cliente.Ciudad = db.Ciudades.Where(x => x.Id == clienteDto.CiudadId).FirstOrDefault();
                    cliente.Zona = db.Zonas.Where(x => x.Id == clienteDto.ZonaId).FirstOrDefault();
                }



                db.SaveChanges();

                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Alta, cliente);
                return cliente.Id;
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }

        public bool Edit(ClienteDto clienteDto)
        {
            try
            {
                Cliente cliente = db.Clientes.Where(x => x.Id == clienteDto.Id && x.Activo).FirstOrDefault();
                if (cliente == null) throw new ClienteNotFoundException(string.Format("El cliente con id {0} no existe.", clienteDto.Id));
                Cliente exists = ExistsWithDifferentId(clienteDto);
                if (exists != null)
                {
                    if (exists.Identificador == clienteDto.Identificador)
                        throw new ClienteAlreadyExistsException(string.Format("El cliente {0} ya se encuentra registrado.", clienteDto.Identificador));
                }
                else
                {
                    cliente.Identificador = clienteDto.Identificador;
                    cliente.Nombre = clienteDto.Nombre;
                    cliente.Apellido = clienteDto.Apellido;
                    cliente.Telefono = clienteDto.Telefono;
                    cliente.Direccion = clienteDto.Direccion;
                    cliente.FechaNacimiento = clienteDto.FechaNacimiento;
                    cliente.FechaHasta = clienteDto.FechaHasta;
                    cliente.CiudadId = clienteDto.CiudadId;
                    cliente.ZonaId = clienteDto.ZonaId;
                    cliente.Hte = clienteDto.Hte;
                    cliente.EstadoId = clienteDto.EstadoActualId;
                    if (clienteDto.EstadoActualId != 0)
                    {
                        clienteEstadoService.Add(cliente, clienteDto.EstadoActualId);
                        if (clienteDto.EstadoActualId != 8)
                        {
                            AgregarCuotaYDarBaja(cliente);
  
                        }

                    }

                    db.SaveChanges();
                    auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Modificacion, cliente);
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }

        public void AgregarCuotaYDarBaja(Cliente cliente)
        {
            try
            {
                //fecha ultimo cierre
                var ultimoCierre = db.Cierres.Select(c => c.Fecha).OrderByDescending(x => x).FirstOrDefault();

                IEnumerable<ClienteProducto> serviciosActivos = db.ClienteProductos.Include(d => d.Cuotas).Include(p => p.ProductoPrecio.Producto)
                           .Where(d => d.ProductoPrecio.Producto.Servicio == true && d.Activo == true && d.ClienteId == cliente.Id).ToList();

                //A cada servicio activo agregar cuota extra con lo que debe y dar de baja
                foreach (var serv in serviciosActivos)
                {

                    if (serv.Cliente.EstadoId != (int)Enum.EnumUtils.EstadoCliente.Deudor) //si es deudor ya se dio de baja el servicio y lo que se debe agregar a este las moras a fin de mes
                    {
                        serv.Activo = false; //BAJA

                        var ultimaCuota = serv.Cuotas.OrderByDescending(c => c.Cuota).FirstOrDefault(); //obtengo ultima cuota
                        if (ultimaCuota != null)
                        {
                            var diasDesdeUltimoCierre = (DateTime.Now.Date - ultimoCierre.Date).Days;
                            var precioPorDia = serv.ProductoPrecio.Precio / 30;
                            var importeTotal = diasDesdeUltimoCierre * precioPorDia;
                            //importe por dias
                            //agrego cuota
                            serv.Cuotas.Add(
                               new ClienteProductoDeuda
                               {
                                   ClienteProductoId = serv.Id,
                                   Cuota = ultimaCuota.Cuota + 1,
                                   Importe = importeTotal,
                                   BajaManual = DateTime.Now.Date,
                                   Activo = true
                               });
                        }

                        
                    }   
                  

                }

                db.SaveChanges();

            }
            catch (Exception ex) {
                throw new Exception(ex.InnerException.Message);
            }
            



        }


        public bool Delete(ClienteDto clienteDto)
        {
            try
            {
                Cliente cliente = db.Clientes.Where(x => x.Id == clienteDto.Id && x.Activo).FirstOrDefault();
                if (cliente == null)
                    throw new ClienteNotFoundException(string.Format("El cliente con id {0} no existe.", clienteDto.Id));
                cliente.Activo = false;
                db.SaveChanges();
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Baja, cliente);
                return true;
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }

        public ClienteProductosResponseDto ProductosAsignados(ClienteProductosRequestDto request)
        {
            Cliente cliente = db.Clientes.Where(x => x.Id == request.ClienteId && x.Activo).FirstOrDefault();
            ClienteDto clienteDto = new ClienteDto(cliente);
            IEnumerable<ClienteProducto> productos = db.ClienteProductos.Where(x => x.ClienteId == request.ClienteId && x.Activo);
            IEnumerable<ClienteProducto> clienteCount = productos;
            
            int count = clienteCount.Count();
            if (productos == null || productos.Count() <= 0)
            {   
                return new ClienteProductosResponseDto(0, null, clienteDto);
            }//throw new ProductosClienteException("Cliente sin productos asignados.");
            

            List<ClienteProductoDto> productosCliente = productos.OrderByDescending(c => c.Id).Skip(request.Offset).Take(request.LimitResult).ToList().Select(x => new ClienteProductoDto(x)).ToList();
            return new ClienteProductosResponseDto(count, productosCliente, clienteDto);
        }






        public List<ClienteProdDeudaResponseDto> AllProductosPendientesPago(AvisoPagoRequestDto request)
        {
            //concatenar a lista parametro los id de todos los inactivos que tengan cuotas pendientes
            try
            {
                List<ClienteProdDeudaResponseDto> clientesProductos = new List<ClienteProdDeudaResponseDto>();

                foreach (var cliente in request.ClientesIds)
                {
                    int avisoPagoId = default;


                    Cliente cli = db.Clientes.Where(c => c.Id == cliente).FirstOrDefault();
                    ClienteDto clienteCuotas = new ClienteDto(cli);

                    IEnumerable<MoraCuota> moras = db.MorasCuota.ToList();
                    IEnumerable<ClienteProductoDeuda> productos = db.ClienteProductoDeudas.Include(p => p.MoraCuota)
                                                            .Where(x => (!x.FechaPago.HasValue
                                                                     && x.ClienteProducto.ClienteId == cliente
                                                                     && x.Activo == true))
                                                            .ToList();

                    int count = productos.Count();
                    var response = new Dictionary<int, List<ClienteProductoDeudaDto>>();
                    //List<ClienteProductoDeudaDto> productosCliente = productos.ToList().Select(x => new ClienteProductoDeudaDto(x)).ToList();
                    var result = productos.ToList().Select(x => new ClienteProductoDeudaDto(x)).ToList()
                                                                                .GroupBy(p => p.ClienteProductoId, (key, g) => new { ClienteProductoId = key, cuotas = g.ToList() }).ToList();

                    foreach (var element in result)
                    {
                        response.Add(element.ClienteProductoId, element.cuotas);
                    }

                    var clienteProducto = new ClienteProdDeudaResponseDto(response, clienteCuotas, count, avisoPagoId);
                    clientesProductos.Add(clienteProducto);
                }
                return clientesProductos;
            }
            catch (Exception e)
            {
                throw new Exception(e.InnerException.Message);
            }
        }

        public List<ClienteProdDeudaResponseDto> ProductosPendientesPagoFecha(AvisoPagoRequestDto request)
        {
            try
            {
                List<ClienteProdDeudaResponseDto> clientesProductos = new List<ClienteProdDeudaResponseDto>();

                foreach (var cliente in request.ClientesIds)
                {
                    int avisoPagoId = default;

                    Cliente cli = db.Clientes.Where(c => c.Id == cliente).FirstOrDefault();
                    ClienteDto clienteCuotas = new ClienteDto(cli);
                    IEnumerable<MoraCuota> moras = db.MorasCuota.ToList();
                    var dateClose = db.Cierres.OrderByDescending(x => x.Fecha).FirstOrDefault().Fecha;
                    IEnumerable<ClienteProductoDeuda> productos = db.ClienteProductoDeudas.Include(p => p.MoraCuota)
                                                            .Where(x => (!x.FechaPago.HasValue
                                                                     && x.ClienteProducto.ClienteId == cliente
                                                                     && x.Activo == true) && (
                                                                        x.ClienteProducto.Desde.Year < dateClose.Year
                                                                        || (x.ClienteProducto.ProductoPrecio.Producto.Servicio && x.ClienteProducto.Desde.Year == dateClose.Year && (x.ClienteProducto.Desde.Month + x.Cuota - 1) <= dateClose.Month)
                                                                        || (!x.ClienteProducto.ProductoPrecio.Producto.Servicio && x.ClienteProducto.Desde.Year == dateClose.Year && ( x.Cuota == 1 || (x.ClienteProducto.Desde.Month + x.Cuota - 1) <= dateClose.Month) )))
                                                            .ToList();

                    int count = productos.Count();
                    var response = new Dictionary<int, List<ClienteProductoDeudaDto>>();
                    //List<ClienteProductoDeudaDto> productosCliente = productos.ToList().Select(x => new ClienteProductoDeudaDto(x)).ToList();
                    var result = productos.ToList().Select(x => new ClienteProductoDeudaDto(x)).ToList()
                                                                                .GroupBy(p => p.ClienteProductoId, (key, g) => new { ClienteProductoId = key, cuotas = g.ToList() }).ToList();

                    foreach (var element in result)
                    {
                        response.Add(element.ClienteProductoId, element.cuotas);
                    }

                    var clienteProducto = new ClienteProdDeudaResponseDto(response, clienteCuotas, count, avisoPagoId);
                    clientesProductos.Add(clienteProducto);
                }
                return clientesProductos;
            }
            catch (Exception e)
            {
                throw new Exception(e.InnerException.Message);
            }
        }





        public List<ClienteProdDeudaResponseDto> ProductosAvisoPago(AvisoPagoRequestDto request)
        {
            //concatenar a lista parametro los id de todos los inactivos que tengan cuotas pendientes
            try
            {
                List<ClienteProdDeudaResponseDto> clientesProductos = new List<ClienteProdDeudaResponseDto>();

                foreach (var cliente in request.ClientesIds)
                {
                    int avisoPagoId = default;


                    Cliente cli = db.Clientes.Where(c => c.Id == cliente).FirstOrDefault();
                    ClienteDto clienteCuotas = new ClienteDto(cli);

                    IEnumerable<MoraCuota> moras = db.MorasCuota.ToList();

                    var dateClose = db.Cierres.OrderByDescending(x => x.Fecha).FirstOrDefault().Fecha;

                    IEnumerable<ClienteProductoDeuda> productos = db.ClienteProductoDeudas.Include(p => p.MoraCuota)
                                                            .Where(x => (!x.FechaPago.HasValue
                                                                     && x.ClienteProducto.ClienteId == cliente
                                                                     && x.Activo == true) && (
                                                                        x.ClienteProducto.Desde.Year < dateClose.Year
                                                                        || (x.ClienteProducto.Desde.Year == dateClose.Year && (x.ClienteProducto.Desde.Month + x.Cuota - 1) <= dateClose.Month)))
                                                            .ToList();


                    if (request.GenerarAvisoPago)
                    {
                        avisoPagoId = avisoPagoService.Add();

                        foreach (var producto in productos)
                        {
                            producto.AvisoPagoId = avisoPagoId;
                        }
                        db.SaveChanges();
                    }

                    int count = productos.Count();
                    var response = new Dictionary<int, List<ClienteProductoDeudaDto>>();
                    //List<ClienteProductoDeudaDto> productosCliente = productos.ToList().Select(x => new ClienteProductoDeudaDto(x)).ToList();
                    var result = productos.ToList().Select(x => new ClienteProductoDeudaDto(x)).ToList()
                                                                                .GroupBy(p => p.ClienteProductoId, (key, g) => new { ClienteProductoId = key, cuotas = g.ToList() }).ToList();


                    foreach (var element in result)
                    {
                        foreach (var e in element.cuotas) 
                        { 
                            if (e.MoraCuota != null) 
                            {
                                double paso = ((double)e.MoraCuota.Porcentaje / (double)100) * (double)e.Importe;
                                var importeConMora = e.Importe + paso ;
                                e.Importe = importeConMora;
                            } 
                        }
                        response.Add(element.ClienteProductoId, element.cuotas);
                       
                    }

                    bool avisoCorte = db.ClienteProductoDeudas
                        .Where(y => y.ClienteProducto.ProductoPrecio.Producto.Servicio && response.Keys.Contains(y.ClienteProducto.Id))
                        .GroupBy(x => x.ClienteProductoId)
                        .Any(y => y.Where(x => x.FechaPago == null).Count() >= 2);

                    var clienteProducto = new ClienteProdDeudaResponseDto(response, clienteCuotas, count, avisoPagoId, avisoCorte);

                    if (clienteProducto.Productos.SelectMany(x => x.Value).Count() <= 5) clientesProductos.Add(clienteProducto); 
                    else
                    {
                        List<ClienteProductoDeudaDto> lineas = clienteProducto.Productos.SelectMany(x => x.Value).ToList();
                        while (lineas.Count > 5)
                        {
                            List<ClienteProductoDeudaDto> a =new List<ClienteProductoDeudaDto>( lineas.Take(5));
                            lineas.RemoveRange(0, 5);
                            var responseTemp = new Dictionary<int, List<ClienteProductoDeudaDto>>();
                            responseTemp.Add(a.FirstOrDefault().ClienteProductoId, a.ToList());             
                            clientesProductos.Add(new ClienteProdDeudaResponseDto(responseTemp, clienteCuotas, 5, avisoPagoId, avisoCorte));

                        }

                        var responseTemp2 = new Dictionary<int, List<ClienteProductoDeudaDto>>();
                        responseTemp2.Add(lineas.FirstOrDefault().ClienteProductoId, lineas);
                        clientesProductos.Add(new ClienteProdDeudaResponseDto(responseTemp2, clienteCuotas, lineas.Count, avisoPagoId, avisoCorte));

                    }

                    
                }
                return clientesProductos;
            }
            catch (Exception e)
            {
                throw new Exception(e.InnerException.Message);
            }
        }

















        public ClienteResponseDto GetAll(ClienteRequestDto request)
        {

            try
            {
                IEnumerable<Cliente> clientees = db.Clientes.Where(x => x.Activo).Include(y => y.Ciudad).Include(z => z.Zona).Include(e =>  e.Estados.Select(es => es.EstadoCliente));
                var list = clientees.ToList();
                if (!string.IsNullOrEmpty(request.Identificador))
                {
                    clientees = clientees.Where(x => x.Identificador.Contains(request.Identificador));
                }
                if(!string.IsNullOrEmpty(request.Nombre) && !string.IsNullOrEmpty(request.Apellido))
                {
                    clientees = clientees.Where(x => x.Nombre.ToLower().Contains(request.Nombre.ToLower()) || x.Nombre.ToLower().Contains(request.Apellido.ToLower()) && x.Apellido.ToLower().Contains(request.Apellido.ToLower()) || x.Apellido.ToLower().Contains(request.Nombre.ToLower()));
                }
                else if (!string.IsNullOrEmpty(request.Nombre))
                {
                    clientees = clientees.Where(x => x.Nombre.ToLower().Contains(request.Nombre.ToLower()) || x.Apellido.ToLower().Contains(request.Nombre.ToLower()));
                }
                if (!string.IsNullOrEmpty(request.Telefono))
                {
                    clientees = clientees.Where(x => x.Telefono.Contains(request.Telefono));
                }
                if (!string.IsNullOrEmpty(request.Direccion))
                {
                    clientees = clientees.Where(x => x.Direccion.Contains(request.Direccion));
                }
                if (request.CiudadId != 0)
                {
                    clientees = clientees.Where(x => x.Ciudad.Id == request.CiudadId);
                }
                if (request.ZonaId != 0)
                {
                    clientees = clientees.Where(x => x.Zona.Id == request.ZonaId);
                }
                if (request.Id != 0)
                {
                    clientees = clientees.Where(x => x.Id == request.Id);
                }
                if (request.ClienteEstadoId != 0)
                {
                    var listClietes = clientees.ToList();
                    clientees = clientees.Where(x => x.EstadoId == request.ClienteEstadoId);
                }

                IEnumerable<Cliente> clienteCount = clientees;
                int count = clienteCount.Count();
          
                List<ClienteDto> clienteesDto = clientees.OrderByDescending(c => c.Id).Skip((request.Offset - 1) * request.Limit).Take(request.LimitResult).Select(x => new ClienteDto(x)).OrderByDescending(o => o.Id).ToList();
                return new ClienteResponseDto(count, clienteesDto);
            }
            catch(Exception e)
            {
                throw new Exception(e.InnerException.Message);
            }
        }

        public bool AsignarProducto(List<AsignarProductoRequestDto> listAsignarProductoRequestDto)
        {

            foreach (var asignarProductoRequestDto in listAsignarProductoRequestDto) {
                Cliente cliente = db.Clientes.Where(x => x.Id == asignarProductoRequestDto.ClienteId && x.Activo).FirstOrDefault();
                Producto producto = db.Productos.Where(x => x.Id == asignarProductoRequestDto.ProductoId && x.Activo).FirstOrDefault();
                MetodoPago metodoPago = db.MetodosPago.Where(x => x.Id == asignarProductoRequestDto.MetodoPagoId && x.Activo).FirstOrDefault();

                if (cliente == null) throw new AsignarProductoException("Cliente invalido.");
                if (producto == null) throw new AsignarProductoException("Producto invalido.");
                if (metodoPago == null) throw new AsignarProductoException("Metodo Pago invalido.");

                ProductoPrecio productoPrecio = productoService.PrecioActual(producto);

                if (productoPrecio == null) throw new AsignarProductoException("El producto no tiene precio.");
                var productoPrecioAux = new ProductoPrecioDto(producto.Precios.Where(x => x.Activo && x.Desde <= DateTime.Now).OrderByDescending(y => y.Desde).FirstOrDefault());

                ClienteProducto clienteProducto = new ClienteProducto()
                {
                    ClienteId = cliente.Id,
                    ProductoPrecioId = productoPrecioAux.Id,
                    Activo = true,
                    MetodoPagoId = asignarProductoRequestDto.MetodoPagoId,
                    Desde = asignarProductoRequestDto.Desde,
                    Hasta = asignarProductoRequestDto.Hasta.AddMonths(metodoPago.Cantidad)
                };

                db.ClienteProductos.Add(clienteProducto);
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Alta, clienteProducto);
                db.SaveChanges();

                foreach (ItemValorDto itemValorDto in asignarProductoRequestDto.Items)
                {
                    ClienteProductoItem nuevo = new ClienteProductoItem()
                    {
                        ClienteProductoId = clienteProducto.Id,
                        ItemId = itemValorDto.ItemId,
                        Valor = itemValorDto.Valor,
                        Activo = true,
                        ClienteProducto = clienteProducto,
                        Item = db.Items.Where(x => x.Id == itemValorDto.ItemId).FirstOrDefault()
                    };
                    db.ClienteProductoItems.Add(nuevo);
                }

                double importeCuota = productoPrecio.Precio / metodoPago.Cantidad;
                DateTime fecha = asignarProductoRequestDto.Desde;
                
                if(!productoPrecio.Producto.Servicio)
                {

                    for (int i = 1; i <= metodoPago.Cantidad; i++)
                    {
                        db.ClienteProductoDeudas.Add(new ClienteProductoDeuda()
                        {
                            ClienteProductoId = clienteProducto.Id,
                            Cuota = i,
                            Importe = importeCuota,
                            Activo = true

                        });
                        fecha = fecha.AddMonths(1);
                    }
                }
            }
       
            db.SaveChanges();
            return true;
        }
        public bool DeleteProductoCliente(ClienteProductosRequestDto clienteProductoDto)
        {
            try
            {
                ClienteProducto producto = db.ClienteProductos.Include(p => p.Cuotas).Where(x => x.ClienteId == clienteProductoDto.ClienteId && clienteProductoDto.ProductoId == x.ProductoPrecioId && x.Activo).FirstOrDefault();
                if (producto == null)
                    throw new ClienteNotFoundException(string.Format("El cliente con id {0} no existe.", producto.Id));
                producto.Activo = false;
                foreach(var cuota in producto.Cuotas)
                {
                    cuota.Activo = false;
                }
                db.SaveChanges();
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Baja, producto);
                return true;
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }

        public Dictionary<DateTime, double> GetProductosPagos()
        {
            try
            {
                var listCierres = db.Cierres.OrderBy(f => f.Fecha).ToList();
                var listPagos = db.ClienteProductoDeudas.ToList();

                Dictionary<DateTime, double> dictVentas = new Dictionary<DateTime, double>();

                foreach (var cierre in listCierres)
                {
                    var pagos = listPagos.Where(d => d.FechaPago <= cierre.Fecha && d.FechaPago >= listCierres[listCierres.IndexOf(cierre) - 1].Fecha).ToList();
                    var suma = pagos.Sum(x => x.Importe);

                    dictVentas.Add(cierre.Fecha, suma);

                }
                db.SaveChanges();
   
                return dictVentas;
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }

 
        private Cliente Exists(ClienteDto clienteDto)
        {
            return db.Clientes.Where(x => x.Id == clienteDto.Id || x.Identificador == clienteDto.Identificador).FirstOrDefault();
        }

        private Cliente ExistsWithDifferentId(ClienteDto clienteDto)
        {
            return db.Clientes.Where(x => x.Identificador == clienteDto.Identificador && x.Id != clienteDto.Id && x.Activo).FirstOrDefault();
        }


        public bool AsignarItemsProducto(int clienteProductoId, List<ClienteItemProductoDto> clienteItemProductoDto)
        {
            //Caso lista vacia, elimino todas filas con el clienteProductoID
            if (clienteItemProductoDto.Count() == 0 && clienteProductoId != 0) {
                List<ClienteProductoItem> listActive = db.ClienteProductoItems.Where(i => i.ClienteProductoId == clienteProductoId).ToList();
                foreach (ClienteProductoItem clientePrducto in listActive){
                    db.ClienteProductoItems.Remove(clientePrducto);
                }
                db.SaveChanges();
                return true;

            }

            foreach (ClienteItemProductoDto cip in clienteItemProductoDto)
            {
                ClienteProductoItem clienteProductoItem = db.ClienteProductoItems.Where(i => i.Id == cip.Id).FirstOrDefault();

                if(clienteProductoItem == null)
                {
                    //crear clienteproductoitem con los datos dto
                    clienteProductoItem = new ClienteProductoItem
                    {
                        Id = cip.Id,
                        ItemId = cip.ItemValor.ItemId,
                        ClienteProductoId =cip.ClienteProductoId,
                        Valor= cip.ItemValor.Valor,
                        Activo = true

                    };
                    db.ClienteProductoItems.Add(clienteProductoItem);
                    db.SaveChanges();

                }
                else
                {
                    List<ClienteProductoItem> listActive = db.ClienteProductoItems.Where(i =>i.ClienteProductoId== clienteProductoItem.ClienteProductoId).ToList();
                    foreach (ClienteProductoItem activo in listActive) {
                        db.ClienteProductoItems.Remove(activo);
                    }
                    db.SaveChanges();
                    clienteProductoItem = new ClienteProductoItem
                    {
                        Id = cip.Id,
                        ItemId = cip.ItemValor.ItemId,
                        ClienteProductoId = cip.ClienteProductoId,
                        Valor = cip.ItemValor.Valor,
                        Activo = true

                    };
                    db.ClienteProductoItems.Add(clienteProductoItem);
                    db.SaveChanges();

                }


            }

            return true;
        }





    }
}
