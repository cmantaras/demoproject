﻿
using GestorClientes.Dominio.DAL;
using GestorClientes.Dominio.Models;
using GestorClientes.Logic.Dto;
using GestorClientes.Logic.Dto.Requests;
using GestorClientes.Logic.Dto.Responses;
using GestorClientes.Logic.Exceptions.MetodoPago;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Services
{
    public class MetodoPagoService : BaseService
    {
        public MetodoPagoService(GestorContext context, IEnumerable<Claim> claims) : base(context, claims) { }

        public int Add(MetodoPagoDto metodoPagoDto)
        {
            try
            {
                if (metodoPagoDto.Cantidad <= 0)
                    throw new MetodoPagoCantidadInvalidaException("La cantidad debe ser mayor a 0.");

                MetodoPago metodoPago = Exists(metodoPagoDto);
                if (metodoPago == null)
                {
                    metodoPago = new MetodoPago()
                    {
                        Descripcion = metodoPagoDto.Descripcion,
                        Cantidad = metodoPagoDto.Cantidad,
                        Activo = true
                    };
                    db.MetodosPago.Add(metodoPago);
                }
                else if (metodoPago.Activo)
                {
                    if (metodoPago.Descripcion == metodoPagoDto.Descripcion)
                        throw new MetodoPagoAlreadyExistsException(string.Format("El metodo {0} ya se encuentra creado.", metodoPago.Descripcion));
                }
                else
                {
                    metodoPago.Activo = true;
                }
                db.SaveChanges();
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Alta, metodoPago);
                return metodoPago.Id;
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }

        public bool Edit(MetodoPagoDto metodoPagoDto)
        {
            try
            {
                MetodoPago metodoPago = db.MetodosPago.Where(x => x.Id == metodoPagoDto.Id && x.Activo).FirstOrDefault();
                if (metodoPago == null) throw new MetodoPagoNotFoundException(string.Format("El metodo con id {0} no existe.", metodoPagoDto.Id));
                MetodoPago exists = ExistsWithDifferentId(metodoPagoDto);
                if (exists != null)
                {
                    if (exists.Descripcion == metodoPagoDto.Descripcion)
                        throw new MetodoPagoAlreadyExistsException(string.Format("El metodo {0} ya se encuentra registrado.", metodoPagoDto.Descripcion));
                }
                else
                {
                    metodoPago.Descripcion = metodoPagoDto.Descripcion;
                    metodoPago.Cantidad = metodoPagoDto.Cantidad;
                    db.SaveChanges();
                    auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Modificacion, metodoPago);
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }

        public bool Delete(MetodoPagoDto metodoPagoDto)
        {
            try
            {
                MetodoPago metodoPago = db.MetodosPago.Where(x => x.Id == metodoPagoDto.Id && x.Activo).FirstOrDefault();
                if (metodoPago == null)
                    throw new MetodoPagoNotFoundException(string.Format("La metodoPago con id {0} no existe.", metodoPagoDto.Id));
                metodoPago.Activo = false;
                db.SaveChanges();
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Baja, metodoPago);
                return true;
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }

        public MetodoPagoResponseDto GetAll(MetodoPagoRequestDto request)
        {
            IEnumerable<MetodoPago> metodoPagoes = db.MetodosPago.Where(x => x.Activo);
            if (!string.IsNullOrEmpty(request.Descripcion))
            {
                metodoPagoes = metodoPagoes.Where(x => x.Descripcion.Contains(request.Descripcion));
            }
            if (request.Cantidad != 0)
            {
                metodoPagoes = metodoPagoes.Where(x => x.Cantidad == request.Cantidad);
            }
            


            IEnumerable<MetodoPago> metodoPagoCount = metodoPagoes;
            int count = metodoPagoCount.Count();
            List<MetodoPagoDto> metodoPagoesDto = metodoPagoes.Skip((request.Offset - 1) * request.Limit).Take(request.LimitResult).ToList().Select(x => new MetodoPagoDto(x)).ToList();
            return new MetodoPagoResponseDto(count, metodoPagoesDto);
        }



        private MetodoPago Exists(MetodoPagoDto metodoPagoDto)
        {
            return db.MetodosPago.Where(x => x.Id == metodoPagoDto.Id || x.Descripcion == metodoPagoDto.Descripcion).FirstOrDefault();
        }

        private MetodoPago ExistsWithDifferentId(MetodoPagoDto metodoPagoDto)
        {
            return db.MetodosPago.Where(x => x.Descripcion == metodoPagoDto.Descripcion && x.Id != metodoPagoDto.Id && x.Activo).FirstOrDefault();
        }
    }
}
