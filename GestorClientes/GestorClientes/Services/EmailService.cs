﻿using GestorClientes.Dominio.DAL;
using MimeKit;
using MailKit.Net.Smtp;
using MimeKit.Utils;

using GestorClientes.Dominio.Models;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Threading;
using static System.Net.Mime.MediaTypeNames;
using System.Net.Mime;
using System.Security.Claims;
using System.IO;

namespace GestorClientes.Logic.Services
{
    public class EmailService : BaseService
    {
        private string Name;
        private string User;
        private string Password;
        private string Host;
        private int Port;
        private bool SSL;
        private bool Active;
        private bool ActiveCopy;
        private string toEmailCopyAddress;
        private string toEmailCopyName;
  

        public EmailService(GestorContext context, int userId, AuditoriaService auditoria) : base(context, userId, auditoria)
        {
            ParametrosService parametrosService = new ParametrosService(db, userId, auditoria);


            List<Parametro> list = parametrosService.GetEmailParametros();
            Name = list.Where(x => x.Codigo == ParametrosService.EMAIL_NAME).FirstOrDefault().Valor;
            User = list.Where(x => x.Codigo == ParametrosService.EMAIL_USER).FirstOrDefault().Valor;
            Password = list.Where(x => x.Codigo == ParametrosService.EMAIL_PASSWORD).FirstOrDefault().Valor;
            Host = list.Where(x => x.Codigo == ParametrosService.EMAIL_HOST).FirstOrDefault().Valor;
            Port = Int32.Parse(list.Where(x => x.Codigo == ParametrosService.EMAIL_PORT).FirstOrDefault().Valor);
            SSL = Boolean.Parse(list.Where(x => x.Codigo == ParametrosService.EMAIL_SSL).FirstOrDefault().Valor);
            Active = Boolean.Parse(list.Where(x => x.Codigo == ParametrosService.EMAIL_ACTIVE).FirstOrDefault().Valor);
            ActiveCopy = Boolean.Parse(list.Where(x => x.Codigo == ParametrosService.EMAIL_ACTIVECOPY).FirstOrDefault().Valor);
            toEmailCopyName = list.Where(x => x.Codigo == ParametrosService.EMAIL_TOEMAILCOPYNAME).FirstOrDefault().Valor;
            toEmailCopyAddress = list.Where(x => x.Codigo == ParametrosService.EMAIL_TOEMAILCOPYADDRESS).FirstOrDefault().Valor;
        }

        //string imgPath = ""
        public void SendAsync(string toEmailName, string toEmailAddress, string subject, string body, byte[] img, string pathFile = "")
        {
            Thread t = new Thread(() => Send(toEmailName, toEmailAddress, subject, body, img, pathFile));
            t.Start();
        }

        private void Send(string toEmailName, string toEmailAddress, string subject, string body, byte[] img, string pathFile = "")
        {
            try
            {



                if (!Active || string.IsNullOrEmpty(toEmailAddress)
                    || string.IsNullOrEmpty(subject) || string.IsNullOrEmpty(body))
                    return;


                var message = new MimeMessage();

                message.From.Add(new MailboxAddress(Name, User));
                message.To.Add(new MailboxAddress(toEmailName, toEmailAddress));

                if (ActiveCopy) message.Bcc.Add(new MailboxAddress(toEmailCopyName, toEmailCopyAddress));
                message.Subject = subject;

                var builder = new BodyBuilder();

                if (pathFile != "")
                {
                    builder.Attachments.Add(pathFile);
                }

                //if (img != null)
                //{

                //    var image = builder.LinkedResources.Add("IMAGEN.png", img);
                //    image.ContentId = MimeUtils.GenerateMessageId();
                //    body = body.Replace("$IMAGEN$", "<img src=\"cid:" + image.ContentId + "\">");
                //}


                builder.HtmlBody = body;

                message.Body = builder.ToMessageBody();
                using (var client = new SmtpClient())
                {
                    // For demo-purposes, accept all SSL certificates (in case the server supports STARTTLS)
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                    client.Connect(Host, Port, SSL);


                    // Note: only needed if the SMTP server requires authentication
                    client.Authenticate(User, Password);
                    
                    client.Send(message);

                    client.Disconnect(true);


                }
                if (pathFile != "") { System.IO.File.Delete(pathFile); }
            }
            catch (Exception ex)
            {
                //string createText = "Hello and Welcome" + Environment.NewLine;
                //File.WriteAllText("C:\\Todosoft\\pdf\\log.txt", ex.Message +" "+ ex.InnerException.Message + " " + ex.StackTrace);

                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }


        }
    }
}
