﻿using GestorClientes.Dominio.DAL;
using GestorClientes.Dominio.Models;
using GestorClientes.Logic.Dto;
using GestorClientes.Logic.Dto.Requests;
using GestorClientes.Logic.Dto.Responses;
using GestorClientes.Logic.Exceptions.EstadoCliente;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Services
{
    public class EstadoClienteService : BaseService
    {
        public EstadoClienteService(GestorContext context, IEnumerable<Claim> claims) : base(context, claims) { }

        public int Add(EstadoClienteDto estadoClienteDto)
        {
            try
            {
                EstadoCliente estadoCliente = Exists(estadoClienteDto);
                if (estadoCliente == null)
                {
                    estadoCliente = new EstadoCliente()
                    {
                        Descripcion = estadoClienteDto.Descripcion,
                        Tipo = estadoClienteDto.Tipo,
                        Activo = true
                    };
                    db.EstadosCliente.Add(estadoCliente);
                }
                else if (estadoCliente.Activo)
                {
                    if (estadoCliente.Descripcion == estadoClienteDto.Descripcion)
                        throw new EstadoClienteDescripcionAlreadyExistsException(string.Format("El estado {0} ya se encuentra creado.", estadoCliente.Descripcion));
                }
                else
                {
                    estadoCliente.Activo = true;
                }
                db.SaveChanges();
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Alta, estadoCliente);
                return estadoCliente.Id;
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }

        

        public bool Edit(EstadoClienteDto estadoClienteDto)
        {
            try
            {
                EstadoCliente estadoCliente = db.EstadosCliente.Where(x => x.Id == estadoClienteDto.Id && x.Activo).FirstOrDefault();
                if (estadoCliente == null) throw new EstadoClienteNotFoundException(string.Format("El estado con id {0} no existe.", estadoClienteDto.Id));
                EstadoCliente exists = ExistsWithDifferentId(estadoClienteDto);
                if (exists != null)
                {
                    if (exists.Descripcion == estadoClienteDto.Descripcion)
                        throw new EstadoClienteAlreadyExistsException(string.Format("El estado {0} ya se encuentra registrado.", estadoClienteDto.Descripcion));
                }
                else
                {
                    estadoCliente.Descripcion = estadoClienteDto.Descripcion;
                    estadoCliente.Tipo = estadoClienteDto.Tipo;
                    db.SaveChanges();
                    auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Modificacion, estadoCliente);
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }

        public bool Delete(EstadoClienteDto estadoClienteDto)
        {
            try
            {
                EstadoCliente estadoCliente = db.EstadosCliente.Where(x => x.Id == estadoClienteDto.Id && x.Activo).FirstOrDefault();
                if (estadoCliente == null)
                    throw new EstadoClienteNotFoundException(string.Format("El estado con id {0} no existe.", estadoClienteDto.Id));
                estadoCliente.Activo = false;
                db.SaveChanges();
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Baja, estadoCliente);
                return true;
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }

        public EstadoClienteResponseDto GetAll(EstadoClienteRequestDto request)
        {
            IEnumerable<EstadoCliente> estadosCliente = db.EstadosCliente.Where(x => x.Activo);
            if (!string.IsNullOrEmpty(request.Descripcion))
            {
                estadosCliente = estadosCliente.Where(x => x.Descripcion.Contains(request.Descripcion));
            }
            if (request.Tipo.HasValue)
            {
                estadosCliente = estadosCliente.Where(x => x.Tipo == request.Tipo);
            }

            IEnumerable<EstadoCliente> estadosClienteCount = estadosCliente;
            int count = estadosClienteCount.Count();
            List<EstadoClienteDto> estadosClienteDto = estadosCliente.Skip((request.Offset - 1) * request.Limit).Take(request.LimitResult).ToList().Select(x => new EstadoClienteDto(x)).ToList();
            return new EstadoClienteResponseDto(count, estadosClienteDto);
        }



        private EstadoCliente Exists(EstadoClienteDto estadoClienteDto)
        {
            return db.EstadosCliente.Where(x => x.Id == estadoClienteDto.Id || x.Descripcion == estadoClienteDto.Descripcion).FirstOrDefault();
        }

        private EstadoCliente ExistsWithDifferentId(EstadoClienteDto estadoClienteDto)
        {
            return db.EstadosCliente.Where(x => x.Descripcion == estadoClienteDto.Descripcion && x.Id != estadoClienteDto.Id && x.Activo).FirstOrDefault();
        }
    }
}
