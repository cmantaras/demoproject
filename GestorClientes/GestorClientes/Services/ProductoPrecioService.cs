﻿
using GestorClientes.Dominio.DAL;
using GestorClientes.Dominio.Models;
using GestorClientes.Logic.Dto;
using GestorClientes.Logic.Dto.Requests;
using GestorClientes.Logic.Dto.Responses;
using GestorClientes.Logic.Exceptions.ProductoPrecio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Services
{
    public class ProductoPrecioService : BaseService
    {
        public ProductoPrecioService(GestorContext context, IEnumerable<Claim> claims) : base(context, claims) { }

        public int Add(ProductoPrecioDto productoPrecioDto)
        {
            try
            {
                ProductoPrecio productoPrecio = Exists(productoPrecioDto);
                if (productoPrecio == null)
                {
                    List<MetodoPago> metodos = db.MetodosPago.Where(x => x.Activo && productoPrecioDto.MetodosIds.Contains(x.Id)).ToList();
                    productoPrecio = new ProductoPrecio()
                    {
                        ProductoId = productoPrecioDto.ProductoId,
                        Precio = productoPrecioDto.Precio,
                        Desde = (productoPrecioDto.Desde == null ) ? DateTime.Now : productoPrecioDto.Desde,
                        ImpuestoId = productoPrecioDto.ImpuestoId,
                        MonedaId = productoPrecioDto.MonedaId,
                        Activo = true,
                        Metodos = metodos
                    };
                    db.ProductoPrecios.Add(productoPrecio);
                }
                else
                {
                    productoPrecio.Activo = true;
                }
                db.SaveChanges();
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Alta, productoPrecio);
                return productoPrecio.Id;
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }

        public bool Edit(ProductoPrecioDto productoPrecioDto)
        {
            try
            {
                ProductoPrecio productoPrecio = db.ProductoPrecios.Where(x => x.Id == productoPrecioDto.Id && x.Activo).FirstOrDefault();
                if (productoPrecio == null) throw new ProductoPrecioNotFoundException(string.Format("La productoPrecio con id {0} no existe.", productoPrecioDto.Id));
                if (productoPrecio != null)
                {
                    //metodos
                    List<MetodoPago> metodos = db.MetodosPago.Where(x => x.Activo && productoPrecioDto.MetodosIds.Contains(x.Id)).ToList();

                    productoPrecio.ProductoId = productoPrecioDto.ProductoId;
                    productoPrecio.Precio = productoPrecioDto.Precio;
                    productoPrecio.Desde = productoPrecioDto.Desde;
                    productoPrecio.ImpuestoId = productoPrecioDto.ImpuestoId;
                    productoPrecio.MonedaId = productoPrecioDto.MonedaId;
                    productoPrecio.Activo = true;
                    //faltaba asignar los metodos de pago
                    productoPrecio.Metodos.Clear();
                    productoPrecio.Metodos = metodos;

                    db.SaveChanges();
                    auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Modificacion, productoPrecio);
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }

        public bool Delete(ProductoPrecioDto productoPrecioDto)
        {
            try
            {
                ProductoPrecio productoPrecio = db.ProductoPrecios.Where(x => x.Id == productoPrecioDto.Id && x.Activo).FirstOrDefault();
                if (productoPrecio == null)
                    throw new ProductoPrecioNotFoundException(string.Format("La productoPrecio con id {0} no existe.", productoPrecioDto.Id));
                productoPrecio.Activo = false;
                db.SaveChanges();
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Baja, productoPrecio);
                return true;
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }


        public ProductoPrecioResponseDto GetAll(ProductoPrecioRequestDto request)
        {
            IEnumerable<ProductoPrecio> productoPrecioes = db.ProductoPrecios.Where(x => x.Activo);
            if (request.ProductoId > 0)
            {
                productoPrecioes = productoPrecioes.Where(x => x.ProductoId == request.ProductoId);
            }
            if (request.Inicio != DateTime.MinValue && request.Final != DateTime.MinValue)
            {
                productoPrecioes = productoPrecioes.Where(x => x.Desde >= request.Inicio && x.Desde <= request.Final);
            }


            IEnumerable<ProductoPrecio> productoPrecioCount = productoPrecioes;
            int count = productoPrecioCount.Count();
            List<ProductoPrecioDto> productoPrecios = productoPrecioes.Skip(request.Offset).Take(request.LimitResult).ToList().Select(x => new ProductoPrecioDto(x)).OrderBy(f => f.Desde).ToList();
            return new ProductoPrecioResponseDto(count, productoPrecios);
        }

        // ProductoPrecio -- Metodos de pago
        public int AddMetodos(ProductoPrecioMetodosDto productoPrecioDto)
        {


            ProductoPrecio productoPrecio = db.ProductoPrecios.Where(x => x.Id == productoPrecioDto.ProductoPrecioId && x.Activo).FirstOrDefault();

            List<MetodoPago> metodos = db.MetodosPago.Where(x => x.Activo && productoPrecioDto.MetodosId.Contains(x.Id)).ToList();
            if (productoPrecio == null) throw new ProductoPrecioNotFoundException(string.Format("La productoPrecio con id {0} no existe.", productoPrecioDto.ProductoPrecioId));

            if (productoPrecio != null)
            {
                productoPrecio.Metodos = metodos;
        
                productoPrecio.Activo = true;
                db.SaveChanges();
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Modificacion, productoPrecio);
                
            }

            return productoPrecio.Id;
        }

        public bool DeleteMetodo(int productoPrecioId, int metodoPagoId)
        {
            try
            {
                ProductoPrecio productoPrecio = db.ProductoPrecios.Where(x => x.Id == productoPrecioId && x.Activo).FirstOrDefault();

                MetodoPago metodo = db.MetodosPago.Where(x => x.Activo && x.Id == metodoPagoId).FirstOrDefault();
                if (productoPrecio == null)
                    throw new ProductoPrecioNotFoundException(string.Format("La productoPrecio con id no existe."));
               

                productoPrecio.Metodos.Remove(metodo);

                db.SaveChanges();
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Baja, productoPrecio);
                return true;
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }

        public ProductoPrecioDto GetProductoPrecioById(int ProductoPrecioId)
        {
           ProductoPrecio productoPrecio = db.ProductoPrecios.Where(x => x.Id == ProductoPrecioId).FirstOrDefault();


            return new ProductoPrecioDto(productoPrecio);
        }

        private ProductoPrecio Exists(ProductoPrecioDto productoPrecioDto)
        {
            return db.ProductoPrecios.Where(x => x.Id == productoPrecioDto.Id).FirstOrDefault();
        }


    }
}
