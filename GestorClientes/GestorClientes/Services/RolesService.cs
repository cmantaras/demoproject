﻿using GestorClientes.Dominio.DAL;
using GestorClientes.Dominio.Models;
using GestorClientes.Logic.Dto;
using GestorClientes.Logic.Dto.Requests;
using GestorClientes.Logic.Dto.Responses;
using GestorClientes.Logic.Exceptions.Roles;
using GestorClientes.Logic.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Services
{
    public class RolesService : BaseService
    {
        public RolesService(GestorContext context, IEnumerable<Claim> claims) : base(context, claims) { }

        public int Add(RolDto rolDto)
        {

            try
            {
                Rol rol = Exists(rolDto);
                if (rol == null)
                {
                    rol = new Rol()
                    {
                        Nombre = rolDto.Nombre,
                        Activo = true,
                    };
                    db.Roles.Add(rol);
                }
                else if (rol.Activo)
                {
                    throw new NombreAlreadyExistsException(string.Format("El rol con nombre {0} ya se encuentra creado", rol.Nombre));
                }
                else
                {
                    rol.Activo = true;
                }
                db.SaveChanges();
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Alta, rol);
                return rol.Id;
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }

        public bool Edit(RolDto rolDto)
        {

            try
            {
                Rol rol = db.Roles.Where(x => x.Id == rolDto.Id && x.Activo).FirstOrDefault();
                if (rol == null) throw new RolNotFoundException(string.Format("El rol con id {0} no existe", rolDto.Id));
                Rol exists = ExistsWithDifferentId(rolDto);
                if (exists != null && exists.Id != rolDto.Id)
                {
                    throw new NombreAlreadyExistsException(string.Format("El rol con nombre {0} ya se encuentra creado", exists.Nombre));
                }
                else
                {
                    rol.Nombre = rolDto.Nombre;
                    db.SaveChanges();
                    auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Modificacion, rol);
                    return true;
                }
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }

        public bool Delete(RolDto rolDto)
        {
            try
            {
                Rol rol = db.Roles.Where(x => x.Id == rolDto.Id && x.Activo).FirstOrDefault();
                if (rol == null) throw new RolNotFoundException(string.Format("El rol con id {0} no existe", rolDto.Id));
                rol.Activo = false;
                db.SaveChanges();
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Baja, rol);
                return true;
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }

        private Rol Exists(RolDto rolDto)
        {
            return db.Roles.Where(x => x.Nombre == rolDto.Nombre).FirstOrDefault();
        }

        private Rol ExistsWithDifferentId(RolDto rolDto)
        {
            return db.Roles.Where(x => x.Nombre == rolDto.Nombre && x.Id != rolDto.Id && x.Activo).
                    FirstOrDefault();
        }

        public RolResponseDto GetAll(RolRequestDto request)
        {
            try
            {

                IEnumerable<Rol> roles = db.Roles.Where(x => x.Activo);
                if (!string.IsNullOrEmpty(request.Nombre))
                {
                    roles = roles.Where(x => x.Nombre.Contains(request.Nombre));
                }

                IEnumerable<Rol> rolesCount = roles;
                int count = rolesCount.Count();
                List<RolDto> rolesDtos = roles.Skip((request.Offset - 1) * request.Limit).Take(request.LimitResult).ToList().Select(x => new RolDto(x)).ToList();
                return new RolResponseDto(count, rolesDtos);

            }
            catch(Exception ex)
            {

                throw ex;
            }
        }
    }
}
