﻿
using GestorClientes.Dominio.DAL;
using GestorClientes.Dominio.Models;
using GestorClientes.Logic.Dto;
using GestorClientes.Logic.Dto.Requests;
using GestorClientes.Logic.Dto.Responses;
using GestorClientes.Logic.Exceptions.Item;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Services
{
    public class ItemService : BaseService
    {
        public ItemService(GestorContext context, IEnumerable<Claim> claims) : base(context, claims) { }

        public int Add(ItemDto itemDto)
        {
            try
            {
                Item item = Exists(itemDto);
                if (item == null)
                {
                    item = new Item()
                    {
                        Descripcion = itemDto.Descripcion,
                        //Color = itemDto.Color,
                        Activo = true
                    };
                    db.Items.Add(item);
                }
                else if (item.Activo)
                {
                    if (item.Descripcion == itemDto.Descripcion)
                        throw new ItemDescripcionAlreadyExistsException(string.Format("El item {0} ya se encuentra creado.", item.Descripcion));
                }
                else
                {
                    item.Activo = true;
                }
                db.SaveChanges();
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Alta, item);
                return item.Id;
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }

        public bool Edit(ItemDto itemDto)
        {
            try
            {
                Item item = db.Items.Where(x => x.Id == itemDto.Id && x.Activo).FirstOrDefault();
                if (item == null) throw new ItemNotFoundException(string.Format("El item con id {0} no existe.", itemDto.Id));
                Item exists = ExistsWithDifferentId(itemDto);
                if (exists != null)
                {
                    if (exists.Descripcion == itemDto.Descripcion)
                        throw new ItemDescripcionAlreadyExistsException(string.Format("El item {0} ya se encuentra registrado.", itemDto.Descripcion));
                }
                else
                {
                    item.Descripcion = itemDto.Descripcion;
                    //item.Color = itemDto.Color;
                    db.SaveChanges();
                    auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Modificacion, item);
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }

        public bool Delete(ItemDto itemDto)
        {
            try
            {
                Item item = db.Items.Where(x => x.Id == itemDto.Id && x.Activo).FirstOrDefault();
                if (item == null)
                    throw new ItemNotFoundException(string.Format("La item con id {0} no existe.", itemDto.Id));
                item.Activo = false;
                db.SaveChanges();
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Baja, item);
                return true;
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }

        public ItemResponseDto GetAll(ItemRequestDto request)
        {
            IEnumerable<Item> itemes = db.Items.Where(x => x.Activo);
            if (!string.IsNullOrEmpty(request.Descripcion))
            {
                itemes = itemes.Where(x => x.Descripcion.Contains(request.Descripcion));
            }


            IEnumerable<Item> itemCount = itemes;
            int count = itemCount.Count();
            List<ItemDto> itemesDto = itemes.OrderByDescending(c => c.Id).Skip(request.Offset).Take(request.LimitResult).ToList().Select(x => new ItemDto(x)).ToList();
            return new ItemResponseDto(count, itemesDto);
        }



        private Item Exists(ItemDto itemDto)
        {
            return db.Items.Where(x => x.Id == itemDto.Id || x.Descripcion == itemDto.Descripcion).FirstOrDefault();
        }

        private Item ExistsWithDifferentId(ItemDto itemDto)
        {
            return db.Items.Where(x => x.Descripcion == itemDto.Descripcion && x.Id != itemDto.Id && x.Activo).FirstOrDefault();
        }
    }
}
