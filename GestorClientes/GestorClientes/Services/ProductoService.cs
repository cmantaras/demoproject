﻿
using GestorClientes.Dominio.DAL;
using GestorClientes.Dominio.Models;
using GestorClientes.Logic.Dto;
using GestorClientes.Logic.Dto.Requests;
using GestorClientes.Logic.Dto.Responses;
using GestorClientes.Logic.Exceptions.Producto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace GestorClientes.Logic.Services
{
    public class ProductoService : BaseService
    {
        public ProductoPrecioService productoPrecioService;
        public ProductoService(GestorContext context, IEnumerable<Claim> claims) : base(context, claims) {
            productoPrecioService = new ProductoPrecioService(context, claims);
        }
     

        public int Add(ProductoDto productoDto)
        {
            try
            {
                Producto producto = Exists(productoDto);
                if (producto == null)
                {

                    List<MetodoPago> metodos = db.MetodosPago.Where(x => x.Activo && productoDto.PrecioActual.MetodosIds.Contains(x.Id)).ToList();

                    producto = new Producto()
                    {
                        Codigo = productoDto.Codigo,
                        Descripcion = productoDto.Descripcion,
                        MarcaId = productoDto.MarcaId != 0 ? productoDto.MarcaId : default,
                        Activo = true,
                        Servicio = productoDto.Servicio
                        
                    };
                    db.Productos.Add(producto);
                }
                else if (producto.Activo)
                {
                    if (producto.Descripcion == productoDto.Descripcion)
                        throw new ProductoAlreadyExistsException(string.Format("El producto {0} ya se encuentra creada.", producto.Codigo));
                }
                else
                {
                    producto.Activo = true;
                }
                db.SaveChanges();
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Alta, producto);

                var productoPrecioDTO = new ProductoPrecioDto()
                {
                    ProductoId = producto.Id,
                    MonedaId = productoDto.PrecioActual.MonedaId,
                    Precio = productoDto.PrecioActual.Precio,
                    MetodosIds = productoDto.PrecioActual.MetodosIds,
                    ImpuestoId = productoDto.PrecioActual.ImpuestoId,
                    Desde = DateTime.Now

                    
                };
  
                productoPrecioService.Add(productoPrecioDTO);
                return producto.Id;
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }

        public bool Edit(ProductoDto productoDto)
        {
            try
            {
                Producto producto = db.Productos.Where(x => x.Id == productoDto.Id && x.Activo).FirstOrDefault();
                if (producto == null) throw new ProductoNotFoundException(string.Format("El producto con id {0} no existe.", productoDto.Id));
                Producto exists = ExistsWithDifferentId(productoDto);
                if (exists != null)
                {
                    if (exists.Descripcion == productoDto.Descripcion)
                        throw new ProductoAlreadyExistsException(string.Format("El producto {0} ya se encuentra registrado.", productoDto.Codigo));
                }
                else
                {
                    producto.Codigo = productoDto.Codigo;
                    producto.Descripcion = productoDto.Descripcion;
                    producto.MarcaId = productoDto.MarcaId;
                    db.SaveChanges();
                    auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Modificacion, producto);
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }

        public bool Delete(ProductoDto productoDto)
        {
            try
            {
                Producto producto = db.Productos.Where(x => x.Id == productoDto.Id && x.Activo).FirstOrDefault();
                if (producto == null)
                    throw new ProductoNotFoundException(string.Format("La producto con id {0} no existe.", productoDto.Id));
                producto.Activo = false;
                db.SaveChanges();
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Baja, producto);
                return true;
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }

        public ProductoResponseDto GetAll(ProductoRequestDto request)
        {
            IEnumerable<Producto> productos = db.Productos.Where(x => x.Activo).Include(y => y.Marca).Include(x => x.Precios);
            if (!string.IsNullOrEmpty(request.Descripcion))
            {
                productos = productos.Where(x => x.Descripcion.Contains(request.Descripcion));
            }
            if (!string.IsNullOrEmpty(request.Codigo))
            {
                productos = productos.Where(x => x.Codigo.Contains(request.Codigo));
            }
            if (!string.IsNullOrEmpty(request.Marca))
            {
                productos = productos.Where(x => x.Marca.Descripcion.Contains(request.Marca));
            }

            IEnumerable<Producto> productoCount = productos;
            int count = productoCount.Count();
            List<ProductoDto> productosDto = productos.OrderByDescending(c => c.Id).Skip(request.Offset).Take(request.LimitResult).ToList().Select(x => new ProductoDto(x)).ToList();
            return new ProductoResponseDto(count, productosDto);
        }


        public ProductoResponseDto GetFiltrados(string productoDescripcion)
        {
            IEnumerable<Producto> productos = db.Productos.Where(x => x.Activo).Include(y => y.Marca).Include(x => x.Precios);
            if (!string.IsNullOrEmpty(productoDescripcion))
            {
                productos = productos.Where(x => x.Descripcion.ToLower().Contains(productoDescripcion.ToLower()));
            }

            IEnumerable<Producto> productoCount = productos;
            int count = productoCount.Count();
            List<ProductoDto> productosDto = productos.OrderByDescending(c => c.Id).ToList().Select(x => new ProductoDto(x)).ToList();
            return new ProductoResponseDto(count, productosDto);
        }



        private Producto Exists(ProductoDto productoDto)
        {
            return db.Productos.Where(x => x.Id == productoDto.Id || x.Codigo == productoDto.Codigo).FirstOrDefault();
        }

        private Producto ExistsWithDifferentId(ProductoDto productoDto)
        {
            return db.Productos.Where(x => x.Codigo == productoDto.Codigo && x.Id != productoDto.Id && x.Activo).FirstOrDefault();
        }

        public ProductoPrecio PrecioActual(Producto producto)
        {
            return producto.Precios.Where(x => x.Activo && x.Desde <= DateTime.Now).OrderByDescending(y => y.Desde).FirstOrDefault();
        }
    }
}
