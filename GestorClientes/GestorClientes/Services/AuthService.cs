﻿using GestorClientes.Dominio.DAL;
using GestorClientes.Dominio.Models;
using GestorClientes.Logic.Dto;
using GestorClientes.Logic.Exceptions;
using GestorClientes.Logic.Exceptions.Usuarios;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Services
{
    public class AuthService
    {
        public async Task<UsuarioDto> Authenticate(string username, string pass)
        {
            try
            {
                using (GestorContext db = new GestorContext())
                {
                    Usuario user = db.Usuarios.Where(u => u.Username == username && u.Activo).FirstOrDefault();
                    if (user != null)
                    {
                        if (ValidatePassword(user.Password, user.Username, pass))
                        {
                            user.LastLogin = DateTime.Now;
                            db.SaveChanges();
                            return new UsuarioDto(user);
                        }
                    }
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ChangePassword(string username, string password, string confirmPassword, string token)
        {
            try
            {
                if (password != confirmPassword) throw new BadRequestException("Las password no coinciden");
                using (GestorContext db = new GestorContext())
                {
                    Usuario usuario = db.Usuarios.Where(x => x.Username == username).FirstOrDefault();
                    ValidateTokenInfo(usuario, token);
                    usuario.Password = GenPassword(username, password);
                    usuario.LastPasswordChange = DateTime.Now;
                    usuario.TokenDate = null;
                    usuario.ChangePasswordToken = null;
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool HasPermission(string permission, int userId)
        {
            try
            {
                using (GestorContext db = new GestorContext())
                {
                    var user = db.Usuarios.Where(x => x.Id == userId).FirstOrDefault();
                    if (user.IsAdmin) return true;
                    if (user.Rol != null)
                    {
                        return user.Rol.Permisos.Where(x => x.Key == permission).FirstOrDefault() != null;
                    }
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        private bool ValidateTokenInfo(Usuario usuario, string token)
        {
            if (usuario == null) throw new UsuarioNotFoundException("El nombre de usuario no existe");
            if (usuario.ChangePasswordToken != token) throw new Exception("Token invalido");
            if (usuario.TokenDate > DateTime.Now.AddMinutes(int.Parse(ConfigurationManager.AppSettings["changePasswordTokenExpireInMinutes"]))) throw new Exception("El token expiro");
            return true;
        }

        public void IsValidToken(string username, string token)
        {
            using (GestorContext db = new GestorContext())
            {
                Usuario usuario = db.Usuarios.Where(x => x.Username == username).FirstOrDefault();
                ValidateTokenInfo(usuario, token);
            }
        }

        public void ResetPassword(string username)
        {
            try
            {
                using (GestorContext db = new GestorContext())
                {
                    Usuario usuario = db.Usuarios.Where(x => x.Username == username).FirstOrDefault();
                    if (usuario == null) throw new UsuarioNotFoundException("El nombre de usuario no existe");
                    if (usuario.TokenDate.HasValue && usuario.TokenDate.Value.AddMinutes(1) > DateTime.Now) throw new BadRequestException("Espere un minuto antes de solicitar otro cambio de contraseña");
                    EmailService emailService = new EmailService(db, 1, null);
                    ParametrosService parametrosService = new ParametrosService(db, 1, null);
                    usuario.ChangePasswordToken = GenPassword(username, usuario.Password).Replace("/", "_").Replace("+", "-");
                    usuario.TokenDate = DateTime.Now;
                    db.SaveChanges();
                    var emailName = usuario.Nombre + " " + usuario.Apellido;
                    var asuntoMail = parametrosService.GetUsuarioReinicioEmailSubject();
                    var url = parametrosService.GetUrlFront();
                    string bodyMail = parametrosService.GetUsuarioReinicioEmailBody();

                    bodyMail = bodyMail.Replace("$URL", url + "/resetpassword?username=" + usuario.Username + "&jwt=" + usuario.ChangePasswordToken)
                                        .Replace("$Nombre", usuario.Nombre + " " + usuario.Apellido)
                                        .Replace("$Username", usuario.Username);
                    emailService.SendAsync(emailName, usuario.Email, asuntoMail, bodyMail, null);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private bool ValidatePassword(string passwordSaved, string username, string password)
        {
            /* Extract the bytes */
            byte[] hashBytes = Convert.FromBase64String(passwordSaved);
            /* Get the salt */
            byte[] salt = new byte[16];
            Array.Copy(hashBytes, 0, salt, 0, 16);
            /* Compute the hash on the password the user entered */
            var pbkdf2 = new Rfc2898DeriveBytes(username + password, salt, 100000);
            byte[] hash = pbkdf2.GetBytes(20);
            /* Compare the results */
            for (int i = 0; i < 20; i++)
                if (hashBytes[i + 16] != hash[i])
                    throw new UnauthorizedAccessException();
            return true;
        }

        public string GenPassword(string username, string pass)
        {
            byte[] salt;
            new RNGCryptoServiceProvider().GetBytes(salt = new byte[16]);
            var pbkdf2 = new Rfc2898DeriveBytes(username + pass, salt, 100000);
            byte[] hash = pbkdf2.GetBytes(20);
            byte[] hashBytes = new byte[36];
            Array.Copy(salt, 0, hashBytes, 0, 16);
            Array.Copy(hash, 0, hashBytes, 16, 20);
            return Convert.ToBase64String(hashBytes);
        }
    }
}
