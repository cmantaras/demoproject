﻿
using GestorClientes.Dominio.DAL;
using GestorClientes.Dominio.Models;
using GestorClientes.Logic.Dto;
using GestorClientes.Logic.Dto.Requests;
using GestorClientes.Logic.Dto.Responses;
using GestorClientes.Logic.Exceptions.Moneda;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Services
{
    public class MonedaService : BaseService
    {
        public MonedaService(GestorContext context, IEnumerable<Claim> claims) : base(context, claims) { }

        public int Add(MonedaDto monedaDto)
        {
            try
            {
                Moneda moneda = Exists(monedaDto);
                if (moneda == null)
                {
                    moneda = new Moneda()
                    {
                        Descripcion = monedaDto.Descripcion,
                        Activo = true
                    };
                    db.Monedas.Add(moneda);
                }
                else if (moneda.Activo)
                {
                    if (moneda.Descripcion == monedaDto.Descripcion)
                        throw new MonedaDescripcionAlreadyExistsException(string.Format("La moneda {0} ya se encuentra creada.", moneda.Descripcion));
                }
                else
                {
                    moneda.Activo = true;
                }
                db.SaveChanges();
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Alta, moneda);
                return moneda.Id;
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }

        public bool Edit(MonedaDto monedaDto)
        {
            try
            {
                Moneda moneda = db.Monedas.Where(x => x.Id == monedaDto.Id && x.Activo).FirstOrDefault();
                if (moneda == null) throw new MonedaNotFoundException(string.Format("La moneda con id {0} no existe.", monedaDto.Id));
                Moneda exists = ExistsWithDifferentId(monedaDto);
                if (exists != null)
                {
                    if (exists.Descripcion == monedaDto.Descripcion)
                        throw new MonedaDescripcionAlreadyExistsException(string.Format("La moneda {0} ya se encuentra registrado.", monedaDto.Descripcion));
                }
                else
                {
                    moneda.Descripcion = monedaDto.Descripcion;
                    db.SaveChanges();
                    auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Modificacion, moneda);
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }

        public bool Delete(MonedaDto monedaDto)
        {
            try
            {
                Moneda moneda = db.Monedas.Where(x => x.Id == monedaDto.Id && x.Activo).FirstOrDefault();
                if (moneda == null)
                    throw new MonedaNotFoundException(string.Format("La moneda con id {0} no existe.", monedaDto.Id));
                moneda.Activo = false;
                db.SaveChanges();
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Baja, moneda);
                return true;
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }

        public MonedaResponseDto GetAll(MonedaRequestDto request)
        {
            IEnumerable<Moneda> monedas = db.Monedas.Where(x => x.Activo);
            if (!string.IsNullOrEmpty(request.Descripcion))
            {
                monedas = monedas.Where(x => x.Descripcion.Contains(request.Descripcion));
            }


            IEnumerable<Moneda> monedaCount = monedas;
            int count = monedaCount.Count();
            List<MonedaDto> monedasDto = monedas.Skip((request.Offset - 1) * request.Limit).Take(request.LimitResult).ToList().Select(x => new MonedaDto(x)).ToList();
            return new MonedaResponseDto(count, monedasDto);
        }

        public MonedaResponseDto GetAllMonedas(MonedaRequestDto request)
        {
            IEnumerable<Moneda> monedas = db.Monedas.ToList();
            IEnumerable<Moneda> monedaCount = monedas;
            int count = monedaCount.Count();
            List<MonedaDto> monedasDto = monedas.Select(x => new MonedaDto(x)).ToList();
            return new MonedaResponseDto(count, monedasDto);
        }



        private Moneda Exists(MonedaDto monedaDto)
        {
            return db.Monedas.Where(x => x.Id == monedaDto.Id || x.Descripcion == monedaDto.Descripcion).FirstOrDefault();
        }

        private Moneda ExistsWithDifferentId(MonedaDto monedaDto)
        {
            return db.Monedas.Where(x => x.Descripcion == monedaDto.Descripcion && x.Id != monedaDto.Id && x.Activo).FirstOrDefault();
        }
    }
}
