﻿using GestorClientes.Dominio.DAL;
using GestorClientes.Dominio.Models;
using GestorClientes.Logic.Dto;
using GestorClientes.Logic.Dto.Requests;
using GestorClientes.Logic.Dto.Responses;
using GestorClientes.Logic.Exceptions.Parametros;
using GestorClientes.Logic.Services;

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Services
{
    public class ParametrosService : BaseService
    {
        public const string TITULO_COMPROBANTE_PAGO = "TITULO_COMPROBANTE_PAGO";
        public const string COMENTARIO_COMPROBANTE_PAGO = "COMENTARIO_COMPROBANTE_PAGO";
        public const string FECHA_CHECK_FIBRA = "FECHA_CHECK_FIBRA";
        public const string VENCIMIENTO_MES = "VENCIMIENTO_MES";
        public const string USUARIOS_REGISTRO_EMAIL_SUBJECT = "USUARIOS_REGISTRO_EMAIL_SUBJECT";
        public const string USUARIOS_REGISTRO_EMAIL_BODY= "USUARIOS_REGISTRO_EMAIL_BODY";
        public const string USUARIOS_REINICIO_EMAIL_BODY = "USUARIOS_REINICIO_EMAIL_BODY";
        public const string USUARIOS_REINICIO_EMAIL_SUBJECT = "USUARIOS_REINICIO_EMAIL_SUBJECT";
        public const string URL_FRONT= "URL_FRONT";
        public const string EMAIL_NAME = "EMAIL_NAME";
        public const string EMAIL_USER = "EMAIL_USER";
        public const string EMAIL_PASSWORD = "EMAIL_PASSWORD";
        public const string EMAIL_HOST = "EMAIL_HOST";
        public const string EMAIL_PORT = "EMAIL_PORT";
        public const string EMAIL_SSL = "EMAIL_SSL";
        public const string EMAIL_ACTIVE = "EMAIL_ACTIVE";
        public const string EMAIL_ACTIVECOPY = "EMAIL_ACTIVECOPY";
        public const string EMAIL_TOEMAILCOPYNAME = "EMAIL_TOEMAILCOPYNAME";
        public const string EMAIL_TOEMAILCOPYADDRESS = "EMAIL_TOEMAILCOPYADDRESS";


        public ParametrosService(GestorContext context, IEnumerable<Claim> claims) : base(context, claims) { }

        public ParametrosService(GestorContext context, int userId, AuditoriaService auditoria) : base(context, userId, auditoria) { }

        public bool Edit(ParametroDto parametroDto)
        {
            try
            {
                Parametro parametro = db.Parametros.Where(x => x.Codigo == parametroDto.Codigo).FirstOrDefault();
                if (parametro == null) throw new ParametroNotFoundException(string.Format("El parametro con codigo {0} no existe.", parametroDto.Codigo));

                parametro.Valor = parametroDto.Valor;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<Parametro> GetEmailParametros()
        {
            List<Parametro> listaParametros = new List<Parametro>();
            Parametro param0 = GetParametroObject(EMAIL_NAME);
            Parametro param1 = GetParametroObject(EMAIL_USER);
            Parametro param2 = GetParametroObject(EMAIL_PASSWORD);
            Parametro param3 = GetParametroObject(EMAIL_HOST);
            Parametro param4 = GetParametroObject(EMAIL_PORT);
            Parametro param5 = GetParametroObject(EMAIL_SSL);
            Parametro param6 = GetParametroObject(EMAIL_ACTIVE);
            Parametro param7 = GetParametroObject(EMAIL_ACTIVECOPY);
            Parametro param8 = GetParametroObject(EMAIL_TOEMAILCOPYNAME);
            Parametro param9 = GetParametroObject(EMAIL_TOEMAILCOPYADDRESS);

            listaParametros.Add(param0);
            listaParametros.Add(param1);
            listaParametros.Add(param2);
            listaParametros.Add(param3);
            listaParametros.Add(param4);
            listaParametros.Add(param5);
            listaParametros.Add(param6);
            listaParametros.Add(param7);
            listaParametros.Add(param8);
            listaParametros.Add(param9);

            return listaParametros;

        }
        public ParametroResponseDto GetAll(ParametroRequestDto request)
        {
            IEnumerable<Parametro> parametros = db.Parametros;
            if (!string.IsNullOrEmpty(request.Codigo))
            {
                parametros = parametros.Where(x => x.Codigo.Contains(request.Codigo));
            }

            IEnumerable<Parametro> parametrosCount = parametros;
            int count = parametrosCount.Count();
            List<ParametroDto> parametrosDto = parametros.ToList().Select(x => new ParametroDto(x)).ToList();
            //antes de irme chequeo que no se halla vencido el mes
            this.CheckVencimientoMes();
            return new ParametroResponseDto(count, parametrosDto);
        }
        public string GetUsuarioRegistroEmailBody()
        {
            return GetParametro(USUARIOS_REGISTRO_EMAIL_BODY);
        }
        public string GetUsuarioRegistroEmailSubject()
        {
            return GetParametro(USUARIOS_REGISTRO_EMAIL_SUBJECT);
        }

        public string GetUsuarioReinicioEmailBody()
        {
            return GetParametro(USUARIOS_REINICIO_EMAIL_BODY);
        }
        public string GetUsuarioReinicioEmailSubject()
        {
            return GetParametro(USUARIOS_REINICIO_EMAIL_SUBJECT);
        }
        public string GetUrlFront()
        {
            return GetParametro(URL_FRONT);
        }
        public string GetParametro(string parametroCodigo)
        {
            Parametro parametro = db.Parametros.Where(x => x.Codigo == parametroCodigo).FirstOrDefault();
            if (parametro == null)
                return "";
            return parametro.Valor;
        }
        public Parametro GetParametroObject(string parametroCodigo)
        {
            Parametro parametro = db.Parametros.Where(x => x.Codigo == parametroCodigo).FirstOrDefault();
            if (parametro == null)
                return null;
            return parametro;
        }



        public Boolean SetTextoComprobantePago(TextoComprobanteRequestDto request)
        {
            string titulo = request.Titulo;
            string comentario = request.Comentario;
            Parametro parametroTitulo = db.Parametros.Where(x => x.Codigo == TITULO_COMPROBANTE_PAGO).FirstOrDefault();
            Parametro parametroComentario = db.Parametros.Where(x => x.Codigo == COMENTARIO_COMPROBANTE_PAGO).FirstOrDefault();
            if (parametroTitulo == null)
            {
                parametroTitulo = new Parametro();
                parametroTitulo.Codigo = TITULO_COMPROBANTE_PAGO;
                parametroTitulo.Valor = titulo.ToString();
                db.Parametros.Add(parametroTitulo);
            }
            else
            {
                parametroTitulo.Valor = titulo.ToString();
            }
            db.SaveChanges();
            if (parametroComentario == null)
            {
                parametroComentario = new Parametro();
                parametroComentario.Codigo = COMENTARIO_COMPROBANTE_PAGO;
                parametroComentario.Valor = comentario.ToString();
                db.Parametros.Add(parametroComentario);
            }
            else
            {
                parametroComentario.Valor = comentario.ToString();
            }
            db.SaveChanges();
            return true;
        }

        
        public Parametro SetCheckFibra(string fechaCheckFibra)
        {
            Parametro parametro = db.Parametros.Where(x => x.Codigo == FECHA_CHECK_FIBRA).FirstOrDefault();
            if (parametro == null)
            {
                parametro = new Parametro();
                parametro.Codigo = FECHA_CHECK_FIBRA;
                parametro.Valor = fechaCheckFibra.ToString();
                db.Parametros.Add(parametro);
            }
            else
            {
                parametro.Valor = fechaCheckFibra.ToString();
            }
            db.SaveChanges();
            return parametro;
        }
        public Parametro SetVencimientoMes(string vencimientoMes)
        {
            Parametro parametro = db.Parametros.Where(x => x.Codigo == VENCIMIENTO_MES).FirstOrDefault();
            if (parametro == null)
            {
                parametro = new Parametro();
                parametro.Codigo = VENCIMIENTO_MES;
                parametro.Valor = vencimientoMes.ToString();
                db.Parametros.Add(parametro);
            }
            else
            {
                parametro.Valor = vencimientoMes.ToString();
            }
            db.SaveChanges();
            return parametro;
        }



        public string GetTituloComprobantePago()
        {
            return GetParametro(TITULO_COMPROBANTE_PAGO);
        }
        public string GetComentarioComprobantePago()
        {
            return GetParametro(COMENTARIO_COMPROBANTE_PAGO);
        }

        public bool CheckVencimientoMes()
        {
            //HOY
            DateTime fechaHoy = DateTime.Now;
 
            //int mesHoy = DateTime.Now.Month;
            //Seteado
            Parametro parametro = db.Parametros.Where(x => x.Codigo == VENCIMIENTO_MES).FirstOrDefault();
            string fechaSeteada =  parametro.Valor;

            DateTime fechaConvertida;

            fechaConvertida = DateTime.ParseExact(fechaSeteada, "dd/MM/yyyy",
                        CultureInfo.InvariantCulture
                        );

            //Chequeo
            while (fechaHoy > fechaConvertida)
            {

                fechaConvertida = fechaConvertida.AddMonths(1);
                //si mes fecha hoy == mes fecha convertida && año fechahoy == año fechaconvertida
                //agregar 1 mes
                //if else timespan es mayor 30dias tomar la fecha actual como mes de referencia
                this.SetVencimientoMes(fechaConvertida.ToString("dd/MM/yyyy"));
            }
          

            db.SaveChanges();
            return true;
        }
    }
}
