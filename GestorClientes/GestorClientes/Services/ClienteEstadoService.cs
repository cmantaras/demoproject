﻿using GestorClientes.Dominio.DAL;
using GestorClientes.Dominio.Models;
using GestorClientes.Logic.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Services
{
    class ClienteEstadoService : BaseService
    {
        private EstadoClienteService estadoClienteService;
        public ClienteEstadoService(GestorContext context, IEnumerable<Claim> claims) : base(context, claims) {

        }

        internal void Add(Cliente cliente, int estadoClienteId)
        {
            try
            {
                ClienteEstado actual = null;
                if (cliente.Estados != null)
                    actual = cliente.Estados.Where(x => x.Activo).OrderByDescending(x => x.Desde).FirstOrDefault();

                if (actual == null || actual.Id != estadoClienteId)
                {
                    ClienteEstado clienteEstado = new ClienteEstado()
                    {
                        ClienteId = cliente.Id,
                        Desde = DateTime.Now,
                        Activo = true,
                       
                        EstadoCliente = db.EstadosCliente.Where(x => x.Id == estadoClienteId).FirstOrDefault(),
                    };
                    if (cliente.Estados == null)
                        cliente.Estados = new List<ClienteEstado>();
                    
                    cliente.Estados.Add(clienteEstado);
                    db.SaveChanges();
                    auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Alta, clienteEstado);
                }
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
            
        }
    }
}
