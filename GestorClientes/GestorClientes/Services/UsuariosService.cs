﻿using GestorClientes.Dominio.DAL;
using GestorClientes.Dominio.Models;
using GestorClientes.Logic.Dto;
using GestorClientes.Logic.Dto.Requests;
using GestorClientes.Logic.Dto.Responses;
using GestorClientes.Logic.Exceptions.Usuarios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace GestorClientes.Logic.Services
{
    public class UsuariosService : BaseService
    {
        private EmailService emailService;
        private ParametrosService parametrosService;
        private AuthService authService;
        public UsuariosService(GestorContext context, IEnumerable<Claim> claims) : base(context, claims)
        {
            emailService = new EmailService(context, userId, auditoria);
            parametrosService = new ParametrosService(context, userId, auditoria);
            authService = new AuthService();
        }

        public int Add(UsuarioDto usuarioDto)
        {
            try
            {
                Usuario usuario = Exists(usuarioDto);
                if (usuario == null)
                {
                    usuario = new Usuario()
                    {
                        Nombre = usuarioDto.Nombre,
                        Apellido = usuarioDto.Apellido,
                        Email = usuarioDto.Email,
                        RolId = usuarioDto.RolId,
                        IsAdmin = usuarioDto.IsAdmin,
                        Username = usuarioDto.Username,
                        ChangePasswordToken = authService.GenPassword(usuarioDto.Username, "nopassword").Replace("/", "_").Replace("+", "-"),
                        Activo = true,
                    };
                    db.Usuarios.Add(usuario);
                }
                else if (usuario.Activo)
                {
                    throw new UsernameAlreadyExistsException(string.Format("El usuario con nombre {0} ya se encuentra creado", usuario.Nombre));
                }
                else
                {
                    usuario.Activo = true;
                    usuario.Nombre = usuarioDto.Nombre;
                    usuario.Apellido = usuarioDto.Apellido;
                    usuario.Email = usuarioDto.Email;
                    usuario.RolId = usuarioDto.RolId;
                    usuario.IsAdmin = usuarioDto.IsAdmin;
                    usuario.Username = usuarioDto.Username;
                    usuario.ChangePasswordToken = authService.GenPassword(usuarioDto.Username, "nopassword").Replace("/", "_").Replace("+", "-");
                }
                db.SaveChanges();
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, usuario);
                var emailName = usuario.Nombre + " " + usuario.Apellido;
                var asuntoMail = parametrosService.GetUsuarioReinicioEmailSubject();
                var url = parametrosService.GetUrlFront();
                string bodyMail = parametrosService.GetUsuarioReinicioEmailBody();

                bodyMail = bodyMail.Replace("$URL", url + "/resetpassword?username=" + usuario.Username + "&jwt=" + usuario.ChangePasswordToken)
                                    .Replace("$Nombre", usuario.Nombre + " " + usuario.Apellido)
                                    .Replace("$Username", usuario.Username);
                emailService.SendAsync(emailName, usuario.Email, asuntoMail, bodyMail, null);
                return usuario.Id;
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }

        public bool Edit(UsuarioDto usuarioDto)
        {
            try
            {
                Usuario usuario = db.Usuarios.Where(x => x.Id == usuarioDto.Id && x.Activo).FirstOrDefault();
                if (usuario == null) throw new UsuarioNotFoundException(string.Format("El usuario con id {0} no existe", usuarioDto.Id));
                Usuario exists = ExistsWithDifferentId(usuarioDto);
                if (exists != null && exists.Id != usuarioDto.Id)
                {
                    throw new UsernameAlreadyExistsException(string.Format("El nombre de usuario {0} ya se encuentra creado", exists.Nombre));
                }
                else
                {
                    usuario.Nombre = usuarioDto.Nombre;
                    usuario.Apellido = usuarioDto.Apellido;
                    usuario.IsAdmin = usuarioDto.IsAdmin;
                    usuario.Email= usuarioDto.Email;
                    usuario.RolId = usuarioDto.RolId;
                    db.SaveChanges();
                    auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Modificacion, usuario);
                    return true;
                }
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }

        public bool Delete(UsuarioDto usuarioDto)
        {
            try
            {
                Usuario usuario = db.Usuarios.Where(x => x.Id == usuarioDto.Id && x.Activo).FirstOrDefault();
                if (usuario == null) throw new UsuarioNotFoundException(string.Format("El usuario con id {0} no existe", usuarioDto.Id));
                if (usuario.Username == "admin") throw new UsuarioNotDeletableException("El usuario admin no puede ser eliminado");
                usuario.Activo = false;
                db.SaveChanges();
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Baja, usuario);
                return true;
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }

        private Usuario Exists(UsuarioDto usuarioDto)
        {
            return db.Usuarios.Where(x => x.Username == usuarioDto.Username).FirstOrDefault();
        }

        private Usuario ExistsWithDifferentId(UsuarioDto usuarioDto)
        {
            return db.Usuarios.Where(x => x.Username == usuarioDto.Username && x.Id != usuarioDto.Id && x.Activo).
                    FirstOrDefault();
        }

        public UsuarioResponseDto GetAll(UsuarioRequestDto request)
        {

           // var userPi = parametrosService.GetUserRaspy();

            IEnumerable<Usuario> usuarios = db.Usuarios.Where(x => x.Activo);
            if (!string.IsNullOrEmpty(request.Nombre))
            {
                usuarios = usuarios.Where(x => x.Nombre.Contains(request.Nombre));
            }
            if (!string.IsNullOrEmpty(request.Apellido))
            {
                usuarios = usuarios.Where(x => x.Apellido.Contains(request.Apellido));
            }
            if (!string.IsNullOrEmpty(request.Nombre))
            {
                usuarios = usuarios.Where(x => x.Username.Contains(request.Username));
            }

            IEnumerable<Usuario> usuariosCount = usuarios;
            int count = usuariosCount.Count();
            List<UsuarioDto> usuariosDtos = usuarios.OrderByDescending(c => c.Id).Skip(request.Offset).Take(request.LimitResult).ToList().Select(x => new UsuarioDto(x)).ToList();
            return new UsuarioResponseDto(count, usuariosDtos);
        }
    }
}
