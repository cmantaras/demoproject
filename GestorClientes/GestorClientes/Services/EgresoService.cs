﻿
using GestorClientes.Dominio.DAL;
using GestorClientes.Dominio.Models;
using GestorClientes.Logic.Dto;
using GestorClientes.Logic.Dto.Requests;
using GestorClientes.Logic.Dto.Responses;
using GestorClientes.Logic.Exceptions.Egreso;
using GestorClientes.Logic.Exceptions.Ciudad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Services
{
    public class EgresoService : BaseService
    {
        public EgresoService(GestorContext context, IEnumerable<Claim> claims) : base(context, claims) { }
        public List<EgresoDto> chequesAndEgresos = new List<EgresoDto>();
        public int Add(EgresoRequestDto egresoDto)
        {
            try
            {
                Egreso egreso = Exists(egresoDto);
                if (egreso == null)
                {
                    egreso = new Egreso()
                    {
                        Importe = egresoDto.Importe,
                        FechaEgreso = egresoDto.FechaEgreso,
                        FechaCheque = egresoDto.FechaCheque,
                        Descripcion = egresoDto.Descripcion,
                        EsCheque = egresoDto.EsCheque,
                        CategoriaId = egresoDto.CategoriaId,
                        ProveedorId = egresoDto.ProveedorId,
                        MonedaId = egresoDto.MonedaId,
                        Activo = true,
                    };
                    db.Egresos.Add(egreso);
                }

                db.SaveChanges();
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Alta, egreso);
                return egreso.Id;
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }

        public bool Edit(EgresoRequestDto egresoDto)
        {
            try
            {
                Egreso egreso = db.Egresos.Where(x => x.Id == egresoDto.Id).FirstOrDefault();

                if (egreso == null) throw new EgresoNotFoundException(string.Format("El egreso con id {0} no existe.", egresoDto.Id));

                egreso.Importe = egresoDto.Importe;
                egreso.MonedaId = egresoDto.MonedaId;
                egreso.CategoriaId = egresoDto.CategoriaId;
                egreso.ProveedorId = egresoDto.ProveedorId;
                egreso.FechaEgreso = egresoDto.FechaEgreso;
                egreso.FechaCheque = egresoDto.FechaCheque;
                egreso.Descripcion = (egresoDto.Descripcion == null) ? "" : egresoDto.Descripcion;
                egreso.EsCheque = egresoDto.EsCheque;
                

                db.SaveChanges();

                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Modificacion, egreso);
                return true;


            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }

        public bool Delete(EgresoDto egresoDto)
        {
            try
            {
                Egreso egreso = db.Egresos.Where(x => x.Id == egresoDto.Id).FirstOrDefault();
                if (egreso == null)
                    throw new CiudadNotFoundException(string.Format("La ciudad con id {0} no existe.", egresoDto.Id));
                egreso.Activo = false;
                db.SaveChanges();
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Baja, egreso);
                return true;
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }

        public EgresoResponseDto GetAll(int IdCategoria, int IdProveedor, int Offset, int Limit, int EsCheque)
        {
            IEnumerable<Egreso> egresos = db.Egresos.Include("Proveedor")
                                                        .Include("Categoria")
                                                        .Include("Moneda")
                                                        .Where(e => e.Activo == true);
            //.Include("Proveedor.Ciudad")
            var counter = egresos.ToList();
                                    

            if (IdCategoria != 0)
            {
                egresos = egresos.Where(x => x.CategoriaId == IdCategoria);
            }
            if (IdProveedor != 0)
            {
                egresos = egresos.Where(x => x.ProveedorId == IdProveedor);
            }
            if (EsCheque != 0)
            {
                var boolCheque = EsCheque == 2 ? true : false;
                egresos = egresos.Where(x => x.EsCheque == boolCheque);
            }

            chequesAndEgresos.AddRange(egresos.Select(x => new EgresoDto(x)).ToList());

            var totalEgresos = egresos.Sum(e => e.Importe);

            IEnumerable<Egreso> egresosCount = egresos;
            int count = egresosCount.Count();
            List<EgresoDto> egresosDto = chequesAndEgresos.OrderByDescending(c => c.Id).Skip((Offset - 1) * Limit).Take(Limit).ToList();
            //List<EgresoDto> egresosDto = egresos.Skip(request.Offset).Take(request.LimitResult).ToList().Select(x => new EgresoDto(x)).ToList();
            return new EgresoResponseDto(count, egresosDto);
        }


        private Egreso Exists(EgresoRequestDto egresoDto)
        {
            return db.Egresos.Where(x => x.Id == egresoDto.Id ).FirstOrDefault();
        }

        private Ciudad ExistsWithDifferentId(CiudadDto ciudadDto)
        {
            return db.Ciudades.Where(x => x.Descripcion == ciudadDto.Descripcion && x.Id != ciudadDto.Id && x.Activo).FirstOrDefault();
        }
    }
}
