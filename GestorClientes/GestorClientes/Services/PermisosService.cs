﻿using GestorClientes.Dominio.DAL;
using GestorClientes.Logic.Dto;
using GestorClientes.Logic.Exceptions.Permisos;
using GestorClientes.Logic.Exceptions.Roles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Services
{
    public class PermisosService : BaseService
    {
        public PermisosService(GestorContext context, IEnumerable<Claim> claims) : base(context, claims) { }

        public List<PermisoGrupoDto> GetAllGroups()
        {
            List<PermisoGrupoDto> list = new List<PermisoGrupoDto>();
            var permisosGrupos = db.PermisosGrupos.ToList();
            foreach(var permisoGrupo in permisosGrupos)
            {
                list.Add(new PermisoGrupoDto(permisoGrupo));
            }
            return list;
        }

        public void Add(int rolId, int permisoId)
        {
            var rol = db.Roles.Where(x => x.Id == rolId).FirstOrDefault();
            if (rol == null) throw new RolNotFoundException(string.Format("El rol con id {0} no existe", rolId));
            if (rol.Permisos.Where(x => x.Id == permisoId).FirstOrDefault() != null) throw new PermisoAlreadyExistsInRolException("El permiso ya se encuentra asignado al rol");
            var permiso = db.Permisos.Where(x => x.Id == permisoId).FirstOrDefault();
            if (permiso == null) throw new PermisoNotFoundException(string.Format("El permiso con id {0} no existe",permisoId));
            rol.Permisos.Add(permiso);
            db.SaveChanges();
        }

        public void Delete(int rolId, int permisoId)
        {
            var rol = db.Roles.Where(x => x.Id == rolId).FirstOrDefault();
            if (rol == null) throw new RolNotFoundException(string.Format("El rol con id {0} no existe", rolId));
            if (rol.Permisos.Where(x => x.Id == permisoId).FirstOrDefault() == null) throw new PermisoNotFoundInRolException("El permiso no se encuentra asignado al rol");
            var permiso = db.Permisos.Where(x => x.Id == permisoId).FirstOrDefault();
            if (permiso == null) throw new PermisoNotFoundException(string.Format("El permiso con id {0} no existe", permisoId));
            rol.Permisos.Remove(permiso);
            db.SaveChanges();
        }
    }
}
