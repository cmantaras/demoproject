﻿using GestorClientes.Dominio.DAL;
using GestorClientes.Dominio.Models;
using GestorClientes.Logic.Dto;
using GestorClientes.Logic.Dto.Requests;
using GestorClientes.Logic.Dto.Responses;
using GestorClientes.Logic.Exceptions.Impuesto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Services
{
    public class ImpuestoService : BaseService
    {
        public ImpuestoService(GestorContext context, IEnumerable<Claim> claims) : base(context, claims) { }

        public int Add(ImpuestoDto impuestoDto)
        {
            try
            {
                Impuesto impuesto = Exists(impuestoDto);
                if (impuesto == null)
                {
                    impuesto = new Impuesto()
                    {
                        Descripcion = impuestoDto.Descripcion,
                        Porcentaje = impuestoDto.Porcentaje,
                        Activo = true
                    };
                    db.Impuestos.Add(impuesto);
                }
                else if (impuesto.Activo)
                {
                    if (impuesto.Descripcion == impuestoDto.Descripcion)
                        throw new ImpuestoDescripcionAlreadyExistsException(string.Format("El impuesto {0} ya se encuentra creado.", impuesto.Descripcion));
                }
                else
                {
                    impuesto.Activo = true;
                    impuesto.Porcentaje = impuestoDto.Porcentaje;
                    impuesto.Descripcion = impuestoDto.Descripcion;
                }
                db.SaveChanges();
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Alta, impuesto);
                return impuesto.Id;
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }

        public bool Edit(ImpuestoDto impuestoDto)
        {
            try
            {
                Impuesto impuesto = db.Impuestos.Where(x => x.Id == impuestoDto.Id && x.Activo).FirstOrDefault();
                if (impuesto == null) throw new ImpuestoNotFoundException(string.Format("El impuesto con id {0} no existe.", impuestoDto.Id));
                Impuesto exists = ExistsWithDifferentId(impuestoDto);
                if (exists != null)
                {
                    if (exists.Descripcion == impuestoDto.Descripcion)
                        throw new ImpuestoDescripcionAlreadyExistsException(string.Format("El impuesto {0} ya se encuentra registrado.", impuestoDto.Descripcion));
                }
                else
                {
                    impuesto.Descripcion = impuestoDto.Descripcion;
                    impuesto.Porcentaje = impuestoDto.Porcentaje;
                    db.SaveChanges();
                    auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Modificacion, impuesto);
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }

        public bool Delete(ImpuestoDto impuestoDto)
        {
            try
            {
                Impuesto impuesto = db.Impuestos.Where(x => x.Id == impuestoDto.Id && x.Activo).FirstOrDefault();
                if (impuesto == null)
                    throw new ImpuestoNotFoundException(string.Format("El impuesto con id {0} no existe.", impuestoDto.Id));
                impuesto.Activo = false;
                db.SaveChanges();
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Baja, impuesto);
                return true;
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }

        public ImpuestoResponseDto GetAll(ImpuestoRequestDto request)
        {
            IEnumerable<Impuesto> impuestos = db.Impuestos.Where(x => x.Activo);
            if (!string.IsNullOrEmpty(request.Descripcion))
            {
                impuestos = impuestos.Where(x => x.Descripcion.Contains(request.Descripcion));
            }


            IEnumerable<Impuesto> impuestoCount = impuestos;
            int count = impuestoCount.Count();
            List<ImpuestoDto> impuestosDto = impuestos.OrderByDescending(c => c.Id).Skip(request.Offset).Take(request.LimitResult).ToList().Select(x => new ImpuestoDto(x)).ToList();
            return new ImpuestoResponseDto(count, impuestosDto);
        }



        private Impuesto Exists(ImpuestoDto impuestoDto)
        {
            return db.Impuestos.Where(x => x.Id == impuestoDto.Id || x.Descripcion == impuestoDto.Descripcion).FirstOrDefault();
        }

        private Impuesto ExistsWithDifferentId(ImpuestoDto impuestoDto)
        {
            return db.Impuestos.Where(x => x.Descripcion == impuestoDto.Descripcion && x.Id != impuestoDto.Id && x.Activo).FirstOrDefault();
        }
    }
}
