﻿
using GestorClientes.Dominio.DAL;
using GestorClientes.Dominio.Models;
using GestorClientes.Logic.Dto;
using GestorClientes.Logic.Dto.Requests;
using GestorClientes.Logic.Dto.Responses;
using GestorClientes.Logic.Exceptions.Categoria;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Services
{
    public class CategoriaService : BaseService
    {
        public CategoriaService(GestorContext context, IEnumerable<Claim> claims) : base(context, claims) { }

        public int Add(CategoriaDto proveedorCategoriaDto)
        {
            try
            {
                Categoria proveedorCategoria = Exists(proveedorCategoriaDto);
                if (proveedorCategoria == null)
                {
                    proveedorCategoria = new Categoria()
                    {
                        Descripcion = proveedorCategoriaDto.Descripcion,
                        Activo = true
                    };
                    db.Categorias.Add(proveedorCategoria);
                }
                else if (proveedorCategoria.Activo)
                {
                    if (proveedorCategoria.Descripcion == proveedorCategoriaDto.Descripcion)
                        
                        throw new CategoriaAlreadyExistsException(string.Format("La categoria {0} ya se encuentra creada.", proveedorCategoria.Descripcion));
                }
                else
                {
                    proveedorCategoria.Activo = true;
                }
                db.SaveChanges();

                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Alta, proveedorCategoria);
                return proveedorCategoria.Id;
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }

        public bool Edit(CategoriaDto proveedorCategoriaDto)
        {
            try
            {
                Categoria proveedorCategoria = db.Categorias.Where(x => x.Id == proveedorCategoriaDto.Id && x.Activo).FirstOrDefault();
                if (proveedorCategoria == null) throw new CategoriaNotFoundException(string.Format("La categoria con id {0} no existe.", proveedorCategoriaDto.Id));
                Categoria exists = ExistsWithDifferentId(proveedorCategoriaDto);
                if (exists != null)
                {
                    if (exists.Descripcion == proveedorCategoriaDto.Descripcion)
                        throw new CategoriaAlreadyExistsException(string.Format("La categoria {0} ya se encuentra registrada.", proveedorCategoriaDto.Descripcion));
                }
                else
                {
                    proveedorCategoria.Descripcion = proveedorCategoriaDto.Descripcion;
                    db.SaveChanges();
                    auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Modificacion, proveedorCategoria);
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }

        public bool Delete(CategoriaDto proveedorCategoriaDto)
        {
            try
            {
                Categoria proveedorCategoria = db.Categorias.Where(x => x.Id == proveedorCategoriaDto.Id && x.Activo).FirstOrDefault();
                if (proveedorCategoria == null)
                    throw new CategoriaNotFoundException(string.Format("La categoria con id {0} no existe.", proveedorCategoriaDto.Id));
                proveedorCategoria.Activo = false;
                db.SaveChanges();
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Baja, proveedorCategoria);
                return true;
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }

        public object GetAllCategorias()
        {
            IEnumerable<Categoria> proveedorCategorias = db.Categorias;
            IEnumerable<Categoria> proveedorCategoriaCount = proveedorCategorias;
            int count = proveedorCategoriaCount.Count();
            List<CategoriaDto> proveedorCategoriasDto = proveedorCategorias.OrderByDescending(c => c.Id).ToList().Select(x => new CategoriaDto(x)).ToList();
            return new CategoriaResponseDto(count, proveedorCategoriasDto);
        }

        public CategoriaResponseDto GetAll(CategoriaRequestDto request)
        {
            IEnumerable<Categoria> proveedorCategorias = db.Categorias.Where(x => x.Activo);
            var list = proveedorCategorias.ToList();
            Console.WriteLine(list);
            if (!string.IsNullOrEmpty(request.Descripcion))
            {
                proveedorCategorias = proveedorCategorias.Where(x => x.Descripcion.Contains(request.Descripcion));
            }


            IEnumerable<Categoria> proveedorCategoriaCount = proveedorCategorias;
            int count = proveedorCategoriaCount.Count();
            List<CategoriaDto> proveedorCategoriasDto = proveedorCategorias.OrderByDescending(c => c.Id).Skip(request.Offset).Take(request.LimitResult).ToList().Select(x => new CategoriaDto(x)).ToList();

            return new CategoriaResponseDto(count, proveedorCategoriasDto);
        }



        private Categoria Exists(CategoriaDto proveedorCategoriaDto)
        {
            return db.Categorias.Where(x => x.Id == proveedorCategoriaDto.Id || x.Descripcion == proveedorCategoriaDto.Descripcion).FirstOrDefault();
        }

        private Categoria ExistsWithDifferentId(CategoriaDto proveedorCategoriaDto)
        {
            return db.Categorias.Where(x => x.Descripcion == proveedorCategoriaDto.Descripcion && x.Id != proveedorCategoriaDto.Id && x.Activo).FirstOrDefault();
        }
    }
}
