﻿using GestorClientes.Dominio.DAL;
using GestorClientes.Dominio.Models;
using GestorClientes.Logic.Dto;
using GestorClientes.Logic.Dto.Requests;
using GestorClientes.Logic.Dto.Responses;
using GestorClientes.Logic.Exceptions.Zona;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Services
{
    public class ZonaService : BaseService
    {
        public ZonaService(GestorContext context, IEnumerable<Claim> claims) : base(context, claims) { }

        public int Add(ZonaDto zonaDto)
        {
            try
            {
                Zona zona = Exists(zonaDto);
                if (zona == null)
                {
                    zona = new Zona()
                    {
                        Descripcion = zonaDto.Descripcion,
                        Codigo = zonaDto.Codigo,
                        Activo = true
                    };
                    db.Zonas.Add(zona);
                }
                else if (zona.Activo)
                {
                    if (zona.Descripcion == zonaDto.Descripcion)
                        throw new ZonaAlreadyExistsException(string.Format("La zona {0} ya se encuentra creada.", zona.Descripcion));
                    if (zona.Codigo == zonaDto.Codigo)
                        throw new ZonaAlreadyExistsException(string.Format("La zona {0} ya se encuentra creada.", zona.Codigo));
                }
                else
                {
                    zona.Activo = true;
                }
                db.SaveChanges();
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Alta, zona);
                return zona.Id;
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }

        public bool Edit(ZonaDto zonaDto)
        {
            try
            {
                Zona zona = db.Zonas.Where(x => x.Id == zonaDto.Id && x.Activo).FirstOrDefault();
                if (zona == null) throw new ZonaNotFoundException(string.Format("La zona con id {0} no existe.", zonaDto.Id));
                Zona exists = ExistsWithDifferentId(zonaDto);
                if (exists != null)
                {
                    if (exists.Descripcion == zonaDto.Descripcion)
                        throw new ZonaAlreadyExistsException(string.Format("La zona {0} ya se encuentra registrada.", zonaDto.Descripcion));
                    if (exists.Codigo == zonaDto.Codigo)
                        throw new ZonaAlreadyExistsException(string.Format("La zona {0} ya se encuentra creada.", zona.Codigo));
                }
                else
                {
                    zona.Descripcion = zonaDto.Descripcion;
                    zona.Codigo = zonaDto.Codigo;
                    db.SaveChanges();
                    auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Modificacion, zona);
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }

        public bool Delete(ZonaDto zonaDto)
        {
            try
            {
                Zona zona = db.Zonas.Where(x => x.Id == zonaDto.Id && x.Activo).FirstOrDefault();
                if (zona == null)
                    throw new ZonaNotFoundException(string.Format("La zona con id {0} no existe.", zonaDto.Id));
                zona.Activo = false;
                db.SaveChanges();
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Baja, zona);
                return true;
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }

        public ZonaResponseDto GetAll(ZonaRequestDto request)
        {
            IEnumerable<Zona> zonas = db.Zonas.Where(x => x.Activo);
            if (!string.IsNullOrEmpty(request.Descripcion))
            {
                zonas = zonas.Where(x => x.Descripcion.Contains(request.Descripcion));
            }
            if (!string.IsNullOrEmpty(request.Codigo))
            {
                zonas = zonas.Where(x => x.Descripcion.Contains(request.Codigo));
            }


            IEnumerable<Zona> zonaCount = zonas;
            int count = zonaCount.Count();
            List<ZonaDto> zonasDto = zonas.Skip((request.Offset - 1) * request.Limit).Take(request.LimitResult).ToList().Select(x => new ZonaDto(x)).ToList();
            return new ZonaResponseDto(count, zonasDto);
        }



        private Zona Exists(ZonaDto zonaDto)
        {
            return db.Zonas.Where(x => x.Id == zonaDto.Id || x.Descripcion == zonaDto.Descripcion || x.Codigo == zonaDto.Codigo).FirstOrDefault();
        }

        private Zona ExistsWithDifferentId(ZonaDto zonaDto)
        {
            return db.Zonas.Where(x => (x.Descripcion == zonaDto.Descripcion || x.Codigo == zonaDto.Codigo) && x.Id != zonaDto.Id && x.Activo).FirstOrDefault();
        }
    }
}
