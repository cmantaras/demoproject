﻿
using GestorClientes.Dominio.DAL;
using GestorClientes.Dominio.Models;
using GestorClientes.Logic.Dto;
using GestorClientes.Logic.Dto.Requests;
using GestorClientes.Logic.Dto.Responses;
using GestorClientes.Logic.Exceptions.Marca;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Services
{
    public class MarcaService : BaseService
    {
        public MarcaService(GestorContext context, IEnumerable<Claim> claims) : base(context, claims) { }

        public int Add(MarcaDto marcaDto)
        {
            try
            {
                Marca marca = Exists(marcaDto);
                if (marca == null)
                {
                    marca = new Marca()
                    {
                        Descripcion = marcaDto.Descripcion,
                        Activo = true
                    };
                    db.Marcas.Add(marca);
                }
                else if (marca.Activo)
                {
                    if (marca.Descripcion == marcaDto.Descripcion)
                        throw new MarcaDescripcionAlreadyExistsException(string.Format("La marca {0} ya se encuentra creada.", marca.Descripcion));
                }
                else
                {
                    marca.Activo = true;
                }
                db.SaveChanges();
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Alta, marca);
                return marca.Id;
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }

        public bool Edit(MarcaDto marcaDto)
        {
            try
            {
                Marca marca = db.Marcas.Where(x => x.Id == marcaDto.Id && x.Activo).FirstOrDefault();
                if (marca == null) throw new MarcaNotFoundException(string.Format("La marca con id {0} no existe.", marcaDto.Id));
                Marca exists = ExistsWithDifferentId(marcaDto);
                if (exists != null)
                {
                    if (exists.Descripcion == marcaDto.Descripcion)
                        throw new MarcaDescripcionAlreadyExistsException(string.Format("La marca {0} ya se encuentra registrado.", marcaDto.Descripcion));
                }
                else
                {
                    marca.Descripcion = marcaDto.Descripcion;
                    db.SaveChanges();
                    auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Modificacion, marca);
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }

        public bool Delete(MarcaDto marcaDto)
        {
            try
            {
                Marca marca = db.Marcas.Where(x => x.Id == marcaDto.Id && x.Activo).FirstOrDefault();
                if (marca == null)
                    throw new MarcaNotFoundException(string.Format("La marca con id {0} no existe.", marcaDto.Id));
                marca.Activo = false;
                db.SaveChanges();
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Baja, marca);
                return true;
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }

        public MarcaResponseDto GetAll(MarcaRequestDto request)
        {
            IEnumerable<Marca> marcaes = db.Marcas.Where(x => x.Activo);
            if (!string.IsNullOrEmpty(request.Descripcion))
            {
                marcaes = marcaes.Where(x => x.Descripcion.Contains(request.Descripcion));
            }


            IEnumerable<Marca> marcaCount = marcaes;
            int count = marcaCount.Count();
            List<MarcaDto> marcaesDto = marcaes.OrderByDescending(c => c.Id).Skip(request.Offset).Take(request.LimitResult).ToList().Select(x => new MarcaDto(x)).ToList();
            return new MarcaResponseDto(count, marcaesDto);
        }



        private Marca Exists(MarcaDto marcaDto)
        {
            return db.Marcas.Where(x => x.Id == marcaDto.Id || x.Descripcion == marcaDto.Descripcion).FirstOrDefault();
        }

        private Marca ExistsWithDifferentId(MarcaDto marcaDto)
        {
            return db.Marcas.Where(x => x.Descripcion == marcaDto.Descripcion && x.Id != marcaDto.Id && x.Activo).FirstOrDefault();
        }
    }
}
