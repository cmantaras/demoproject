﻿
using GestorClientes.Dominio.DAL;
using GestorClientes.Dominio.Models;
using GestorClientes.Logic.Dto;
using GestorClientes.Logic.Dto.Requests;
using GestorClientes.Logic.Dto.Responses;
using GestorClientes.Logic.Exceptions.Ciudad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestorClientes.Logic.Services
{
    public class CiudadService : BaseService
    {
        public CiudadService(GestorContext context, IEnumerable<Claim> claims) : base(context, claims) { }

        public int Add(CiudadDto ciudadDto)
        {
            try
            {
                Ciudad ciudad = Exists(ciudadDto);
                if (ciudad == null)
                {
                    ciudad = new Ciudad()
                    {
                        Descripcion = ciudadDto.Descripcion,
                        Activo = true
                    };
                    db.Ciudades.Add(ciudad);
                }
                else if (ciudad.Activo)
                {
                    if (ciudad.Descripcion == ciudadDto.Descripcion)
                        throw new CiudadDescripcionAlreadyExistsException(string.Format("La ciudad {0} ya se encuentra creada.", ciudad.Descripcion));
                }
                else
                {
                    ciudad.Activo = true;
                }
                db.SaveChanges();
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Alta, ciudad);
                return ciudad.Id;
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }

        public bool Edit(CiudadDto ciudadDto)
        {
            try
            {
                Ciudad ciudad = db.Ciudades.Where(x => x.Id == ciudadDto.Id && x.Activo).FirstOrDefault();
                if (ciudad == null) throw new CiudadNotFoundException(string.Format("La ciudad con id {0} no existe.", ciudadDto.Id));
                Ciudad exists = ExistsWithDifferentId(ciudadDto);
                if (exists != null)
                {
                    if (exists.Descripcion == ciudadDto.Descripcion)
                        throw new CiudadDescripcionAlreadyExistsException(string.Format("La ciudad {0} ya se encuentra registrado.", ciudadDto.Descripcion));
                }
                else
                {
                    ciudad.Descripcion = ciudadDto.Descripcion;
                    db.SaveChanges();
                    auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Modificacion, ciudad);
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }

        public bool Delete(CiudadDto ciudadDto)
        {
            try
            {
                Ciudad ciudad = db.Ciudades.Where(x => x.Id == ciudadDto.Id && x.Activo).FirstOrDefault();
                if (ciudad == null)
                    throw new CiudadNotFoundException(string.Format("La ciudad con id {0} no existe.", ciudadDto.Id));
                ciudad.Activo = false;
                db.SaveChanges();
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Baja, ciudad);
                return true;
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }

        public CiudadResponseDto GetAll(CiudadRequestDto request)
        {
            IEnumerable<Ciudad> ciudades = db.Ciudades.Where(x => x.Activo);
            if (!string.IsNullOrEmpty(request.Descripcion))
            {
                ciudades = ciudades.Where(x => x.Descripcion.Contains(request.Descripcion));
            }
           

            IEnumerable<Ciudad> ciudadCount = ciudades;
            int count = ciudadCount.Count();
            List<CiudadDto> ciudadesDto = ciudades.OrderByDescending(c => c.Id).ToList().Select(x => new CiudadDto(x)).ToList();
            
            return new CiudadResponseDto(count, ciudadesDto);
        }



        public Ciudad Exists(CiudadDto ciudadDto)
        {
            return db.Ciudades.Where(x => x.Id == ciudadDto.Id || x.Descripcion == ciudadDto.Descripcion).FirstOrDefault();
        }

        private Ciudad ExistsWithDifferentId(CiudadDto ciudadDto)
        {
            return db.Ciudades.Where(x => x.Descripcion == ciudadDto.Descripcion && x.Id != ciudadDto.Id && x.Activo).FirstOrDefault();
        }
    }
}
