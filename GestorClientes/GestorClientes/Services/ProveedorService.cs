﻿
using GestorClientes.Dominio.DAL;
using GestorClientes.Dominio.Models;
using GestorClientes.Logic.Dto;
using GestorClientes.Logic.Dto.Requests;
using GestorClientes.Logic.Dto.Responses;
using GestorClientes.Logic.Exceptions.Proveedor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace GestorClientes.Logic.Services
{
    public class ProveedorService : BaseService
    {
        private CiudadService ciudadService;
        public ProveedorService(GestorContext context, IEnumerable<Claim> claims) : base(context, claims) {
            ciudadService = new CiudadService(context, claims);
        }

        public int Add(ProveedorDto proveedorDto)
        {
            try
            {


                /*Ciudad ciudad = db.Ciudades.Where(x => x.Descripcion.ToLower() == proveedorDto.CiudadDescripcion.ToLower()).FirstOrDefault();
                if (ciudad == null)
                {
                    ciudad = new Ciudad()
                    {
                        Descripcion = proveedorDto.CiudadDescripcion,
                        Activo = true
                    };
                    db.Ciudades.Add(ciudad);
                }
               
                db.SaveChanges();*/
                Proveedor proveedor = Exists(proveedorDto);
                if (proveedor == null)
                {
                    proveedor = new Proveedor()
                    {
                        RUT = proveedorDto.RUT,
                        RazonSocial = proveedorDto.RazonSocial,
                        Direccion = proveedorDto.Direccion,
                        Telefono = proveedorDto.Telefono,
                        Correo = proveedorDto.Correo,
                        Nombre = proveedorDto.Nombre,
                        Contacto = proveedorDto.Contacto,
                        ContactoCorreo = proveedorDto.ContactoCorreo,
                        ContactoTelefono = proveedorDto.ContactoTelefono,
                        Ciudad = proveedorDto.Ciudad,
                        //CiudadId = ciudad.Id,                    
                        Activo = true
                    };
                    db.Proveedores.Add(proveedor);

                }
                else if (proveedor.Activo)
                {
                    if (proveedor.RUT == proveedorDto.RUT && proveedor.RUT != null && proveedor.RUT != "")
                        throw new ProveedorAlreadyExistsException(string.Format("El proveedor {0} ya se encuentra creado.", proveedor.RUT));
                    if (proveedor.Nombre == proveedorDto.Nombre)
                        throw new ProveedorAlreadyExistsException(string.Format("El proveedor {0} ya se encuentra creado.", proveedor.Nombre));
                    if (proveedor.RazonSocial == proveedorDto.RazonSocial && proveedor.RazonSocial != null && proveedor.RazonSocial != "")
                        throw new ProveedorAlreadyExistsException(string.Format("El proveedor {0} ya se encuentra creado.", proveedor.RazonSocial));
                }
                else
                {
                    proveedor.Activo = true;
                    proveedor.RUT = proveedorDto.RUT;
                    proveedor.RazonSocial = proveedorDto.RazonSocial;
                    proveedor.Direccion = proveedorDto.Direccion;
                    proveedor.Telefono = proveedorDto.Telefono;
                    proveedor.Correo = proveedorDto.Correo;
                    proveedor.Nombre = proveedorDto.Nombre;
                    proveedor.Contacto = proveedorDto.Contacto;
                    proveedor.ContactoTelefono = proveedorDto.ContactoTelefono;
                    proveedor.ContactoCorreo = proveedorDto.ContactoCorreo;
                    proveedor.Ciudad = proveedorDto.Ciudad;
                    //proveedor.CiudadId = ciudad.Id;      
                    //proveedor.Ciudad = db.Ciudades.Where(x => x.Id == proveedor.CiudadId).FirstOrDefault();
                }
                db.SaveChanges();
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Alta, proveedor);
                return proveedor.Id;
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }

        public List<CuentaTipoDto> GetTiposCuentas()
        {
            try
            {
                List<CuentaTipo> cuentaTipos = db.CuentaTipo.ToList();
                var result = new List<CuentaTipoDto>();
                foreach (CuentaTipo c in cuentaTipos) {
                    result.Add( new CuentaTipoDto(c));
                }
                return result;

            }
            catch (Exception e)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, e);
                throw e;
            }
        }

        public bool UpdateProveedorBanco(ProveedorBancoDto proveedorBancoDto)
        {
            try
            {
                ProveedorBanco proveedorBanco = ExistsBanco(proveedorBancoDto);
                if (proveedorBanco == null)
                {
                    proveedorBanco = new ProveedorBanco()
                    {
                        Nombre = proveedorBancoDto.Nombre,
                        ProveedorId = proveedorBancoDto.ProveedorId,
                        Cuenta = proveedorBancoDto.Cuenta,
                        CuentaTipoId = proveedorBancoDto.CuentaTipoId,
                        MonedaId = proveedorBancoDto.MonedaId
                    };
                    db.ProveedorBanco.Add(proveedorBanco);
                }
                else
                {
                    proveedorBanco.Nombre = proveedorBancoDto.Nombre;
                    proveedorBanco.ProveedorId = proveedorBancoDto.ProveedorId;
                    proveedorBanco.Cuenta = proveedorBancoDto.Cuenta;
                    proveedorBanco.CuentaTipoId = proveedorBancoDto.CuentaTipoId;
                    proveedorBanco.MonedaId = proveedorBancoDto.MonedaId;
                }
                db.SaveChanges();
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Alta, proveedorBanco);
                return true;
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }


        public ProveedorBancoDto GetInfoBancariaProveedor(int id)
        {
            try {
                ProveedorBanco proveedorBanco = db.ProveedorBanco.Where(x => x.ProveedorId == id).FirstOrDefault();
                if (proveedorBanco != null) { 
                    var result = new ProveedorBancoDto(proveedorBanco); 
                    return result; } 
               
                else {
                    var proveedor = new ProveedorBanco()
                    {
                        Nombre = "",
                        Cuenta = "",
                    };


                    var result = new ProveedorBancoDto(proveedor);
                    return result;
                }
                

            } catch (Exception e)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, e);
                throw e;
            }
            
        }

       



        public bool Edit(ProveedorDto proveedorDto)
        {
            try
            {
                /*Ciudad ciudad = db.Ciudades.Where(x => x.Descripcion.ToLower() == proveedorDto.CiudadDescripcion.ToLower()).FirstOrDefault();
                if (ciudad == null)
                {
                    ciudad = new Ciudad()
                    {
                        Descripcion = proveedorDto.CiudadDescripcion,
                        Activo = true
                    };
                    db.Ciudades.Add(ciudad);
                }
                db.SaveChanges();*/
                Proveedor proveedor = db.Proveedores.Where(x => x.Id == proveedorDto.Id && x.Activo).FirstOrDefault();
                if (proveedor == null) throw new ProveedorNotFoundException(string.Format("La proveedor con id {0} no existe.", proveedorDto.Id));
                Proveedor exists = ExistsWithDifferentId(proveedorDto);
                if (exists != null)
                {
                    if (proveedor.RUT == proveedorDto.RUT)
                        throw new ProveedorAlreadyExistsException(string.Format("La proveedor {0} ya se encuentra creado.", proveedor.RUT));
                    if (proveedor.RazonSocial == proveedorDto.RazonSocial)
                        throw new ProveedorAlreadyExistsException(string.Format("La proveedor {0} ya se encuentra creado.", proveedor.RazonSocial));
                }
                else
                {
                    proveedor.RUT = proveedorDto.RUT;
                    proveedor.Nombre = proveedorDto.Nombre;
                    proveedor.RazonSocial = proveedorDto.RazonSocial;
                    proveedor.Direccion = proveedorDto.Direccion;
                    proveedor.Telefono = proveedorDto.Telefono;
                    proveedor.Correo = proveedorDto.Correo;
                    proveedor.Contacto = proveedorDto.Contacto;
                    proveedor.ContactoCorreo = proveedorDto.ContactoCorreo;
                    proveedor.ContactoTelefono = proveedorDto.ContactoTelefono;
                    proveedor.Ciudad = proveedorDto.Ciudad;
                    //proveedor.CiudadId = ciudad.Id;

                    db.SaveChanges();
                    auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Modificacion, proveedor);
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }

        public bool Delete(ProveedorDto proveedorDto)
        {
            try
            {
                Proveedor proveedor = db.Proveedores.Where(x => x.Id == proveedorDto.Id && x.Activo).FirstOrDefault();
                if (proveedor == null)
                    throw new ProveedorNotFoundException(string.Format("La proveedor con id {0} no existe.", proveedorDto.Id));
                proveedor.Activo = false;
                db.SaveChanges();
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Baja, proveedor);
                return true;
            }
            catch (Exception ex)
            {
                auditoria.Add(Enum.EnumUtils.AuditoriaAccion.Error, ex);
                throw ex;
            }
        }

        public ProveedorResponseDto GetAll(ProveedorRequestDto request)
   {
            IEnumerable<Proveedor> proveedores = db.Proveedores.Where(x => x.Activo);
            //.Include(y => y.Ciudad)

            if (request.Todos)
            {
                IEnumerable<Proveedor> proveedoresActivosInactivos = db.Proveedores;
                //.Include(y => y.Ciudad)
                IEnumerable<Proveedor> proveedorCount0 = proveedoresActivosInactivos;
                int count0 = proveedorCount0.Count();
                List<ProveedorDto> proveedoresDto0 = proveedoresActivosInactivos.Skip((request.Offset - 1) * request.Limit).Take(request.LimitResult).ToList().Select(x => new ProveedorDto(x)).OrderByDescending(p => p.Id).ToList();
                return new ProveedorResponseDto(count0, proveedoresDto0);
            }

            if (!string.IsNullOrEmpty(request.RUT))
            {
                proveedores = proveedores.Where(x => x.RUT.Contains(request.RUT));
            }
            if (!string.IsNullOrEmpty(request.Nombre))
            {
                proveedores = proveedores.Where(x => x.Nombre != null && x.Nombre.Trim().ToLower().Contains(request.Nombre.Trim().ToLower()));
            }
            if (!string.IsNullOrEmpty(request.RazonSocial))
            {
                proveedores = proveedores.Where(x => x.RazonSocial.Contains(request.RazonSocial));
            }
            if (!string.IsNullOrEmpty(request.Direccion))
            {
                proveedores = proveedores.Where(x => x.Direccion.Contains(request.Direccion));
            }
            if (!string.IsNullOrEmpty(request.Telefono))
            {
                proveedores = proveedores.Where(x => x.Telefono.Contains(request.Telefono));
            }
            if (!string.IsNullOrEmpty(request.Correo))
            {
                proveedores = proveedores.Where(x => x.Correo.Contains(request.Correo));
            }
            if (!string.IsNullOrEmpty(request.Contacto))
            {
                proveedores = proveedores.Where(x => x.Contacto.Contains(request.Contacto));
            }
            if (!string.IsNullOrEmpty(request.ContactoCorreo))
            {
                proveedores = proveedores.Where(x => x.ContactoCorreo.Contains(request.ContactoCorreo));
            }
            if (!string.IsNullOrEmpty(request.ContactoTelefono))
            {
                proveedores = proveedores.Where(x => x.ContactoTelefono.Contains(request.ContactoTelefono));
            }
            if (!string.IsNullOrEmpty(request.Ciudad))
            {
                proveedores = proveedores.Where(x => x.Ciudad.Contains(request.Ciudad));
            }
            /*if (!string.IsNullOrEmpty(request.Ciudad))
            {
                proveedores = proveedores.Where(x => x.Ciudad.Descripcion.Contains(request.Ciudad));
            }*/




            IEnumerable<Proveedor> proveedorCount = proveedores;
            int count = proveedorCount.Count();
            List<ProveedorDto> proveedoresDto = proveedores.OrderByDescending(c => c.Id).Skip((request.Offset - 1) * request.Limit).Take(request.LimitResult).ToList().Select(x => new ProveedorDto(x)).OrderByDescending(p => p.Id).ToList();
            return new ProveedorResponseDto(count, proveedoresDto);
        }



        private Proveedor Exists(ProveedorDto proveedorDto)
        {
            return db.Proveedores.Where(x => x.Id == proveedorDto.Id || x.Nombre == proveedorDto.Nombre ).FirstOrDefault();
        }
        private ProveedorBanco ExistsBanco(ProveedorBancoDto proveedorBancoDto)
        {
            return db.ProveedorBanco.Where(x => x.Id == proveedorBancoDto.Id || x.Nombre == proveedorBancoDto.Nombre).FirstOrDefault();
        }

        private Proveedor ExistsWithDifferentId(ProveedorDto proveedorDto)
        {
            return db.Proveedores.Where(x => x.Nombre == proveedorDto.Nombre  && x.Id != proveedorDto.Id && x.Activo).FirstOrDefault();
        }
    }
}
