﻿using GestorClientes.Dominio.DAL;
using GestorClientes.Dominio.Models;
using GestorClientes.Logic.Dto.Requests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Services
{
    public class CierreMesService : BaseService
    {
        private ClienteEstadoService clienteEstadoService;
        private ProductoService productoService;

        public CierreMesService(GestorContext context, IEnumerable<Claim> claims) : base(context, claims)
        {
            clienteEstadoService = new ClienteEstadoService(context, claims);
            productoService = new ProductoService(context, claims);
        }

        public Boolean FinalizarMes(FinalizarMesRequestDto request)
        {
            try
            {

                //Aplicar mora a las cuotas impagas 10% primer mes, 20% segundo mes, inhabilitar cliente 3er mes
                var hoy = request.Fecha;
                var mesFin = new CierreMes { Fecha = request.Fecha };
                db.Cierres.Add(mesFin);

                db.SaveChanges();

                var fechasFin = db.Cierres.OrderByDescending(f => f.Fecha).ToList();

                var cierreHoy = fechasFin[0];
                var fechaLastMonth = fechasFin[1];
                var fechaLast2months = fechasFin[2];
                var fechaLast3months = fechasFin[3];

                var mora3erMes = db.MorasCuota.Where(m => m.Porcentaje == 1).FirstOrDefault();
                var mora1erMes = db.MorasCuota.Where(m => m.Porcentaje == 10).FirstOrDefault();
                var mora2doMes = db.MorasCuota.Where(m => m.Porcentaje == 20).FirstOrDefault();


                AgregarCuotasServicio(cierreHoy);

                //Obtener todos los productos comprados antes del cierre de mes anterior que no esten deshabilitados y
                //que tengan cuotas pendientes de pago
                IEnumerable<ClienteProducto> productos = db.ClienteProductos.Include(a => a.Cuotas)
                                            .Where(d => d.Desde <= fechaLastMonth.Fecha && d.Cuotas.Any(c => c.FechaPago == null)).ToList();



                foreach (var prod in productos)
                {
                    //Si el cliente ya es deudor no se hace nada
                    if (prod.Cliente.EstadoId != (int)Enum.EnumUtils.EstadoCliente.Deudor)
                    {
                       
                        //Iterar por cuotas 1-2-3-4-...
                        foreach (var cu in prod.Cuotas.OrderBy(c => c.Cuota))
                        {
                            var esServicio = cu.ClienteProducto.ProductoPrecio.Producto.Servicio;
                            var yearCuota = prod.Desde.AddMonths(cu.Cuota).Year;

                            //Se agrega la cantidad de cuotas a la fecha de compra
                            var dateCuotaSameyear = prod.Desde.AddMonths(cu.Cuota);

                            var dateCuotaDiffYear = prod.Desde.Month + cu.Cuota;

                            if (cu.Cierre == null
                                && ((dateCuotaSameyear.Month <= cierreHoy.Fecha.Month && yearCuota == cierreHoy.Fecha.Year) 
                                || (yearCuota != cierreHoy.Fecha.Year && dateCuotaDiffYear <= cierreHoy.Fecha.Month + 12)) )
                            {
                                
                                //Si es null, es la cuota actual => asignar cierre
                                cu.CierreMesId = cierreHoy.Id;

                                //Esta Paga?
                                if (cu.FechaPago == null && cu.Importe !=0)
                                {
                                    //Si no pago ver 1 mese atras 
                                    var cuota2meses = prod.Cuotas.Where(c => c.Cuota == cu.Cuota - 1).FirstOrDefault();

                                    //Tampoco pago el anterior aplicar las dos moras
                                    if (cuota2meses != null && cuota2meses.FechaPago == null && cuota2meses.Importe !=0)
                                    {

                                        var cuotaDeuda20 = prod.Cuotas.Where(c => c.MoraCuota != null && c.MoraCuota.Porcentaje == mora2doMes.Porcentaje).FirstOrDefault();
                                        //Si ya existe mora de 20% no se aplican mas moras y se procede al 3 er mes impago quedando como deudor
                                        if(cuotaDeuda20 == null)
                                        {
                                            cuota2meses.MoraCuotaId = mora2doMes.Id;
                                            cu.MoraCuotaId = mora1erMes.Id;
                                        }
                                        var cuota3Meses = prod.Cuotas.Where(c => c.Cuota == cu.Cuota - 2).FirstOrDefault();
                                        //Ver cuota dos meses atras
                                        if (cuota3Meses != null && cuota3Meses.FechaPago == null)
                                        {
                                            //Inahbilitar cliente 
                                            //Y seguimos con proximo producto
                                            //Se agrega estado cliente
                                            prod.Cliente.EstadoId = (int)Enum.EnumUtils.EstadoCliente.Deudor;
                                            prod.Cliente.FechaHasta = DateTime.Now;
                                            clienteEstadoService.Add(prod.Cliente, (int)Enum.EnumUtils.EstadoCliente.Deudor);
                                            break;
                                        }
                                        break;
                                    }
                                    if (!(prod.Cuotas.Count() == 1 && prod.ProductoPrecio.Producto.Servicio))
                                        cu.MoraCuotaId = mora1erMes.Id;
                                    break;
                                }
                                else
                                {
                                    break;
                                }
                            }
                            else
                            {
                                //Si la proxima cuota tiene fecha de cierre, es que la mora fue actualizada en el mes anterior
                                //Pasar a proxima cuota
                                continue;
                            }
                        }
                    }
                }

                db.SaveChanges();

                return true;
            }
            catch (Exception e)
            {

                throw new Exception(e.InnerException.Message);
            }
        }

        public void AgregarCuotasServicio(CierreMes cierreHoy)
        {
            IEnumerable<ClienteProducto> productosServicios = db.ClienteProductos.Include(d => d.Cuotas).Include(p => p.ProductoPrecio.Producto)
                        .Where(d => d.ProductoPrecio.Producto.Servicio == true && d.Activo == true).ToList();
                                        
            //If producto es un servicio agregar cuota
            foreach (var prod in productosServicios)
            {
                var ultimaCuota = prod.Cuotas.OrderByDescending(c => c.Cuota).FirstOrDefault();
                if (prod.Cliente.EstadoId != (int)Enum.EnumUtils.EstadoCliente.Deudor) {
                    if (ultimaCuota != null)
                    {

                        prod.Cuotas.Add(
                           new ClienteProductoDeuda
                           {
                               ClienteProductoId = prod.Id,
                               Cuota = ultimaCuota.Cuota + 1,
                               Importe = prod.ProductoPrecio.Precio,
                               Activo = true
                           });
                    }
                    //Si es el primer mes se hace prorateo desde que se compro el servicio
                    else
                    {
                        //si es un cliente viejo se le cobrara la mensualidad entera sino, prorrateo
                        if (!prod.Cliente.ClienteViejo) 
                        {
                            var diasServicio = cierreHoy.Fecha - prod.Desde;
                            int days = DateTime.DaysInMonth(prod.Desde.Year, prod.Desde.Month);
                            var importe = prod.ProductoPrecio.Precio;

                            prod.Cuotas.Add(
                            new ClienteProductoDeuda
                            {
                                ClienteProductoId = prod.Id,
                                Cuota = 1,
                                Importe = diasServicio.Days < 30 ? (prod.ProductoPrecio.Precio / days) * diasServicio.Days : prod.ProductoPrecio.Precio,
                                Activo = true
                            });

                            if (prod.Cuotas.FirstOrDefault().Importe == 0) {
                                prod.Cuotas.FirstOrDefault().FechaPago = DateTime.Now;// para evitar generar una cuota con valor 0 la cual nadie pague y genere moras en las siguientes
                            }

                        }
                        else {
                            prod.Cuotas.Add(
                              new ClienteProductoDeuda
                              {
                                  ClienteProductoId = prod.Id,
                                  Cuota = 1, ///ultima cuota es null, asi que cuota numero 1
                                  Importe = prod.ProductoPrecio.Precio,
                                  Activo = true
                              });
                        }
                       
                    }
                }

            }

            db.SaveChanges();
        }
        public List<DateTime> GetCierresMes()
        {
            return db.Cierres.Select(c => c.Fecha).OrderByDescending(x => x).Take(3).ToList();

        }


    }

}