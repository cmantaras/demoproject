﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Exceptions.EstadoCliente
{
    public class EstadoClienteDescripcionAlreadyExistsException : Exception
    {
        public EstadoClienteDescripcionAlreadyExistsException(string message) : base(message)
        {
        }
    }
}
