﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Exceptions.EstadoCliente
{
    public class EstadoClienteAlreadyExistsException : Exception
    {
        public EstadoClienteAlreadyExistsException(string message) : base(message)
        {
        }
    }
}
