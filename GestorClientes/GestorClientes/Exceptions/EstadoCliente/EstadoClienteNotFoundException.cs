﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Exceptions.EstadoCliente
{
    public class EstadoClienteNotFoundException : Exception
    {
        public EstadoClienteNotFoundException(string message) : base(message)
        {
        }
    }
}
