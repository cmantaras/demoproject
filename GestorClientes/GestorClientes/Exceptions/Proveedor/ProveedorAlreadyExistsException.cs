﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Exceptions.Proveedor
{
    public class ProveedorAlreadyExistsException : Exception
    {
        public ProveedorAlreadyExistsException(string message) : base(message)
        {
        }

    }
}
