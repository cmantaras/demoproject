﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Exceptions.Proveedor
{
    public class ProveedorNotFoundException : Exception
    {
        public ProveedorNotFoundException(string message) : base(message)
        {
        }

    }
}
