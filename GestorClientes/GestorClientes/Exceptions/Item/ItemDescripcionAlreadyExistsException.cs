﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Exceptions.Item
{
    public class ItemDescripcionAlreadyExistsException : Exception
    {
        public ItemDescripcionAlreadyExistsException(string message) : base(message)
        {
        }
    }
}
