﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Exceptions.Producto
{
    public class ProductoAlreadyExistsException : Exception
    {
        public ProductoAlreadyExistsException(string message) : base(message)
        {
        }

    }
}
