﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Exceptions.Producto
{
    public class ProductoNotFoundException : Exception
    {
        public ProductoNotFoundException(string message) : base(message)
        {
        }

    }
}
