﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Exceptions.Permisos
{
    public class PermisoAlreadyExistsInRolException : Exception
    {
        public PermisoAlreadyExistsInRolException(string message) : base(message)
        {
        }
    }
}
