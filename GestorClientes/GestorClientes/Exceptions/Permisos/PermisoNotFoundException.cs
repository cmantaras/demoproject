﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Exceptions.Permisos
{
    public class PermisoNotFoundException : Exception
    {
        public PermisoNotFoundException(string message) : base(message)
        {
        }
    }
}
