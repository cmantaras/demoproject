﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Exceptions.Parametros 
{  
    public class ParametroNotFoundException : Exception
    {
        public ParametroNotFoundException(string message) : base(message)
        {
        }
    }
}
