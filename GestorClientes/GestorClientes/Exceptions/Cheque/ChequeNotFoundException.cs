﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Exceptions.Cheque
{

    public class ChequeNotFoundException : Exception
    {
        public ChequeNotFoundException(string message) : base(message)
        {
        }
    }
}
