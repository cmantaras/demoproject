﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Exceptions.Cheque
{

    public class ChequeAlreadyExistsException : Exception
    {
        public ChequeAlreadyExistsException(string message) : base(message)
        {
        }
    }
}

