﻿using System;
using System.Runtime.Serialization;

namespace GestorClientes.Logic.Exceptions.Pago
{
    [Serializable]
    internal class PagoNotFoundException : Exception
    {
        public PagoNotFoundException()
        {
        }

        public PagoNotFoundException(string message) : base(message)
        {
        }

        public PagoNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected PagoNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}