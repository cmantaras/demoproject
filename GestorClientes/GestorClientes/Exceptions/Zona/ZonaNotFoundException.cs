﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Exceptions.Zona
{
    public class ZonaNotFoundException : Exception
    {
        public ZonaNotFoundException(string message) : base(message)
        {
        }
    }
}
