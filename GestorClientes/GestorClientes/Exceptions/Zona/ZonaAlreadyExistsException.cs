﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Exceptions.Zona
{
    public class ZonaAlreadyExistsException : Exception
    {
        public ZonaAlreadyExistsException(string message) : base(message)
        {
        }
    }
}
