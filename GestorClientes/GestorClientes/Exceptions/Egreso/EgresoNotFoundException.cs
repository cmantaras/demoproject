﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Exceptions.Egreso
{

    public class EgresoNotFoundException : Exception
    {
        public EgresoNotFoundException(string message) : base(message)
        {
        }
    }
}
