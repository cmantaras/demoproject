﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Exceptions.Usuarios
{
   
    public class UsuarioNotDeletableException : Exception
    {
        public UsuarioNotDeletableException(string message) : base(message)
        {
        }
    }
}
