﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Exceptions.Usuarios
{
    
    public class UsuarioNotFoundException : Exception
    {
        public UsuarioNotFoundException(string message) : base(message)
        {
        }
    }
}
