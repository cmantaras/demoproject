﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Exceptions.ProductoPrecio
{
    public class ProductoPrecioNotFoundException : Exception
    {
        public ProductoPrecioNotFoundException(string message) : base(message)
        {
        }

    }
}
