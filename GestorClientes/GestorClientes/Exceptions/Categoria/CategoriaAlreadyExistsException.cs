﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Exceptions.Categoria
{
    public class CategoriaAlreadyExistsException : Exception
    {
        public CategoriaAlreadyExistsException(string message) : base(message)
        {
        }
    }
}
