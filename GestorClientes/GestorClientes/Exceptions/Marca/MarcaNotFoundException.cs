﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Exceptions.Marca
{
    public class MarcaNotFoundException : Exception
    {
        public MarcaNotFoundException(string message) : base(message)
        {
        }
    }
}
