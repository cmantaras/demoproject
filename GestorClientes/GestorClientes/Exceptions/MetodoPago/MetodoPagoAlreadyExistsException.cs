﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Exceptions.MetodoPago
{
    public class MetodoPagoAlreadyExistsException : Exception
    {
        public MetodoPagoAlreadyExistsException(string message) : base(message)
        {
        }

    }
}
