﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Exceptions.Roles
{
    public class RolNotFoundException : Exception
    {
        public RolNotFoundException(string message) : base(message)
        {
        }

    }
}
