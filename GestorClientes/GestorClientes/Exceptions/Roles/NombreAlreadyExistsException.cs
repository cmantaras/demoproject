﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Exceptions.Roles
{
    public class NombreAlreadyExistsException : Exception
    {
        public NombreAlreadyExistsException(string message) : base(message)
        {
        }
    }
}
