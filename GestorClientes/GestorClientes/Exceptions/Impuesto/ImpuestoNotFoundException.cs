﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Exceptions.Impuesto
{
    public class ImpuestoNotFoundException : Exception
    {
        public ImpuestoNotFoundException(string message) : base(message)
        {
        }
    }
}
