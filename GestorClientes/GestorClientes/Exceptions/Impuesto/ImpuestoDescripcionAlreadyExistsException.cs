﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Exceptions.Impuesto
{
    public class ImpuestoDescripcionAlreadyExistsException : Exception
    {
        public ImpuestoDescripcionAlreadyExistsException(string message) : base(message)
        {
        }
    }
}
