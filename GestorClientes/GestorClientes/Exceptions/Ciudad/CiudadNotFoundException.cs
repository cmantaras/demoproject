﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Exceptions.Ciudad
{
   
    public class CiudadNotFoundException : Exception
    {
        public CiudadNotFoundException(string message) : base(message)
        {
        }
    }
}
