﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Exceptions.Moneda
{

    public class MonedaDescripcionAlreadyExistsException : Exception
    {
        public MonedaDescripcionAlreadyExistsException(string message) : base(message)
        {
        }
    }
}

