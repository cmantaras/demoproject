﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Exceptions.Moneda
{

    public class MonedaNotFoundException : Exception
    {
        public MonedaNotFoundException(string message) : base(message)
        {
        }
    }
}
