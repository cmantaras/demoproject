﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Exceptions.Cliente
{
    public class AsignarProductoException : Exception
    {
        public AsignarProductoException(string message) : base(message)
        {
        }
    }
}
