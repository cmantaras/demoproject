﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorClientes.Logic.Exceptions.Cliente
{
    public class ClienteNotFoundException : Exception
    {
        public ClienteNotFoundException(string message) : base(message)
        {
        }
    }
}
