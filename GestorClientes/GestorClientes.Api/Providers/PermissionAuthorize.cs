﻿using GestorClientes.Logic.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace GestorClientes.Api.Providers
{
    public class PermissionAuthorize : AuthorizeAttribute
    {
        private string Permission { get; set; }

        public PermissionAuthorize(string permission)
        {
            this.Permission = permission;
        }

        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            try
            {
                bool retorno = false;
                var claims = (actionContext.Request.GetRequestContext().Principal as ClaimsPrincipal).Claims;
                if (claims != null)
                {
                    int userId = Int32.Parse(claims.Where(x => x.Type == "UID").SingleOrDefault().Value);
                    AuthService auth = new AuthService();
                    return auth.HasPermission(this.Permission, userId);
                }
                return retorno;
            }
            catch (Exception)
            {
                return false;
            }
        }

        protected override void HandleUnauthorizedRequest(HttpActionContext actionContext)
        {
            actionContext.Response = new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.Forbidden,
                Content = new StringContent("You are unauthorized to access this resource")
            };

        }
    }
}