﻿using GestorClientes.Logic.Dto;
using GestorClientes.Logic.Services;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace GestorClientes.Api.Providers
{
    public class AuthProvider : OAuthAuthorizationServerProvider
    {
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {

            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });

            AuthService authService = new AuthService();
            UsuarioDto dataUser = await authService.Authenticate(context.UserName, context.Password);

            if (dataUser == null)
            {
                context.SetError("invalid_grant", "El usuario o contraseña es incorrecta.");
                return;
            }

            var props = new AuthenticationProperties(new Dictionary<string, string>());
            props.Dictionary.Add("permissions", JsonConvert.SerializeObject(new UsuarioPermisoDto(dataUser)));
            props.Dictionary.Add("user_data", JsonConvert.SerializeObject(new { dataUser.Nombre, dataUser.Apellido, dataUser.Username, }));
            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
            //Username
            identity.AddClaim(new Claim("UN", context.UserName));
            //User ID
            identity.AddClaim(new Claim("UID", dataUser.Id.ToString()));

            var ticket = new AuthenticationTicket(identity, props);
            context.Validated(ticket);
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }
            return Task.FromResult<object>(null);
        }
    }
}