﻿using GestorClientes.Api.Providers;
using GestorClientes.Dominio.DAL;
using GestorClientes.Logic.Dto;
using GestorClientes.Logic.Dto.Requests;
using GestorClientes.Logic.Exceptions.Moneda;
using GestorClientes.Logic.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace GestorClientes.Api.Controllers
{
    public class MonedaController : BaseController
    {
        [PermissionAuthorize("AGREGAR_MONEDAS")]
        public IHttpActionResult Post(MonedaDto monedaDto)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    MonedaService monedaService = new MonedaService(_context, claims);
                    return Ok(monedaService.Add(monedaDto));
                }
                catch (Exception ex)
                {
                    if (ex is MonedaDescripcionAlreadyExistsException)
                        return BadRequest(ex.Message);
                    else
                        return InternalServerError(ex);
                }
            }
        }

        [PermissionAuthorize("EDITAR_MONEDAS")]
        public IHttpActionResult Put(MonedaDto monedaDto)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    MonedaService monedaService = new MonedaService(_context, claims);
                    return Ok(monedaService.Edit(monedaDto));
                }
                catch (Exception ex)
                {
                    if (ex is MonedaDescripcionAlreadyExistsException)
                        return BadRequest(ex.Message);
                    else if (ex is MonedaNotFoundException)
                        return NotFound();
                    else
                        return InternalServerError(ex);
                }
            }
        }

        [PermissionAuthorize("ELIMINAR_MONEDAS")]
        public IHttpActionResult Delete(MonedaDto monedaDto)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    MonedaService monedaService = new MonedaService(_context, claims);
                    return Ok(monedaService.Delete(monedaDto));
                }
                catch (Exception ex)
                {
                    if (ex is MonedaNotFoundException)
                        return NotFound();
                    else
                        return InternalServerError(ex);
                }
            }
        }

        [AllowAnonymous]

        public IHttpActionResult Get([FromUri] MonedaRequestDto request)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    MonedaService monedaService = new MonedaService(_context, claims);
                    return Ok(monedaService.GetAll(request));
                }
                catch (Exception ex)
                {
                    return InternalServerError(ex);
                }
            }
        }
        [AllowAnonymous]
        [Route("api/moneda/GetAllMonedas")]
        [HttpGet]
        public IHttpActionResult GetAllMonedas([FromUri] MonedaRequestDto request)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    MonedaService monedaService = new MonedaService(_context, claims);
                    return Ok(monedaService.GetAllMonedas(request));
                }
                catch (Exception ex)
                {
                    return InternalServerError(ex);
                }
            }
        }


    }
}
