﻿using GestorClientes.Api.Providers;
using GestorClientes.Dominio.DAL;
using GestorClientes.Logic.Dto;
using GestorClientes.Logic.Dto.Requests;
using GestorClientes.Logic.Exceptions.Egreso;
using GestorClientes.Logic.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace GestorClientes.Api.Controllers
{
    public class PagoClienteController : BaseController
    {
        [AllowAnonymous]
        public IHttpActionResult Get([FromUri]int clienteId)
        {

            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    PagoClienteService pagoClienteService = new PagoClienteService(_context, claims);
                    return Ok(pagoClienteService.GetPagosCliente(clienteId));
                }
                catch (Exception ex)
                {
                    return InternalServerError(ex);
                }
            }
        }
        [PermissionAuthorize("EDITAR_CLIENTES")]
        public IHttpActionResult Delete([FromUri] int pagoId)
        {

            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    PagoClienteService pagoClienteService = new PagoClienteService(_context, claims);
                    return Ok(pagoClienteService.DeletePagoCliente(pagoId));
                }
                catch (Exception ex)
                {
                    return InternalServerError(ex);
                }
            }
        }
        [PermissionAuthorize("VER_CLIENTES")]
        [Route("api/pagocliente/cuotas")]
        public IHttpActionResult GetCuotasPago([FromUri] int pagoId)
        {

            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    PagoClienteService pagoClienteService = new PagoClienteService(_context, claims);
                    return Ok(pagoClienteService.GetCuotasPago(pagoId));
                }
                catch (Exception ex)
                {
                    return InternalServerError(ex);
                }
            }
        }
        [PermissionAuthorize("EDITAR_CLIENTES")]
        public IHttpActionResult Post(PagoClienteDto request)
        {

            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    PagoClienteService pagoClienteService = new PagoClienteService(_context, claims);
                    return Ok(pagoClienteService.AddPagoCliente(request));
                }
                catch (Exception ex)
                {
                    return InternalServerError(ex);
                }
            }
        }

        [PermissionAuthorize("EDITAR_CLIENTES")]
        [Route("api/pagocliente/realizarpago")]
        [HttpPost]
        public IHttpActionResult RealizarPago(CuotasPagasRequestDto clienteProductosRequestDto)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    PagoClienteService pagoClienteService = new PagoClienteService(_context, claims);
                    return Ok(pagoClienteService.RealizarPago(clienteProductosRequestDto));
                }
                catch (Exception ex)
                {
                    return InternalServerError(ex);
                }
            }
        }


        

    }
}