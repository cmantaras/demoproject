﻿using GestorClientes.Api.Providers;
using GestorClientes.Dominio.DAL;
using GestorClientes.Logic.Dto;
using GestorClientes.Logic.Dto.Requests;
using GestorClientes.Logic.Exceptions.Marca;
using GestorClientes.Logic.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace GestorClientes.Api.Controllers
{
    public class MarcaController : BaseController
    {
        [PermissionAuthorize("AGREGAR_MARCAS")]
        public IHttpActionResult Post(MarcaDto marcaDto)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    MarcaService marcaService = new MarcaService(_context, claims);
                    return Ok(marcaService.Add(marcaDto));
                }
                catch (Exception ex)
                {
                    if (ex is MarcaDescripcionAlreadyExistsException)
                        return BadRequest(ex.Message);
                    else
                        return InternalServerError(ex);
                }
            }
        }

        [PermissionAuthorize("EDITAR_MARCAS")]
        public IHttpActionResult Put(MarcaDto marcaDto)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    MarcaService marcaService = new MarcaService(_context, claims);
                    return Ok(marcaService.Edit(marcaDto));
                }
                catch (Exception ex)
                {
                    if (ex is MarcaDescripcionAlreadyExistsException)
                        return BadRequest(ex.Message);
                    else if (ex is MarcaNotFoundException)
                        return NotFound();
                    else
                        return InternalServerError(ex);
                }
            }
        }

        [PermissionAuthorize("ELIMINAR_MARCAS")]
        public IHttpActionResult Delete(MarcaDto marcaDto)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    MarcaService marcaService = new MarcaService(_context, claims);
                    return Ok(marcaService.Delete(marcaDto));
                }
                catch (Exception ex)
                {
                    if (ex is MarcaNotFoundException)
                        return NotFound();
                    else
                        return InternalServerError(ex);
                }
            }
        }

        [AllowAnonymous]
        public IHttpActionResult Get([FromUri] MarcaRequestDto request)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    MarcaService marcaService = new MarcaService(_context, claims);
                    return Ok(marcaService.GetAll(request));
                }
                catch (Exception ex)
                {
                    return InternalServerError(ex);
                }
            }
        }


    }
}
