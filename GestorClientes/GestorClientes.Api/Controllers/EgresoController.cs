﻿using GestorClientes.Api.Providers;
using GestorClientes.Dominio.DAL;
using GestorClientes.Logic.Dto;
using GestorClientes.Logic.Dto.Requests;
using GestorClientes.Logic.Exceptions.Egreso;
using GestorClientes.Logic.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace GestorClientes.Api.Controllers
{
    public class EgresoController : BaseController
    {
        [PermissionAuthorize("AGREGAR_EGRESO")]
        public IHttpActionResult Post(EgresoRequestDto request)
        {

            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    EgresoService ciudadService = new EgresoService(_context, claims);
                    return Ok(ciudadService.Add(request));
                }
                catch (Exception ex)
                {
                    return InternalServerError(ex);
                }

            }
        }
        [PermissionAuthorize("VER_EGRESOS")]
        public IHttpActionResult Get([FromUri] int IdCategoria, int IdProveedor, int Offset, int Limit, int EsCheque)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    EgresoService egresoService = new EgresoService(_context, claims);
                    return Ok(egresoService.GetAll(IdCategoria, IdProveedor, Offset, Limit, EsCheque));

                }
                catch (Exception ex)
                {
                    return InternalServerError(ex);
                }
            }
        }



        [PermissionAuthorize("EDITAR_EGRESO")]
        public IHttpActionResult Put(EgresoRequestDto request)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    EgresoService egresoService = new EgresoService(_context, claims);
                    return Ok(egresoService.Edit(request));

                }
                catch (Exception ex)
                {
                    return InternalServerError(ex);
                }
            }
        }


        [PermissionAuthorize("ELIMINAR_EGRESO")]
        public IHttpActionResult Delete(EgresoDto egresoDto)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    EgresoService egresoService = new EgresoService(_context, claims);
                    return Ok(egresoService.Delete(egresoDto));
                }
                catch (Exception ex)
                {
                    if (ex is EgresoNotFoundException)
                        return NotFound();
                    else
                        return InternalServerError(ex);
                }
            }
        }

    }
}