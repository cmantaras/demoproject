﻿using GestorClientes.Api.Providers;
using GestorClientes.Dominio.DAL;
using GestorClientes.Logic.Dto;
using GestorClientes.Logic.Dto.Requests;
using GestorClientes.Logic.Exceptions.Item;
using GestorClientes.Logic.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace GestorClientes.Api.Controllers
{
    public class ReportesController : BaseController
    {


        [PermissionAuthorize("VER_REPORTES_INGRESOS-EGRESOS")]
        [Route("api/reportes/GetAllPagos")]
        [HttpPost]
        public IHttpActionResult GetAllPagos(EgresoIngresoRequestDto request)
        {

            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    ReportesService reportesService = new ReportesService(_context, claims);
                    return Ok(reportesService.GetAllPagos(request));
                }
                catch (Exception ex)
                {
                    return InternalServerError(ex);
                }
            }
        }
        [Route("api/reportes/GetAllEgresos")]
        [HttpPost]
        [PermissionAuthorize("VER_REPORTES_INGRESOS-EGRESOS")]
        public IHttpActionResult GetAllEgresos(EgresoIngresoRequestDto request)
        {

            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    ReportesService reportesService = new ReportesService(_context, claims);
                    return Ok(reportesService.GetAllEgresos(request));
                }
                catch (Exception ex)
                {
                    return InternalServerError(ex);
                }
            }
        }

        [PermissionAuthorize("VER_REPORTES_INGRESOS-EGRESOS")]
        [Route("api/reportes/GetEgresosByCierre")]
        [HttpGet]
        public IHttpActionResult GetEgresosByCierre()
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    ReportesService reportesService = new ReportesService(_context, claims);
                    return Ok(reportesService.GetEgresosByCierre());

                }
                catch (Exception ex)
                {
                    return InternalServerError(ex);
                }
            }
        }

        [PermissionAuthorize("VER_REPORTES_INGRESOS-EGRESOS")]
        [Route("api/reportes/GetIngresosByCierre")]
        [HttpGet]
        public IHttpActionResult GetIngresosByCierre()
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    ReportesService reportesService = new ReportesService(_context, claims);
                    return Ok(reportesService.GetIngresosByCierre());

                }
                catch (Exception ex)
                {
                    return InternalServerError(ex);
                }
            }
        }

        [PermissionAuthorize("VER_REPORTES_DEUDORES")]
        [Route("api/reportes/deudores")]
        [HttpPost]
        public IHttpActionResult GetDeudores(DeudorRequestDto request)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    ReportesService reportesService = new ReportesService(_context, claims);
                    return Ok(reportesService.GetDeudores(request));

                }
                catch (Exception ex)
                {
                    return InternalServerError(ex);
                }
            }
        }

        [PermissionAuthorize("VER_CLIENTES")]
        [Route("api/reportes/GetBirthdays")]
        [HttpGet]
        public IHttpActionResult GetBirthdays()
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    ReportesService reportesService = new ReportesService(_context, claims);
                    return Ok(reportesService.GetBirthdays());

                }
                catch (Exception ex)
                {
                    return InternalServerError(ex);
                }
            }
        }
        [PermissionAuthorize("VER_CLIENTES")]
        [Route("api/reportes/getfincontratos")]
        [HttpGet]
        public IHttpActionResult GetFinContratos()
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    ReportesService reportesService = new ReportesService(_context, claims);
                    return Ok(reportesService.GetContratosPorVencer());

                }
                catch (Exception ex)
                {
                    return InternalServerError(ex);
                }
            }
        }

        [PermissionAuthorize("VER_CLIENTES")]
        [Route("api/reportes/getclientes")]
        [HttpPost]
        public IHttpActionResult GetClientes( ClienteReporteRequestDto request)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    ReportesService reportesService = new ReportesService(_context, claims);
                    return Ok(reportesService.GetClientes(request));

                }
                catch (Exception ex)
                {
                    return InternalServerError(ex);
                }
            }
        }

        [PermissionAuthorize("VER_REPORTES_INGRESOS-EGRESOS")]
        [Route("api/reportes/getcheques")]
        [HttpGet]
        public IHttpActionResult GetCheques()
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    ReportesService reportesService = new ReportesService(_context, claims);
                    return Ok(reportesService.GetCheques());

                }
                catch (Exception ex)
                {
                    return InternalServerError(ex);
                }
            }
        }


    }
}
