﻿
using GestorClientes.Logic.Exceptions.Usuarios;
using GestorClientes.Logic.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using GestorClientes.Dominio.DAL;
using GestorClientes.Logic.Dto;
using GestorClientes.Logic.Exceptions;

namespace GestorClientes.Api.Controllers
{
    public class AccountController : ApiController
    {
        [AllowAnonymous]
        [HttpPost]
        [Route("api/account/change_password")]
        public IHttpActionResult Post(AccountDto account)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    AuthService authService = new AuthService();
                    return Ok(authService.ChangePassword(account.Username, account.Password, account.ConfirmPassword, account.Token));
                }
                catch (Exception ex)
                {
                    if (ex is UsuarioNotFoundException)
                        return NotFound();
                    else
                        return InternalServerError(ex);
                }
            }
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("api/account/valid_token")]
        public IHttpActionResult Post([FromUri] string username, [FromUri] string token)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    AuthService authService = new AuthService();
                    authService.IsValidToken(username, token);
                    return Ok();
                }
                catch (Exception ex)
                {
                    if (ex is UsuarioNotFoundException)
                        return NotFound();
                    else
                        return InternalServerError(ex);
                }
            }
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("api/account/reset_password")]
        public IHttpActionResult Post([FromUri] string username)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    AuthService authService = new AuthService();
                    authService.ResetPassword(username);
                    return Ok();
                }
                catch (Exception ex)
                {
                    if (ex is BadRequestException)
                        return BadRequest();
                    else if (ex is UsuarioNotFoundException)
                        return NotFound();
                    else
                        return InternalServerError(ex);
                }
            }
        }
    }
}
