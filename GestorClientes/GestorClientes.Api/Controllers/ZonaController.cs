﻿using GestorClientes.Api.Providers;
using GestorClientes.Dominio.DAL;
using GestorClientes.Logic.Dto;
using GestorClientes.Logic.Dto.Requests;
using GestorClientes.Logic.Exceptions.Zona;
using GestorClientes.Logic.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace GestorClientes.Api.Controllers
{
    public class ZonaController : BaseController
    {
        [PermissionAuthorize("AGREGAR_ZONAS")]
        public IHttpActionResult Post(ZonaDto zonaDto)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    ZonaService zonaService = new ZonaService(_context, claims);
                    return Ok(zonaService.Add(zonaDto));
                }
                catch (Exception ex)
                {
                    if (ex is ZonaAlreadyExistsException)
                        return BadRequest(ex.Message);
                    else
                        return InternalServerError(ex);
                }
            }
        }

        [PermissionAuthorize("EDITAR_ZONAS")]
        public IHttpActionResult Put(ZonaDto zonaDto)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    ZonaService zonaService = new ZonaService(_context, claims);
                    return Ok(zonaService.Edit(zonaDto));
                }
                catch (Exception ex)
                {
                    if (ex is ZonaAlreadyExistsException)
                        return BadRequest(ex.Message);
                    else if (ex is ZonaNotFoundException)
                        return NotFound();
                    else
                        return InternalServerError(ex);
                }
            }
        }

        [PermissionAuthorize("ELIMINAR_ZONAS")]
        public IHttpActionResult Delete(ZonaDto zonaDto)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    ZonaService zonaService = new ZonaService(_context, claims);
                    return Ok(zonaService.Delete(zonaDto));
                }
                catch (Exception ex)
                {
                    if (ex is ZonaNotFoundException)
                        return NotFound();
                    else
                        return InternalServerError(ex);
                }
            }
        }
        [AllowAnonymous]
        public IHttpActionResult Get([FromUri] ZonaRequestDto request)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    ZonaService zonaService = new ZonaService(_context, claims);
                    return Ok(zonaService.GetAll(request));
                }
                catch (Exception ex)
                {
                    return InternalServerError(ex);
                }
            }
        }


    }
}
