﻿using GestorClientes.Api.Providers;
using GestorClientes.Dominio.DAL;
using GestorClientes.Logic.Dto;
using GestorClientes.Logic.Dto.Requests;
using GestorClientes.Logic.Exceptions.Proveedor;
using GestorClientes.Logic.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace GestorClientes.Api.Controllers
{
    public class ProveedorController : BaseController
    {
        [PermissionAuthorize("AGREGAR_PROVEEDORES")]
        public IHttpActionResult Post(ProveedorDto proveedorDto)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    ProveedorService proveedorService = new ProveedorService(_context, claims);
                    return Ok(proveedorService.Add(proveedorDto));
                }
                catch (Exception ex)
                {
                    if (ex is ProveedorAlreadyExistsException)
                        return BadRequest(ex.Message);
                    else
                        return InternalServerError(ex);
                }
            }
        }

        [PermissionAuthorize("EDITAR_PROVEEDORES")]
        [Route("api/Proveedor/UpdateProveedorBanco")]
        [HttpPut]
        public IHttpActionResult UpdateProveedorBanco(ProveedorBancoDto request)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    ProveedorService proveedorService = new ProveedorService(_context, claims);
                    return Ok(content: proveedorService.UpdateProveedorBanco(request));
                }
                catch (Exception ex)
                {
                    return InternalServerError(ex);
                }
            }
        }
        [PermissionAuthorize("VER_PROVEEDORES")]
        [Route("api/proveedor/GetInfoBancariaProveedor")]
        [HttpPost]
        public IHttpActionResult GetInfoBancariaProveedor([FromUri] int id)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    ProveedorService proveedorService = new ProveedorService(_context, claims);
                    return Ok(proveedorService.GetInfoBancariaProveedor(id));
                }
                catch (Exception ex)
                {
                    return InternalServerError(ex);
                }
            }
        }

        [PermissionAuthorize("EDITAR_PROVEEDORES")]
        [Route("api/proveedor/GetTiposCuentas")]
        [HttpGet]
        public IHttpActionResult GetTiposCuentas()
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    ProveedorService proveedorService = new ProveedorService(_context, claims);
                    return Ok(proveedorService.GetTiposCuentas());
                }
                catch (Exception ex)
                {
                    return InternalServerError(ex);
                }
            }
        }


        [PermissionAuthorize("EDITAR_PROVEEDORES")]
        public IHttpActionResult Put(ProveedorDto proveedorDto)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    ProveedorService proveedorService = new ProveedorService(_context, claims);
                    return Ok(proveedorService.Edit(proveedorDto));
                }
                catch (Exception ex)
                {
                    if (ex is ProveedorAlreadyExistsException)
                        return BadRequest(ex.Message);
                    else if (ex is ProveedorNotFoundException)
                        return NotFound();
                    else
                        return InternalServerError(ex);
                }
            }
        }

        [PermissionAuthorize("ELIMINAR_PROVEEDORES")]
        public IHttpActionResult Delete(ProveedorDto proveedorDto)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    ProveedorService proveedorService = new ProveedorService(_context, claims);
                    return Ok(proveedorService.Delete(proveedorDto));
                }
                catch (Exception ex)
                {
                    if (ex is ProveedorNotFoundException)
                        return NotFound();
                    else
                        return InternalServerError(ex);
                }
            }
        }

        [AllowAnonymous]
        [Route("api/Proveedor/GetProveedores")]
        [HttpPost]
        public IHttpActionResult GetProveedores(ProveedorRequestDto request)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    ProveedorService proveedorService = new ProveedorService(_context, claims);
                    return Ok(proveedorService.GetAll(request));
                }
                catch (Exception ex)
                {
                    return InternalServerError(ex);
                }
            }
        }


    }
}
