﻿using GestorClientes.Api.Providers;
using GestorClientes.Dominio.DAL;
using GestorClientes.Logic.Dto;
using GestorClientes.Logic.Dto.Requests;
using GestorClientes.Logic.Exceptions.Categoria;
using GestorClientes.Logic.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace GestorClientes.Api.Controllers
{
    public class CategoriaController : BaseController

    {
        [PermissionAuthorize("AGREGAR_CATEGORIAS")]
        public IHttpActionResult Post(CategoriaDto CategoriaDto)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    CategoriaService CategoriaService = new CategoriaService(_context, claims);
                    return Ok(CategoriaService.Add(CategoriaDto));
                }
                catch (Exception ex)
                {
                    if (ex is CategoriaAlreadyExistsException)
                        return BadRequest(ex.Message);
                    else
                        return InternalServerError(ex);
                }
            }
        }

        [PermissionAuthorize("EDITAR_CATEGORIAS")]
        public IHttpActionResult Put(CategoriaDto CategoriaDto)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    CategoriaService CategoriaService = new CategoriaService(_context, claims);
                    return Ok(CategoriaService.Edit(CategoriaDto));
                }
                catch (Exception ex)
                {
                    if (ex is CategoriaAlreadyExistsException)
                        return BadRequest(ex.Message);
                    else if (ex is CategoriaNotFoundException)
                        return NotFound();
                    else
                        return InternalServerError(ex);
                }
            }
        }

        [PermissionAuthorize("ELIMINAR_CATEGORIAS")]
        public IHttpActionResult Delete(CategoriaDto CategoriaDto)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    CategoriaService CategoriaService = new CategoriaService(_context, claims);
                    return Ok(CategoriaService.Delete(CategoriaDto));
                }
                catch (Exception ex)
                {
                    if (ex is CategoriaNotFoundException)
                        return NotFound();
                    else
                        return InternalServerError(ex);
                }
            }
        }

        [AllowAnonymous]
        public IHttpActionResult Get([FromUri] CategoriaRequestDto request)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    CategoriaService CategoriaService = new CategoriaService(_context, claims);
                    return Ok(CategoriaService.GetAll(request));
                }
                catch (Exception ex)
                {
                    return InternalServerError(ex);
                }
            }
        }
        [Route("api/categoria/GetAllCategorias")]
        [AllowAnonymous]
        public IHttpActionResult GetAllCategorias()
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    CategoriaService CategoriaService = new CategoriaService(_context, claims);
                    return Ok(CategoriaService.GetAllCategorias());
                }
                catch (Exception ex)
                {
                    return InternalServerError(ex);
                }
            }
        }


    }
}
