﻿using GestorClientes.Api.Providers;
using GestorClientes.Dominio.DAL;
using GestorClientes.Logic.Dto;
using GestorClientes.Logic.Dto.Requests;
using GestorClientes.Logic.Exceptions.Impuesto;
using GestorClientes.Logic.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace GestorClientes.Api.Controllers
{
    public class ImpuestoController : BaseController
    {
        [PermissionAuthorize("AGREGAR_IMPUESTOS")]
        public IHttpActionResult Post(ImpuestoDto impuestoDto)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    ImpuestoService impuestoService = new ImpuestoService(_context, claims);
                    return Ok(impuestoService.Add(impuestoDto));
                }
                catch (Exception ex)
                {
                    if (ex is ImpuestoDescripcionAlreadyExistsException)
                        return BadRequest(ex.Message);
                    else
                        return InternalServerError(ex);
                }
            }
        }

        [PermissionAuthorize("EDITAR_IMPUESTOS")]
        public IHttpActionResult Put(ImpuestoDto impuestoDto)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    ImpuestoService impuestoService = new ImpuestoService(_context, claims);
                    return Ok(impuestoService.Edit(impuestoDto));
                }
                catch (Exception ex)
                {
                    if (ex is ImpuestoDescripcionAlreadyExistsException)
                        return BadRequest(ex.Message);
                    else if (ex is ImpuestoNotFoundException)
                        return NotFound();
                    else
                        return InternalServerError(ex);
                }
            }
        }

        [PermissionAuthorize("ELIMINAR_IMPUESTOS")]
        public IHttpActionResult Delete(ImpuestoDto impuestoDto)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    ImpuestoService impuestoService = new ImpuestoService(_context, claims);
                    return Ok(impuestoService.Delete(impuestoDto));
                }
                catch (Exception ex)
                {
                    if (ex is ImpuestoNotFoundException)
                        return NotFound();
                    else
                        return InternalServerError(ex);
                }
            }
        }

        [AllowAnonymous]
        public IHttpActionResult Get([FromUri] ImpuestoRequestDto request)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    ImpuestoService impuestoService = new ImpuestoService(_context, claims);
                    return Ok(impuestoService.GetAll(request));
                }
                catch (Exception ex)
                {
                    return InternalServerError(ex);
                }
            }
        }


    }
}
