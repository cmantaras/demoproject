﻿using GestorClientes.Api.Providers;
using GestorClientes.Dominio.DAL;
using GestorClientes.Logic.Dto;
using GestorClientes.Logic.Dto.Requests;
using GestorClientes.Logic.Exceptions.ProductoPrecio;
using GestorClientes.Logic.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace GestorClientes.Api.Controllers
{
    public class ProductoPrecioController : BaseController
    {
        [PermissionAuthorize("EDITAR_PRODUCTOS")]
        public IHttpActionResult Post(ProductoPrecioDto productoPrecioDto)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    ProductoPrecioService productoPrecioService = new ProductoPrecioService(_context, claims);
                    return Ok(productoPrecioService.Add(productoPrecioDto));
                }
                catch (Exception ex)
                {
                    if (ex is ProductoPrecioAlreadyExistsException)
                        return BadRequest(ex.Message);
                    else
                        return InternalServerError(ex);
                }
            }
        }

        [PermissionAuthorize("EDITAR_PRODUCTOS")]
        public IHttpActionResult Put(ProductoPrecioDto productoPrecioDto)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    ProductoPrecioService productoPrecioService = new ProductoPrecioService(_context, claims);
                    return Ok(productoPrecioService.Edit(productoPrecioDto));
                }
                catch (Exception ex)
                {
                    if (ex is ProductoPrecioAlreadyExistsException)
                        return BadRequest(ex.Message);
                    else if (ex is ProductoPrecioNotFoundException)
                        return NotFound();
                    else
                        return InternalServerError(ex);
                }
            }
        }

        [PermissionAuthorize("ELIMINAR_PRODUCTOS")]
        public IHttpActionResult Delete(ProductoPrecioDto productoPrecioDto)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    ProductoPrecioService productoPrecioService = new ProductoPrecioService(_context, claims);
                    return Ok(productoPrecioService.Delete(productoPrecioDto));
                }
                catch (Exception ex)
                {
                    if (ex is ProductoPrecioNotFoundException)
                        return NotFound();
                    else
                        return InternalServerError(ex);
                }
            }
        }

        [AllowAnonymous]
        [Route("api/ProductoPrecio/GetPrecios")]
        [HttpPost]
        public IHttpActionResult GetPrecios(ProductoPrecioRequestDto request)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    ProductoPrecioService productoPrecioService = new ProductoPrecioService(_context, claims);
                    return Ok(productoPrecioService.GetAll(request));
                }
                catch (Exception ex)
                {
                    return InternalServerError(ex);
                }
            }
        }


        [PermissionAuthorize("EDITAR_PRODUCTOS")]
        [Route("api/ProductoPrecio/AddMetodos")]
        [HttpPost]
        public IHttpActionResult AddMetodos(ProductoPrecioMetodosDto productoPrecioMetodos)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    ProductoPrecioService productoPrecioService = new ProductoPrecioService(_context, claims);
                    return Ok(productoPrecioService.AddMetodos(productoPrecioMetodos));
                }
                catch (Exception ex)
                {
                    return InternalServerError(ex);
                }
            }

        }


        [PermissionAuthorize("ELIMINAR_PRODUCTOS")]
        [Route("api/ProductoPrecio/DeleteMetodo")]
        [HttpDelete]
        public IHttpActionResult DeleteMetodo([FromUri] int ProductoPrecioId, int MetodoId)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    ProductoPrecioService productoPrecioService = new ProductoPrecioService(_context, claims);
                    return Ok(productoPrecioService.DeleteMetodo(ProductoPrecioId, MetodoId));
                }
                catch (Exception ex)
                {
                    return InternalServerError(ex);
                }
            }

        }

        [AllowAnonymous]
        [Route("api/ProductoPrecio/GetProductoPrecioById")]
        [HttpGet]
        public IHttpActionResult GetProductoPrecioById([FromUri] int ProductoPrecioId)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    ProductoPrecioService productoPrecioService = new ProductoPrecioService(_context, claims);
                    return Ok(productoPrecioService.GetProductoPrecioById(ProductoPrecioId));
                }
                catch (Exception ex)
                {
                    return InternalServerError(ex);
                }
            }
        }


    }
}
