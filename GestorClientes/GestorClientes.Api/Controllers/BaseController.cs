﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;

namespace GestorClientes.Api.Controllers
{
    [Authorize]
    public class BaseController : ApiController
    {
        public IEnumerable<Claim> claims;
        public BaseController()
        {
            claims = (User.Identity as ClaimsIdentity).Claims;
        }
    }
}
