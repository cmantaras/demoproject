﻿using GestorClientes.Api.Providers;
using GestorClientes.Dominio.DAL;
using GestorClientes.Logic.Dto;
using GestorClientes.Logic.Dto.Requests;
using GestorClientes.Logic.Exceptions.Producto;
using GestorClientes.Logic.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace GestorClientes.Api.Controllers
{
    public class ProductoController : BaseController
    {
        [PermissionAuthorize("AGREGAR_PRODUCTOS")]
        public IHttpActionResult Post(ProductoDto productoDto)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    ProductoService productoService = new ProductoService(_context, claims);
                    return Ok(productoService.Add(productoDto));
                }
                catch (Exception ex)
                {
                    if (ex is ProductoAlreadyExistsException)
                        return BadRequest(ex.Message);
                    else
                        return InternalServerError(ex);
                }
            }
        }

        [PermissionAuthorize("EDITAR_PRODUCTOS")]
        public IHttpActionResult Put(ProductoDto productoDto)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    ProductoService productoService = new ProductoService(_context, claims);
                    return Ok(productoService.Edit(productoDto));
                }
                catch (Exception ex)
                {
                    if (ex is ProductoAlreadyExistsException)
                        return BadRequest(ex.Message);
                    else if (ex is ProductoNotFoundException)
                        return NotFound();
                    else
                        return InternalServerError(ex);
                }
            }
        }

        [PermissionAuthorize("ELIMINAR_PRODUCTOS")]
        public IHttpActionResult Delete(ProductoDto productoDto)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    ProductoService productoService = new ProductoService(_context, claims);
                    return Ok(productoService.Delete(productoDto));
                }
                catch (Exception ex)
                {
                    if (ex is ProductoNotFoundException)
                        return NotFound();
                    else
                        return InternalServerError(ex);
                }
            }
        }

        [AllowAnonymous]
        public IHttpActionResult Get([FromUri] ProductoRequestDto request)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    ProductoService productoService = new ProductoService(_context, claims);
                    return Ok(productoService.GetAll(request));
                }
                catch (Exception ex)
                {
                    return InternalServerError(ex);
                }
            }
        }

        [AllowAnonymous]
        [Route("api/producto/ProductosFiltrados")]
        public IHttpActionResult GetFiltrados([FromUri] string productoDescripcion)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    ProductoService productoService = new ProductoService(_context, claims);
                    return Ok(productoService.GetFiltrados(productoDescripcion));
                }
                catch (Exception ex)
                {
                    return InternalServerError(ex);
                }
            }
        }

    }
}
