﻿using GestorClientes.Api.Providers;
using GestorClientes.Dominio.DAL;
using GestorClientes.Logic.Dto;
using GestorClientes.Logic.Dto.Requests;
using GestorClientes.Logic.Exceptions.Parametros;
using GestorClientes.Logic.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace GestorClientes.Api.Controllers
{
    public class ParametrosController : BaseController
    {
        [PermissionAuthorize("EDIT_PARAMETROS")]
        public IHttpActionResult Put(ParametroDto parametroDto)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    ParametrosService parametrosService = new ParametrosService(_context, claims);
                    return Ok(parametrosService.Edit(parametroDto));
                }
                catch (Exception ex)
                {
                    if (ex is ParametroAlreadyExistsException)
                        return BadRequest();
                    else if (ex is ParametroNotFoundException)
                        return NotFound();
                    else
                        return InternalServerError(ex);
                }
            }
        }

        [Route("api/Parametros/textoFactura")]
        [HttpPut]
        [PermissionAuthorize("EDIT_PARAMETROS")]
        public IHttpActionResult TextoComprabantePago(TextoComprobanteRequestDto request)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    ParametrosService parametrosService = new ParametrosService(_context, claims);
                    return Ok(parametrosService.SetTextoComprobantePago(request));
                }
                catch (Exception ex)
                {
                    if (ex is ParametroAlreadyExistsException)
                        return BadRequest();
                    else if (ex is ParametroNotFoundException)
                        return NotFound();
                    else
                        return InternalServerError(ex);
                }
            }
        }


        [Route("api/Parametros/checkfibra")]
        [HttpPut]
        [PermissionAuthorize("EDITAR_PARAMETROS")]
        public IHttpActionResult SetChekFibra(string checkFibra)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    ParametrosService parametrosService = new ParametrosService(_context, claims);
                    return Ok(parametrosService.SetCheckFibra(checkFibra));
                }
                catch (Exception ex)
                {
                    if (ex is ParametroAlreadyExistsException)
                        return BadRequest();
                    else if (ex is ParametroNotFoundException)
                        return NotFound();
                    else
                        return InternalServerError(ex);
                }
            }
        }
        [Route("api/Parametros/vencimientomes")]
        [HttpPut]
        [PermissionAuthorize("EDITAR_PARAMETROS")]
        public IHttpActionResult SetVencimientoMes(string vencimientoMes)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    ParametrosService parametrosService = new ParametrosService(_context, claims);
                    return Ok(parametrosService.SetVencimientoMes(vencimientoMes));
                }
                catch (Exception ex)
                {
                    if (ex is ParametroAlreadyExistsException)
                        return BadRequest();
                    else if (ex is ParametroNotFoundException)
                        return NotFound();
                    else
                        return InternalServerError(ex);
                }
            }
        }




        [AllowAnonymous]
        public IHttpActionResult Get([FromUri] ParametroRequestDto request)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    ParametrosService parametrosService = new ParametrosService(_context, claims);
                    return Ok(parametrosService.GetAll(request));
                }
                catch (Exception ex)
                {
                    return InternalServerError(ex);
                }
            }
        }
    }
}