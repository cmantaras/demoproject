﻿using GestorClientes.Api.Providers;
using GestorClientes.Dominio.DAL;
using GestorClientes.Logic.Dto;
using GestorClientes.Logic.Dto.Requests;
using GestorClientes.Logic.Exceptions;
using GestorClientes.Logic.Exceptions.Roles;
using GestorClientes.Logic.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace GestorClientes.Api.Controllers
{
    public class RolesController : BaseController
    {
        [PermissionAuthorize("AGREGAR_ROLES")]
        public IHttpActionResult Post(RolDto rolDto)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    rolDto.ValidatePost();
                    RolesService rolesService = new RolesService(_context, claims);
                    return Ok(rolesService.Add(rolDto));
                }
                catch (Exception ex)
                {
                    if (ex is NombreAlreadyExistsException || ex is BadRequestException)
                        return BadRequest(ex.Message);
                    else
                        return InternalServerError(ex);
                }
            }
        }

        [PermissionAuthorize("EDITAR_ROLES")]
        public IHttpActionResult Put(RolDto rolDto)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    rolDto.ValidatePut();
                    RolesService rolsService = new RolesService(_context, claims);
                    return Ok(rolsService.Edit(rolDto));
                }
                catch (Exception ex)
                {
                    if (ex is NombreAlreadyExistsException || ex is BadRequestException)
                        return BadRequest(ex.Message);
                    else if (ex is RolNotFoundException)
                        return NotFound();
                    else
                        return InternalServerError(ex);
                }
            }
        }

        [PermissionAuthorize("ELIMINAR_ROLES")]
        public IHttpActionResult Delete(RolDto rolDto)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    RolesService rolesService = new RolesService(_context, claims);
                    return Ok(rolesService.Delete(rolDto));
                }
                catch (Exception ex)
                {
                    if (ex is RolNotFoundException)
                        return NotFound();
                    else
                        return InternalServerError(ex);
                }
            }
        }

        [AllowAnonymous]
        public IHttpActionResult Get([FromUri] RolRequestDto request)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    RolesService rolesService = new RolesService(_context, claims);
                    return Ok(rolesService.GetAll(request));
                }
                catch (Exception ex)
                {
                    return InternalServerError(ex);
                }
            }
        }
    }
}
