﻿using GestorClientes.Api.Providers;
using GestorClientes.Dominio.DAL;
using GestorClientes.Logic.Dto;
using GestorClientes.Logic.Dto.Requests;
using GestorClientes.Logic.Exceptions.MetodoPago;
using GestorClientes.Logic.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace GestorClientes.Api.Controllers
{
    public class MetodoPagoController : BaseController
    {
        [PermissionAuthorize("AGREGAR_METODOS_PAGO")]
        public IHttpActionResult Post(MetodoPagoDto metodoPagoDto)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    MetodoPagoService metodoPagoService = new MetodoPagoService(_context, claims);
                    return Ok(metodoPagoService.Add(metodoPagoDto));
                }
                catch (Exception ex)
                {
                    if (ex is MetodoPagoAlreadyExistsException || ex is MetodoPagoCantidadInvalidaException)
                        return BadRequest(ex.Message);
                    else
                        return InternalServerError(ex);
                }
            }
        }

        [PermissionAuthorize("EDITAR_METODOS_PAGO")]
        public IHttpActionResult Put(MetodoPagoDto metodoPagoDto)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    MetodoPagoService metodoPagoService = new MetodoPagoService(_context, claims);
                    return Ok(metodoPagoService.Edit(metodoPagoDto));
                }
                catch (Exception ex)
                {
                    if (ex is MetodoPagoAlreadyExistsException)
                        return BadRequest(ex.Message);
                    else if (ex is MetodoPagoNotFoundException)
                        return NotFound();
                    else
                        return InternalServerError(ex);
                }
            }
        }

        [PermissionAuthorize("ELIMINAR_METODOS_PAGO")]
        public IHttpActionResult Delete(MetodoPagoDto metodoPagoDto)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    MetodoPagoService metodoPagoService = new MetodoPagoService(_context, claims);
                    return Ok(metodoPagoService.Delete(metodoPagoDto));
                }
                catch (Exception ex)
                {
                    if (ex is MetodoPagoNotFoundException)
                        return NotFound();
                    else
                        return InternalServerError(ex);
                }
            }
        }

        [AllowAnonymous]
        public IHttpActionResult Get([FromUri] MetodoPagoRequestDto request)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    MetodoPagoService metodoPagoService = new MetodoPagoService(_context, claims);
                    return Ok(metodoPagoService.GetAll(request));
                }
                catch (Exception ex)
                {
                    return InternalServerError(ex);
                }
            }
        }


    }
}
