﻿using GestorClientes.Api.Providers;
using GestorClientes.Dominio.DAL;
using GestorClientes.Logic.Dto;
using GestorClientes.Logic.Dto.Requests;
using GestorClientes.Logic.Exceptions.Ciudad;
using GestorClientes.Logic.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace GestorClientes.Api.Controllers
{
    public class CiudadController : BaseController
    {
        [PermissionAuthorize("AGREGAR_CIUDADES")]
        public IHttpActionResult Post(CiudadDto ciudadDto)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    CiudadService ciudadService = new CiudadService(_context, claims);
                    return Ok(ciudadService.Add(ciudadDto));
                }
                catch (Exception ex)
                {
                    if (ex is CiudadDescripcionAlreadyExistsException)
                        return BadRequest(ex.Message);
                    else
                        return InternalServerError(ex);
                }
            }
        }

        [PermissionAuthorize("EDITAR_CIUDADES")]
        public IHttpActionResult Put(CiudadDto ciudadDto)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    CiudadService ciudadService = new CiudadService(_context, claims);
                    return Ok(ciudadService.Edit(ciudadDto));
                }
                catch (Exception ex)
                {
                    if (ex is CiudadDescripcionAlreadyExistsException)
                        return BadRequest(ex.Message);
                    else if (ex is CiudadNotFoundException)
                        return NotFound();
                    else
                        return InternalServerError(ex);
                }
            }
        }

        [PermissionAuthorize("ELIMINAR_CIUDADES")]
        public IHttpActionResult Delete(CiudadDto ciudadDto)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    CiudadService ciudadService = new CiudadService(_context, claims);
                    return Ok(ciudadService.Delete(ciudadDto));
                }
                catch (Exception ex)
                {
                    if (ex is CiudadNotFoundException)
                        return NotFound();
                    else
                        return InternalServerError(ex);
                }
            }
        }

        [AllowAnonymous]
        public IHttpActionResult Get([FromUri] CiudadRequestDto request)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    CiudadService ciudadService = new CiudadService(_context, claims);
                    return Ok(ciudadService.GetAll(request));
                }
                catch (Exception ex)
                {
                    return InternalServerError(ex);
                }
            }
        }


    }
}
