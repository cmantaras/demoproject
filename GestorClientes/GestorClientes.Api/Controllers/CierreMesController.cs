﻿using GestorClientes.Api.Providers;
using GestorClientes.Dominio.DAL;
using GestorClientes.Logic.Dto.Requests;
using GestorClientes.Logic.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace GestorClientes.Api.Controllers
{
    public class CierreMesController : BaseController
    {
        [AllowAnonymous]
        [Route("api/CierreMes/FinalizarMes")]
        [HttpPost]
        public IHttpActionResult FinalizarMes(FinalizarMesRequestDto fecha)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    CierreMesService cierreMesService = new CierreMesService(_context, claims);
                    return Ok(cierreMesService.FinalizarMes(fecha));
                }
                catch (Exception ex)
                {
                    return InternalServerError(ex);
                }
            }
        }
        [AllowAnonymous]
        [Route("api/CierreMes/GetCierresMes")]
        [HttpGet]
        public IHttpActionResult GetCierresMes()
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    CierreMesService cierreMes = new CierreMesService(_context, claims);
                    return Ok(cierreMes.GetCierresMes());
                }
                catch (Exception ex)
                {
                    return InternalServerError(ex);
                }
            }
        }

    }
}