﻿using GestorClientes.Api.Providers;
using GestorClientes.Dominio.DAL;
using GestorClientes.Logic.Exceptions.Permisos;
using GestorClientes.Logic.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using GestorClientes.Logic.Exceptions;

namespace GestorClientes.Api.Controllers
{
    public class PermisosController : BaseController
    {
        [PermissionAuthorize("VER_ROLES")]
        [Route("api/Permisos/Grupos")]
        public IHttpActionResult GetPermissions()
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    PermisosService permisosService = new PermisosService(_context, claims);
                    return Ok(permisosService.GetAllGroups());
                }
                catch (Exception ex)
                {
                    return InternalServerError(ex);
                }
            }
        }

        [PermissionAuthorize("EDITAR_ROLES")]
        public IHttpActionResult Post([FromUri] int rolId, int permisoId)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    PermisosService permisosService = new PermisosService(_context, claims);
                    permisosService.Add(rolId, permisoId);
                    return Ok();
                }
                catch (Exception ex)
                {
                    if (ex is PermisoAlreadyExistsInRolException || ex is PermisoNotFoundException)
                    {
                        return BadRequest(ex.Message);
                    }
                    return InternalServerError(ex);
                }
            }
        }


        [PermissionAuthorize("EDITAR_ROLES")]
        public IHttpActionResult Delete([FromUri] int rolId, int permisoId)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    PermisosService permisosService = new PermisosService(_context, claims);
                    permisosService.Delete(rolId, permisoId);
                    return Ok();
                }
                catch (Exception ex)
                {
                    if(ex is PermisoNotFoundInRolException || ex is PermisoNotFoundException)
                    {
                        return BadRequest(ex.Message);
                    }
                    return InternalServerError(ex);
                }
            }
        }
    }
}
