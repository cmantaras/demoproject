﻿using GestorClientes.Api.Providers;
using GestorClientes.Dominio.DAL;
using GestorClientes.Logic.Dto;
using GestorClientes.Logic.Dto.Requests;
using GestorClientes.Logic.Exceptions.Item;
using GestorClientes.Logic.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace GestorClientes.Api.Controllers
{
    public class ItemController : BaseController
    {
        [PermissionAuthorize("AGREGAR_ITEMS")]
        public IHttpActionResult Post(ItemDto itemDto)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    ItemService itemService = new ItemService(_context, claims);
                    return Ok(itemService.Add(itemDto));
                }
                catch (Exception ex)
                {
                    if (ex is ItemDescripcionAlreadyExistsException)
                        return BadRequest(ex.Message);
                    else
                        return InternalServerError(ex);
                }
            }
        }

        [PermissionAuthorize("EDITAR_ITEMS")]
        public IHttpActionResult Put(ItemDto itemDto)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    ItemService itemService = new ItemService(_context, claims);
                    return Ok(itemService.Edit(itemDto));
                }
                catch (Exception ex)
                {
                    if (ex is ItemDescripcionAlreadyExistsException)
                        return BadRequest(ex.Message);
                    else if (ex is ItemNotFoundException)
                        return NotFound();
                    else
                        return InternalServerError(ex);
                }
            }
        }

        [PermissionAuthorize("ELIMINAR_ITEMS")]
        public IHttpActionResult Delete(ItemDto itemDto)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    ItemService itemService = new ItemService(_context, claims);
                    return Ok(itemService.Delete(itemDto));
                }
                catch (Exception ex)
                {
                    if (ex is ItemNotFoundException)
                        return NotFound();
                    else
                        return InternalServerError(ex);
                }
            }
        }

        [AllowAnonymous]
        public IHttpActionResult Get([FromUri] ItemRequestDto request)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    ItemService itemService = new ItemService(_context, claims);
                    return Ok(itemService.GetAll(request));
                }
                catch (Exception ex)
                {
                    return InternalServerError(ex);
                }
            }
        }


    }
}
