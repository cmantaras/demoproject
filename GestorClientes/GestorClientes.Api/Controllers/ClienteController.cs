﻿using GestorClientes.Api.Providers;
using GestorClientes.Dominio.DAL;
using GestorClientes.Logic.Dto;
using GestorClientes.Logic.Dto.Requests;
using GestorClientes.Logic.Exceptions.Cliente;
using GestorClientes.Logic.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace GestorClientes.Api.Controllers
{
    public class ClienteController : BaseController
    {
        [PermissionAuthorize("AGREGAR_CLIENTES")]
        public IHttpActionResult Post(ClienteDto clienteDto)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    ClienteService clienteService = new ClienteService(_context, claims);
                    return Ok(clienteService.Add(clienteDto));
                }
                catch (Exception ex)
                {
                    if (ex is ClienteAlreadyExistsException)
                        return BadRequest(ex.Message);
                    else
                        return InternalServerError(ex);
                }
            }
        }

        [PermissionAuthorize("EDITAR_CLIENTES")]
        public IHttpActionResult Put(ClienteDto clienteDto)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    ClienteService clienteService = new ClienteService(_context, claims);
                    return Ok(clienteService.Edit(clienteDto));
                }
                catch (Exception ex)
                {
                    if (ex is ClienteAlreadyExistsException)
                        return BadRequest(ex.Message);
                    else if (ex is ClienteNotFoundException)
                        return NotFound();
                    else
                        return InternalServerError(ex);
                }
            }
        }

        [PermissionAuthorize("ELIMINAR_CLIENTES")]
        public IHttpActionResult Delete(ClienteDto clienteDto)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    ClienteService clienteService = new ClienteService(_context, claims);
                    return Ok(clienteService.Delete(clienteDto));
                }
                catch (Exception ex)
                {
                    if (ex is ClienteNotFoundException)
                        return NotFound();
                    else
                        return InternalServerError(ex);
                }
            }
        }

        [PermissionAuthorize("VER_CLIENTES")]
        [Route("api/Cliente/GetClientes")]
        [HttpPost]
        public IHttpActionResult GetClientes( ClienteRequestDto request)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    ClienteService clienteService = new ClienteService(_context, claims);
                    return Ok(clienteService.GetAll(request));
                }
                catch (Exception ex)
                {
                    return InternalServerError(ex);
                }
            }
        }

        [PermissionAuthorize("EDITAR_CLIENTES")]
        [Route("api/Cliente/AsignarProductos")]
        [HttpPost]
        public IHttpActionResult AsignarProductos(List<AsignarProductoRequestDto> asignarProductoRequestDto)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    ClienteService clienteService = new ClienteService(_context, claims);
                    return Ok(clienteService.AsignarProducto(asignarProductoRequestDto));
                }
                catch (Exception ex)
                {
                    return InternalServerError(ex);
                }
            }
        }

        [PermissionAuthorize("EDITAR_CLIENTES")]
        [Route("api/Cliente/AsignarItemsProducto")]
        [HttpPost]
        public IHttpActionResult AsignarItemsProducto([FromUri]int clienteProductoId, List<ClienteItemProductoDto> clienteItemProductoDto)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    ClienteService clienteService = new ClienteService(_context, claims);
                    return Ok(clienteService.AsignarItemsProducto(clienteProductoId, clienteItemProductoDto));
                }
                catch (Exception ex)
                {
                    return InternalServerError(ex);
                }
            }
        }


        [PermissionAuthorize("EDITAR_CLIENTES")]
        [Route("api/Cliente/ProductosAsignados")]
        [HttpGet]
        public IHttpActionResult ProductosAsignados([FromUri] ClienteProductosRequestDto clienteProductosRequestDto)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    ClienteService clienteService = new ClienteService(_context, claims);
                    return Ok(clienteService.ProductosAsignados(clienteProductosRequestDto));
                }
                catch (Exception ex)
                {
                    return InternalServerError(ex);
                }
            }
        }



        [PermissionAuthorize("EDITAR_CLIENTES")]
        [Route("api/Cliente/DeleteProductoCliente")]
        [HttpDelete]
        public IHttpActionResult DeleteProductoCliente(ClienteProductosRequestDto clienteProductoDto)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    ClienteService clienteService = new ClienteService(_context, claims);
                    return Ok(clienteService.DeleteProductoCliente(clienteProductoDto));
                }
                catch (Exception ex)
                {
                    if (ex is ClienteNotFoundException)
                        return NotFound();
                    else
                        return InternalServerError(ex);
                }
            }
        }



        [PermissionAuthorize("VER_CLIENTES")]
        [Route("api/Cliente/ProductosPendientePago")]
        [HttpPost]
        public IHttpActionResult ProductosPendientePago(AvisoPagoRequestDto clienteProductosRequestDto)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    ClienteService clienteService = new ClienteService(_context, claims);
                    return Ok(clienteService.AllProductosPendientesPago(clienteProductosRequestDto));
                }
                catch (Exception ex)
                {
                    return InternalServerError(ex);
                }
            }
        }

        [PermissionAuthorize("VER_CLIENTES")]
        [Route("api/Cliente/ProductosAvisoPago")]
        [HttpPost]
        public IHttpActionResult ProductosAvisoPago(AvisoPagoRequestDto clienteProductosRequestDto)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    ClienteService clienteService = new ClienteService(_context, claims);
                    return Ok(clienteService.ProductosAvisoPago(clienteProductosRequestDto));
                }
                catch (Exception ex)
                {
                    return InternalServerError(ex);
                }
            }
        }

        [PermissionAuthorize("VER_CLIENTES")]
        [Route("api/Cliente/ProductosPendientesPagoFecha")]
        [HttpPost]
        public IHttpActionResult ProductosPendientesPagoFecha(AvisoPagoRequestDto clienteProductosRequestDto)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    ClienteService clienteService = new ClienteService(_context, claims);
                    return Ok(clienteService.ProductosPendientesPagoFecha(clienteProductosRequestDto));
                }
                catch (Exception ex)
                {
                    return InternalServerError(ex);
                }
            }
        }





        [PermissionAuthorize("EDITAR_CLIENTES")]
        [Route("api/Cliente/GetProductosPagos")]
        [HttpGet]
        public IHttpActionResult GetProductosPagos()
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    ClienteService clienteService = new ClienteService(_context, claims);
                    return Ok(clienteService.GetProductosPagos());
                }
                catch (Exception ex)
                {
                    return InternalServerError(ex);
                }
            }
        }


    }
}
