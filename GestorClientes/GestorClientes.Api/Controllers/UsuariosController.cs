﻿using GestorClientes.Api.Controllers;
using GestorClientes.Api.Providers;
using GestorClientes.Dominio.DAL;
using GestorClientes.Logic.Dto;
using GestorClientes.Logic.Dto.Requests;
using GestorClientes.Logic.Exceptions.Usuarios;
using GestorClientes.Logic.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace GestorClientes.Api.Controllers
{
    public class UsuariosController : BaseController
    {
        [PermissionAuthorize("AGREGAR_USUARIOS")]
        public IHttpActionResult Post(UsuarioDto usuarioDto)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    UsuariosService usuariosService = new UsuariosService(_context, claims);
                    return Ok(usuariosService.Add(usuarioDto));
                }
                catch (Exception ex)
                {
                    if (ex is UsernameAlreadyExistsException)
                        return BadRequest(ex.Message);
                    else
                        return InternalServerError(ex);
                }
            }
        }

        [PermissionAuthorize("EDITAR_USUARIOS")]
        public IHttpActionResult Put(UsuarioDto usuarioDto)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    UsuariosService rolsService = new UsuariosService(_context, claims);
                    return Ok(rolsService.Edit(usuarioDto));
                }
                catch (Exception ex)
                {
                    if (ex is UsernameAlreadyExistsException)
                        return BadRequest();
                    else if (ex is UsuarioNotFoundException)
                        return NotFound();
                    else
                        return InternalServerError(ex);
                }
            }
        }

        [PermissionAuthorize("ELIMINAR_USUARIOS")]
        public IHttpActionResult Delete(UsuarioDto usuarioDto)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    UsuariosService usuariosService = new UsuariosService(_context, claims);
                    return Ok(usuariosService.Delete(usuarioDto));
                }
                catch (Exception ex)
                {
                    if (ex is UsuarioNotFoundException)
                        return NotFound();
                    else if (ex is UsuarioNotDeletableException)
                        return BadRequest(ex.Message);
                    else
                        return InternalServerError(ex);
                }
            }
        }

        [AllowAnonymous]
        public IHttpActionResult Get([FromUri] UsuarioRequestDto request)
        {
            using (GestorContext _context = new GestorContext())
            {
                try
                {
                    UsuariosService usuariosService = new UsuariosService(_context, claims);
                    return Ok(usuariosService.GetAll(request));
                }
                catch (Exception ex)
                {
                    return InternalServerError(ex);
                }
            }
        }
    }
}
