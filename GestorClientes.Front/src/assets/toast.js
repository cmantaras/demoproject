function createToast(message, type = 0) {
    let toastElList = [].slice.call(document.querySelectorAll('.toast'));

    let finded = false;
    let position = 0;
    if (!finded && !toastElList[2].classList.contains("show")) {
        finded = true;
        position = 2
    }
    if (!finded && !toastElList[1].classList.contains("show")) {
        finded = true;
        position = 1
    }
    if (!finded && !toastElList[0].classList.contains("show")) {
        finded = true;
        position = 0
    }

    let color;
    let textColor;
    switch (type) {
        case 0:
            color = 'bg-light';
            textColor = 'text-dark';
            break;
        case 1:
            color = 'bg-danger';
            textColor = 'text-light';
            break;
        case 2:
            color = 'bg-success';
            textColor = 'text-light';
            break;
    }

    let classL = document.getElementById("toast-" + position);
    classL.classList.remove('bg-success', 'bg-danger', 'bg-light', 'text-light', 'text-dark');
    classL.classList.add(color);
    classL.classList.add(textColor);
    let body = document.getElementById("message-" + position);
    body.innerHTML = message;
    let alert = new bootstrap.Toast(toastElList[position]);

    alert.show();
}