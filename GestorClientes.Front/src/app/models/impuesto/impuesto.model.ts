import { DecimalPipe } from "@angular/common";

export class ImpuestoModel {
  Id: number;
  Descripcion : string;
  Porcentaje : string;
  Activo : boolean;

  constructor() { }

}
