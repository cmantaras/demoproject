import { MonedaModel } from "../moneda/moneda.model";
import { CategoriaModel } from "../categoria/categoria.model";
import { ClienteModel } from "./cliente.model";
import { ClienteProdDeudaModel } from "./clienteProdDeuda.model";
import { ClienteProductoModel } from "./productoCliente.model";

export class AvisoPagoModel {

  Cliente : ClienteModel;
  Productos : ClienteProdDeudaModel [];

  constructor() {
    this.Cliente= new ClienteModel();
    this.Productos = [];
  }

}
