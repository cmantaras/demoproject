import { ZonaComponent } from "src/app/components/configuracion/zona/zona.component";
import { CiudadModel } from "../ciudad/ciudad.model";
import { ZonaModel } from "../zona/zona.model";
import { ProductoModel } from "../producto/producto.model";
import { ClienteProductoItemModel } from "./productoItemCliente.model";

export class ClienteModel {

  Id : Number;
  Hte : string;
  Identificador : string;
  Nombre : string;
  Apellido : string;
  Telefono : string;
  Direccion : string;
  FechaNacimiento : string;
  FechaHasta : string;
  CiudadId : number;
  ZonaId : number;
  Limit : number;
  Offset : number;
  EstadoActualId : number;
  Estado : EstadoClienteModel;
  Ciudad : CiudadModel;
  Zona : ZonaModel;
  Servicios : string;
  ClienteViejo : boolean;


  constructor() {
    this.Ciudad = new CiudadModel();
    this.Zona = new ZonaModel();
    this.Estado= new EstadoClienteModel();
  }






}


export class EstadoClienteModel {
  Id: number;
  Descripcion : string;
  Tipo : number;

  constructor() {

  }

}
