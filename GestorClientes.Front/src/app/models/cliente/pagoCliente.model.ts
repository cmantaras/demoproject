import { DatepickerViewModel } from "@ng-bootstrap/ng-bootstrap/datepicker/datepicker-view-model";
import { MonedaModel } from "../moneda/moneda.model";
import { CategoriaModel } from "../categoria/categoria.model";
import { ClienteModel } from "./cliente.model";
import { ClienteProdDeudaModel } from "./clienteProdDeuda.model";
import { ClienteProductoModel } from "./productoCliente.model";

export class PagoClienteModel {

  Id : number;
  Cliente : ClienteModel;
  MontoTotal : number;
  Fecha : string;
  Detalle : string;
  CanDelete : boolean;
  constructor() {
    this.Cliente = new ClienteModel();
  }

}

