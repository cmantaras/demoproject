import { ClienteProductoModel } from './productoCliente.model';
import { ZonaComponent } from "src/app/components/configuracion/zona/zona.component";
import { CiudadModel } from "../ciudad/ciudad.model";
import { ItemModel } from "../item/item.model";
import { MetodoPagoModel } from "../pagos-metodo/metodo-pago.model";
import { ProductoModel } from "../producto/producto.model";
import { ProductoPrecioModel } from "../producto/productoPrecio.model";
import { ZonaModel } from "../zona/zona.model";

export class CuotaPagaModel {

  Id : Number;
  ClienteId : Number;
  Desde : string;
  ProductoPrecio : ClienteProductoModel;
  MetodoPago : MetodoPagoModel;
  Items : ItemModel [];


  constructor() {
    this.Items = [];
  }

}