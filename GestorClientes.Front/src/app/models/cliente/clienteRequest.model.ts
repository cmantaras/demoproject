import { NumberSymbol } from "@angular/common";
import { ZonaComponent } from "src/app/components/configuracion/zona/zona.component";
import { CiudadModel } from "../ciudad/ciudad.model";
import { ZonaModel } from "../zona/zona.model";

export class ClienteRequestModel {

  Id : Number;
  Identificador : string;
  Nombre : string;
  Apellido : string;
  Telefono : string;
  Direccion : string;
  CiudadId : number;
  ZonaId : number;
  Limit : number;
  Offset : number;
  ClienteEstadoId : number;



  constructor() {


  }

}
