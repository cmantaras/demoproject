import { throwError } from 'rxjs';
import { ClienteModel } from './cliente.model';
import { ClienteProductoModel } from './productoCliente.model';
import { ZonaComponent } from "src/app/components/configuracion/zona/zona.component";
import { CiudadModel } from "../ciudad/ciudad.model";
import { ItemModel } from "../item/item.model";
import { MetodoPagoModel } from "../pagos-metodo/metodo-pago.model";
import { ProductoModel } from "../producto/producto.model";
import { ProductoPrecioModel } from "../producto/productoPrecio.model";
import { ZonaModel } from "../zona/zona.model";

export class FacturaPagoModel {

    IdPago : number;
    Cliente : ClienteModel;
    Detalles: {};
    Titulo?: string;
    Comentario?: string;
    FechaHora : Date;
    Total : number;
    AplicaMoras :boolean;



  constructor() {
   this.Cliente = new ClienteModel();
   this.Detalles = {};
   this.Comentario = 'Ahora podés pedir tu catálogo online a través de whatsapp! al 099 099 099'
   this.Titulo = 'TEVECA'
  }


}