import { DateSelectionModelChange } from "@angular/material/datepicker";
import { ZonaComponent } from "src/app/components/configuracion/zona/zona.component";
import { CiudadModel } from "../ciudad/ciudad.model";
import { ZonaModel } from "../zona/zona.model";
import { ClienteModel } from "./cliente.model";
import { ClienteProductoModel } from "./productoCliente.model";

export class ClienteProdDeudaModel {

  ProductoId : String;
  Cliente : ClienteModel;
  CuotasProducto :  CuotaProductoModel[];
  TotalDeuda : number;

  constructor() {
    this.CuotasProducto = [];
    this.Cliente = new ClienteModel();

  }
}

export class CuotaProductoModel {

  Importe : number;
  Cuota : number;
  Deuda : boolean;
  FechaPago? : Date;
  ClienteProducto : ClienteProductoModel;
  Pagar :boolean = false;
  MoraCuota : MoraCuotaModel;
  CierreMes : CierreMesModel;
  Id : number;
  Show : boolean;

  constructor() {
    this.ClienteProducto = new ClienteProductoModel();
    this.CierreMes = new CierreMesModel();
    this.MoraCuota = new MoraCuotaModel();
  }

}
export class MoraCuotaModel {
  Id: number;
  Descripcion : string;
  Porcentaje : number;

  constructor() {
  }
}


export class CierreMesModel {
  Fecha: string;

  constructor() {
  }
}
