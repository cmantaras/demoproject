import { ClienteModel } from 'src/app/models/cliente/cliente.model';
export class DeudorModel {
    Cliente: ClienteModel;
    FechaUltimoPago : Date;
    DiasDesdePago: number;
    TotalDeuda : number;
  
    constructor() { 
        this.Cliente = new ClienteModel();
    }
  
  }
  