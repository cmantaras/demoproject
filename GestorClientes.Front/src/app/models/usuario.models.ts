import { Token } from '@angular/compiler';
import { RolModel } from './rol.models';

export class UsuarioModel {
  Id : number;
  Username : string;
  Nombre : string;
  Apellido : string;
  Email : string;
  IsAdmin : boolean;
  RolId : number;

  Rol : RolModel;

  constructor() {
    this.Rol = new RolModel();
  }
}
