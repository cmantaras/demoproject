import { MonedaModel } from "../moneda/moneda.model";
import { ProveedorModel } from "./proveedor.model";

export class ProveedorRequestModel {

  RUT : string;
  Nombre : string;

  Offset : number;
  Limit : number;

  Todos?: boolean;


  constructor() { }

}
