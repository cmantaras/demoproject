import { CiudadModel } from "../ciudad/ciudad.model";

export class ProveedorModel {
  Nombre : string;
  Id: number;
  RazonSocial? : string;
  RUT? : string;
  Direccion : string;
  Ciudad : string;
 // Ciudad : CiudadModel;
  Telefono : string;
  Correo : string;
  Contacto : string;
  ContactoTelefono : string;
  ContactoCorreo : string ;
  Color: string;
  Activo : boolean;
 // CiudadDescripcion : string;

  constructor() {
   }

}
