import { StringDecoder } from "string_decoder";
import { ItemValorModel } from "../item/itemValor.model";
import { MarcaModel } from "../marca/marca.model";
import { MetodoPagoModel } from "../pagos-metodo/metodo-pago.model";
import { ProductoModel } from "./producto.model";
import { ProductoPrecioModel } from "./productoPrecio.model";

export class AsignarProductoModel {
  ClienteId?: string;
  ProductoId? : string;
  MetodoPagoId? : string;
  MetodoId : string;
  Desde : string;
  Hasta : string;

  Items?: ItemValorModel [];
  Producto : ProductoModel;
  Metodo : MetodoPagoModel;



  constructor() {

    this.Producto = new ProductoModel();

  }

}
