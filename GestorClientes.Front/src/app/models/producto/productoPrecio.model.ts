import { ThrowStmt } from "@angular/compiler";
import { MetodoPagoComponent } from "src/app/components/configuracion/metodo-pago/metodo-pago.component";
import { ImpuestoModel } from "../impuesto/impuesto.model";
import { MarcaModel } from "../marca/marca.model";
import { MonedaModel } from "../moneda/moneda.model";
import { MetodoPagoModel } from "../pagos-metodo/metodo-pago.model";
import { ProductoModel } from "./producto.model";

export class ProductoPrecioModel {
  Id: number;
  Precio : number;
  Desde : string;
  Impuesto : ImpuestoModel;
  Moneda : MonedaModel;
  Metodos : MetodoPagoModel[];
  Color : boolean;
  ProductoId : number;


  Producto : ProductoModel;
  MetodosIds : number [];
  ImpuestoId : number;
  MonedaId : number;



  constructor() {
    this.MetodosIds = [];
    this.Impuesto = new ImpuestoModel();
    this.Moneda = new MonedaModel();


  }

}
