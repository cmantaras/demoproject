import { MarcaModel } from "../marca/marca.model";
import { ProductoPrecioModel } from "./productoPrecio.model";

export class ProductoModel {
  Id: number;
  Codigo : string;
  Descripcion : string;
  MarcaId: string;
  Marca?: MarcaModel;
  PrecioActual: ProductoPrecioModel;
  Color : boolean;
  Servicio : boolean;




  constructor() {
    this.Marca = new MarcaModel();
    this.PrecioActual = new ProductoPrecioModel();
  }

}
