import { MonedaModel } from "../moneda/moneda.model";
import { CategoriaModel } from "../categoria/categoria.model";
import { ProveedorModel } from "../proveedor/proveedor.model";

export class EgresoModel {
  Id: number;
  Importe : number;
  EsCheque : boolean;
  FechaCheque? : string;
  FechaEgreso: string;
  Descripcion? : string;

  MonedaId : number;
  ProveedorId: number;
  CategoriaId : number;

  Moneda: MonedaModel;
  Categoria : CategoriaModel;
  Proveedor : ProveedorModel;

  constructor() {
    this.Moneda= new MonedaModel();
    this.Categoria = new CategoriaModel();
    this.Proveedor = new ProveedorModel();

  }

}
