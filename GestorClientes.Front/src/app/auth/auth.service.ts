import { Injectable } from '@angular/core';


@Injectable()
export class AuthService {

    private isloggedIn: boolean;
    private userName:string;


    constructor() {

    }
g
    public getToken(): string {
      return localStorage.getItem('jwt');
    }

    isUserLoggedIn(): boolean {
        var token = this.getToken();
        if(token){
          return true;
        }
        else { return false; }

    }

    isAdminUser():boolean {
        if (this.userName=='Admin') {
            return true;
        }
        return false;
    }

      logoutUser(): void{
         localStorage.removeItem("jwt");
    }

}
