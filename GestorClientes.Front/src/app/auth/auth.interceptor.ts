import { HttpErrorResponse, HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, finalize } from 'rxjs/operators';



import { AuthService } from './auth.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private router: Router ){

  }

  //Por cada solicitud al server se envia token

  auth : AuthService = new AuthService()

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const token: string = localStorage.getItem('jwt');

    let request = req;
    if (token) {
      req = req.clone({
        headers: req.headers.set('Authorization', `Bearer ${token}`)
      });

    }

      return next.handle(req).pipe(
          catchError(err => {
            if (err instanceof HttpErrorResponse) {

              if (err.status === 401 || err.status === 403) {
                  // Invalidate user session and redirect to login/home
                  this.auth.logoutUser();
                  this.router.navigate(["/login"]);
              }
              // return the error back to the caller
              return throwError(err);
            }
          }),
          finalize(() => {

          })
        );

  }
}
