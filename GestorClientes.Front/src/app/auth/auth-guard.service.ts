import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { Subject } from 'rxjs';

@Injectable()
export class AuthGuard implements CanActivate {

  public onLoginChange = new Subject();
  
  constructor(private router: Router) {
  }


  canActivate() {
    const token = localStorage.getItem("jwt");

    if (token){
      return true;
    }
    this.router.navigate(["/login"]);

    return false;
  }
}
