import { Component, OnDestroy, OnInit } from '@angular/core';
import { takeUntil, tap } from 'rxjs/operators';
import { AuthGuard } from './auth/auth-guard.service';
import { AuthService } from './auth/auth.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private authService: AuthGuard){

  }

   loggedIn (){
    return this.authService.canActivate();
  }

  title = 'Gestor Clientes';


}
