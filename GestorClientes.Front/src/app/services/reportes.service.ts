import { IngresoEgresoFilter } from './../models/Reportes/ingresosEgresosFilter.model';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { map } from 'rxjs/operators';
import { NumberLiteralType } from 'typescript';
import * as myGlobals from '../global.component';
import { DeudoresFilter } from '../models/Reportes/deudoresFilter.model';
import { ClienteReporteFilterModel } from '../models/Reportes/clienteReporteFilter.model';


@Injectable({
  providedIn: 'root'
})

export class ReportesService {

  constructor(private http: HttpClient) {


  }
  //Querys to Api
  private url = myGlobals.BASE_URL + '/reportes';


  getAllPagos (request : IngresoEgresoFilter) {

    var httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    return this.http.post( `${this.url}/GetAllPagos`, request, httpOptions);
  }

  getAllEgresos(request : IngresoEgresoFilter) {

    var httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    return this.http.post( `${this.url}/GetAllEgresos`, request, httpOptions);
  }
  getEgresosByCierre() {

    var httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    return this.http.get( `${this.url}/GetEgresosByCierre`, httpOptions);
  }
  getIngresosByCierre() {

    var httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    return this.http.get( `${this.url}/GetIngresosByCierre`, httpOptions);
  }

  getDeudores(request : DeudoresFilter) {

    var httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    return this.http.post( `${this.url}/deudores`,request, httpOptions);
  }


  getBirthdays() {

    var httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    return this.http.get( `${this.url}/getbirthdays`, httpOptions);
  }

  getFinContratos() {

    var httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    return this.http.get( `${this.url}/getfincontratos`, httpOptions);
  }

  getClientes(clienteReporteFilterModel : ClienteReporteFilterModel) {

    var httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    return this.http.post( `${this.url}/getclientes`,clienteReporteFilterModel, httpOptions);
  }
  getCheques() {

    var httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    return this.http.get( `${this.url}/getCheques`, httpOptions);
  }


}

