import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { ZonaModel } from '../models/zona/zona.model';
import * as myGlobals from '../global.component';


@Injectable({
  providedIn: 'root'
})

export class ZonasService {

  constructor(private http: HttpClient) { }

  //URL to Api
  private url = myGlobals.BASE_URL + '/zona';


  getZonas () {


     return this.http.get(`${this.url}`)
               .pipe( map ( data => {
                 data['zonas'];
               return data;
               }) );
  }


  addZona (zona: ZonaModel) {

    var httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    return this.http.post(this.url, zona, httpOptions);
  }

  updateZona (zona : ZonaModel) {

    var httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    return this.http.put(this.url,zona, httpOptions);
  }

  deleteZona (zona: ZonaModel) {

    let data = JSON.stringify(zona);

     var httpOptions = {
       headers: new HttpHeaders({'Content-Type': 'application/json'}),
       body: data
     }

    return this.http.delete(this.url, httpOptions);
  }

}
