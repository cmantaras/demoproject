import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { CiudadModel } from '../models/ciudad/ciudad.model';
import * as myGlobals from '../global.component';


@Injectable({
  providedIn: 'root'
})

export class CiudadesService {

  constructor(private http: HttpClient) { }

  //URL to Api
  private url = myGlobals.BASE_URL + '/ciudad';


  getCiudades () {


     return this.http.get(`${this.url}`)
               .pipe( map ( data => {
                 data['ciudades'];
               return data;
               }) );
  }


  addCiudad (ciudad: CiudadModel) {

    var httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    return this.http.post(this.url, ciudad, httpOptions);
  }

  updateCiudad (ciudad : CiudadModel) {

    var httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    return this.http.put(this.url,ciudad, httpOptions);
  }

  deleteCiudad (ciudad: CiudadModel) {

    let data = JSON.stringify(ciudad);

     var httpOptions = {
       headers: new HttpHeaders({'Content-Type': 'application/json'}),
       body: data
     }

    return this.http.delete(this.url, httpOptions);
  }

}
