import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { ItemModel } from '../models/item/item.model';
import * as myGlobals from '../global.component';


@Injectable({
  providedIn: 'root'
})

export class ItemsService {

  constructor(private http: HttpClient) { }

  //URL to Api
  private url = myGlobals.BASE_URL + '/item';


  getItems () {
     return this.http.get(this.url)
               .pipe( map ( data => {
                 data['items'];
               return data;
               }) );
  }


  addItem (item: ItemModel) {

    var httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    return this.http.post(this.url, item, httpOptions);
  }

  updateItem (item : ItemModel) {

    var httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    return this.http.put(this.url,item, httpOptions);
  }

  deleteItem (item: ItemModel) {

    let data = JSON.stringify(item);

     var httpOptions = {
       headers: new HttpHeaders({'Content-Type': 'application/json'}),
       body: data
     }

    return this.http.delete(this.url, httpOptions);
  }

}
