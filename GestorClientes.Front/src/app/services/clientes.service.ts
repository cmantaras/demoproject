import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { ClienteModel } from '../models/cliente/cliente.model';
import * as myGlobals from '../global.component';
import { ClienteRequestModel } from '../models/cliente/clienteRequest.model';
import { ClienteProductoModel } from '../models/cliente/productoCliente.model';
import { NumberInput } from '@angular/cdk/coercion';


@Injectable({
  providedIn: 'root'
})

export class ClientesService {

  constructor(private http: HttpClient) { }

  //URL to Api
  private url = myGlobals.BASE_URL + '/cliente';


  getClientes (cliente?  : ClienteRequestModel) {


     return this.http.post(`${this.url}/GetClientes`, cliente)
               .pipe( map ( data => {
                 data['clientees'];
               return data;
               }) );
  }


  addCliente (cliente: ClienteModel) {

    var httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    return this.http.post(this.url, cliente, httpOptions);
  }

  updateCliente (cliente : ClienteModel) {

    var httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    return this.http.put(this.url,cliente, httpOptions);
  }

  deleteCliente (cliente: ClienteModel) {

    let data = JSON.stringify(cliente);

     var httpOptions = {
       headers: new HttpHeaders({'Content-Type': 'application/json'}),
       body: data
     }

    return this.http.delete(this.url, httpOptions);
  }

  getEstadosClientes () {

    return this.http.get(`${myGlobals.BASE_URL}/EstadoCliente`)
    .pipe( map ( data => {
      data['clientees'];
    return data;
    }) );
 }


  getProductosByClienteId (clienteId) {

    return this.http.get(`${this.url}/ProductosAsignados?ClienteId=${clienteId}`)
    .pipe( map ( data => {
      data['Productos'];
    return data;
    }) );
  }

  getProductosPendientesPago (request : any) {

    return this.http.post(`${this.url}/ProductosPendientepago`, request )
    .pipe( map ( data => {
      data['Productos'];
    return data;
    }));
  }
  getProductosPendientesPagoFecha (request : any) {

    return this.http.post(`${this.url}/ProductosPendientesPagoFecha`, request )
    .pipe( map ( data => {
      data['Productos'];
    return data;
    }));
  }



  getProductosAvisoPago (request : any) {

    return this.http.post(`${this.url}/ProductosAvisoPago`, request )
    .pipe( map ( data => {
      data['Productos'];
    return data;
    }));
  }


  getAvisosPago (clientesId? : Number[]) {

    return this.http.post(`${this.url}/GetAvisosPago`, clientesId)
    .pipe( map ( data => {
      data['Productos'];
    return data;
    }));
  }
  getProductosPagos () {

    return this.http.get(`${this.url}/GetProductosPagos`)
    .pipe( map ( data => {
      data['Productos'];
    return data;
    }) );
  }


  asignarProductos (productos : any) {
    var httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    return this.http.post( `${this.url}/AsignarProductos`, productos, httpOptions);
  }

  asignarItemsProducto (clienteProductoId : number ,clienteProducto : any) {
    var httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    return this.http.post( `${this.url}/AsignarItemsProducto?clienteproductoid=${clienteProductoId}`, clienteProducto, httpOptions);
  }



  deleteProductoCliente (producto : ClienteProductoModel) {

    let data = {
      ClienteId : producto.ClienteId,
      ProductoId : producto.ProductoPrecio.Id
    }

     var httpOptions = {
       headers: new HttpHeaders({'Content-Type': 'application/json'}),
       body: data
     }
    return this.http.delete(this.url+ '/DeleteProductoCliente', httpOptions);
  }




}
