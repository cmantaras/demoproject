import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { map } from 'rxjs/operators';
import { NumberLiteralType } from 'typescript';
import { GrupoPermisosModel } from '../models/grupoPermisos.models';
import * as myGlobals from '../global.component';


@Injectable({
  providedIn: 'root'
})

export class PermisosService {

  constructor(private http: HttpClient) {


  }

  //Querys to Api
  private url = myGlobals.BASE_URL + '/Permisos/Grupos';

  // getQuery ( query: string ) {
  //   const url = `${ query }`;
  //   return this.http.get(url);
  // }

  // postQuery ( query: string, permiso : any ) {
  //   const url = `baseUrl${ query }`;
  //   return this.http.post(url, permiso );
  // }

 getPermisos () {

     return this.http.get(this.url)
               .pipe( map ( data => {
                 data['permisos'];
               return data;
               }) );
   }


  addPermiso (rolId: number, permisoId: number ) {

    var httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
   // &permisoId=${permisoId} ?rolId=${rolId}
    return this.http.post(`${myGlobals.BASE_URL}/Permisos?rolId=${rolId}&permisoId=${permisoId}`, httpOptions);
  }

  updatePermiso (permiso) {

    var httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    return this.http.put(this.url,permiso, httpOptions);
  }

  deletePermiso (rolId: number, permisoId: number ) {

    var httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
   // &permisoId=${permisoId} ?rolId=${rolId}
    return this.http.delete(`${myGlobals.BASE_URL}/Permisos?rolId=${rolId}&permisoId=${permisoId}`, httpOptions);

  }

}
