import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { EgresoModel } from '../models/egreso/egreso.model';
import * as myGlobals from '../global.component';
import { EgresosFilter } from '../models/egreso/egresosFilter.model';


@Injectable({
  providedIn: 'root'
})

export class EgresosService {

  constructor(private http: HttpClient) { }

  //URL to Api
  private url = myGlobals.BASE_URL + '/egreso';


  getEgresos (request : EgresosFilter) {


     return this.http.get(`${this.url}?IdCategoria=${request.IdCategoria}&IdProveedor=${request.IdProveedor}&Offset=${request.Offset}&Limit=${request.Limit}&EsCheque=${request.EsCheque}`)
               .pipe( map ( data => {
                 data['egresos'];
               return data;
               }) );
  }

  GetEgresosByCierre () {


    return this.http.get(`${this.url}/GetEgresosByCierre`)
              .pipe( map ( data => {
                data['egresos'];
              return data;
              }) );
 }



  addEgreso (egreso: EgresoModel) {

    var httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    return this.http.post(this.url, egreso, httpOptions);
  }

  updateEgreso (egreso : EgresoModel) {

    var httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    return this.http.put(this.url,egreso, httpOptions);
  }

  deleteEgreso (egreso: EgresoModel) {

    let data = JSON.stringify(egreso);

     var httpOptions = {
       headers: new HttpHeaders({'Content-Type': 'application/json'}),
       body: data
     }

    return this.http.delete(this.url, httpOptions);
  }


}
