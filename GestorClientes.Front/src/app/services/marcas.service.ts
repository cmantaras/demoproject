import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { MarcaModel } from '../models/marca/marca.model';
import * as myGlobals from '../global.component';


@Injectable({
  providedIn: 'root'
})

export class MarcasService {

  constructor(private http: HttpClient) { }

  //URL to Api
  private url = myGlobals.BASE_URL + '/marca';


  getMarcas () {


     return this.http.get(`${this.url}`)
               .pipe( map ( data => {
                 data['Marcas'];
               return data;
               }) );
  }


  addMarca (marca: MarcaModel) {

    var httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    return this.http.post(this.url, marca, httpOptions);
  }

  updateMarca (marca : MarcaModel) {

    var httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    return this.http.put(this.url,marca, httpOptions);
  }

  deleteMarca (marca: MarcaModel) {

    let data = JSON.stringify(marca);

     var httpOptions = {
       headers: new HttpHeaders({'Content-Type': 'application/json'}),
       body: data
     }

    return this.http.delete(this.url, httpOptions);
  }

}
