import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { map } from 'rxjs/operators';
import { NumberLiteralType, StringLiteralLike } from 'typescript';

import * as myGlobals from '../global.component';
import { faPassport } from '@fortawesome/free-solid-svg-icons';
import { UserRecoverModel } from '../models/userRecover.model';



@Injectable({
  providedIn: 'root'
})

export class AuthService {
  

  constructor(private http: HttpClient) {


  }

  //Querys to Api
  private url = myGlobals.BASE_URL;

  authentication (user: string, pass:string ) {

    const body = 'grant_type=password&username='+user+'&password='+ pass;

    var httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded'})
    }

    return this.http.post(this.url+'/token', body, httpOptions);
    }


  resetPass (username: string ) {

    //Envia mail

    var httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json; charset=utf-8'})
    }

    return this.http.post(`${this.url}/account/reset_password?username=${username}`, httpOptions);
    }

    validateToken (username: string, token : StringLiteralLike ) {

      var httpOptions = {
        headers: new HttpHeaders({'Content-Type': 'application/json; charset=utf-8'})
      }

      return this.http.post(`${this.url}/account/valid_token?username=${username}&token=${token}`, httpOptions);
      }

      changePassword (user : UserRecoverModel ) {

        var httpOptions = {
          headers: new HttpHeaders({'Content-Type': 'application/json; charset=utf-8'})
        }

        return this.http.post(`${this.url}/account/change_password`, user, httpOptions);
        }

  // this.http.post("http://localhost:50301/api/token", body, {
  //   headers: new HttpHeaders({
  //     "Content-Type": "application/x-www-form-urlencoded"
  //   })


}
