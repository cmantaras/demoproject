import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { ImpuestoModel } from '../models/impuesto/impuesto.model';
import * as myGlobals from '../global.component';


@Injectable({
  providedIn: 'root'
})

export class ImpuestosService {

  constructor(private http: HttpClient) { }

  //URL to Api
  private url = myGlobals.BASE_URL + '/impuesto';


  getImpuestos () {
     return this.http.get(this.url)
               .pipe( map ( data => {
                 data['impuestos'];
               return data;
               }) );
  }


  addImpuesto (impuesto: ImpuestoModel) {

    var httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    return this.http.post(this.url, impuesto, httpOptions);
  }

  updateImpuesto (impuesto : ImpuestoModel) {

    var httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    return this.http.put(this.url,impuesto, httpOptions);
  }

  deleteImpuesto (impuesto: ImpuestoModel) {

    let data = JSON.stringify(impuesto);

     var httpOptions = {
       headers: new HttpHeaders({'Content-Type': 'application/json'}),
       body: data
     }

    return this.http.delete(this.url, httpOptions);
  }

}
