import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import * as myGlobals from '../global.component';
import { CategoriaModel } from "../models/categoria/categoria.model";


@Injectable({
  providedIn: 'root'
})
export class CategoriaService {
  constructor(private http: HttpClient) {}

  //URL to Api
  private url = myGlobals.BASE_URL + '/categoria';

  //Servicios categorias

  getCategorias() {
    return this.http.get(myGlobals.BASE_URL + '/categoria').pipe(
      map((data) => {
        data['Categorias'];
        return data;
      })
    );
  }
  getCategoriasActivasInactivas () {

    return this.http.get(`${this.url}/GetAllCategorias`)
              .pipe( map ( data => {
                data['Categorias'];
              return data;
              }) );
 }

  addCategoria(categoria: CategoriaModel) {
    var httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
    };
    return this.http.post(
      myGlobals.BASE_URL + '/categoria',
      categoria,
      httpOptions
    );
  }

  updateCategoria(categoria: CategoriaModel) {
    var httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
    };
    return this.http.put(
      myGlobals.BASE_URL + '/categoria',
      categoria,
      httpOptions
    );
  }

  deleteCategoria(categoria: CategoriaModel) {
    let data = JSON.stringify(categoria);

    var httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
      body: data,
    };
    

    return this.http.delete(
      myGlobals.BASE_URL + '/categoria',
      httpOptions
    );
  }
}