import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { ProductoPrecioModel } from '../models/producto/productoPrecio.model';
import * as myGlobals from '../global.component';
import { ProductoModel } from '../models/producto/producto.model';
import { NumberInput } from '@angular/cdk/coercion';
import { ProductoComponent } from '../components/configuracion/producto/producto.component';


@Injectable({
  providedIn: 'root'
})

export class ProductosService {

  constructor(private http: HttpClient) { }

  //URL to Api
  private url = myGlobals.BASE_URL + '/producto';


  getProductos () {

     return this.http.get(`${this.url}`)
               .pipe( map ( data => {
                 data['productos'];
               return data;
               }) );
  }
  getProductosFiltrados (productoDescripcion : string) {

    return this.http.get(`${this.url}/ProductosFiltrados?productoDescripcion=${productoDescripcion}`)
              .pipe( map ( data => {
                data['productos'];
              return data;
              }) );
 }

  addProducto (producto: ProductoModel) {

    var httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    return this.http.post(this.url, producto, httpOptions);
  }

  updateProducto (producto : ProductoModel) {

    var httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    return this.http.put(this.url,producto, httpOptions);
  }

  deleteProducto (producto: ProductoModel) {

    let data = JSON.stringify(producto);

     var httpOptions = {
       headers: new HttpHeaders({'Content-Type': 'application/json'}),
       body: data
     }

    return this.http.delete(this.url, httpOptions);
  }



  // Servicios de producto precios
  getPrecios (producto : ProductoModel){

    let prod = {
      ProductoId : producto.Id
    }
    return this.http.post(`${myGlobals.BASE_URL}/ProductoPrecio/GetPrecios`,prod)
    .pipe( map ( data => {
      data['productos'];
    return data;
    }) );
  }

  addProductoPrecio (productoPrecio: ProductoPrecioModel) {

    var httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    return this.http.post(`${myGlobals.BASE_URL}/ProductoPrecio`, productoPrecio, httpOptions);
  }


  updateProductoPrecio (productoPrecio : ProductoPrecioModel) {

    var httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    return this.http.put(`${myGlobals.BASE_URL}/ProductoPrecio`,productoPrecio, httpOptions);
  }

  deleteProductoPrecio (productoPrecio: ProductoPrecioModel) {

    let data = JSON.stringify(productoPrecio);

     var httpOptions = {
       headers: new HttpHeaders({'Content-Type': 'application/json'}),
       body: data
     }

    return this.http.delete(`${myGlobals.BASE_URL}/ProductoPrecio`, httpOptions);
  }

  addMetodosToProductoPrecio ( metodos : number [], productoPrecioId : number ){

    var body = {
      MetodosId : metodos,
      ProductoPrecioId : productoPrecioId
    }
    var httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    return this.http.post(`${myGlobals.BASE_URL}/ProductoPrecio/AddMetodos`, body, httpOptions);


  }

  getProductoPrecioById (ProductoPrecioId : number){


    return this.http.get(`${myGlobals.BASE_URL}/ProductoPrecio/GetProductoPrecioById?ProductoPrecioId=${ProductoPrecioId}`)
    .pipe( map ( data => {
      data['productos'];
    return data;
    }) );
  }
  deleteMetodoPago (productoPrecioId: number, metodoPagoId :number) {


     var httpOptions = {
       headers: new HttpHeaders({'Content-Type': 'application/json'}),


     }

    return this.http.delete(`${myGlobals.BASE_URL}/ProductoPrecio/DeleteMetodo?ProductoPrecioId=${productoPrecioId}&MetodoId=${metodoPagoId}`, httpOptions);
  }

}
