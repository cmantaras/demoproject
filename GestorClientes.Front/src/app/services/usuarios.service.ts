import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as myGlobals from '../global.component';
import { UsuarioModel } from '../models/usuario.models';


@Injectable({
  providedIn: 'root'
})
export class UsuariosService {

  constructor(private http: HttpClient) {


  }

  //Querys to Api
  private url = myGlobals.BASE_URL + '/Usuarios';

  // getQuery ( query: string ) {
  //   const url = `${ query }`;
  //   return this.http.get(url);
  // }

  // postQuery ( query: string, Usuario : any ) {
  //   const url = `baseUrl${ query }`;
  //   return this.http.post(url, Usuario );
  // }

 getUsuarios () {

     return this.http.get(this.url)
               .pipe( map ( data => {
                 data['Usuarios'];
               return data;
               }) );
   }


  addUsuario (Usuario: UsuarioModel) {

    var httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    return this.http.post(this.url, Usuario, httpOptions);
  }

  updateUsuario (Usuario) {


    var httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    return this.http.put(this.url,Usuario, httpOptions);
  }

  deleteUsuario (Usuario) {

    let data = JSON.stringify(Usuario);

     var httpOptions = {
       headers: new HttpHeaders({'Content-Type': 'application/json'}),
       body: data
     }

    return this.http.delete(this.url, httpOptions);

  }
    // public user: Usuario = new Usuario();

    // private userSubject = new BehaviorSubject(this.user);

    // Lo demas componentes se ponen a la esucha de cambio
  //   // a través de esté método
  //   getUserObservable(): Observable<Usuario> {
  //       return this.userSubject.asObservable();
  //   }

  // // Este método se usa para enviar los cambios a todos los componentes a la escucha
  // private setUser(user: Usuario) {
  //     this.user = user;
  //     // Refrescar user en los observables
  //     this.userSubject.next(this.user);
  //  }
}
