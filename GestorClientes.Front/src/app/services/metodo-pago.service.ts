import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { MetodoPagoModel } from '../models/pagos-metodo/metodo-pago.model';
import * as myGlobals from '../global.component';


@Injectable({
  providedIn: 'root'
})


export class MetodoPagoService {

  constructor(private http: HttpClient) { }

  //URL to Api
  private url = myGlobals.BASE_URL + '/metodoPago';


  getMetodosPago () {


     return this.http.get(`${this.url}`)
               .pipe( map ( data => {
                 data['metodoPagos'];
               return data;
               }) );
  }


  addMetodoPago (metodoPago: MetodoPagoModel) {

    var httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    return this.http.post(this.url, metodoPago, httpOptions);
  }

  updateMetodoPago (metodoPago : MetodoPagoModel) {

    var httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    return this.http.put(this.url,metodoPago, httpOptions);
  }

  deleteMetodoPago (metodoPago: MetodoPagoModel) {

    let data = JSON.stringify(metodoPago);

     var httpOptions = {
       headers: new HttpHeaders({'Content-Type': 'application/json'}),
       body: data
     }

    return this.http.delete(this.url, httpOptions);
  }

}
