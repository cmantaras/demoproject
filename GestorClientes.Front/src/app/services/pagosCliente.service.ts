import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { ImpuestoModel } from '../models/impuesto/impuesto.model';
import * as myGlobals from '../global.component';


@Injectable({
  providedIn: 'root'
})

export class PagosClienteService {


  constructor(private http: HttpClient) { }

  //URL to Api
  private url = myGlobals.BASE_URL + '/pagocliente';


  realizarPago (productos : any) {

    var httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    return this.http.post( `${this.url}/realizarpago`, productos, httpOptions);
  }

  getPagos (clienteId : Number) {

    var httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    return this.http.get( `${this.url}?clienteId=${clienteId}`, httpOptions);
  }
  
  deletePagos(pagoId: number) {
    var httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    return this.http.delete( `${this.url}?pagoId=${pagoId}`, httpOptions);
  }

  getCuotasPago (pagoId : number) {

    var httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    return this.http.get( `${this.url}/cuotas?pagoId=${pagoId}`, httpOptions);
  }



}
