import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { map } from 'rxjs/operators';
import { NumberLiteralType } from 'typescript';
import { RolModel } from '../models/rol.models';
import * as myGlobals from '../global.component';


@Injectable({
  providedIn: 'root'
})

export class RolesService {

  constructor(private http: HttpClient) {


  }

  //Querys to Api
  private url = myGlobals.BASE_URL + '/roles';

  // getQuery ( query: string ) {
  //   const url = `${ query }`;
  //   return this.http.get(url);
  // }

  // postQuery ( query: string, rol : any ) {
  //   const url = `baseUrl${ query }`;
  //   return this.http.post(url, rol );
  // }

 getRoles () {

     return this.http.get(this.url)
               .pipe( map ( data => {
                 data['Roles'];
               return data;
               }) );
   }


  addrol (rol: RolModel) {

    var httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    return this.http.post(this.url, rol, httpOptions);
  }

  updaterol (rol) {

    var httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    return this.http.put(this.url,rol, httpOptions);
  }

  deleterol (rol) {

    let data = JSON.stringify(rol);

     var httpOptions = {
       headers: new HttpHeaders({'Content-Type': 'application/json'}),
       body: data
     }

    return this.http.delete(this.url, httpOptions);

  }

}
