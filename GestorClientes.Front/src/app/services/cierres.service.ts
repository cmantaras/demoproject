import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { ItemModel } from '../models/item/item.model';
import * as myGlobals from '../global.component';


@Injectable({
  providedIn: 'root'
})

export class CierresService {

  constructor(private http: HttpClient) { }

  //URL to Api
  private url = myGlobals.BASE_URL + '/CierreMes';


  getCierresMes () {

    return this.http.get(`${this.url}/GetCierresMes`)
    .pipe( map ( data => {
      data['Productos'];
    return data;
    }) );
  }


  async finalizarMes (fecha : any) {

    var httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    return this.http.post( `${this.url}/FinalizarMes`,{ Fecha : fecha }, httpOptions);
  }


}
