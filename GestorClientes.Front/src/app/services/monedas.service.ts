import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { MonedaModel } from '../models/moneda/moneda.model';
import * as myGlobals from '../global.component';


@Injectable({
  providedIn: 'root'
})

export class MonedasService {

  constructor(private http: HttpClient) { }

  //URL to Api
  private url = myGlobals.BASE_URL + '/moneda';


  getMonedas () {


     return this.http.get(`${this.url}`)
               .pipe( map ( data => {
                 data['monedas'];
               return data;
               }) );
  }
  getAllMonedas () {


    return this.http.get(`${this.url}/GetAllMonedas`)
              .pipe( map ( data => {
                data['monedas'];
              return data;
            }) );
 }
  
  addMoneda (moneda: MonedaModel) {

    var httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    return this.http.post(this.url, moneda, httpOptions);
  }

  updateMoneda (moneda : MonedaModel) {

    var httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    return this.http.put(this.url,moneda, httpOptions);
  }

  deleteMoneda (moneda: MonedaModel) {

    let data = JSON.stringify(moneda);

     var httpOptions = {
       headers: new HttpHeaders({'Content-Type': 'application/json'}),
       body: data
     }

    return this.http.delete(this.url, httpOptions);
  }

}
