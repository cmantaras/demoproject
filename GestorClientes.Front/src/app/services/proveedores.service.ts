import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { ProveedorModel } from '../models/proveedor/proveedor.model';
import * as myGlobals from '../global.component';
import { CategoriaModel } from '../models/categoria/categoria.model';
import { ProveedorRequestModel } from '../models/proveedor/proveedorRequest.model';
import { ProveedorBancoModel } from '../models/proveedor/proveedorBanco.model';


@Injectable({
  providedIn: 'root'
})

export class ProveedoresService {


  constructor(private http: HttpClient) { }

  //URL to Api
  private url = myGlobals.BASE_URL + '/proveedor';

  
  getInfoBancariaProveedor(proveedorId : number) {
    return this.http.post(`${this.url}/GetInfoBancariaProveedor?Id=${proveedorId}`, {});
  }

  updateProveedorBanco(proveedorBanco: ProveedorBancoModel): import("rxjs").Observable<any> {
    
    var httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    return this.http.put(`${this.url}/UpdateProveedorBanco`,proveedorBanco, httpOptions);
  }
  
  getTiposCuentas() {
    return this.http.get(`${this.url}/GetTiposCuentas`)
    .pipe( map ( data => {
      data['cuentaTipos'];
    return data; }) );
  }

  getProveedores (proveedor?  : ProveedorRequestModel) {

    return this.http.post(`${this.url}/GetProveedores`, proveedor)
              .pipe( map ( data => {
                data['Proveedors'];

              return data;
              
              }) );
 }

  addProveedor (proveedor: ProveedorModel) {

    var httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    return this.http.post(this.url, proveedor, httpOptions);
  }

  updateProveedor (proveedor : ProveedorModel) {

    var httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    return this.http.put(this.url,proveedor, httpOptions);
  }

  deleteProveedor (proveedor: ProveedorModel) {

    let data = JSON.stringify(proveedor);

     var httpOptions = {
       headers: new HttpHeaders({'Content-Type': 'application/json'}),
       body: data
     }

    return this.http.delete(this.url, httpOptions);
  }




 //Cheques
 getCheques (proveedorId? : string) {
  return this.http.get(`${myGlobals.BASE_URL}/cheque?ProveedorId=${proveedorId}`)
            .pipe( map ( data => {
              data['Categorias'];
            return data;
            }) );
}




}
