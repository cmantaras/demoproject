import { ParametroModel } from './../models/parametro.model';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { ImpuestoModel } from '../models/impuesto/impuesto.model';
import * as myGlobals from '../global.component';
import { componentFactoryName } from '@angular/compiler';
import { title } from 'process';


@Injectable({
  providedIn: 'root'
})

export class ParametrosService {

  constructor(private http: HttpClient) { }

  //URL to Api
  private url = myGlobals.BASE_URL + '/parametros';


  getComentario () {
     return this.http.get(this.url)
               .pipe( map ( data => {
                 data['impuestos'];
               return data;
               }) );
  }

  getTitulo () {
    var httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    return this.http.get(this.url, httpOptions);
  }

  get (request : string) {
    var httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    return this.http.get(this.url+"?Codigo="+request, httpOptions);
  }

  setCheckFibra (date : string) {
    var httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    return this.http.put(this.url+"/checkfibra?checkFibra="+date, httpOptions);
  }
  setParamsFactura(titulo : string, comentario : string){
    let data = {
      comentario : comentario,
      titulo : titulo
    }
    var httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    return this.http.put(this.url+"/textoFactura?titulo?comentario="+titulo+comentario, data,httpOptions);
  }

  setVencimientoMes (date : string) {
    var httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    return this.http.put(this.url+"/vencimientomes?vencimientoMes="+date, httpOptions);
  }


}
