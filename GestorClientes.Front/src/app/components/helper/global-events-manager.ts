import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';


@Injectable()
export class GlobalEventsManager {

    private _showNavBar: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    public showNavBarEmitter: Observable<boolean> = this._showNavBar.asObservable();
    
    //header update
    private _headerUpdater: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    public headerEmitter: Observable<boolean> = this._headerUpdater.asObservable();

    constructor() {}

    showNavBar(ifShow: boolean) {
        this._showNavBar.next(ifShow);

    }
    //header update
    updateHeader(dataUser: any) {
        this._headerUpdater.next(dataUser);

    }


}
