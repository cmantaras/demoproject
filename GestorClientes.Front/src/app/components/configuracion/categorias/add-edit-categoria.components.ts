import { Component, EventEmitter, Injectable, OnInit, Output } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgForm } from '@angular/forms';
import Swal from 'sweetalert2';
import { Observable } from 'rxjs';
import { CategoriaService } from 'src/app/services/categoria.service';
import { CategoriaModel } from 'src/app/models/categoria/categoria.model';


@Component({
  selector: 'app-add-edit-categoria',
  template: `
    <div class="modal-header">
      <h4 class="modal-title"> {{ modalHeader }} </h4>
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">


        <!-- BASIC FORM ELELEMNTS -->
              <form autocomplete="off" (ngSubmit)="guardar( form )" #form="ngForm" class="form-horizontal style-form" method="get">
                <div class="form-group">

                  <label class="col-sm-2 col-sm-2 control-label">Descripción</label>
                  <div class="col-sm-10">
                  <input type="text"
                            class="form-control is-invalid"
                            name="Descripcion"
                            [ngClass]="{ 'is-invalid': form.submitted && descripcion.invalid }"
                            [class.is-invalid]="descripcion.invalid && descripcion.touched"
                            [(ngModel)] = "categoria.Descripcion"
                            required
                            placeholder="Descripción"
                            minlength="1"
                            #descripcion="ngModel"

                           >
                           <small   *ngIf="descripcion.invalid && descripcion.touched"
                                   class="form-text text-danger">Ingrese una descripción de categoría válida</small>
                  </div>

                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-danger" (click)="activeModal.close('Close click')">Salir</button>
                  <button type="submit" [disabled]="loadingInfo" class="btn btn-success">Guardar</button>
                </div>
              </form>

    </div>


  `,
  styles: [
  ]
})
@Injectable()
export class AddEditCategoriaComponent   {

  public categoria : CategoriaModel = new CategoriaModel();
  public modalHeader : string;
  public categorias : CategoriaModel[] = [];
  public loadingInfo : boolean = false;

  @Output() sendCategoria= new EventEmitter();


  constructor( public activeModal: NgbActiveModal,
              private categoriasService: CategoriaService
                       ) {
  }

  guardar(form : NgForm ) {

    if(form.invalid) {
      return;
    }


    this.loadingInfo = true;
    Swal.fire('categoria', 'La categoría se está ingresando', 'info');
    let peticion : Observable<any>;

    if ( this.categoria.Id )  {


        //Actualizar categoria
        peticion = this.categoriasService.updateCategoria(this.categoria);

        peticion.subscribe(resp => {
          Swal.fire(this.categoria.Descripcion, 'Se actualizo correctamente','success' );
          this.activeModal.close();
         

          }, (error) => {
            

            if(error.error) {
            let mensajeError = error.error.Message;
            Swal.fire('Error', mensajeError, 'error');
            }
            else { Swal.fire('Error', 'No se pudo actualizar el categoria', 'error') }

        });

    } else {

           //Agregar categoria
          peticion = this.categoriasService.addCategoria(this.categoria);
          peticion.subscribe(resp => {

            this.categoria.Id = resp;

              Swal.fire(this.categoria.Descripcion, 'Se agregó correctamente','success' );
              this.activeModal.close();
              }, (error) => {

                if(error.error)
                {

                  let mensajeError = error.error.Message;
                  Swal.fire('Error', mensajeError, 'error');
                }
                else { Swal.fire('Error', 'No se pudo agregar el categoria', 'error') }
          });
       }

       Swal.hideLoading();

    this.loadingInfo = false;
  }
}

