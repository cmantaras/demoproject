import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';
import Swal from 'sweetalert2';
import { CategoriaModel } from 'src/app/models/categoria/categoria.model';
import { CategoriaService } from 'src/app/services/categoria.service';



@Component({
  selector: 'app-eliminar-categoria',
  template: `
  <div class="modal-header">
    <h4 class="modal-title" id="modal-title">Eliminar categoría</h4>
    <button type="button" class="close" aria-describedby="modal-title" (click)="activeModal.dismiss('Cross click')">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="modal-body">
    <p><strong>¿ Estás seguro que deseas eliminar categoria ? <span class="text-primary"> {{ categoria.Descripcion }}   </span> </strong></p>

  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-outline-secondary" (click)="activeModal.dismiss('cancel click')">Salir</button>
    <button type="button" class="btn btn-danger" (click)="eliminar()">Confirmar</button>
  </div>
  `,
  styles: [
  ]
})

export class DeleteCategoriaComponent implements OnInit {

  public categoria : CategoriaModel = new CategoriaModel();
  public modalHeader : string;
  categorias : CategoriaModel [] = [];

  constructor( public activeModal: NgbActiveModal,
               private categoriasService: CategoriaService, ) { }

  ngOnInit(): void {  }

  eliminar(){

    this.categoriasService.deleteCategoria(this.categoria).
      subscribe(resp => {

      Swal.fire('', 'Se eliminó categoria correctamente','success' );

      var indice = this.categorias.indexOf(this.categoria);
      this.categorias.splice(indice, 1);

    }, (error) => {
      if(error.error)
      {
        let mensajeError = error.error.Message;
        Swal.fire('Error', mensajeError, 'error');
      }
      else { Swal.fire('Error', 'No se pudo eliminar categoria', 'error') }
    });

    this.activeModal.close();
  }

}
