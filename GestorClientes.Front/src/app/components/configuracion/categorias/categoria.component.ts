import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';


import { AddEditCategoriaComponent } from 'src/app/components/configuracion/categorias/add-edit-categoria.components';
import { NgbModal, NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';
import { CategoriaModel } from '../../../models/categoria/categoria.model';

import { DeleteCategoriaComponent } from './delete-categoria.components';
import { CategoriaService } from 'src/app/services/categoria.service';

@Component({
  selector: 'app-categoriaProv',
  templateUrl: './categoria.component.html',
  styleUrls: ['./categoria.component.css'],
  providers: [NgbPaginationConfig]
})
export class CategoriaComponent implements OnInit {

  loading : boolean = true;
  categorias : CategoriaModel  [] = [];


  //Paginación
  page = 1;
  pageSize =10;
  items = [];
  totalItems : number;


  constructor(private categoriasService: CategoriaService,
              public router:ActivatedRoute,
              public modalService: NgbModal,
              private config: NgbPaginationConfig,)
    {

    }

    ngOnInit(): void {
      this.page =1;
      this.fillCategorias();
    }


    fillCategorias(){
      this.categoriasService.getCategorias()
        .subscribe( (data : any) => {

          this.loading = true;

          this.categorias = data.Categorias;

          this.loading = false;

        },( errorServicio ) => {

        });

    }


    openModalAdd() {
      const modalRef = this.modalService.open(AddEditCategoriaComponent, { backdrop: 'static'});
      modalRef.componentInstance.modalHeader = 'Agregar categoría';
      modalRef.componentInstance.categorias = this.categorias;
      modalRef.result.then(() =>{
        this.ngOnInit();
      })
    }

    openModalDelete( categoria : CategoriaModel ) {

      const modalRef = this.modalService.open(DeleteCategoriaComponent, { backdrop: 'static'});
      modalRef.componentInstance.modalHeader = 'Eliminar categoría';
      modalRef.componentInstance.categoria = categoria;

      modalRef.componentInstance.categorias = this.categorias;
    }

    openModalEdit(categoria: CategoriaModel){

      var categoriaAux = JSON.parse( JSON.stringify( categoria ) );

      const modalRef = this.modalService.open(AddEditCategoriaComponent, { backdrop: 'static'});
      modalRef.componentInstance.modalHeader = 'Editar categoría';
      modalRef.componentInstance.categorias = this.categorias;

      modalRef.componentInstance.categoria = categoriaAux;
      modalRef.result.then(() =>{
        this.ngOnInit();
      })

    }
}
