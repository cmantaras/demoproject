import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ImpuestosService } from 'src/app/services/impuestos.service';
import { AddEditImpuestoComponent } from 'src/app/components/configuracion/impuesto/add-edit-impuesto.component';
import { NgbModal, NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';
import { ImpuestoModel } from '../../../models/impuesto/impuesto.model';

import { DeleteImpuestoComponent } from './delete-impuesto.component';

@Component({
  selector: 'app-impuesto',
  templateUrl: './impuesto.component.html',
  styleUrls: ['./impuesto.component.css'],
  providers: [NgbPaginationConfig]
})
export class ImpuestoComponent implements OnInit {

  loading : boolean = true;
  impuestos : ImpuestoModel  [] = [];


  //Paginación
  page = 1;
  pageSize =5;
  items = [];
  totalItems : number;


  constructor(private impuestosService: ImpuestosService,
              public router:ActivatedRoute,
              public modalService: NgbModal,
              private config: NgbPaginationConfig,)
    {

    }

    ngOnInit(): void {
      this.page =1;
      this.fillimpuestos();
    }


    fillimpuestos(){
      this.loading = true;
      this.impuestosService.getImpuestos()
        .subscribe( (data : any) => {


          this.impuestos = data.Impuestos;
          this.loading = false;
        },( errorServicio ) => {
          this.loading = false;
        });
    }


    openModalAdd() {
      const modalRef = this.modalService.open(AddEditImpuestoComponent, { backdrop: 'static'});
      modalRef.componentInstance.modalHeader = 'Agregar impuesto';
      modalRef.componentInstance.impuestos = this.impuestos;

    }

    openModalDelete( impuesto : ImpuestoModel ) {

      const modalRef = this.modalService.open(DeleteImpuestoComponent, { backdrop: 'static'});
      modalRef.componentInstance.modalHeader = 'Eliminar impuesto';
      modalRef.componentInstance.impuesto = impuesto;

      modalRef.componentInstance.impuestos = this.impuestos;
    }

    openModalEdit(impuesto: ImpuestoModel){

      var impuestoAux = JSON.parse( JSON.stringify( impuesto ) );

      const modalRef = this.modalService.open(AddEditImpuestoComponent, { backdrop: 'static'});
      modalRef.componentInstance.modalHeader = 'Editar impuesto';
      modalRef.componentInstance.impuestos = this.impuestos;

      modalRef.componentInstance.impuesto = impuestoAux;

    }
}
