import { Component, EventEmitter, Injectable, OnInit, Output } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgForm } from '@angular/forms';
import { ImpuestosService } from 'src/app/services/impuestos.service';
import { ImpuestoModel } from 'src/app/models/impuesto/impuesto.model';
import Swal from 'sweetalert2';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-add-edit-impuesto',
  template: `
    <div class="modal-header">
      <h4 class="modal-title"> {{ modalHeader }} </h4>
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">


        <!-- BASIC FORM ELELEMNTS -->
              <form autocomplete="off" (ngSubmit)="guardar( form )" #form="ngForm" class="form-horizontal style-form" method="get">
                <div class="form-group">

                  <label class="col-sm-2 col-sm-2 control-label">Descripción</label>
                  <div class="col-sm-10">
                  <input type="text"
                            class="form-control is-invalid"
                            name="Descripcion"
                            [ngClass]="{ 'is-invalid': form.submitted && descripcion.invalid}"
                            [class.is-invalid]="descripcion.invalid && descripcion.touched"
                            [(ngModel)] = "impuesto.Descripcion"
                            required
                            placeholder="Descripción"
                            minlength="1"
                            #descripcion="ngModel"

                           >
                           <small   *ngIf="descripcion.invalid && descripcion.touched"
                                   class="form-text text-danger">Ingrese una descripción de impuesto válida</small>
                  </div>

                  <label class="col-sm-2 col-sm-2 control-label">Porcentaje</label>
                  <div class="col-sm-10">
                  <input type="number" step="0.001" min="0" max="999999999.999"
                            class="form-control is-invalid"
                            name="Porcentaje"
                            [ngClass]="{ 'is-invalid': form.submitted && porcentaje.touched }"
                            [class.is-invalid]="porcentaje.invalid && porcentaje.touched"
                            [(ngModel)] = "impuesto.Porcentaje"
                            required
                            placeholder="Porcentaje"
                            minlength="1"
                            #porcentaje="ngModel"

                           >
                           <small   *ngIf="porcentaje.invalid && porcentaje.touched"
                                   class="form-text text-danger">Ingrese un numero válido para el porcentaje</small>
                  </div>

                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-danger" (click)="activeModal.close('Close click')">Salir</button>
                  <button type="submit" [disabled]="loadingInfo" class="btn btn-success">Guardar</button>
                </div>
              </form>

    </div>


  `,
  styles: [
  ]
})
@Injectable()
export class AddEditImpuestoComponent   {

  public impuesto : ImpuestoModel = new ImpuestoModel();
  public modalHeader : string;
  public impuestos : ImpuestoModel[] = [];
  public loadingInfo : boolean = false;

  @Output() sendImpuesto= new EventEmitter();


  constructor( public activeModal: NgbActiveModal,
              private impuestosService: ImpuestosService
                       ) {
  }

  guardar(form : NgForm ) {

    if(form.invalid) {
      return;
    }
    this.loadingInfo = true;
    // if(this.impuestos.find(x => x.Descripcion.toLocaleLowerCase === this.impuesto.Descripcion.toLocaleLowerCase)){
    //   Swal.fire('impuesto', 'El impuesto ingresado ya existe', 'info');
    //   return;
    // }


    Swal.fire('impuesto', 'El impuesto se está ingresando', 'info');
    let peticion : Observable<any>;

    if ( this.impuesto.Id )  {


        //Actualizar impuesto
        peticion = this.impuestosService.updateImpuesto(this.impuesto);

        peticion.subscribe(resp => {
          Swal.fire(this.impuesto.Descripcion, 'Se actualizo correctamente','success' );

          Object.assign(this.impuestos[this.impuestos.findIndex(el => el.Id === this.impuesto.Id)], this.impuesto);

          }, (error) => {
            // console.log(error);

            if(error.error) {
            let mensajeError = error.error.Message;
            Swal.fire('Error', mensajeError, 'error');
            }
            else { Swal.fire('Error', 'No se pudo actualizar el impuesto', 'error') }

        });

    } else {

           //Agregar impuesto
          peticion = this.impuestosService.addImpuesto(this.impuesto);
          peticion.subscribe(resp => {

            this.impuesto.Id = resp;

            this.impuestos.push(this.impuesto);
              Swal.fire(this.impuesto.Descripcion, 'Se agregó correctamente','success' );

              }, (error) => {

                if(error.error)
                {
                  let mensajeError = error.error.Message;
                  Swal.fire('Error', mensajeError, 'error');
                }
                else { Swal.fire('Error', 'No se pudo agregar el impuesto', 'error') }
          });
       }

       this.activeModal.close();
       this.loadingInfo= false;
       Swal.hideLoading();


  }
}

