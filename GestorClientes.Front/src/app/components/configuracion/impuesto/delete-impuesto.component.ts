import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';
import { ImpuestosService } from 'src/app/services/impuestos.service';
import Swal from 'sweetalert2';
import { ImpuestoModel } from 'src/app/models/impuesto/impuesto.model';



@Component({
  selector: 'app-eliminar-impuesto',
  template: `
  <div class="modal-header">
    <h4 class="modal-title" id="modal-title">Eliminar Impuesto</h4>
    <button type="button" class="close" aria-describedby="modal-title" (click)="activeModal.dismiss('Cross click')">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="modal-body">
    <p><strong>¿ Estás seguro que deseas eliminar el impuesto ? <span class="text-primary"> {{ impuesto.Descripcion }}   </span> </strong></p>

  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-outline-secondary" (click)="activeModal.dismiss('cancel click')">Salir</button>
    <button type="button" class="btn btn-danger" (click)="eliminar()">Confirmar</button>
  </div>
  `,
  styles: [
  ]
})

export class DeleteImpuestoComponent implements OnInit {

  public impuesto : ImpuestoModel = new ImpuestoModel();
  public modalHeader : string;
  impuestos : ImpuestoModel [] = [];

  constructor( public activeModal: NgbActiveModal,
               private impuestosService: ImpuestosService, ) { }

  ngOnInit(): void {  }

  eliminar(){

    this.impuestosService.deleteImpuesto(this.impuesto).
      subscribe(resp => {

      Swal.fire('', 'Se eliminó impuesto correctamente','success' );

      var indice = this.impuestos.indexOf(this.impuesto);
      this.impuestos.splice(indice, 1);

    }, (error) => {
      if(error.error)
      {
        let mensajeError = error.error.Message;
        Swal.fire('Error', mensajeError, 'error');
      }
      else { Swal.fire('Error', 'No se pudo eliminar impuesto', 'error') }
    });

    this.activeModal.close();
  }

}
