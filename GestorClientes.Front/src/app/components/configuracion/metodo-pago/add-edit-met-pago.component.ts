import { Component, EventEmitter, Injectable, OnInit, Output } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgForm } from '@angular/forms';
import { MetodoPagoService } from 'src/app/services/metodo-pago.service';
import { MetodoPagoModel } from 'src/app/models/pagos-metodo/metodo-pago.model';
import Swal from 'sweetalert2';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-add-edit-metodoPago',
  template: `
    <div class="modal-header">
      <h4 class="modal-title">  {{ modalHeader}}  </h4>
      <button type="button" class="close" aria-label="Close" [disabled]="loadingInfo" (click)="activeModal.dismiss('Cross click')">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">


        <!-- BASIC FORM ELELEMNTS -->
              <form autocomplete="off" (ngSubmit)="guardar( form )" #form="ngForm" class="form-horizontal style-form" method="get">
                <div class="form-group">

                  <label class="col-sm-2 col-sm-2 control-label">Descripción</label>
                  <div class="col-sm-10">
                  <input type="text"
                            class="form-control is-invalid"

                            name="Descripcion"
                            [ngClass]="{ 'is-invalid': form.submitted && descripcion.invalid }"
                            [class.is-invalid]="descripcion.invalid && descripcion.touched"
                            [(ngModel)] = "metodoPago.Descripcion"
                            required
                            placeholder="Descripción"
                            minlength="1"
                            #descripcion="ngModel"

                           >
                           <small   *ngIf="descripcion.invalid && descripcion.touched"
                                   class="form-text text-danger">Ingrese una descripción de metodo de pago válida</small>
                  </div>

                  <label class="col-sm-2 col-sm-2 control-label">Cantidad</label>
                  <div class="col-sm-10">
                  <input type="number"
                            class="form-control is-invalid"
                            name="Cantidad cuotas"
                            [ngClass]="{ 'is-invalid': form.submitted }"
                            [class.is-invalid]="metodo.invalid && metodo.touched"
                            [(ngModel)] = "metodoPago.Cantidad"
                            required
                            placeholder="Cantidad cuotas"
                            minlength="1"
                            #metodo="ngModel"

                           >
                           <small   *ngIf="metodo.invalid && metodo.touched"
                                   class="form-text text-danger">Ingrese una cantidad válida</small>
                  </div>

                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-danger" (click)="activeModal.close('Close click')">Salir</button>
                  <button type="submit" class="btn btn-success">Guardar</button>
                </div>
              </form>

    </div>


  `,
  styles: [
  ]
})
@Injectable()
export class AddEditMetodoPagoComponent   {

  public metodoPago : MetodoPagoModel = new MetodoPagoModel();
  public modalHeader : string;
  public metodosPago : MetodoPagoModel[] = [];
  public loadingInfo : boolean = false;

  @Output() sendMetodoPago= new EventEmitter();


  constructor( public activeModal: NgbActiveModal,
              private metodoPagosService: MetodoPagoService
                       ) {
  }

  guardar(form : NgForm ) {

    if(form.invalid) {
      return;
    }

    // if(this.metodoPagos.find(x => x.Descripcion === this.metodoPago.Descripcion)){

    //   Swal.fire('metodoPago', 'La metodoPago ingresada ya existe', 'info');
    //   return;
    // }

    this.loadingInfo = true;
    Swal.fire('metodoPago', 'El metodoPago se está ingresando', 'info');
    let peticion : Observable<any>;

    if ( this.metodoPago.Id )  {


        //Actualizar metodoPago
        peticion = this.metodoPagosService.updateMetodoPago(this.metodoPago);

        peticion.subscribe(resp => {
          Swal.fire(this.metodoPago.Descripcion, 'Se actualizo correctamente','success' );

          Object.assign(this.metodosPago[this.metodosPago.findIndex(el => el.Id === this.metodoPago.Id)], this.metodoPago);

          }, (error) => {
            // console.log(error);

            if(error.error) {
            let mensajeError = error.error.Message;
            Swal.fire('Error', mensajeError, 'error');
            }
            else { Swal.fire('Error', 'No se pudo actualizar el metodoPago', 'error') }

        });

    } else {

           //Agregar metodoPago
          peticion = this.metodoPagosService.addMetodoPago(this.metodoPago);
          peticion.subscribe(resp => {

            this.metodoPago.Id = resp;

            this.metodosPago.push(this.metodoPago);
              Swal.fire(this.metodoPago.Descripcion, 'Se agregó correctamente','success' );

              }, (error) => {

                if(error.error)
                {
                  let mensajeError = error.error.Message;
                  Swal.fire('Error', mensajeError, 'error');
                }
                else { Swal.fire('Error', 'No se pudo agregar el metodoPago', 'error') }
          });
       }

       this.activeModal.close();
       this.loadingInfo = false;
       Swal.hideLoading();


  }
}

