import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';
import { MetodoPagoService } from 'src/app/services/metodo-pago.service';
import Swal from 'sweetalert2';
import { MetodoPagoModel } from 'src/app/models/pagos-metodo/metodo-pago.model';



@Component({
  selector: 'app-eliminar-metodoPago',
  template: `
  <div class="modal-header">
    <h4 class="modal-title" id="modal-title">Eliminar método de pago</h4>
    <button type="button" class="close" aria-describedby="modal-title" (click)="activeModal.dismiss('Cross click')">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="modal-body">
    <p><strong>¿ Estás seguro que deseas eliminar el método de pago ? <span class="text-primary"> {{ metodoPago.Descripcion }}   </span> </strong></p>

  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-outline-secondary" (click)="activeModal.dismiss('cancel click')">Salir</button>
    <button type="button" class="btn btn-danger" (click)="eliminar()">Confirmar</button>
  </div>
  `,
  styles: [
  ]
})

export class DeleteMetodoPagoComponent implements OnInit {

  public metodoPago : MetodoPagoModel = new MetodoPagoModel();
  public modalHeader : string;
  metodosPago : MetodoPagoModel [] = [];

  constructor( public activeModal: NgbActiveModal,
               private metodoPagosService: MetodoPagoService, ) { }

  ngOnInit(): void {  }

  eliminar(){

    this.metodoPagosService.deleteMetodoPago(this.metodoPago).
      subscribe(resp => {

      Swal.fire('', 'Se eliminó metodoPago correctamente','success' );

      var indice = this.metodosPago.indexOf(this.metodoPago);
      this.metodosPago.splice(indice, 1);

    }, (error) => {
      if(error.error)
      {
        let mensajeError = error.error.Message;
        Swal.fire('Error', mensajeError, 'error');
      }
      else { Swal.fire('Error', 'No se pudo eliminar metodoPago', 'error') }
    });

    this.activeModal.close();
  }

}
