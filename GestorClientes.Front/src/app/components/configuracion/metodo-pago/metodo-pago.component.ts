import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { MetodoPagoService } from 'src/app/services/metodo-pago.service';
import { AddEditMetodoPagoComponent } from 'src/app/components/configuracion/metodo-pago/add-edit-met-pago.component';
import { NgbModal, NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';
import { MetodoPagoModel } from '../../../models/pagos-metodo/metodo-pago.model';

import { DeleteMetodoPagoComponent } from './delete-metodo-pago.component';

@Component({
  selector: 'app-metodoPago',
  templateUrl: './metodo-pago.component.html',
  styleUrls: ['./metodo-pago.component.css'],
  providers: [NgbPaginationConfig]
})
export class MetodoPagoComponent implements OnInit {

  loading : boolean = true;
  metodosPago : MetodoPagoModel  [] = [];


  //Paginación
  page = 1;
  pageSize =5;
  items = [];
  totalItems : number;


  constructor(private metodoPagoService: MetodoPagoService,
              public router:ActivatedRoute,
              public modalService: NgbModal,
              private config: NgbPaginationConfig,)
    {

    }

    ngOnInit(): void {
      this.page =1;
      this.fillMetodoPago();
    }


    fillMetodoPago(){

      this.loading = true;
      this.metodoPagoService.getMetodosPago()
        .subscribe( (data : any) => {

          this.metodosPago = data.Metodos;

          this.loading = false;

        },( errorServicio ) => {
          this.loading = false;
        });

    }


    openModalAdd() {
      const modalRef = this.modalService.open(AddEditMetodoPagoComponent, { backdrop: 'static'});
      modalRef.componentInstance.modalHeader = 'Agregar método de pago';
      modalRef.componentInstance.metodosPago = this.metodosPago;

    }

    openModalDelete( metodoPago : MetodoPagoModel ) {

      const modalRef = this.modalService.open(DeleteMetodoPagoComponent, { backdrop: 'static'});
      modalRef.componentInstance.modalHeader = 'Eliminar método pago';
      modalRef.componentInstance.metodoPago = metodoPago;

      modalRef.componentInstance.metodosPago = this.metodosPago;
    }

    openModalEdit(metodoPago: MetodoPagoModel){

      var metodoPagoAux = JSON.parse( JSON.stringify( metodoPago ) );

      const modalRef = this.modalService.open(AddEditMetodoPagoComponent, { backdrop: 'static'});
      modalRef.componentInstance.modalHeader = 'Editar método de pago';
      modalRef.componentInstance.metodosPago = this.metodosPago;

      modalRef.componentInstance.metodoPago = metodoPagoAux;

    }
}
