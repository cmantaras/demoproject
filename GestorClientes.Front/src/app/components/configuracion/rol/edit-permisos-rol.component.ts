import { Component, OnInit } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgForm } from '@angular/forms';
import { RolesService } from 'src/app/services/roles.service';
import { ActivatedRoute } from '@angular/router';
import { RolModel } from 'src/app/models/rol.models';
import Swal from 'sweetalert2';
import { Observable } from 'rxjs';
import { stringify } from 'querystring';
import { FormsModule } from '@angular/forms';
import { RolComponent } from './rol.component';
import { faShower } from '@fortawesome/free-solid-svg-icons';
import { PermisosService } from 'src/app/services/permisos.service';
import { GrupoPermisosModel } from 'src/app/models/grupoPermisos.models';
import { PermisoModel } from 'src/app/models/permiso.models';

declare function createToast(params: string, type: number);

@Component({
  selector: 'app-add-edit-rol',
  template: `
    <div class="modal-header">
      <h4 class="modal-title"> {{ modalHeader }} </h4>
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">

        <!-- BASIC FORM ELELEMNTS -->
      <form autocomplete="off" (ngSubmit)="guardar( form )" #form="ngForm" class="form-horizontal style-form" method="get">
      <div class="d-flex flex-row   ">
      <div class="col-sm-6">
          <table class="animated fadeIn table table-striped table-advance table-hover">
            <thead>
              <tr>
                <th  class="hidden-phone" ><i class=" d-flex"></i> Permisos disponibles </th>
              </tr>
            </thead>
            <tbody>
              <tr *ngFor="let permiso of listaPermisos" >
                    <td>
                      {{ permiso.Descripcion }}
                    </td>
                    <td>
                      <a type="button" (click)="agregarPermiso(permiso)"> <i class="fa-2x fas fa-plus-circle"></i> </a>
                    </td>
              </tr>
            </tbody>
          </table>
      </div>

      <div  class="col-sm-6">
          <table class="animated fadeIn table table-striped table-advance table-hover">
            <thead>
              <tr>
                <th  class="hidden-phone" ><i class=""></i> Permisos asociados al rol </th>
              </tr>
            </thead>
            <tbody>

              <tr *ngFor="let permiso of rol.Permisos"  >

                  <td>
                     {{ permiso.Descripcion }}
                  </td>
                  <td>
                  <!-- *appCheckPermissions="['ADD_PERMISSIONS']" -->
                    <a type="button"  (click)="quitarPermiso(permiso)"> <i class="fa-2x fas fa-minus-circle"></i> </a>
                  </td>

              </tr>

            </tbody>
          </table>
      </div>
      </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-outline-dark" (click)="activeModal.close('Close click')">Salir</button>
                </div>
              </form>

</div>

  `,
  styles: [
  ]
})
export class EditPermisosRolComponent implements OnInit  {

  public rol : RolModel = new RolModel();
  public ListpermisosDisponibles : any[] = [];
  public listAux : PermisoModel [] = [];
  public listIndices : any =   [];
  public listaPermisos : PermisoModel [] = [];
  public grupoPermisos : any = [];
  public modalHeader : string;
  rolesList : any = [];


  constructor( public activeModal: NgbActiveModal,
              private rolsService: RolesService,
              private permisosService: PermisosService,


                       ) {
                        this.obtenerPermisos();
  }


  ngOnInit (){

  }

  obtenerPermisos() {
    //Se obtienen todos los permisos
    this.permisosService.getPermisos()
        .subscribe(resp => {

          this.grupoPermisos = resp;

          this.grupoPermisos.forEach(grupo => {
            grupo.Permisos.forEach(permiso => {
              this.listaPermisos.push(permiso)
            });
          });

          this.permisosDisponibles();

        });
 }

permisosDisponibles(){

   this.rol.Permisos.forEach((permiso: any) => {

    var indice = this.listaPermisos.findIndex(obj => obj.Id === permiso.Id)

    this.listaPermisos.splice(indice, 1);

  });


 }
 ngOnDestroy () {

  this.obtenerPermisos();
 }
//  this.rolesList.push(this.rol);

//  var indice = this.rolsList.indexOf(this.rol);
//  this.rolsList.splice(indice, 1);



  guardar(form : NgForm ) {
    if(form.invalid) {
      return;
    }

    Swal.fire('rol', 'El rol se está ingresando', 'info');
    let peticion : Observable<any>;


    if ( this.rol.Id) {

        //Actualizar rol

       // peticion = this.permisosService.addPermiso(this.rol, this.listaPermisos);

        peticion.subscribe(resp => {
          Swal.fire(this.rol.Nombre, 'Se actualizo correctamente','success' );
          Object.assign(this.rolesList[this.rolesList.findIndex(el => el.Id === this.rol.Id)], this.rol);

          }, (error) => {


            if(error.error) {
            let mensajeError = error.error.Message;
            Swal.fire('Error', mensajeError, 'error');
            }
            else { Swal.fire('Error', 'No se pudo actualizar el rol', 'error') }

        });

    } else {

           //Agregar rol
          peticion = this.rolsService.addrol(this.rol);
          peticion.subscribe(resp => {

            this.rol.Id = resp;
            this.rolesList.push(this.rol);

              Swal.fire(this.rol.Nombre, 'Se agregó correctamente','success' );
              }, (error) => {

                if(error.error)
                {
                  let mensajeError = error.error.Message;
                  Swal.fire('Error', mensajeError, 'error');
                }
                else { Swal.fire('Error', 'No se pudo agregar el rol', 'error') }
          });
       }
       this.activeModal.close();

       Swal.hideLoading();
  }

  quitarPermiso(permiso : PermisoModel){

    this.permisosService.deletePermiso( this.rol.Id, permiso.Id).subscribe(resp => {
      createToast("Quitado",2);

    });

    var indice = this.rol.Permisos.findIndex(obj => obj.Id === permiso.Id)

    this.rol.Permisos.splice(indice, 1);

    this.listaPermisos.push(permiso);



  }

  agregarPermiso(permiso : PermisoModel){

    this.permisosService.addPermiso( this.rol.Id,permiso.Id).subscribe(resp => {
    createToast("Agregado",2);
    });


    var indice = this.listaPermisos.findIndex(obj => obj.Id === permiso.Id)
    this.listaPermisos.splice(indice, 1);

    this.rol.Permisos.push(permiso);



  }
}

