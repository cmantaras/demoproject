import { Component  } from '@angular/core';
import { NgbActiveModal  } from '@ng-bootstrap/ng-bootstrap';
import { NgForm } from '@angular/forms';
import { RolesService } from 'src/app/services/roles.service';

import { RolModel } from 'src/app/models/rol.models';
import Swal from 'sweetalert2';
import { Observable } from 'rxjs';



@Component({
  selector: 'app-add-edit-rol',
  template: `
    <div class="modal-header">
      <h4 class="modal-title"> {{ modalHeader }} </h4>
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">

              <form autocomplete="off" (ngSubmit)="guardar( form )" #form="ngForm" class="form-horizontal style-form" method="get">
                <div class="form-group">

                  <label class="col-sm-2 col-sm-2 control-label">Descripción</label>
                  <div class="col-sm-10">
                  <input type="text"
                            class="form-control is-invalid"
                            
                            name="Descripcion"
                            [class.is-invalid]="descripcion.invalid && descripcion.touched"
                            [(ngModel)] = "rol.Nombre"
                            required
                            placeholder="Descripcion"
                            minlength="1"
                            #descripcion="ngModel"
                           >
                           <small   *ngIf="descripcion.invalid && descripcion.touched"
                                   class="form-text text-danger">Ingrese una descripción de rol válida</small>
                  </div>

                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-danger" (click)="activeModal.close('Close click')">Salir</button>
                  <button type="submit" class="btn btn-success">Guardar</button>
                </div>
              </form>

    </div>
  `,
  styles: [
  ]
})
export class AddEditRolComponent   {

  public rol : RolModel = new RolModel();
  public modalHeader : string;
  rolesList : any = [];

  constructor( public activeModal: NgbActiveModal,
              private rolsService: RolesService
                       ) {
  }

  guardar(form : NgForm ) {
    if(form.invalid) {
      return;
    }

    Swal.fire('rol', 'El rol se está ingresando', 'info');
    let peticion : Observable<any>;

    if ( this.rol.Id) {

        //Actualizar rol
        peticion = this.rolsService.updaterol(this.rol);

        peticion.subscribe(resp => {
          Swal.fire(this.rol.Nombre, 'Se actualizó correctamente','success' );
          Object.assign(this.rolesList[this.rolesList.findIndex(el => el.Id === this.rol.Id)], this.rol);
          }, (error) => {


            if(error.error) {
            let mensajeError = error.error.Message;
            Swal.fire('Error', mensajeError, 'error');
            }
            else { Swal.fire('Error', 'No se pudo actualizar el rol', 'error') }

        });

    } else {

           //Agregar rol
          peticion = this.rolsService.addrol(this.rol);
          peticion.subscribe(resp => {

            this.rol.Id = resp;
            this.rolesList.push(this.rol);

              Swal.fire(this.rol.Nombre, 'Se agregó correctamente','success' );
              }, (error) => {

                if(error.error)
                {
                  let mensajeError = error.error.Message;
                  Swal.fire('Error', mensajeError, 'error');
                }
                else { Swal.fire('Error', 'No se pudo agregar el rol', 'error') }
          });
       }
       this.activeModal.close();

       Swal.hideLoading();
  }
}

