import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { RolesService } from 'src/app/services/roles.service';

import { NgbModal, NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';
import { RolModel } from '../../../models/rol.models';

import { DeleteRolComponent } from './delete-rol.component';
import {Pipe ,PipeTransform } from "@angular/core";
import { AddEditRolComponent } from './add-edit-rol.component';
import { EditPermisosRolComponent } from './edit-permisos-rol.component';
import { PermisosService } from 'src/app/services/permisos.service';

declare function createToast(params: string, type: number);

@Component({
  selector: 'app-rol',
  templateUrl: './rol.component.html',
  styleUrls: ['./rol.component.css'],
  providers: [NgbPaginationConfig]
})
export class RolComponent implements OnInit {

  rol = new RolModel();
  loading : boolean = true;
  rolesList : any = [];
  page = 1;
  pageSize =10;
  items = [];
  totalItems : number;

  public grupoPermisos : any = [];

  constructor(private rolesService: RolesService,
    public router:ActivatedRoute,
    public permisosService : PermisosService,
    public modalService: NgbModal,
    private config: NgbPaginationConfig,)
    {

    }
    fillrols(){

      this.rolesService.getRoles()
      .subscribe( (data : any) => {
        this.loading = true;

        if((!data && !data.Roles) || (data && data.Roles && data.Roles.length ==0))
        {
          this.rolesList= [];
        }
        else
        {
          this.rolesList = data;
          this.totalItems = data.Count;
        }

        this.rolesList = data.Roles;

        this.loading = false;

        },( errorServicio ) => {
              this.loading = false;
            // this.loading = false;
            // this.error = true;
          // this.mensajeError = errorServicio.message;
        });


    }
    openModalAdd() {
      const modalRef = this.modalService.open(AddEditRolComponent, {backdrop : 'static' });
      modalRef.componentInstance.modalHeader = 'Agregar rol';
      modalRef.componentInstance.rolesList = this.rolesList;
    }

    openModalDelete( rol : RolModel ) {
      const modalRef = this.modalService.open(DeleteRolComponent, {backdrop : 'static' });
      modalRef.componentInstance.modalHeader = 'Eliminar rol';
      modalRef.componentInstance.rol = rol;
      modalRef.componentInstance.rolesList = this.rolesList;

    }

    openModalEdit(rol: RolModel){
      var rolAux = JSON.parse( JSON.stringify( rol ) );
      const modalRef = this.modalService.open(AddEditRolComponent, {backdrop : 'static' });
      modalRef.componentInstance.modalHeader = 'Editar rol';
      modalRef.componentInstance.rol = rolAux;
      modalRef.componentInstance.rolesList = this.rolesList;
    }

    openModalEditPermisos(rol: RolModel){
      var rolAux = JSON.parse( JSON.stringify( rol ) );
      const modalRef = this.modalService.open(EditPermisosRolComponent, { size: 'lg', backdrop : 'static' });
      modalRef.componentInstance.modalHeader = 'Editar permisos';

      modalRef.result.then(
        () => {
          this.fillrols();
        },
        ()=> {
          this.fillrols();
      });

      modalRef.componentInstance.rol = rolAux;
      modalRef.componentInstance.rolesList = this.rolesList;

    }

    ngOnInit(): void {
      this.page =1;
      this.fillrols();
      

    }
}
