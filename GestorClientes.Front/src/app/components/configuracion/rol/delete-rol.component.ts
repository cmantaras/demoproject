import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';
import { RolModel } from '../../../models/rol.models';
import { RolesService } from 'src/app/services/roles.service';
import Swal from 'sweetalert2';



@Component({
  selector: 'app-eliminar-rol',
  template: `
  <div class="modal-header">
    <h4 class="modal-title" id="modal-title"> {{ modalHeader }} </h4>
    <button type="button" class="close" aria-describedby="modal-title" (click)="activeModal.dismiss('Cross click')">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="modal-body">
    <p><strong>¿ Estás seguro que deseas eliminar el rol ? <span class="text-primary"> {{ rol.Nombre }}   </span> </strong></p>

  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-outline-secondary" (click)="activeModal.dismiss('cancel click')">Salir</button>
    <button type="button" class="btn btn-danger" (click)="eliminar()">Confirmar</button>
  </div>
  `,
  styles: [
  ]
})
export class DeleteRolComponent implements OnInit {

  public rol : RolModel = new RolModel();
  public modalHeader : string;
  rolesList : any = [];

  constructor( public activeModal: NgbActiveModal,
    private rolsService: RolesService, ) { }

  ngOnInit(): void {

  }

  eliminar(){

    let peticion : Observable<any>;

    peticion = this.rolsService.deleterol(this.rol);
    peticion.subscribe(resp => {
    Swal.fire('', 'Se eliminó rol correctamente','success' );

    var indice = this.rolesList.indexOf(this.rol);

    this.rolesList.splice(indice, 1);

    }, (error) => {

      if(error.error)
      {
        let mensajeError = error.error.Message;
        Swal.fire('Error', mensajeError, 'error');
      }
      else { Swal.fire('Error', 'No se pudo agregar el funcionario', 'error') }
    });

    this.activeModal.close();
  }
}
