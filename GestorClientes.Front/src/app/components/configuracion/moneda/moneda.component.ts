import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { MonedasService } from 'src/app/services/monedas.service';
import { AddEditMonedaComponent } from 'src/app/components/configuracion/moneda/add-edit-moneda.component';
import { NgbModal, NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';
import { MonedaModel } from '../../../models/moneda/moneda.model';

import { DeleteMonedaComponent } from './delete-moneda.component';

@Component({
  selector: 'app-moneda',
  templateUrl: './moneda.component.html',
  styleUrls: ['./moneda.component.css'],
  providers: [NgbPaginationConfig]
})
export class MonedaComponent implements OnInit {

  loading : boolean = true;
  monedas : MonedaModel  [] = [];


  //Paginación
  page = 1;
  pageSize =5;
  items = [];
  totalItems : number;
  mostrarCartel :boolean= false;


  constructor(private monedasService: MonedasService,
              public router:ActivatedRoute,
              public modalService: NgbModal,
              private config: NgbPaginationConfig,)
    {

    }

    ngOnInit(): void {
      this.page =1;
      this.fillmonedas();
    }


    fillmonedas(){

      this.loading = true;
      this.monedasService.getMonedas()
        .subscribe( (data : any) => {

          this.monedas = data.Monedas;

          this.loading = false;

        },( errorServicio ) => {
          this.loading = false;
        });

    }


    openModalAdd() {
      const modalRef = this.modalService.open(AddEditMonedaComponent, { backdrop: 'static'});
      modalRef.componentInstance.modalHeader = 'Agregar moneda';
      modalRef.componentInstance.monedas = this.monedas;

    }

    openModalDelete( moneda : MonedaModel ) {

      const modalRef = this.modalService.open(DeleteMonedaComponent, { backdrop: 'static'});
      modalRef.componentInstance.modalHeader = 'Eliminar moneda';
      modalRef.componentInstance.moneda = moneda;

      modalRef.componentInstance.monedas = this.monedas;
    }

    openModalEdit(moneda: MonedaModel){

      var monedaAux = JSON.parse( JSON.stringify( moneda ) );

      const modalRef = this.modalService.open(AddEditMonedaComponent, { backdrop: 'static'});
      modalRef.componentInstance.modalHeader = 'Editar moneda';
      modalRef.componentInstance.monedas = this.monedas;

      modalRef.componentInstance.moneda = monedaAux;

    }
}
