import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';
import { MonedasService } from 'src/app/services/monedas.service';
import Swal from 'sweetalert2';
import { MonedaModel } from 'src/app/models/moneda/moneda.model';



@Component({
  selector: 'app-eliminar-moneda',
  template: `
  <div class="modal-header">
    <h4 class="modal-title" id="modal-title">Eliminar moneda</h4>
    <button type="button" class="close" aria-describedby="modal-title" (click)="activeModal.dismiss('Cross click')">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="modal-body">
    <p><strong>¿ Estás seguro que deseas eliminar la moneda ? <span class="text-primary"> {{ moneda.Descripcion }}   </span> </strong></p>

  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-outline-secondary" (click)="activeModal.dismiss('cancel click')">Salir</button>
    <button type="button" class="btn btn-danger" (click)="eliminar()">Confirmar</button>
  </div>
  `,
  styles: [
  ]
})

export class DeleteMonedaComponent implements OnInit {

  public moneda : MonedaModel = new MonedaModel();
  public modalHeader : string;
  monedas : MonedaModel [] = [];

  constructor( public activeModal: NgbActiveModal,
               private monedasService: MonedasService, ) { }

  ngOnInit(): void {  }

  eliminar(){

    this.monedasService.deleteMoneda(this.moneda).
      subscribe(resp => {

      Swal.fire('', 'Se eliminó moneda correctamente','success' );

      var indice = this.monedas.indexOf(this.moneda);
      this.monedas.splice(indice, 1);

    }, (error) => {
      if(error.error)
      {
        let mensajeError = error.error.Message;
        Swal.fire('Error', mensajeError, 'error');
      }
      else { Swal.fire('Error', 'No se pudo eliminar moneda', 'error') }
    });

    this.activeModal.close();
  }

}
