import { Component, EventEmitter, Injectable, OnInit, Output } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgForm } from '@angular/forms';
import { MonedasService } from 'src/app/services/monedas.service';
import { MonedaModel } from 'src/app/models/moneda/moneda.model';
import Swal from 'sweetalert2';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-add-edit-moneda',
  template: `
    <div class="modal-header">
      <h4 class="modal-title"> {{ modalHeader }} </h4>
      <button type="button" class="close" aria-label="Close" [disabled]="loadingInfo" (click)="activeModal.dismiss('Cross click')">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">


        <!-- BASIC FORM ELELEMNTS -->
              <form autocomplete="off" (ngSubmit)="guardar( form )" #form="ngForm" class="form-horizontal style-form" method="get">
                <div class="form-group">

                  <label class="col-sm-2 col-sm-2 control-label">Descripción</label>
                  <div class="col-sm-10">
                  <input type="text"
                            class="form-control is-invalid"
                            name="Descripcion"
                            [ngClass]="{ 'is-invalid': form.submitted && descripcion.touched }"
                            [class.is-invalid]="descripcion.invalid && descripcion.touched"
                            [(ngModel)] = "moneda.Descripcion"
                            required
                            placeholder="Descripción"
                            minlength="1"
                            #descripcion="ngModel"

                           >
                           <small   *ngIf="descripcion.invalid && descripcion.touched"
                                   class="form-text text-danger">Ingrese una descripción de moneda válida</small>
                  </div>

                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-danger" (click)="activeModal.close('Close click')">Salir</button>
                  <button type="submit" class="btn btn-success">Guardar</button>
                </div>
              </form>

    </div>


  `,
  styles: [
  ]
})
@Injectable()
export class AddEditMonedaComponent   {

  public moneda : MonedaModel = new MonedaModel();
  public modalHeader : string;
  public monedas : MonedaModel[] = [];
  public loadingInfo : boolean = false;

  @Output() sendmoneda= new EventEmitter();


  constructor( public activeModal: NgbActiveModal,
              private monedasService: MonedasService
                       ) {
  }

  guardar(form : NgForm ) {

    if(form.invalid) {
      return;
    }
    this.loadingInfo = true;

    // if(this.monedas.find(x => x.Descripcion === this.moneda.Descripcion)){

    //   Swal.fire('moneda', 'La moneda ingresada ya existe', 'info');
    //   return;
    // }


    Swal.fire('moneda', 'El moneda se está ingresando', 'info');
    let peticion : Observable<any>;

    if ( this.moneda.Id )  {


        //Actualizar moneda
        peticion = this.monedasService.updateMoneda(this.moneda);

        peticion.subscribe(resp => {
          Swal.fire(this.moneda.Descripcion, 'Se actualizo correctamente','success' );

          Object.assign(this.monedas[this.monedas.findIndex(el => el.Id === this.moneda.Id)], this.moneda);

          }, (error) => {
            // console.log(error);

            if(error.error) {
            let mensajeError = error.error.Message;
            Swal.fire('Error', mensajeError, 'error');
            }
            else { Swal.fire('Error', 'No se pudo actualizar el moneda', 'error') }

        });

    } else {

           //Agregar moneda
          peticion = this.monedasService.addMoneda(this.moneda);
          peticion.subscribe(resp => {

            this.moneda.Id = resp;

            this.monedas.push(this.moneda);
              Swal.fire(this.moneda.Descripcion, 'Se agregó correctamente','success' );

              }, (error) => {

                if(error.error)
                {
                  let mensajeError = error.error.Message;
                  Swal.fire('Error', mensajeError, 'error');
                }
                else { Swal.fire('Error', 'No se pudo agregar el moneda', 'error') }
          });
       }

       this.activeModal.close();
        this.loadingInfo = false;
       Swal.hideLoading();


  }
}

