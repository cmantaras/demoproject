import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { MarcasService } from 'src/app/services/marcas.service';
import { AddEditMarcaComponent } from 'src/app/components/configuracion/marca/add-edit-marca.component';
import { NgbModal, NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';
import { MarcaModel } from '../../../models/marca/marca.model';

import { DeleteMarcaComponent } from './delete-marca.component';

@Component({
  selector: 'app-marca',
  templateUrl: './marca.component.html',
  styleUrls: ['./marca.component.css'],
  providers: [NgbPaginationConfig]
})
export class MarcaComponent implements OnInit {

  loading : boolean = true;
  marcas : MarcaModel  [] = [];


  //Paginación
  page = 1;
  pageSize =10;
  items = [];
  totalItems : number;


  constructor(private marcasService: MarcasService,
              public router:ActivatedRoute,
              public modalService: NgbModal,
              private config: NgbPaginationConfig,)
    {

    }

    ngOnInit(): void {
      this.page =1;
      this.fillMarcas();
    }


    fillMarcas(){

      this.loading = true;
      this.marcasService.getMarcas()
        .subscribe( (data : any) => {

          this.marcas = data.Marcas;

          this.loading = false;

        },( errorServicio ) => {
          this.loading = false;
        });

    }


    openModalAdd() {
      const modalRef = this.modalService.open(AddEditMarcaComponent, { backdrop: 'static'});
      modalRef.componentInstance.modalHeader = 'Agregar marca';
      modalRef.componentInstance.marcas = this.marcas;

    }

    openModalDelete( marca : MarcaModel ) {

      const modalRef = this.modalService.open(DeleteMarcaComponent, { backdrop: 'static'});
      modalRef.componentInstance.modalHeader = 'Eliminar marca';
      modalRef.componentInstance.marca = marca;

      modalRef.componentInstance.marcas = this.marcas;
    }

    openModalEdit(marca: MarcaModel){

      var marcaAux = JSON.parse( JSON.stringify( marca ) );

      const modalRef = this.modalService.open(AddEditMarcaComponent, { backdrop: 'static'});
      modalRef.componentInstance.modalHeader = 'Editar marca';
      modalRef.componentInstance.marcas = this.marcas;

      modalRef.componentInstance.marca = marcaAux;

    }
}
