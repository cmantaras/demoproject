import { Component, EventEmitter, Injectable, OnInit, Output } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgForm } from '@angular/forms';
import { MarcasService } from 'src/app/services/marcas.service';
import { MarcaModel } from 'src/app/models/marca/marca.model';
import Swal from 'sweetalert2';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-add-edit-marca',
  template: `
    <div class="modal-header">
      <h4 class="modal-title"> {{ modalHeader }} </h4>
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">


        <!-- BASIC FORM ELELEMNTS -->
              <form autocomplete="off" (ngSubmit)="guardar( form )" #form="ngForm" class="form-horizontal style-form" method="get">
                <div class="form-group">

                  <label class="col-sm-2 col-sm-2 control-label">Descripción</label>
                  <div class="col-sm-10">
                  <input type="text"
                            class="form-control is-invalid"
                            name="Descripcion"
                            [ngClass]="{ 'is-invalid': form.submitted && descripcion.invalid}"
                            [class.is-invalid]="descripcion.invalid && descripcion.touched"
                            [(ngModel)] = "marca.Descripcion"
                            required
                            placeholder="Descripción"
                            minlength="1"
                            #descripcion="ngModel"

                           >
                          <small   *ngIf="descripcion.invalid && descripcion.touched"
                                   class="form-text text-danger">Ingrese una descripción de marca válida</small>
                  </div>

                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-danger" (click)="activeModal.close('Close click')">Salir</button>
                  <button type="submit" [disabled]="loadingInfo" class="btn btn-success">Guardar</button>
                </div>
              </form>

    </div>


  `,
  styles: [
  ]
})
@Injectable()
export class AddEditMarcaComponent   {

  public marca : MarcaModel = new MarcaModel();
  public modalHeader : string;
  public marcas : MarcaModel[] = [];
  public loadingInfo : boolean = false;

  @Output() sendmarca= new EventEmitter();


  constructor( public activeModal: NgbActiveModal,
              private marcasService: MarcasService
                       ) {
  }

  guardar(form : NgForm ) {

    if(form.invalid) {
      return;
    }
    this.loadingInfo = true;

    if(this.marcas.find(x => x.Descripcion === this.marca.Descripcion)){

      Swal.fire('marca', 'La marca ingresada ya existe', 'info');
      return;
    }


    Swal.fire('marca', 'El marca se está ingresando', 'info');
    let peticion : Observable<any>;

    if ( this.marca.Id )  {


        //Actualizar marca
        peticion = this.marcasService.updateMarca(this.marca);

        peticion.subscribe(resp => {
          Swal.fire(this.marca.Descripcion, 'Se actualizo correctamente','success' );

          Object.assign(this.marcas[this.marcas.findIndex(el => el.Id === this.marca.Id)], this.marca);

          }, (error) => {
            // console.log(error);

            if(error.error) {
            let mensajeError = error.error.Message;
            Swal.fire('Error', mensajeError, 'error');
            }
            else { Swal.fire('Error', 'No se pudo actualizar el marca', 'error') }

        });

    } else {

           //Agregar marca
          peticion = this.marcasService.addMarca(this.marca);
          peticion.subscribe(resp => {

            this.marca.Id = resp;

            this.marcas.push(this.marca);
              Swal.fire(this.marca.Descripcion, 'Se agregó correctamente','success' );

              }, (error) => {

                if(error.error)
                {
                  let mensajeError = error.error.Message;
                  Swal.fire('Error', mensajeError, 'error');
                }
                else { Swal.fire('Error', 'No se pudo agregar el marca', 'error') }
          });
       }

       this.activeModal.close();
       this.loadingInfo = false;
       Swal.hideLoading();


  }
}

