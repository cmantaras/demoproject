import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';
import { MarcasService } from 'src/app/services/marcas.service';
import Swal from 'sweetalert2';
import { MarcaModel } from 'src/app/models/marca/marca.model';



@Component({
  selector: 'app-eliminar-marca',
  template: `
  <div class="modal-header">
    <h4 class="modal-title" id="modal-title">Eliminar marca</h4>
    <button type="button" class="close" aria-describedby="modal-title" (click)="activeModal.dismiss('Cross click')">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="modal-body">
    <p><strong>¿ Estás seguro que deseas eliminar la marca ? <span class="text-primary"> {{ marca.Descripcion }}   </span> </strong></p>

  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-outline-secondary" (click)="activeModal.dismiss('cancel click')">Salir</button>
    <button type="button" class="btn btn-danger" (click)="eliminar()">Confirmar</button>
  </div>
  `,
  styles: [
  ]
})

export class DeleteMarcaComponent implements OnInit {

  public marca : MarcaModel = new MarcaModel();
  public modalHeader : string;
  marcas : MarcaModel [] = [];

  constructor( public activeModal: NgbActiveModal,
               private marcasService: MarcasService, ) { }

  ngOnInit(): void {  }

  eliminar(){

    this.marcasService.deleteMarca(this.marca).
      subscribe(resp => {

      Swal.fire('', 'Se eliminó marca correctamente','success' );

      var indice = this.marcas.indexOf(this.marca);
      this.marcas.splice(indice, 1);

    }, (error) => {
      if(error.error)
      {
        let mensajeError = error.error.Message;
        Swal.fire('Error', mensajeError, 'error');
      }
      else { Swal.fire('Error', 'No se pudo eliminar marca', 'error') }
    });

    this.activeModal.close();
  }

}
