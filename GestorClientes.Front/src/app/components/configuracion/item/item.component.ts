import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ItemsService } from 'src/app/services/items.service';
import { AddEditItemComponent } from 'src/app/components/configuracion/item/add-edit-item.component';
import { NgbModal, NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';
import { ItemModel } from '../../../models/item/item.model';

import { DeleteItemComponent } from './delete-item.components';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css'],
  providers: [NgbPaginationConfig]
})
export class ItemComponent implements OnInit {

  loading : boolean = true;
  items : ItemModel  [] = [];


  //Paginación
  page = 1;
  pageSize =5;
  totalItems : number;


  constructor(private itemsService: ItemsService,
              public router:ActivatedRoute,
              public modalService: NgbModal,
              private config: NgbPaginationConfig,)
    {

    }

    ngOnInit(): void {
      this.page =1;
      this.fillitems();
    }


    fillitems(){
      this.loading = true;
      this.itemsService.getItems()
        .subscribe( (data : any) => {


          this.items = data.Items;
          this.loading = false;
        },( errorServicio ) => {
              this.loading = false;
        });
    }


    openModalAdd() {
      const modalRef = this.modalService.open(AddEditItemComponent, { backdrop: 'static'});
      modalRef.componentInstance.modalHeader = 'Agregar item';
      modalRef.componentInstance.items = this.items;

    }

    openModalDelete( item : ItemModel ) {

      const modalRef = this.modalService.open(DeleteItemComponent, { backdrop: 'static'});
      modalRef.componentInstance.modalHeader = 'Eliminar item';
      modalRef.componentInstance.item = item;

      modalRef.componentInstance.items = this.items;
    }

    openModalEdit(item: ItemModel){

      var itemAux = JSON.parse( JSON.stringify( item ) );

      const modalRef = this.modalService.open(AddEditItemComponent, { backdrop: 'static'});
      modalRef.componentInstance.modalHeader = 'Editar item';
      modalRef.componentInstance.items = this.items;

      modalRef.componentInstance.item = itemAux;

    }
}
