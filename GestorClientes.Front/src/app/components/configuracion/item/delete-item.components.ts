import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';
import { ItemsService } from 'src/app/services/items.service';
import Swal from 'sweetalert2';
import { ItemModel } from 'src/app/models/item/item.model';



@Component({
  selector: 'app-eliminar-item',
  template: `
  <div class="modal-header">
    <h4 class="modal-title" id="modal-title">Eliminar Item</h4>
    <button type="button" class="close" aria-describedby="modal-title" (click)="activeModal.dismiss('Cross click')">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="modal-body">
    <p><strong>¿ Estás seguro que deseas eliminar item ? <span class="text-primary"> {{ item.Descripcion }}   </span> </strong></p>

  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-outline-secondary" (click)="activeModal.dismiss('cancel click')">Salir</button>
    <button type="button" class="btn btn-danger" (click)="eliminar()">Confirmar</button>
  </div>
  `,
  styles: [
  ]
})

export class DeleteItemComponent implements OnInit {

  public item : ItemModel = new ItemModel();
  public modalHeader : string;
  items : ItemModel [] = [];

  constructor( public activeModal: NgbActiveModal,
               private itemsService: ItemsService, ) { }

  ngOnInit(): void {  }

  eliminar(){

    this.itemsService.deleteItem(this.item).
      subscribe(resp => {

      Swal.fire('', 'Se eliminó item correctamente','success' );

      var indice = this.items.indexOf(this.item);
      this.items.splice(indice, 1);

    }, (error) => {
      if(error.error)
      {
        let mensajeError = error.error.Message;
        Swal.fire('Error', mensajeError, 'error');
      }
      else { Swal.fire('Error', 'No se pudo eliminar item', 'error') }
    });

    this.activeModal.close();
  }

}
