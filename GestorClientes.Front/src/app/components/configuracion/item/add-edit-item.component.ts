import { Component, EventEmitter, Injectable, OnInit, Output } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgForm } from '@angular/forms';
import { ItemsService } from 'src/app/services/items.service';
import { ItemModel } from 'src/app/models/item/item.model';
import Swal from 'sweetalert2';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-add-edit-item',
  template: `
    <div class="modal-header">
      <h4 class="modal-title"> {{ modalHeader }} </h4>
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">

        <!-- BASIC FORM ELELEMNTS -->
              <form autocomplete="off" (ngSubmit)="guardar( form )" #form="ngForm" class="form-horizontal style-form" method="get">
                <div class="form-group">

                  <label class="col-sm-2 col-sm-2 control-label">Descripción</label>
                  <div class="col-sm-10">
                  <input type="text"
                            class="form-control is-invalid"
                            name="Descripcion"
                            [ngClass]="{ 'is-invalid': form.submitted && descripcion.invalid }"
                            [class.is-invalid]="descripcion.invalid && descripcion.touched"
                            [(ngModel)] = "item.Descripcion"
                            required
                            placeholder="Descripción"
                            minlength="1"
                            #descripcion="ngModel"

                           >
                           <small   *ngIf="descripcion.invalid && descripcion.touched"
                                   class="form-text text-danger">Ingrese una descripción de item válida</small>
                  </div>

                  <!-- <label class="col-sm-2 col-sm-2 control-label">Color</label>
                  <div class="col-sm-10">
                  <input type="text"
                            class="form-control is-invalid"
                            name="Color"
                            [ngClass]="{ 'is-invalid': form.submitted }"
                            [class.is-invalid]="color.invalid && color.touched"
                            [(ngModel)] = "item.Color"
                            required
                            placeholder="Color"
                            minlength="1"
                            #color="ngModel"

                           >
                          <small   *ngIf="descripcion.invalid && descripcion.touched"
                                   class="form-text text-danger">Ingrese 5 letras</small>
                  </div> -->

                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-danger" (click)="activeModal.close('Close click')">Salir</button>
                  <button type="submit" [disabled]="loadingInfo" class="btn btn-success">Guardar</button>
                </div>
              </form>
    </div>


  `,
  styles: [
  ]
})
@Injectable()
export class AddEditItemComponent   {

  public item : ItemModel = new ItemModel();
  public modalHeader : string;
  public items : ItemModel[] = [];
  public loadingInfo : boolean = false;

  @Output() senditem= new EventEmitter();


  constructor( public activeModal: NgbActiveModal,
              private itemsService: ItemsService
                       ) {
  }

  guardar(form : NgForm ) {

    if(form.invalid) {
      return;
    }
    this.loadingInfo = true;
    // if(this.items.find(x => x.Descripcion.toLocaleLowerCase === this.item.Descripcion.toLocaleLowerCase)){
    //   Swal.fire('item', 'El item ingresado ya existe', 'info');
    //   return;
    // }


    Swal.fire('item', 'El item se está ingresando', 'info');
    let peticion : Observable<any>;

    if ( this.item.Id )  {


        //Actualizar item
        peticion = this.itemsService.updateItem(this.item);

        peticion.subscribe(resp => {
          Swal.fire(this.item.Descripcion, 'Se actualizó correctamente','success' );

          Object.assign(this.items[this.items.findIndex(el => el.Id === this.item.Id)], this.item);

          }, (error) => {
            // console.log(error);

            if(error.error) {
            let mensajeError = error.error.Message;
            Swal.fire('Error', mensajeError, 'error');
            }
            else { Swal.fire('Error', 'No se pudo actualizar el item', 'error') }

        });

    } else {

           //Agregar item
          peticion = this.itemsService.addItem(this.item);
          peticion.subscribe(resp => {

            this.item.Id = resp;

            this.items.push(this.item);
              Swal.fire(this.item.Descripcion, 'Se agregó item correctamente','success' );

              }, (error) => {

                if(error.error)
                {
                  let mensajeError = error.error.Message;
                  Swal.fire('Error', mensajeError, 'error');
                }
                else { Swal.fire('Error', 'No se pudo agregar el item', 'error') }
          });
       }

       this.activeModal.close();
       this.loadingInfo= false;
       Swal.hideLoading();


  }
}

