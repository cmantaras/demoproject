import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';
import { ZonasService } from 'src/app/services/zona.service';
import Swal from 'sweetalert2';
import { ZonaModel } from 'src/app/models/zona/zona.model';



@Component({
  selector: 'app-eliminar-zona',
  template: `
  <div class="modal-header">
    <h4 class="modal-title" id="modal-title">Eliminar zona</h4>
    <button type="button" class="close" aria-describedby="modal-title" (click)="activeModal.dismiss('Cross click')">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="modal-body">
    <p><strong>¿ Estás seguro que deseas eliminar la zona ? <span class="text-primary"> {{ zona.Descripcion }}   </span> </strong></p>

  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-outline-secondary" (click)="activeModal.dismiss('cancel click')">Salir</button>
    <button type="button" class="btn btn-danger" (click)="eliminar()">Ok</button>
  </div>
  `,
  styles: [
  ]
})

export class DeleteZonaComponent implements OnInit {

  public zona : ZonaModel = new ZonaModel();
  public modalHeader : string;
  zonas : ZonaModel [] = [];

  constructor( public activeModal: NgbActiveModal,
               private zonasService: ZonasService, ) { }

  ngOnInit(): void {  }

  eliminar(){

    this.zonasService.deleteZona(this.zona).
      subscribe(resp => {

      Swal.fire('', 'Se eliminó zona correctamente','success' );

      var indice = this.zonas.indexOf(this.zona);
      this.zonas.splice(indice, 1);

    }, (error) => {
      if(error.error)
      {
        let mensajeError = error.error.Message;
        Swal.fire('Error', mensajeError, 'error');
      }
      else { Swal.fire('Error', 'No se pudo eliminar zona', 'error') }
    });

    this.activeModal.close();
  }

}
