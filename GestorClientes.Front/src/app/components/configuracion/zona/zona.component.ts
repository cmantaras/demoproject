import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ZonasService } from 'src/app/services/zona.service';
import { AddEditzonaComponent } from 'src/app/components/configuracion/zona/add-edit-zona.component';
import { NgbModal, NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';
import { ZonaModel } from '../../../models/zona/zona.model';

import { DeleteZonaComponent } from './delete-zona.components';


@Component({
  selector: 'app-zona',
  templateUrl: './zona.component.html',
  styleUrls: ['./zona.component.css'],
  providers: [NgbPaginationConfig]
})
export class ZonaComponent implements OnInit {

  loading : boolean = true;
  zonas : ZonaModel  [] = [];

  //Paginación
  page = 1;
  pageSize =10;
  items = [];
  totalItems : number;


  constructor(private zonasService: ZonasService,
              public router:ActivatedRoute,
              public modalService: NgbModal,
              private config: NgbPaginationConfig,)
    {

    }

    ngOnInit(): void {
      this.page =1;
      this.fillzonas();
    }


    fillzonas(){

      this.loading = true;
      this.zonasService.getZonas()
        .subscribe( (data : any) => {

          this.zonas = data.Zonas;

          this.loading = false;

        },( errorServicio ) => {
          this.loading = false;
        });

    }


    openModalAdd() {
      const modalRef = this.modalService.open(AddEditzonaComponent, { backdrop: 'static'});
      modalRef.componentInstance.modalHeader = 'Agregar zona';
      modalRef.componentInstance.zonas = this.zonas;

    }

    openModalDelete( zona : ZonaModel ) {

      const modalRef = this.modalService.open(DeleteZonaComponent, { backdrop: 'static'});
      modalRef.componentInstance.modalHeader = 'Eliminar zona';
      modalRef.componentInstance.zona = zona;

      modalRef.componentInstance.zonas = this.zonas;
    }

    openModalEdit(zona: ZonaModel){

      var zonaAux = JSON.parse( JSON.stringify( zona ) );

      const modalRef = this.modalService.open(AddEditzonaComponent, { backdrop: 'static'});
      modalRef.componentInstance.modalHeader = 'Editar zona';
      modalRef.componentInstance.zonas = this.zonas;

      modalRef.componentInstance.zona = zonaAux;

    }
}
