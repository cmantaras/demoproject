import { Component, EventEmitter, Injectable, OnInit, Output } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgForm } from '@angular/forms';
import { ZonasService } from 'src/app/services/zona.service';
import { ZonaModel } from 'src/app/models/zona/zona.model';
import Swal from 'sweetalert2';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-add-edit-zona',
  template: `
    <div class="modal-header">
      <h4 class="modal-title"> {{ modalHeader }} </h4>
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">


        <!-- BASIC FORM ELELEMNTS -->
              <form autocomplete="off" (ngSubmit)="guardar( form )" #form="ngForm" class="form-horizontal style-form" method="get">
                <div class="form-group">

                  <label class="col-sm-2 col-sm-2 control-label">Descripción</label>
                  <div class="col-sm-10">
                  <input type="text"
                            class="form-control is-invalid"
                            name="Descripcion"
                            [ngClass]="{ 'is-invalid': form.submitted && descripcion.invalid }"
                            [class.is-invalid]="descripcion.invalid && descripcion.touched"
                            [(ngModel)] = "zona.Descripcion"
                            required
                            placeholder="Descripción"
                            minlength="1"
                            #descripcion="ngModel"

                           >
                           <small   *ngIf="descripcion.invalid && descripcion.touched"
                                   class="form-text text-danger">Ingrese una descripción de zona válida</small>
                  </div>

                  <label class="col-sm-2 col-sm-2 control-label">Código</label>
                  <div class="col-sm-10">
                  <input type="text"
                            class="form-control is-invalid"
                            name="Codigo"
                            [ngClass]="{ 'is-invalid': form.submitted && codigo.invalid}"
                            [class.is-invalid]="codigo.invalid && codigo.touched"
                            [(ngModel)] = "zona.Codigo"
                            required
                            placeholder="Código"
                            minlength="1"
                            #codigo="ngModel"

                           >
                           <small   *ngIf="codigo.invalid && codigo.touched"
                                   class="form-text text-danger">Debe ingresar un código</small>
                  </div>

                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-danger" (click)="activeModal.close('Close click')">Salir</button>
                  <button type="submit" class="btn btn-success">Guardar</button>
                </div>
              </form>

    </div>


  `,
  styles: [
  ]
})
@Injectable()
export class AddEditzonaComponent   {

  public zona : ZonaModel = new ZonaModel();
  public modalHeader : string;
  public zonas : ZonaModel[] = [];

  @Output() sendzona= new EventEmitter();


  constructor( public activeModal: NgbActiveModal,
              private zonasService: ZonasService
                       ) {
  }

  guardar(form : NgForm ) {

    if(form.invalid) {
      return;
    }


    Swal.fire('zona', 'La zona se está ingresando', 'info');
    let peticion : Observable<any>;

    if ( this.zona.Id )  {


        //Actualizar zona
        peticion = this.zonasService.updateZona(this.zona);

        peticion.subscribe(resp => {
          Swal.fire(this.zona.Descripcion, 'Se actualizo correctamente','success' );

          Object.assign(this.zonas[this.zonas.findIndex(el => el.Id === this.zona.Id)], this.zona);
          this.activeModal.close();
          }, (error) => {
            // console.log(error);

            if(error.error) {
            let mensajeError = error.error.Message;
            Swal.fire('Info', mensajeError, 'info');
            }
            else { Swal.fire('Error', 'No se pudo actualizar la zona', 'error') }

        });

    } else {

           //Agregar zona
          peticion = this.zonasService.addZona(this.zona);
          peticion.subscribe(resp => {

            this.zona.Id = resp;

            this.zonas.push(this.zona);
              Swal.fire(this.zona.Descripcion, 'Se agregó correctamente','success' );
              this.activeModal.close();
              }, (error) => {
                // console.log(error)
                if(error.error)
                {
                  let mensajeError = error.error.Message;
                  Swal.fire('Error', mensajeError, 'info');
                }
                else { Swal.fire('Error', 'No se pudo agregar la zona', 'error') }
          });
       }



       Swal.hideLoading();


  }
}

