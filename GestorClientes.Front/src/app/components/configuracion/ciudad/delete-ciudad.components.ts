import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';
import { CiudadesService } from 'src/app/services/ciudades.service';
import Swal from 'sweetalert2';
import { CiudadModel } from 'src/app/models/ciudad/ciudad.model';



@Component({
  selector: 'app-eliminar-ciudad',
  template: `
  <div class="modal-header">
    <h4 class="modal-title" id="modal-title">Eliminar ciudad</h4>
    <button type="button" class="close" aria-describedby="modal-title" (click)="activeModal.dismiss('Cross click')">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="modal-body">
    <p><strong>¿ Estás seguro que deseas eliminar la ciudad ? <span class="text-primary"> {{ ciudad.Descripcion }}   </span> </strong></p>

  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-outline-secondary" (click)="activeModal.dismiss('cancel click')">Salir</button>
    <button type="button" class="btn btn-danger" (click)="eliminar()">Confirmar</button>
  </div>
  `,
  styles: [
  ]
})

export class DeleteCiudadComponent implements OnInit {

  public ciudad : CiudadModel = new CiudadModel();
  public modalHeader : string;
  ciudades : CiudadModel [] = [];

  constructor( public activeModal: NgbActiveModal,
               private ciudadesService: CiudadesService, ) { }

  ngOnInit(): void {  }

  eliminar(){

    this.ciudadesService.deleteCiudad(this.ciudad).
      subscribe(resp => {

      Swal.fire('', 'Se eliminó ciudad correctamente','success' );

      var indice = this.ciudades.indexOf(this.ciudad);
      this.ciudades.splice(indice, 1);

    }, (error) => {
      if(error.error)
      {
        let mensajeError = error.error.Message;
        Swal.fire('Error', mensajeError, 'error');
      }
      else { Swal.fire('Error', 'No se pudo eliminar ciudad', 'error') }
    });

    this.activeModal.close();
  }

}
