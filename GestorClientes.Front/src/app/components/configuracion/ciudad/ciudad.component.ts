import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { CiudadesService } from 'src/app/services/ciudades.service';
import { AddEditCiudadComponent } from 'src/app/components/configuracion/ciudad/add-edit-ciudad.component';
import { NgbModal, NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';
import { CiudadModel } from '../../../models/ciudad/ciudad.model';

import { DeleteCiudadComponent } from './delete-ciudad.components';

@Component({
  selector: 'app-ciudad',
  templateUrl: './ciudad.component.html',
  styleUrls: ['./ciudad.component.css'],
  providers: [NgbPaginationConfig]
})
export class CiudadComponent implements OnInit {

  loading : boolean = true;
  ciudades : CiudadModel  [] = [];
  loadingInfo : boolean = false;

  ciudad : CiudadModel;

  //Paginación
  page = 1;
  pageSize =10;
  items = [];
  totalItems : number;


  constructor(private ciudadesService: CiudadesService,
              public router:ActivatedRoute,
              public modalService: NgbModal,
              private config: NgbPaginationConfig,)
    {

      this.ciudad = new CiudadModel();

    }

    ngOnInit(): void {
      this.page =1;
      this.fillCiudades();
    }


    fillCiudades(){

      this.loading = true;
      this.ciudadesService.getCiudades()
        .subscribe( (data : any) => {
          this.ciudades = data.Ciudades;

          this.loading = false;

        },( errorServicio ) => {
          this.loading = false;
        });

    }


    openModalAdd() {
      const modalRef = this.modalService.open(AddEditCiudadComponent, { backdrop: 'static'});
      modalRef.componentInstance.modalHeader = 'Agregar ciudad';
      modalRef.componentInstance.ciudades = this.ciudades;
      modalRef.result.then(() =>{
        this.ngOnInit();
      })


    }

    openModalDelete( ciudad : CiudadModel ) {

      const modalRef = this.modalService.open(DeleteCiudadComponent, { backdrop: 'static'});
      modalRef.componentInstance.modalHeader = 'Eliminar ciudad';
      modalRef.componentInstance.ciudad = ciudad;

      modalRef.componentInstance.ciudades = this.ciudades;
    }

    openModalEdit(ciudad: CiudadModel){

      var ciudadAux = JSON.parse( JSON.stringify( ciudad ) );

      const modalRef = this.modalService.open(AddEditCiudadComponent, { backdrop: 'static'});
      modalRef.componentInstance.modalHeader = 'Editar ciudad';
      modalRef.componentInstance.ciudades = this.ciudades;

      modalRef.componentInstance.ciudad = ciudadAux;
      modalRef.result.then(() =>{
        this.ngOnInit();
      })

    }
}
