import { Component, EventEmitter, Injectable, OnInit, Output } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgForm } from '@angular/forms';
import { CiudadesService } from 'src/app/services/ciudades.service';
import { CiudadModel } from 'src/app/models/ciudad/ciudad.model';
import Swal from 'sweetalert2';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-add-edit-ciudad',
  template: `
    <div class="modal-header">
      <h4 class="modal-title"> {{ modalHeader }} </h4>
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">


        <!-- BASIC FORM ELELEMNTS -->
              <form autocomplete="off" (ngSubmit)="guardar( form )" #form="ngForm" class="form-horizontal style-form" method="get">
                <div class="form-group">

                  <label class="col-sm-2 col-sm-2 control-label">Descripción</label>
                  <div class="col-sm-10">
                  <input type="text"
                            class="form-control is-invalid"

                            name="Descripcion"
                            [ngClass]="{ 'is-invalid': form.submitted && descripcion.touched }"
                            [class.is-invalid]="descripcion.invalid && descripcion.touched"
                            [(ngModel)] = "ciudad.Descripcion"
                            required
                            placeholder="Descripción"
                            minlength="1"
                            #descripcion="ngModel"

                           >
                           <small   *ngIf="descripcion.invalid && descripcion.touched"
                                   class="form-text text-danger">Ingrese una descripción de ciudad válida</small>
                  </div>

                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-danger" (click)="activeModal.close('Close click')">Salir</button>
                  <button type="submit"[disabled]="loadingInfo" class="btn btn-success">Guardar</button>
                </div>
              </form>

    </div>


  `,
  styles: [
  ]
})
@Injectable()
export class AddEditCiudadComponent   {

  public ciudad : CiudadModel = new CiudadModel();
  public modalHeader : string;
  public ciudades : CiudadModel[] = [];
  public loadingInfo : boolean = false;

  @Output() sendciudad= new EventEmitter();


  constructor( public activeModal: NgbActiveModal,
              private ciudadsService: CiudadesService
                       ) {
  }

  guardar(form : NgForm ) {

    if(form.invalid) {
      return;
    }
    this.loadingInfo = true;
    // if(this.ciudades.find(x => x.Descripcion === this.ciudad.Descripcion)){

    //   Swal.fire('ciudad', 'La ciudad ingresada ya existe', 'info');
    //   return;
    // }


    Swal.fire('ciudad', 'El ciudad se está ingresando', 'info');
    let peticion : Observable<any>;

    if ( this.ciudad.Id )  {


        //Actualizar ciudad
        peticion = this.ciudadsService.updateCiudad(this.ciudad);

        peticion.subscribe(resp => {
          Swal.fire(this.ciudad.Descripcion, 'Se actualizo correctamente','success' );
          this.activeModal.close();
          //Object.assign(this.ciudades[this.ciudades.findIndex(el => el.Id === this.ciudad.Id)], this.ciudad);

          }, (error) => {
            // console.log(error);

            if(error.error) {
            let mensajeError = error.error.Message;
            Swal.fire('Error', mensajeError, 'error');
            }
            else { Swal.fire('Error', 'No se pudo actualizar el ciudad', 'error') }

        });

    } else {

           //Agregar ciudad
          peticion = this.ciudadsService.addCiudad(this.ciudad);
          peticion.subscribe(resp => {

            this.ciudad.Id = resp;

            //this.ciudades.push(this.ciudad);
              Swal.fire(this.ciudad.Descripcion, 'Se agregó correctamente','success' );
              this.activeModal.close();
              }, (error) => {

                if(error.error)
                {
                  let mensajeError = error.error.Message;
                  Swal.fire('Error', mensajeError, 'error');
                }
                else { Swal.fire('Error', 'No se pudo agregar el ciudad', 'error') }
          });
       }

      

       Swal.hideLoading();

    this.loadingInfo = false;
  }
}

