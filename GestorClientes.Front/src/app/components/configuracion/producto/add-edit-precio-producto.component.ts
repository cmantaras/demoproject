import { Component, EventEmitter, Injectable, OnInit, Output } from '@angular/core';
import { NgbActiveModal, NgbDate, NgbDateStruct, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgForm } from '@angular/forms';
import { ProductosService } from 'src/app/services/productos.service';
import { ProductoPrecioModel } from 'src/app/models/producto/productoPrecio.model';
import Swal from 'sweetalert2';
import { Observable } from 'rxjs';
import { MarcaModel } from 'src/app/models/marca/marca.model';
import { MarcasService } from 'src/app/services/marcas.service';
import { MonedaModel } from 'src/app/models/moneda/moneda.model';
import { ImpuestoModel } from 'src/app/models/impuesto/impuesto.model';
import { MetodoPagoModel } from 'src/app/models/pagos-metodo/metodo-pago.model';
import { ImpuestosService } from 'src/app/services/impuestos.service';
import { MetodoPagoService } from 'src/app/services/metodo-pago.service';
import { MonedasService } from 'src/app/services/monedas.service';
import { ProductoModel } from 'src/app/models/producto/producto.model';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';


@Component({
  selector: 'app-add-edit-productoPrecio',
  template: `
    <div class="modal-header">
      <h4 class="modal-title"> {{ modalHeader }} </h4>
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">


        <!-- BASIC FORM ELELEMNTS -->
              <form autocomplete="off" (ngSubmit)="guardar( form )" #form="ngForm" class="form-horizontal style-form" method="get">
                <div class="form-group">



                  <label class="col-sm-2 col-sm-2 control-label">Precio</label>
                  <div class="col-sm-10">
                    <input type="number"
                              class="form-control is-invalid"
                              name="Precio"
                              [ngClass]="{ 'is-invalid': form.submitted && precio.invalid}"
                              [class.is-invalid]="precio.invalid && precio.touched"
                              [(ngModel)] = "productoPrecio.Precio"
                              required
                              placeholder="Precio"
                              minlength="1"
                              #precio="ngModel"

                            >
                           <small   *ngIf="precio.invalid && precio.touched"
                                   class="form-text text-danger">Ingrese un precio válido</small> 
                  </div>
                  <label class=" col-sm-12 control-label">Fecha desde</label>
                  <div class="input-group  col-sm-9">
                      <input class="form-control"
                            placeholder="dd-mm-yyyy"
                            name="dp"
                            [(ngModel)]="desde"
                            ngbDatepicker 
                            readonly
                            #d1="ngbDatepicker"
                          >

                        <div class="input-group-append">
                        <button class="btn btn-outline-secondary btn-fecha calendar" (click)="d1.toggle()" type="button"><i class="fa fa-calendar"></i></button>
                      </div>
                      </div>

                      <label class="col-sm-2 col-sm-2 control-label">Moneda</label>
                 <div class="col-sm-9">
                   <!--Using items input-->
                    <ng-select [items]="monedas"
                        [disabled]="true"
                        [ngClass]="{ 'is-invalid': form.submitted && moneda.invalid }"
                        [class.is-invalid]="moneda.invalid && moneda.touched"
                        name="monedas"
                        required
                        bindLabel="Descripcion"
                        bindValue="Id"
                        [(ngModel)]="productoPrecio.MonedaId"
                        #moneda="ngModel">
                    </ng-select>
                   <!-- <small    *ngIf="form.submitted && moneda.invalid && !moneda"
                                        class="form-text text-danger">  Seleccione una moneda</small>-->

                  </div>

                  <label class="col-sm-12 control-label">Impuesto</label>
                 <div class="col-sm-10 flex-sw ">
                   <!--Using items input-->
                    <ng-select [items]="impuestos"

                        name="impuesto"
                        required
                        bindLabel="Descripcion"
                        bindValue="Id"
                        [(ngModel)]="productoPrecio.ImpuestoId"
                        #impuesto="ngModel">
                    </ng-select>
                    <small   *ngIf="!productoPrecio.ImpuestoId && impuesto.touched"
                                   class="form-text text-danger">Seleccione un impuesto</small>

                  </div>

                  <label class="col control-label">Métodos de pago</label>
                  <div class="col-sm-10 flex-sw ">
                  <ng-select
                        [disabled]= "deshabilitado"
                        [items]="metodosSinMensual"
                        [multiple]="true"
                        bindLabel="Descripcion"
                        [selectableGroup]="true"
                        [closeOnSelect]="false"
                        bindValue="Id"
                        [(ngModel)]="selectedMetodos"
                        name="func"
                        ngDefaultControl 
                        #metodo="ngModel">
                        
                        <!-- <ng-template ng-optgroup-tmp let-item="item" let-item$="item$" let-index="index">
                            <input id="item-{{index}}" type="checkbox" [ngModel]="item$.selected"/> {{item.Nombre | uppercase}}
                        </ng-template> -->
                        <ng-template name="ngtem" ng-option-tmp let-item="item" let-item$="item$" let-index="index">
                            <div name="f" id="item-{{index}}"  >  </div>{{item.Descripcion}}
                        </ng-template>
                  </ng-select>
                  <small   *ngIf="selectedMetodos == [] && metodo.touched"
                                   class="form-text text-danger">Seleccione un método</small>
                  </div>




                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-danger" (click)="activeModal.close('Close click')">Salir</button>
                  <button type="submit" class="btn btn-success">Guardar</button>
                </div>
              </form>

    </div>


  `,
  styles: [
  ]
})
@Injectable()
export class AddEditProductoPrecioComponent implements OnInit  {

  public productoPrecio : ProductoPrecioModel = new ProductoPrecioModel();
  public modalHeader : string;
  public productoPrecios : ProductoPrecioModel[] = [];
  public productoSelected : ProductoModel = new ProductoModel();
  public marcas : MarcaModel [] = [];
  public monedas : MonedaModel [] = [];
  public impuestos : ImpuestoModel [] = [];
  public metodos : MetodoPagoModel [] = [];
  public metodosSinMensual : MetodoPagoModel [] = [];
  desde : NgbDateStruct = {  year : null, month : null, day: null };


  public selectedMetodos : number [] = [];
  public deshabilitado  : boolean;



  @Output() sendproductoPrecio= new EventEmitter();



  constructor( public activeModal: NgbActiveModal,
              private productoPreciosService: ProductosService,
              private marcasService : MarcasService,
              private impuestosService : ImpuestosService,
              private metodosService : MetodoPagoService,
              private monedasService : MonedasService,
                                     ) {

  }

  ngOnInit () {


    if(this.productoPrecio.Id){
      var fecha = new Date(this.productoPrecio.Desde);
      this.desde = { day : fecha.getDate(), month : fecha.getMonth() + 1, year : fecha.getFullYear() }

      this.selectedMetodos = this.productoPrecio.Metodos.map(x => x.Id);

    }
    else{
        this.productoPrecio.Producto = this.productoSelected;
        
    }

    this.monedasService.getMonedas()
    .subscribe( ( data : any) => {
      this.monedas = data.Monedas;
      var monedaId = this.monedas.filter(m => m.Descripcion == '$')[0].Id;

      this.productoPrecio.MonedaId = monedaId;
      //this.producto.PrecioActual.MonedaId = monedaId
    });

    this.impuestosService.getImpuestos()
    .subscribe( ( data : any) => {
      this.impuestos = data.Impuestos;
    });

    this.metodosService.getMetodosPago()
    .subscribe( ( data : any) => {
      this.metodos = data.Metodos;
      this.obtenerSinMensual();
      this.changeValuesModel();




    });

    
  }

  obtenerSinMensual(){
    this.metodosSinMensual = this.metodos.slice();
    const mensual = this.metodosSinMensual.find(elem => elem.Descripcion.toLowerCase() == 'mensual');
    const index = this.metodosSinMensual.indexOf(mensual);
    this.metodosSinMensual.splice(index,1);
  }


  changeValuesModel(){

    if (this.productoPrecio.Producto.Servicio) {
      var metodo = this.metodos.find(el => el.Descripcion.toLowerCase() == 'mensual');

      this.metodosSinMensual= [metodo];

      var metodoId = metodo.Id;
      this.selectedMetodos = [metodoId];
      this.deshabilitado= true;
    } else {
      this.obtenerSinMensual();
      this.selectedMetodos = [];
      this.selectedMetodos = this.productoPrecio.MetodosIds;
      this.deshabilitado = false;
    }
  }


  guardar(form : NgForm ) {

    if(form.invalid) {
      return;
    }

    // if(this.productoPrecios.find(x => x.Descripcion === this.productoPrecio.Descripcion)){

    //   Swal.fire('productoPrecio', 'La productoPrecio ingresada ya existe', 'info');
    //   return;
    // }

    this.productoPrecio.Desde = this.desde.month + "/" + this.desde.day +"/" + this.desde.year;
    this.productoPrecio.MetodosIds= this.selectedMetodos;
    Swal.fire('productoPrecio', 'El productoPrecio se está ingresando', 'info');
    let peticion : Observable<any>;

    if ( this.productoPrecio.Id )  {

        //Actualizar productoPrecio
        peticion = this.productoPreciosService.updateProductoPrecio(this.productoPrecio);

        peticion.subscribe(resp => {
          Swal.fire('', 'Se actualizo correctamente','success' );

         //Object.assign(this.productoPrecios[this.productoPrecios.findIndex(el => el.Id === this.productoPrecio.Id)], this.productoPrecio);
          this.activeModal.close();
          }, (error) => {
            // console.log(error);

            if(error.error) {
            let mensajeError = error.error.Message;
            Swal.fire('Error', mensajeError, 'error');
            }
            else { Swal.fire('Error', 'No se pudo actualizar el productoPrecio', 'error') }

        });

    } else {

           //Agregar productoPrecio

           this.productoPrecio.MetodosIds =  this.selectedMetodos;

          this.productoPrecio.ProductoId = this.productoSelected.Id;

          peticion = this.productoPreciosService.addProductoPrecio(this.productoPrecio);
          peticion.subscribe(resp => {

            this.productoPrecio.Id = resp;
            this.activeModal.close();
            this.productoPrecios.push(this.productoPrecio);
              Swal.fire('', 'Se agregó precio','success' );
              this.activeModal.close();
              }, (error) => {

                if(error.error)
                {
                  let mensajeError = error.error.Message;
                  Swal.fire('Error', mensajeError, 'error');

                }
                else { Swal.fire('Error', 'No se pudo agregar el productoPrecio', 'error') }
          });
       }



       Swal.hideLoading();


  }
}