import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';
import { ProductosService } from 'src/app/services/productos.service';
import Swal from 'sweetalert2';
import { ProductoModel } from 'src/app/models/producto/producto.model';



@Component({
  selector: 'app-eliminar-producto',
  template: `
  <div class="modal-header">
    <h4 class="modal-title" id="modal-title">Eliminar producto</h4>
    <button type="button" class="close" aria-describedby="modal-title" (click)="activeModal.dismiss('Cross click')">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="modal-body">
    <p><strong>¿ Estás seguro que deseas eliminar el producto ? <span class="text-primary"> {{ producto.Descripcion }}   </span> </strong></p>

  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-outline-secondary" (click)="activeModal.dismiss('cancel click')">Salir</button>
    <button type="button" class="btn btn-danger" (click)="eliminar()">Confirmar</button>
  </div>
  `,
  styles: [
  ]
})

export class DeleteProductoComponent implements OnInit {

  public producto : ProductoModel = new ProductoModel();
  public modalHeader : string;
  productos : ProductoModel [] = [];

  constructor( public activeModal: NgbActiveModal,
               private productosService: ProductosService, ) { }

  ngOnInit(): void {  }

  eliminar(){

    this.productosService.deleteProducto(this.producto).
      subscribe(resp => {

      Swal.fire('', 'Se eliminó producto correctamente','success' );

      var indice = this.productos.indexOf(this.producto);
      this.productos.splice(indice, 1);

    }, (error) => {
      if(error.error)
      {
        let mensajeError = error.error.Message;
        Swal.fire('Error', mensajeError, 'error');
      }
      else { Swal.fire('Error', 'No se pudo eliminar producto', 'error') }
    });

    this.activeModal.close();
  }

}
