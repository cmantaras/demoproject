import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { registerLocaleData } from '@angular/common';
import { ProductosService } from 'src/app/services/productos.service';
import { AddEditProductoComponent } from 'src/app/components/configuracion/producto/add-edit-producto.component';
import { NgbModal, NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';
import { ProductoModel } from '../../../models/producto/producto.model';
import es from '@angular/common/locales/es';
import { DeleteProductoComponent } from './delete-producto.component';
import { ProductoPrecioModel } from 'src/app/models/producto/productoPrecio.model';
import { debounceTime, delay, distinctUntilChanged } from 'rxjs/operators';
import { MetodoPagoModel } from 'src/app/models/pagos-metodo/metodo-pago.model';
import { faNewspaper } from '@fortawesome/free-solid-svg-icons';
import { AddEditProductoPrecioComponent } from './add-edit-precio-producto.component';
import { DeleteProductoPrecioComponent } from './delete-precio-producto.component';
import { MetodoPagoService } from 'src/app/services/metodo-pago.service';
import Swal from 'sweetalert2';
import { Subject } from 'rxjs/internal/Subject';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.css'],
  providers: [NgbPaginationConfig]
})
export class ProductoComponent implements OnInit {

  loading : boolean = true;
  productos : ProductoModel  [] = [];
  productoSelected : ProductoModel = new ProductoModel();
  productoPrecios : ProductoPrecioModel [] = [];
  precioMetodos : MetodoPagoModel [] = [];

  precioActual : ProductoPrecioModel;

  metodos : MetodoPagoModel [] = [];
  readonly allMetodos : MetodoPagoModel [] = this.metodos;
  metodosDisponibles : MetodoPagoModel [] = [];
  selectedMetodos : number [] = [];
  selectedProductoPrecio : ProductoPrecioModel = new ProductoPrecioModel();
  
  filtroProducto : ProductoModel = new ProductoModel();

  loadingData : boolean = false;
  loadingDataMetodos : boolean = false;

  //Paginación
  pageMetodo = 1;
  pageProducto = 1;
  pagePrecio = 1;

  pageSize =10;
  items = [];
  totalItems : number;

  filtroCampoUsuario : Subject<string> = new Subject<string>();
  campoFiltro: string;
  
  constructor(private productosService: ProductosService,
              private metodosService : MetodoPagoService,
              public router:ActivatedRoute,
              public modalService: NgbModal,
              private config: NgbPaginationConfig,
              private route : Router)
    {
      registerLocaleData( es );
      

      this.filtroCampoUsuario.pipe(debounceTime(800), distinctUntilChanged())
      .subscribe(model => {
        this.campoFiltro = model;
        this.filter();
      })
    }


    ngOnInit(): void {
      this.pageMetodo = 1;
      this.pageProducto = 1;
      this.pagePrecio = 1;
      this.productos = [];
      this.metodosService.getMetodosPago()
      .subscribe((data : any) => {

        this.metodos= data.Metodos;
        this.metodosDisponibles = this.metodos;
      });
      this.fillproductos();
    }


    fillproductos(){

      this.loading = true;
      this.productosService.getProductos()
        .subscribe( (data : any) => {

          this.productos = data.Productos;

          this.loading = false;

        },( errorServicio ) => {
          this.loading = false;
        });

    }


    getProductPrecioById(productoPrecio : ProductoPrecioModel){

      this.selectedProductoPrecio = productoPrecio;

      this.productoPrecios.forEach(x => {
        x.Color = false
      });
      productoPrecio.Color = true;
      Object.assign(this.productoPrecios[this.productoPrecios.findIndex(el => el.Id === productoPrecio.Id)], productoPrecio);

      this.loadingDataMetodos = true;
      setTimeout(() => {

        this.productosService.getProductoPrecioById(productoPrecio.Id)
        .subscribe((data : ProductoPrecioModel) =>{
          delay(700),



          this.precioMetodos = data.Metodos;
          this.loadMetodosDisponibles();

          this.loadingDataMetodos = false;
        }, (error : any) => {

        });
      }, 700);
    }


    getPrecios(producto : ProductoModel){

      this.productoSelected = producto;

      this.productos.forEach(x => {
        x.Color = false
      });
      Object.assign(this.productos[this.productos.findIndex(el => el.Id === producto.Id)], producto);
      producto.Color = true;
      this.loadingData = true;
      this.precioMetodos = [];

      setTimeout(() => {

        this.productosService.getPrecios(producto)
        .subscribe((data : any) =>{
          delay(2000),
          this.productoPrecios = data.ProductoPrecios;

          var date = new Date();
          date.setDate(date.getDate() + 1)
          var aryFiltered = this.productoPrecios.filter(p => new Date(p.Desde) <= date  )



         this.precioActual= aryFiltered[aryFiltered.length -1]

          producto.PrecioActual = this.precioActual

          this.loadingData = false;
        }, (error : any) => {

        });
      }, 700);
    }


    openModalAdd() {
      const modalRef = this.modalService.open(AddEditProductoComponent, { backdrop: 'static'});
      modalRef.componentInstance.modalHeader = 'Agregar producto';
      modalRef.componentInstance.productos = this.productos;
      modalRef.result.then(res => {
        // this.fillproductos();
         this.route.navigateByUrl('/', {skipLocationChange: true}).then(() => {
           this.route.navigate([`/producto`]);
     });

      });
    }

    openModalDelete( producto : ProductoModel ) {

      const modalRef = this.modalService.open(DeleteProductoComponent, { backdrop: 'static'});
      modalRef.componentInstance.modalHeader = 'Eliminar producto';
      modalRef.componentInstance.producto = producto;
      modalRef.componentInstance.productos = this.productos;

      modalRef.result.then(res => {
        // this.fillproductos();
         this.route.navigateByUrl('/', {skipLocationChange: true}).then(() => {
           this.route.navigate([`/producto`]);
          });
     });
    }

    openModalEdit(producto: ProductoModel){

      var productoAux = JSON.parse( JSON.stringify( producto ) );

      const modalRef = this.modalService.open(AddEditProductoComponent, { backdrop: 'static'});
      modalRef.componentInstance.modalHeader = 'Editar producto';
      modalRef.componentInstance.productos = this.productos;
      modalRef.componentInstance.producto = productoAux;

      modalRef.result.then(
        (result : any) => {
          this.ngOnInit();
        },
        ()=> {

      });


    }

    //Modals producto precio
    openModalAddPrecio() {
      const modalRef = this.modalService.open(AddEditProductoPrecioComponent, { backdrop: 'static'});
      modalRef.componentInstance.modalHeader = 'Agregar precio a producto';
      modalRef.componentInstance.productoSelected = this.productoSelected;
      modalRef.componentInstance.productos = this.productos;
      
      modalRef.result.then(
        (result : any) => {
          this.getPrecios(this.productoSelected);
        },
        ()=> {

      });
    }

    openModalDeleteProductoPrecio( producto : ProductoModel ) {

      const modalRef = this.modalService.open(DeleteProductoPrecioComponent, { backdrop: 'static'});
      modalRef.componentInstance.modalHeader = 'Eliminar producto';
      modalRef.componentInstance.productoPrecio = producto;
      modalRef.componentInstance.productoPrecios = this.productoPrecios;

      modalRef.result.then(
              (result : any) => {
                this.getPrecios(this.productoSelected);
              },
              ()=> {

            });
    }

    openModalEditProductoPrecio(productoPrecio : ProductoPrecioModel) {
      const modalRef = this.modalService.open(AddEditProductoPrecioComponent, { backdrop: 'static'});
      modalRef.componentInstance.modalHeader = 'Editar precio de producto';
      modalRef.componentInstance.productoSelected = this.productoSelected;
      modalRef.componentInstance.productoPrecio = productoPrecio;
      modalRef.componentInstance.productos = this.productos;

      modalRef.result.then(
        (result : any) => {
          this.getPrecios(this.productoSelected);
        },
        ()=> {

      });
    }

    loadMetodosDisponibles(){
      this.metodosService.getMetodosPago()
      .subscribe((data : any) => {
        this.metodos= data.Metodos;
        this.precioMetodos.forEach( element => {
          this.metodos.forEach( (item, index) => {
            if(item.Id === element.Id)     this.metodos.splice(index,1);
          });
        });
      });

    }

    openModalAddMetodos(content) {

    const modalRef = this.modalService.open(content, { backdrop: 'static'});
      modalRef.result.then(
        (result : any) => {
          this.productosService.addMetodosToProductoPrecio(this.selectedMetodos, this.selectedProductoPrecio.Id)
            .subscribe((resp : any) => {
              this.getProductPrecioById(this.selectedProductoPrecio);
            })
        },
        ()=> {

      });

    }

    eliminarMetodo(){
    }

    openModalDeleteMetodo( content, MetodoPago : MetodoPagoModel ) {
      if(this.precioMetodos.length <= 1){
        Swal.fire('El precio debe tener asignado al menos un método de pago');
        return;
      }
      const modalRef = this.modalService.open(content, { backdrop: 'static'}).result.then(result => {
        this.productosService.deleteMetodoPago(this.selectedProductoPrecio.Id, MetodoPago.Id)
          .subscribe( data => {
            this.getProductPrecioById(this.selectedProductoPrecio);
          });
      });
    }

  //FILTRO
  onCampoFiltroChange(query:string){
    this.filtroCampoUsuario.next(query);
  }

  filter(print : boolean = false)  :void
  { 
    
    if(this.campoFiltro == ""){

      this.productosService.getProductos()
      .subscribe( (data : any) => {
      this.loadingData=true;
      this.productos = data.Productos;

      this.loadingData = false; 
    },( errorServicio ) => {
        this.loadingData = false;
    });
    }
    else{

      this.loadingData = true;

      this.filtroProducto.Descripcion =  this.campoFiltro;
      
      //this.filtroProducto.Offset = this.page;
      //this.filtroProducto.Limit = this.pageSize;

      this.productosService.getProductosFiltrados(this.filtroProducto.Descripcion)
        .subscribe( (data : any) => {
          

        this.productos = data.Productos;
  
        this.loadingData = false;
        this.filtroProducto = new ProductoModel();
  
      },( errorServicio ) => {
          this.loadingData = false;
      });
  
    }


    
  }






}
