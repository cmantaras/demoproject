import { Component, EventEmitter, Injectable, OnInit, Output } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgForm } from '@angular/forms';
import { ProductosService } from 'src/app/services/productos.service';
import { ProductoModel } from 'src/app/models/producto/producto.model';
import Swal from 'sweetalert2';
import { Observable } from 'rxjs';
import { MarcaModel } from 'src/app/models/marca/marca.model';
import { MarcasService } from 'src/app/services/marcas.service';
import { MonedaModel } from 'src/app/models/moneda/moneda.model';
import { ImpuestoModel } from 'src/app/models/impuesto/impuesto.model';
import { MetodoPagoModel } from 'src/app/models/pagos-metodo/metodo-pago.model';
import { ImpuestosService } from 'src/app/services/impuestos.service';
import { MetodoPagoService } from 'src/app/services/metodo-pago.service';
import { MonedasService } from 'src/app/services/monedas.service';
import { ProductoPrecioModel } from 'src/app/models/producto/productoPrecio.model';


@Component({
  selector: 'app-add-edit-producto',
  template: `
    <div class="modal-header">
      <h4 class="modal-title"> {{ modalHeader }} </h4>
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">


        <!-- BASIC FORM ELELEMNTS -->
              <form autocomplete="off" (ngSubmit)="guardar( form )" #form="ngForm" class="form-horizontal style-form" method="get">
                <div class="form-group">

                  <label class="col-sm-2 col-sm-2 control-label">Descripción</label>
                  <div class="col-sm-10">
                    <input type="text"
                              class="form-control is-invalid"
                              name="Descripcion"
                              [ngClass]="{ 'is-invalid': form.submitted && descripcion.invalid }"
                              [class.is-invalid]="descripcion.invalid && descripcion.touched"
                              [(ngModel)] = "producto.Descripcion"
                              required
                              placeholder="Descripción"
                              minlength="1"
                              #descripcion="ngModel"

                            >
                           <small   *ngIf="descripcion.invalid && descripcion.touched"
                                   class="form-text text-danger">Ingrese una descripción válida</small> 
                  </div>

                  <label class="col-sm-2 col-sm-2 control-label">Marca</label>
                 <div class="col-sm-10 flex-sw ">
                   <!--Using items input-->
                    <ng-select [items]="marcas"
                    [disabled]="disabled"
  
                        name="marca"
                        required
                        bindLabel="Descripcion"
                        bindValue="Id"
                        [(ngModel)]="producto.MarcaId"
                        #marca="ngModel">
                        
                    </ng-select>
                    <small    *ngIf="!producto.MarcaId && marca.touched"
                                        class="form-text text-danger">  Seleccione una marca</small>
                    

                  </div>

                  <label class="col-sm-2 col-sm-2 control-label">Código</label>
                  <div class="col-sm-10">
                    <input type="text"
                              class="form-control is-invalid"
                              name="Código"
                              [ngClass]="{ 'is-invalid': form.submitted && codigo.invalid }"
                              [class.is-invalid]="codigo.invalid && codigo.touched"
                              [(ngModel)] = "producto.Codigo"
                              required
                              placeholder="Código"
                              minlength="1"
                              #codigo="ngModel"
                            >
                            <small    *ngIf="codigo.touched && codigo.invalid"
                                        class="form-text text-danger">  Ingrese un código</small>
                  </div>

      <div *ngIf="!producto.Id">
                  <label class="col-sm-2 col-sm-2 control-label">Precio</label>
                  <div class="col-sm-10">
                    <input type="number"
                              class="form-control is-invalid"
                              name="Precio"
                              [ngClass]="{ 'is-invalid': form.submitted && precio.invalid }"
                              [class.is-invalid]="precio.invalid && precio.touched"
                              [(ngModel)] = "producto.PrecioActual.Precio"
                              required
                              placeholder="Precio"
                              minlength="1"
                              #precio="ngModel"

                            >
                            <small    *ngIf="precio.touched && precio.invalid"
                                        class="form-text text-danger">  Ingrese un precio válido </small>
                  </div>

                  <label class="col-sm-2 col-sm-2 control-label">Moneda</label>
                 <div class="col-sm-9">
                   <!--Using items input-->
                    <ng-select [items]="monedas"
                        [disabled]="true"
                        [ngClass]="{ 'is-invalid': form.submitted && moneda.invalid }"
                        [class.is-invalid]="moneda.invalid && moneda.touched"
                        name="monedas"
                        required
                        bindLabel="Descripcion"
                        bindValue="Id"
                        [(ngModel)]="producto.PrecioActual.MonedaId"
                        #moneda="ngModel">
                    </ng-select>
                    <!-- <small    *ngIf="form.submitted && moneda.invalid && !moneda"
                                        class="form-text text-danger">  Seleccione una moneda</small>-->

                  </div>

                  <label class="col-sm-2 col-sm-2 control-label">Impuesto</label>
                 <div class="col-sm-9">
                   <!--Using items input-->
                    <ng-select [items]="impuestos"
                        [ngClass]="{ 'is-invalid': form.submitted && impuesto.invalid }"
                        [class.is-invalid]="impuesto.invalid && impuesto.touched"
                        name="impuesto"
                        required
                        bindLabel="Descripcion"
                        bindValue="Id"
                        [(ngModel)]="producto.PrecioActual.ImpuestoId"
                        #impuesto="ngModel">
                    </ng-select>
                    <small    *ngIf="impuesto.touched && impuesto.invalid"
                                        class="form-text text-danger">Seleccione un impuesto</small>

                  </div>

                  <label class="col control-label">Métodos de pago</label>
                  <div class="col-sm-9">
                    <ng-select
                          [disabled]="disabled"
                          [items]="metodosSinMensual"
                          [multiple]="true"
                          
                          bindValue="Id"
                          bindLabel="Descripcion"

                          [selectableGroup]="true"
                          [closeOnSelect]="false"
                          [(ngModel)]="selectedMetodos"
                          name="func"
                          ngDefaultControl >

                          <ng-template name="ngtem" ng-option-tmp let-item="item" let-item$="item$" let-index="index">
                              <div name="f" id="item-{{index}}"  >  </div>{{item.Descripcion}}
                          </ng-template>
                    </ng-select>


                  </div>

                   <div class="col-sm-9 mt-2">
                    <div class="custom-control custom-switch">

                      <input
                             [(ngModel)]="producto.Servicio"
                              name="servicio"
                             (ngModelChange)="changeValuesModel()"
                            style="cursor:pointer;"
                            type="checkbox"
                            class="custom-control-input" id="switch1">

                      <label class="custom-control-label" for="switch1"> Es servicio</label>
                     </div>
                  </div>

                  </div>

                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-danger" (click)="activeModal.close('Close click')">Salir</button>
                  <button type="submit" class="btn btn-success">Guardar</button>
                </div>
              </form>

    </div>


  `,
  styles: [
  ]
})
@Injectable()
export class AddEditProductoComponent implements OnInit  {

  public producto : ProductoModel = new ProductoModel();
  public modalHeader : string;
  public productos : ProductoModel[] = [];
  public marcas : MarcaModel [] = [];
  public monedas : MonedaModel [] = [];
  public impuestos : ImpuestoModel [] = [];
  public metodos : MetodoPagoModel [] = [];
  public metodosSinMensual : MetodoPagoModel [] = [];
  public disabled : boolean;
  public selectedMetodos : number [] = [];

  public monedaId : number;



  @Output() sendproducto= new EventEmitter();


  constructor( public activeModal: NgbActiveModal,
              private productosService: ProductosService,
              private marcasService : MarcasService,
              private impuestosService : ImpuestosService,
              private metodosService : MetodoPagoService,
              private monedasService : MonedasService,
                                     ) {
    this.producto.PrecioActual = new ProductoPrecioModel();
    this.producto.PrecioActual.MetodosIds = [];
    this.producto.Servicio = false;
    this.disabled = false;

  }

  ngOnInit () {

    this.marcasService.getMarcas()
      .subscribe( ( data : any) => {
        this.marcas = data.Marcas;
      });

      this.monedasService.getMonedas()
      .subscribe( ( data : any) => {
        this.monedas = data.Monedas;
        this.monedaId = this.monedas.filter(m => m.Descripcion == '$')[0].Id;

        this.producto.PrecioActual.MonedaId = this.monedaId
      });

    this.impuestosService.getImpuestos()
    .subscribe( ( data : any) => {
      this.impuestos = data.Impuestos;
    });

    this.metodosService.getMetodosPago()
    .subscribe( ( data : any) => {
      this.metodos = data.Metodos;
      this.obtenerSinMensual();
    });


  }
  obtenerSinMensual(){
    this.metodosSinMensual = this.metodos.slice();
    const mensual = this.metodosSinMensual.find(elem => elem.Descripcion.toLowerCase() == 'mensual');
    const index = this.metodosSinMensual.indexOf(mensual);
    this.metodosSinMensual.splice(index,1);
  }


  changeValuesModel(){

    if (this.producto.Servicio) {
      var metodo = this.metodos.find(el => el.Descripcion.toLowerCase() == 'mensual');

      this.metodosSinMensual= [metodo];

      var metodoId = metodo.Id;
      this.selectedMetodos = [metodoId];
      this.disabled = true;
    } else {
      this.obtenerSinMensual();
      this.selectedMetodos = [];
      this.disabled = false;
    }


  }

  guardar(form : NgForm ) {

    if(form.invalid) {
      return;
    }

    // if(this.productos.find(x => x.Descripcion === this.producto.Descripcion)){

    //   Swal.fire('producto', 'La producto ingresada ya existe', 'info');
    //   return;
    // }

    Swal.fire('producto', 'El producto se está ingresando', 'info');
    let peticion : Observable<any>;

    if ( this.producto.Id )  {

        //Actualizar producto
        peticion = this.productosService.updateProducto(this.producto);

        peticion.subscribe(resp => {
          Swal.fire(this.producto.Descripcion, 'Se actualizo correctamente','success' );

          Object.assign(this.productos[this.productos.findIndex(el => el.Id === this.producto.Id)], this.producto);
          this.activeModal.close( );
          }, (error) => {
            // console.log(error);

            if(error.error) {
            let mensajeError = error.error.Message;
            Swal.fire('Error', mensajeError, 'error');
            }
            else { Swal.fire('Error', 'No se pudo actualizar el producto', 'error') }

        });

    } else {

           //Agregar producto

          this.producto.PrecioActual.MetodosIds =  this.selectedMetodos;


          peticion = this.productosService.addProducto(this.producto);
          peticion.subscribe(resp => {

            this.producto.Id = resp;



            this.productos.push(this.producto);
              Swal.fire(this.producto.Descripcion, 'Se agregó correctamente','success' );

              var result = {
                producto : this.producto
              }
              this.activeModal.close( result);

              }, (error) => {

                if(error.error)
                {
                  let mensajeError = error.error.Message;
                  Swal.fire('Error', mensajeError, 'error');
                }
                else { Swal.fire('Error', 'No se pudo agregar el producto', 'error') }
          });
       }



       Swal.hideLoading();


  }
}

