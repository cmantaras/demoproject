import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbModal, NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';
import { UsuarioModel } from 'src/app/models/usuario.models';
import { UsuariosService } from 'src/app/services/usuarios.service';
import { AddEditUsuarioComponent } from './add-edit-usuario.component';
import { DeleteUsuarioComponent } from './delete-usuario.component';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})
export class UsuarioComponent implements OnInit {

  usuario = new UsuarioModel();
  loading : boolean = true;
  usuariosList : UsuarioModel  []= [];
  page = 1;
  pageSize =8;
  items = [];
  totalItems : number;

  constructor(private usuariosService: UsuariosService,
    public router:ActivatedRoute,
    public modalService: NgbModal,
    private config: NgbPaginationConfig,)
    {

    }
    fillusuarios(){

      this.usuariosService.getUsuarios()
      .subscribe( (data : any) => {
        this.loading = true;

        if((!data && !data.usuarios) || (data && data.usuarios && data.result.length ==0))
        {
          this.usuariosList= [];
        }
        else
        {
          this.usuariosList = data;
          this.totalItems = data.Count;
        }
        this.usuariosList = data.Usuarios;

        this.loading = false;

        },( errorServicio ) => {
              this.loading = false;
            // this.loading = false;
            // this.error = true;
          // this.mensajeError = errorServicio.message;
        });


    }
    openModalAdd() {
      const modalRef = this.modalService.open(AddEditUsuarioComponent, {backdrop : 'static' });
      modalRef.componentInstance.modalHeader = 'Agregar usuario';
      modalRef.componentInstance.usuariosList = this.usuariosList;

      modalRef.result.then(
        () => {
          this.fillusuarios();
        },
        ()=> {
          this.fillusuarios();
      });


    }

    openModalDelete( usuario : UsuarioModel ) {
      const modalRef = this.modalService.open(DeleteUsuarioComponent, {backdrop : 'static' });
      modalRef.componentInstance.modalHeader = 'Eliminar usuario';
      modalRef.componentInstance.usuario = usuario;
      modalRef.componentInstance.usuariosList = this.usuariosList;

    }

    openModalEdit(usuario: UsuarioModel){
      var usuarioAux = JSON.parse( JSON.stringify( usuario ) );


      const modalRef = this.modalService.open(AddEditUsuarioComponent, {backdrop : 'static' });
      modalRef.componentInstance.modalHeader = 'Editar usuario';
      modalRef.componentInstance.usuario = usuarioAux;
      modalRef.componentInstance.usuariosList = this.usuariosList;

      modalRef.result.then(
        () => {
          this.fillusuarios();
        },
        ()=> {
          this.fillusuarios();
      });

    }

    ngOnInit(): void {
      this.page =1;
      this.fillusuarios();
    }



}
