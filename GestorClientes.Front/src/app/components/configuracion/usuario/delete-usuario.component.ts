import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';
import { UsuarioModel } from '../../../models/usuario.models';
import { UsuariosService } from 'src/app/services/usuarios.service';
import Swal from 'sweetalert2';



@Component({
  selector: 'app-eliminar-usuario',
  template: `
  <div class="modal-header">
    <h4 class="modal-title" id="modal-title">Profile deletion</h4>
    <button type="button" class="close" aria-describedby="modal-title" (click)="activeModal.dismiss('Cross click')">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="modal-body">
    <p><strong>¿ Estás seguro que deseas eliminar el usuario ? <span class="text-primary"> {{ usuario.Nombre }}   </span> </strong></p>

  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-outline-secondary" (click)="activeModal.dismiss('cancel click')">Salir</button>
    <button type="button" class="btn btn-danger" (click)="eliminar()">Confirmar</button>
  </div>
  `,
  styles: [
  ]
})
export class DeleteUsuarioComponent implements OnInit {

  public usuario : UsuarioModel = new UsuarioModel();
  public modalHeader : string;
  usuariosList : any = [];

  constructor( public activeModal: NgbActiveModal,
    private usuariosService: UsuariosService, ) { }

  ngOnInit(): void {
  }

  eliminar(){

    let peticion : Observable<any>;
    peticion = this.usuariosService.deleteUsuario(this.usuario);

    peticion.subscribe(resp => {
      Swal.fire('', 'Se eliminó usuario correctamente','success' );
      var indice = this.usuariosList.indexOf(this.usuario);
      this.usuariosList.splice(indice, 1);


    }, (error) => {

      if(error.error)
      {
        let mensajeError = error.error.Message;
        Swal.fire('Error', mensajeError, 'error');
      }
      else { Swal.fire('Error', 'No se pudo agregar el funcionario', 'error') }

    });

    this.activeModal.close();

  }
}
