import { Component, OnInit } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgForm } from '@angular/forms';
import { UsuariosService } from 'src/app/services/usuarios.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UsuarioModel } from 'src/app/models/usuario.models';
import Swal from 'sweetalert2';
import { Observable } from 'rxjs';
import { stringify } from 'querystring';
import { FormsModule } from '@angular/forms';

import { faShower } from '@fortawesome/free-solid-svg-icons';
import { RolesService } from 'src/app/services/roles.service';
import { RolModel } from 'src/app/models/rol.models';


@Component({
  selector: 'app-add-edit-usuario',
  template: `
    <div class="modal-header">
      <h4 class="modal-title"> {{ modalHeader }} </h4>
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">

        <!-- BASIC FORM ELELEMNTS -->

              <form autocomplete="off" (ngSubmit)="guardar( form )" #form="ngForm" class="form-horizontal style-form" method="get">
                <div class="form-group">

                  <label class="col-sm-2 col-sm-2 control-label">Nombre</label>
                  <div class="col-sm-10">
                  <input type="text"
                            class="form-control is-invalid"
                            pattern="^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$"
                            name="Nombre"
                            [ngClass]="{ 'is-invalid': form.submitted && nombre.invalid}"
                           [class.is-invalid]="nombre.invalid && nombre.touched"
                            [(ngModel)] = "usuario.Nombre"
                            required
                            placeholder="Nombre"
                            minlength="1"
                            #nombre="ngModel"
                           >
                            <small   *ngIf="nombre.invalid && nombre.touched"
                                   class="form-text text-danger">Ingrese un nombre válido</small>
                  </div>

                  <label class="col-sm-2 col-sm-2 control-label">Apellido</label>
                  <div class="col-sm-10">
                  <input type="text"
                            class="form-control is-invalid"
                            pattern="^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$"
                            name="Apellido"
                            [ngClass]="{ 'is-invalid': form.submitted && apellido.invalid}"
                             [class.is-invalid]="apellido.invalid && apellido.touched"
                            [(ngModel)] = "usuario.Apellido"
                            required
                            placeholder="Apellido"
                            minlength="1"
                            #apellido="ngModel"
                           >
                           <small   *ngIf="apellido.invalid && apellido.touched"
                                   class="form-text text-danger">Ingrese un apellido válido</small>
                  </div>

                <div *ngIf="!usuario.Id">
                  <label class="col-sm-2 col-sm-2 control-label">Usuario</label>
                  <div class="col-sm-10">
                  <input type="text"
                            class="form-control is-invalid"
                            name="Username"
                            [ngClass]="{ 'is-invalid': form.submitted && username.invalid}"
                             [class.is-invalid]="username.invalid && username.touched"
                            [(ngModel)] = "usuario.Username"
                            required
                            placeholder="Usuario"
                            minlength="1"
                            #username="ngModel"
                           >
                           <small   *ngIf="username.invalid && username.touched"
                                   class="form-text text-danger">Ingrese un nombre de usuario válido</small>
                  </div>
                  </div>


                  <label class="col-sm-2 col-sm-2 control-label">Email</label>
                  <div class="col-sm-10">
                  <input type="text"
                            class="form-control "
                            name="Email"

                            [(ngModel)] = "usuario.Email"

                            placeholder="Email"
                            minlength="1"
                            #email="ngModel"
                           >
                           <!-- <small   *ngIf="Nombre.invalid && Nombre.touched"
                                   class="form-text text-danger">Ingrese 5 letras</small> -->
                  </div>


   
                  <div class="col-sm-9 mt-2">
                    <div class="custom-control custom-switch">

                      <input
                             [(ngModel)]="usuario.IsAdmin"
                              name="IsAdmin"
                             (ngModelChange)="changeIsAdminField()"
                            style="cursor:pointer;"
                            type="checkbox"
                            class="custom-control-input" id="switch0">

                      <label class="custom-control-label" for="switch0"> Es Admin</label>
                     </div>
                  </div>


                <label  class="col-sm-2 col-sm-2 control-label">Rol</label>
                <div  class="col-sm-9">
                <!--Using items input-->
                <ng-select [items]="rolesAMostrar"
                     name="rol"
                     [ngClass]="{ 'is-invalid': form.submitted && rol.invalid }"
                     [class.is-invalid]="rol.invalid && rol.touched"

                     required
                     bindLabel="Nombre"
                     bindValue="Id"
                     [(ngModel)]="usuario.Rol.Id"
                     [disabled]= "esAdministrador"
                     #rol="ngModel">
                 </ng-select>
                 <small    *ngIf="form.submitted && rol.invalid && !usuario.Rol.Id"
                 class="form-text text-danger">  Ingrese un rol</small>
                </div>




                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-danger" (click)="activeModal.close('Close click')">Salir</button>
                  <button type="submit" class="btn btn-success">Guardar</button>
                </div>
              </form>
    </div>



  `,
  styles: [
  ]
})
export class AddEditUsuarioComponent implements OnInit   {

  public usuario : UsuarioModel = new UsuarioModel();
  public esAdministrador : boolean = false;
  public modalHeader : string;
  public roles : RolModel [] = [];
  usuariosList : any = [];
  rolesSinAdmin: RolModel[];
  rolAdmin: RolModel;
  rolesAMostrar: RolModel[];

  constructor( public activeModal: NgbActiveModal,
              private usuariosService: UsuariosService,
              private rolesService : RolesService,
              private router: Router
                       ) {

  }
  ngOnInit(){

    this.rolesService.getRoles().subscribe(resp => {

      this.roles = resp['Roles'];

      this.rolesSinAdmin = this.roles.slice();
      this.rolAdmin = this.rolesSinAdmin.find(elem => elem.Nombre.toLowerCase() == 'admin');
      const index = this.rolesSinAdmin.indexOf(this.rolAdmin);
      this.rolesSinAdmin.splice(index,1);

      this.rolesAMostrar = this.rolesSinAdmin.slice();

    })
  }


  changeIsAdminField(){
    if(this.esAdministrador){
      this.esAdministrador=false;
      this.usuario.Rol.Id  = undefined;
      this.usuario.Rol.Nombre = undefined;



      this.rolesAMostrar = this.rolesSinAdmin.slice();

    }
    else{
      this.esAdministrador = true;
      this.rolesAMostrar = this.roles.slice();
      //console.log (this.roles)
      const index = this.roles.indexOf(this.rolAdmin);
      this.usuario.Rol.Id = index
      this.usuario.Rol.Nombre =JSON.parse( JSON.stringify( this.rolAdmin.Nombre ) ) 
      this.usuario.Rol= JSON.parse( JSON.stringify( this.rolAdmin) )
    }
  }




  guardar(form : NgForm ) {

    if(form.invalid) {
      return;
    }

    Swal.fire('usuario', 'El usuario se está ingresando', 'info');
    let peticion : Observable<any>;


    if ( this.usuario.Id) {

      this.usuario.RolId = this.usuario.Rol.Id;
      this.usuario.IsAdmin = this.esAdministrador;

        //Actualizar usuario
        peticion = this.usuariosService.updateUsuario(this.usuario);


        peticion.subscribe(resp => {
          Swal.fire(this.usuario.Nombre, 'Se actualizo correctamente','success' );

          Object.assign(this.usuariosList[this.usuariosList.findIndex(el => el.Id === this.usuario.Id)], this.usuario);
          this.usuario = new UsuarioModel();
          this.usuario.Rol = new RolModel();
          window.location.reload();
          }, (error) => {


            if(error.error) {
            let mensajeError = error.error.Message;
            Swal.fire('Error', mensajeError, 'error');
            }
            else { Swal.fire('Error', 'No se pudo actualizar el usuario', 'error') }

        });

    } else {

           //Agregar usuario
           this.usuario.RolId = this.usuario.Rol.Id
          peticion = this.usuariosService.addUsuario(this.usuario);
          peticion.subscribe(resp => {

            this.usuario.Id = resp;
            this.usuariosList.unshift(this.usuario);
            this.usuario = new UsuarioModel();
            this.usuario.Rol = new RolModel();
              Swal.fire(this.usuario.Nombre, 'Se agregó correctamente','success' );
              window.location.reload();
              }, (error) => {


                if(error.error)
                {
                  let mensajeError = error.error.Message;
                  Swal.fire('Error', mensajeError, 'error');
                }
                else { Swal.fire('Error', 'No se pudo agregar el usuario', 'error') }
          });
       }
       this.activeModal.close();

       Swal.hideLoading();

  }
}

