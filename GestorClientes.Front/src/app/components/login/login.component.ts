import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, FormGroupDirective, NgForm, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalEventsManager } from '../helper/global-events-manager';
import jwt_decode from 'jwt-decode';
import { AuthService } from 'src/app/services/auth.service';
import { UserRecoverModel } from 'src/app/models/userRecover.model';
import Swal from 'sweetalert2';
import { CustomValidators } from 'src/app/directives/custom-validators';
import { ErrorStateMatcher } from '@angular/material/core';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit  {

  invalidLogin : boolean;
  username : string;
  password : string;

  recoverPassword  =false;
  formPass= false;
  formConfirmPass = false;

  pass : string = '';
  confirmPassword : string = '';
  submitted : boolean;

  token : string;
  user : string;
  loading = false;
  loadingLogin = false;

  public frmSignup: FormGroup;
  userRecover : UserRecoverModel = new UserRecoverModel();

  constructor( private router: Router,
               private http: HttpClient,
               private fb: FormBuilder,
               private globalEventsManager: GlobalEventsManager,
               private authService : AuthService,
               private activatedRoute: ActivatedRoute) {
                const token = localStorage.getItem("jwt");
                if (token){
                  this.router.navigate(["/home"]);
                }

                else{
                  this.globalEventsManager.showNavBar(false);


                this.activatedRoute.queryParams.subscribe(params => {
                  const user = params['username'];
                  const jwt = params['jwt'];

                  if((user) && (jwt) ){

                    this.authService.validateToken(user, jwt).subscribe(res => {
                        this.iniciarForm();

                        this.pass = '';
                        this.confirmPassword  = '';
                        this.formConfirmPass = true;
                        this.formPass = false;
                        this.userRecover.Username = user;
                        this.userRecover.Token = jwt;

                    }, (error) => {
                      // console.log(error)
                    })
                  }

              });
            }
            }



ngOnInit(): void {

}
checkPasswords(group: FormGroup) { // here we have the 'passwords' group
  let pass = group.get('password').value;
  let confirmPass = group.get('confirmPass').value;

  return pass === confirmPass ? null : { notSame: true }
}


iniciarForm(){
  this.frmSignup =  this.fb.group({
    passId: new FormControl(this.pass, [
      Validators.required,
      Validators.minLength(8),
      validateUpperCase,
      lowerCase,
      validateNumber
      ]),
    confirmPassId : new FormControl (this.confirmPassword, [
      Validators.required
      ]),
    }, {

      validator: MustMatch('passId', 'confirmPassId')


    })
}


get passId() { return this.frmSignup.get('passId'); }


get confirmPassId() { return this.frmSignup.get('confirmPassId'); }

get power() { return this.frmSignup.get('power'); }

get f() { return this.frmSignup.controls; }

formResetPassword(valor : boolean) {
  this.submitted  = true;

  this.formPass = valor;
}


login(form: NgForm) {
  this.loadingLogin = true;
  this.invalidLogin = false;

  const body = 'grant_type=password&username='+this.username+'&password='+ this.password

    this.authService.authentication(this.username, this.password).subscribe(response => {

    const token = (<any>response).access_token;

    let tokenInfo = JSON.parse((<any>response).permissions)
    let userData = JSON.parse((<any>response).user_data)

    var objectPermisos : any  = JSON.stringify(tokenInfo)

    localStorage.setItem("jwt", token);
    localStorage.setItem("user", JSON.stringify(userData));
    localStorage.setItem("dataPermissions", objectPermisos);


    var permisos = JSON.parse(localStorage.getItem("dataPermissions"));
    localStorage.setItem("permisos",  JSON.stringify(permisos.Permisos));

    this.invalidLogin = false;
    this.globalEventsManager.showNavBar(true);
    this.globalEventsManager.updateHeader(userData);
    this.router.navigate(["home"]);
    this.loading = false;
    this.loadingLogin = false;
    }, err => {
      this.loading = false;
      this.loadingLogin = false;
      this.invalidLogin = true;
    });
  }



resetPass() {


  this.authService.resetPass(this.username).subscribe(res =>
    {
      Swal.fire('Los datos fueron enviados correctamente','', 'info')
      this.username = '';

    });
    //LLega mail?
  }

confirmPass() {
  this.userRecover.Password= this.pass;
  this.userRecover.ConfirmPassword= this.confirmPassword;


  this.authService.changePassword(this.userRecover).subscribe( res => {
    Swal.fire('La contraseña fue cambiada correctamente', '', 'info')
    this.router.navigate(['login']);

    })
  }

getDecodedAccessToken(token: string): any {
  try{
      return jwt_decode(token);
  }
  catch(Error){
      return null;
  }
}


}

function validateUpperCase(c: FormControl) {
  let UPPER_CASE = /(?=.*[A-Z])/

  return UPPER_CASE.test(c.value) ? null : {
    validateUpperCase: {
      valid: false
    }
  };
}
function validateNumber(c: FormControl) {
  let NUMBER = /(?=.*[0-9])/

  return NUMBER.test(c.value) ? null : {
    validateNumber: {
      valid: false
    }
  };
}
function specialCase(c: FormControl) {
  let SPECIAL_CASE = /(?=.*[\W])/

  return SPECIAL_CASE.test(c.value) ? null : {
    specialCase: {
      valid: false
    }
  };
}

function lowerCase(c: FormControl) {
  let LOWER_CASE = /(?=.*[a-z])/

  return LOWER_CASE.test(c.value) ? null : {
    lowerCase: {
      valid: false
    }
  };
}

export function MustMatch(controlName: string, matchingControlName: string) {
  return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];



      if (matchingControl.errors && !matchingControl.errors.mustMatch) {

          // return if another validator has already found an error on the matchingControl
          return;
      }

      // set error on matchingControl if validation fails
      if (control.value !== matchingControl.value) {
          matchingControl.setErrors({ mustMatch: true });

      } else {

          matchingControl.setErrors(null);
      }
  }
}
