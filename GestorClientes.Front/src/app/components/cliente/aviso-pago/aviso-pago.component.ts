import { ParametrosService } from './../../../services/parametros.service';
import { CierreMesModel, CuotaProductoModel } from './../../../models/cliente/clienteProdDeuda.model';
import { CierresService } from './../../../services/cierres.service';
import { AfterViewInit, Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AvisoPagoModel } from 'src/app/models/cliente/avisoPago.model';
import { ClienteModel } from 'src/app/models/cliente/cliente.model';
import { ClienteProdDeudaModel } from 'src/app/models/cliente/clienteProdDeuda.model';
import { ImpuestoModel } from 'src/app/models/impuesto/impuesto.model';
import { ClientesService } from 'src/app/services/clientes.service';
import { ImpuestosService } from 'src/app/services/impuestos.service';
import { ClienteComponent } from '../cliente.component';
import { PagarProductosComponent } from '../productos-cliente/pagar-productos/pagar-productos.component';
import { Observable, Subscription } from 'rxjs';
import { ClienteProductoModel } from 'src/app/models/cliente/productoCliente.model';
import { Console } from 'console';

@Component({
  selector: 'app-aviso-pago',
  templateUrl: './aviso-pago.component.html',
  styleUrls: ['./aviso-pago.component.css']
})
export class AvisoPagoComponent implements OnInit, AfterViewInit {


  today : Date;

  impuestos : ImpuestoModel [];
  iva : number = 22;
  totalDeuda :number;
  cierres : CierreMesModel;
  avisoPagoId : number;
  fechaVencimiento : string;

  mostrarCuotas : boolean;
  fechaCorte: Date;
  ultimoCierre: Date;
  generarHtml : boolean;
  count : number;
  avisosPago : Array<Array< ClienteProdDeudaModel >>;
  @Output() loadChild = new EventEmitter<boolean>();

  @Input() private uploadSuccess: EventEmitter<Array<Array< ClienteProdDeudaModel >> >;

  private eventsSubscription: Subscription;

    @Input() events: Observable<void>;

 

  constructor(public cierresService: CierresService,
      public parametrosService : ParametrosService) {
    this.avisoPagoId = undefined;
    this.avisosPago = []
    this.generarHtml = false;

   }
   ngAfterViewInit(){

   }

  ngOnInit(): void {

      this.parametrosService.get("VENCIMIENTO_MES").subscribe((data : any) => {
        this.fechaVencimiento = data.Parametros[0].Valor;
       
      })
      this.cierresService.getCierresMes().subscribe((data : any) => {
        this.cierres = data;
        this.ultimoCierre = data[0];
        var fechaCierre = new Date(this.ultimoCierre);
        var ano = fechaCierre.getFullYear();
        var mes= fechaCierre.getMonth() +1;
        this.fechaCorte = new Date(ano,mes,20);
      });

      this.uploadSuccess.subscribe((data:Array<Array< ClienteProdDeudaModel >>) => {
        // Do something in the childComponent after parent emits the event.
        this.avisosPago = data;
      


        Object.values(this.avisosPago).forEach((aviso : Array< ClienteProdDeudaModel >  ) =>{

          this.checkIfshowCuota(aviso);
        });
        setTimeout(() => {
          this.printAvisoPago();
      }, 2500)
      });
      
  }

  generateFecha(itemCuota : CuotaProductoModel){
    var desde =new Date(itemCuota.ClienteProducto.Desde);

    //Fecha de compra + mes de la cuota.
    //Para mostrar unicamanete las cuotas anterioras no pagas
    var month = new Date(desde.setMonth(desde.getMonth()+ (itemCuota.Cuota -1 ) ));
    return month;
  }



  public checkIfshowCuota(cuotasProductos : Array< ClienteProdDeudaModel > ){
    this.totalDeuda = 0
    //Obtner los cierres de mes y mostrar cuotas que comprenden el periodo entre los meses
    var cierreLastMonth = new Date(this.cierres[0]);
    cuotasProductos["TotalDeuda"] = 0;

    this.avisoPagoId = cuotasProductos["IdAvisoPago"];

    Object.values(cuotasProductos["Productos"]).forEach((producto ) => {

      var primerCuota = Object.values(producto)[0];
      var desde =new Date(primerCuota.ClienteProducto.Desde);

      if(primerCuota.Cuota == 1 && desde.getMonth() == cierreLastMonth.getMonth()
          || new Date(primerCuota.CierreMes).getMonth() == desde.getMonth() +1
      )
      {
        var number = -1;
      }//
      else{
        var number = 0;
      }

      Object.values(producto).forEach(cuota => {

          var desde =new Date(cuota.ClienteProducto.Desde);
          cuota.Show = true;
             cuotasProductos["TotalDeuda"] = parseInt(cuota.Importe) + cuotasProductos["TotalDeuda"]
        });
      });
      this.generarHtml = true;
      this.mostrarCuotas = false;
  }

  printAvisoPago(){
      let printContents, popupWin;
      printContents = document.getElementById('print-section').innerHTML;
      popupWin = window.open('', '_blank', 'top=0,left=0,height=auto,width=auto');
      popupWin.document.open();

      popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
          <style>

          </style>
        </head>
      <body onload="window.print();window.close()">${printContents}</body>
      </html>`
      );
      popupWin.document.close();
      popupWin.focus();
      popupWin.print();
      popupWin.close();

      this.loadChild.emit(false);
    }

}

