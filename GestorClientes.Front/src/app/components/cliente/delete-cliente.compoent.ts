import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';
import { ClientesService } from 'src/app/services/clientes.service';
import Swal from 'sweetalert2';
import { ClienteModel, EstadoClienteModel } from 'src/app/models/cliente/cliente.model';



@Component({
  selector: 'app-eliminar-cliente',
  template: `
  <div class="modal-header">
    <h4 class="modal-title" id="modal-title"  style="font-size: 20px;" >Cambiar estado cliente</h4>
    <button type="button" class="close" aria-describedby="modal-title" (click)="activeModal.dismiss('Cross click')">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="modal-body">
    <p style="font-size: 17px;"><strong> Cambiar estado del cliente <span class="text-primary"> {{ cliente.Nombre }}   </span> </strong></p>

          <div class="d-flex">
            <p class="col-sm-2 col-sm-2">Motivo</p>
                      <div  class="d-flex col-sm-6 flex-sw flex-row ">
                      <ng-select [items]="estados"
                          bindLabel="Descripcion"
                          bindValue="Id"
                          class="w-100"
                          [(ngModel)]="cliente.EstadoActualId"
                          name="ciudad"
                          #estado="ngModel">
                      </ng-select>
                    </div>
            </div>

  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-outline-secondary" (click)="activeModal.dismiss('cancel click')">Salir</button>
    <button type="button" class="btn btn-danger" [disabled]="borrando" (click)="eliminar()">Confirmar</button>
  </div>
  `,
  styles: [
  ]
})

export class DeleteClienteComponent implements OnInit {

  public cliente : ClienteModel = new ClienteModel();
  public modalHeader : string;
  public estados  : EstadoClienteModel[];
  clientes : ClienteModel [] = [];
  estadoId : number;
  borrando : boolean;
  estabaInactivo: boolean;

  constructor( public activeModal: NgbActiveModal,
               private clientesService: ClientesService, ) {
                 this.estados = []
               }

  ngOnInit(): void {
     this.estadoId = this.cliente.EstadoActualId;
     if(this.estadoId !=8){ this.estabaInactivo = true}

    this.clientesService.getEstadosClientes().subscribe( (res : any) => {
      var respuesta : EstadoClienteModel [] = res.EstadosCliente;
      this.estados = respuesta.filter( this.filtrarPorMotivo);
    });
   }

  filtrarPorMotivo(motivo : EstadoClienteModel){
    if(motivo.Descripcion !== 'REACTIVADO'){ return true}
    else {return false}
  }

  eliminar(){
    //Eliminar
    //o Editar y cambiar estado
    this.borrando = true;

    //si esta siendo reactivado se le agreegan 2 años
    if(this.cliente.EstadoActualId == 8 && this.estabaInactivo){
      var hoy = new Date();
        var year = hoy.getFullYear();
        var month = hoy.getMonth();
        var day = hoy.getDate();
      
        var c = new Date(year + 2, month, day);


      this.cliente.FechaHasta = c.getFullYear() +"-"+ (c.getMonth()+1) +"-"+ (c.getDate());
    }
    //si esta siendo inactivado setea la fecha de hoy en fecha hasta
    if(this.cliente.EstadoActualId != 8 && !this.estabaInactivo){
      var hoy = new Date();
      this.cliente.FechaHasta = hoy.getFullYear() +"-"+ (hoy.getMonth()+1) +"-"+ (hoy.getDate());
    }

   


    this.clientesService.updateCliente(this.cliente).
      subscribe(resp => {

      Swal.fire('', 'Se actualizó  estado correctamente','success' );

      var indice = this.clientes.indexOf(this.cliente);
      this.clientes.splice(indice, 1);

      this.activeModal.close();

    }, (error) => {
      if(error.error)
      {
        let mensajeError = error.error.Message;
        Swal.fire('Error', mensajeError, 'error');
      }
      else { Swal.fire('Error', 'No se pudo actualizar el estado del cliente', 'error') }
    });

    this.borrando = false;
  }
  

}
