import { CuotaPagaModel } from './../../models/cliente/cuotaPaga.model';
import { AfterViewInit, Component, EventEmitter, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ClientesService } from 'src/app/services/clientes.service';
import { AddEditClienteComponent } from 'src/app/components/cliente/add-edit-cliente.component';
import { NgbDateParserFormatter, NgbModal, NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';
import { ClienteModel, EstadoClienteModel } from '../../models/cliente/cliente.model';
import { DeleteClienteComponent } from './delete-cliente.compoent';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { ClienteRequestModel } from 'src/app/models/cliente/clienteRequest.model';
import { ZonasService } from 'src/app/services/zona.service';
import { CiudadesService } from 'src/app/services/ciudades.service';
import { ZonaModel } from 'src/app/models/zona/zona.model';
import { CiudadModel } from 'src/app/models/ciudad/ciudad.model';
import { AvisoPagoComponent } from './aviso-pago/aviso-pago.component';
import { ProductosClienteComponent } from './productos-cliente/productos-cliente.component';
import { listLazyRoutes } from '@angular/compiler/src/aot/lazy_routes';
import { AvisoPagoModel } from 'src/app/models/cliente/avisoPago.model';
import { ClienteProdDeudaModel, CuotaProductoModel } from 'src/app/models/cliente/clienteProdDeuda.model';
import { PagosClienteService } from 'src/app/services/pagosCliente.service';
import { PagoClienteModel } from 'src/app/models/cliente/pagoCliente.model';
import Swal from 'sweetalert2';
import {NgbDateCustomParserFormatter} from 'src/app/components/helper/dateformat'

@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.css'],
  providers: [NgbPaginationConfig,
              {provide:  NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter}]
})
export class ClienteComponent implements OnInit,AfterViewInit {

  loading : boolean = true;
  loadingData: boolean = false;
  loadChild :boolean = false;
  loadingInfo : boolean = false;

  clientes : ClienteModel  [] = [];
  clientesForPrint : ClienteModel  [] = [];
  zonas : ZonaModel  [] = [];
  ciudades : CiudadModel  [];
  imprimir = false;
  estadosMotivos : EstadoClienteModel [];
  cliente : ClienteModel;
  clienteFilter : ClienteRequestModel = new ClienteRequestModel();
  seBorro: boolean;
;
  clienteInfo : ClienteModel;
  avisosPago :    Array< ClienteProdDeudaModel > [];
  cuotasPagas : ClienteProdDeudaModel[];
  pagosCliente : PagoClienteModel[]
  nombre : string;
  estado : number;
  zonaFilterId : number;
  ciudadFilterId : number;
  motivoActivo : EstadoClienteModel = new EstadoClienteModel();

  filtroNombre : Subject<string> = new Subject<string>();
  filtroEstado : Subject<number> = new Subject<number>();
  filtroZona: Subject<number> = new Subject<number>();
  filtroCiudad: Subject<number> = new Subject<number>();
  //Paginación
  page : number = 1;
  pagePagos : number = 1;
  pageSize =4;
  items = [];
  count : number;
  totalItems : number;
  loadingCuotas: boolean=false;
   public uploadSuccess: EventEmitter<Array< ClienteProdDeudaModel > []> = new EventEmitter();

  constructor(private clientesService?: ClientesService,
              private zonaService? : ZonasService,
              private ciudadService? : CiudadesService,
              private pagosService? : PagosClienteService,
              public router?:ActivatedRoute,
              public modalService?: NgbModal,
              private config?: NgbPaginationConfig,
              )
    {
      this.avisosPago = [];
      this.clienteFilter.Limit =this.pageSize;
      this.clienteFilter.Offset = this.page;

      this.cuotasPagas= []

      this.filtroEstado.pipe(debounceTime(800), distinctUntilChanged())
        .subscribe(model => {
          this.estado = model;
          this.filter();
        })

      this.filtroZona.pipe(debounceTime(800), distinctUntilChanged())
        .subscribe(model => {
          this.zonaFilterId = model;
          this.filter();
        })

      this.filtroCiudad.pipe(debounceTime(800), distinctUntilChanged())
        .subscribe(model => {
          this.ciudadFilterId = model;
          this.filter();
        })

      this.filtroNombre.pipe(debounceTime(800), distinctUntilChanged())
        .subscribe(model => {
          this.nombre = model;
          this.filter();
        })
    }
    ngAfterViewInit(){

    }
    ngOnInit(): void {
      this.page =1;

      this.clientesService.getEstadosClientes().subscribe( (res : any) => {
        var respuesta : EstadoClienteModel [] = res.EstadosCliente;
          this.estadosMotivos = respuesta.filter((x) => { return this.filtrarPorMotivo(x) });

          //Obtener clientes por defecto activos
          this.fillClientes();

      });

      this.zonaService.getZonas().subscribe( (res : any) => {
        this.zonas = res.Zonas;
      });

      this.ciudadService.getCiudades().subscribe( (res : any) => {
          this.ciudades = res.Ciudades;
      });

    }

    filtrarPorMotivo(motivo : EstadoClienteModel){
      if(motivo.Descripcion === 'ACTIVO'){
        this.motivoActivo =  motivo;

        return false}
      else {
        return true}
    }

    fillClientes(){

      this.loading = true;

      if(this.clienteFilter.ClienteEstadoId == null){
        this.clienteFilter.ClienteEstadoId = this.motivoActivo.Id;
      }else {
        this.clienteFilter.ClienteEstadoId = this.estado;
      }

      this.clientesService.getClientes(this.clienteFilter)
        .subscribe( (data : any) => {


          this.clientes = data.Clientes;

          this.count = data.Count;
          this.loading = false;

        },( errorServicio ) => {
          this.loading = false;
        });
    }

    onNombreChange(query:string){
      this.filtroNombre.next(query);
    }

    onEstadoChange(query:number){
      this.filtroEstado.next(query);
    }
    onZonaChange(query:number){
      this.filtroZona.next(query);
    }
    onCiudadChange(query:number){
      this.filtroCiudad.next(query);
    }

    filter(print : boolean = false)  :void
    {
      if( this.nombre)
      {
        //Chequeamos si el filtro contiene solo numeros o solo letras y asignar al modelo request
        this.checkIfFilterHasNumbersOrStringOrAllWhatdoYouthink();
      }

      this.clienteFilter.Offset = this.page;
      if(print === true){
        this.clienteFilter.Limit = 10000;
        this.clienteFilter.Offset = 1;
      }
      else{

      
      this.clienteFilter.Limit = this.pageSize;
    }
      this.clienteFilter.ZonaId = this.zonaFilterId;
      this.clienteFilter.CiudadId = this.ciudadFilterId;


      //Si no se selecciono un estado en los filtros, obtener unicamente los clientes activos
      if(!this.estado){
        this.clienteFilter.ClienteEstadoId = this.motivoActivo.Id;
      } else{
        this.clienteFilter.ClienteEstadoId = this.estado;
      }

        this.loadingData = true;
        this.clientesService.getClientes(this.clienteFilter)
          .subscribe( (data : any) => {
          
          if(print === true){
            this.clientesForPrint = data.Clientes.map(d => d.Id);
            var lista = this.clientesForPrint.map(d => d.Id);
            this.getData( data.Clientes.map(d => d.Id));
          }
          else{

          
          this.clientes = data.Clientes;
        }
          this.count = data.Count;

          this.loadingData = false;
          this.clienteFilter = new ClienteRequestModel();

        },( errorServicio ) => {
           this.loadingData = false;
        });
    }

    checkIfFilterHasNumbersOrStringOrAllWhatdoYouthink(){

      if(Number(this.nombre) ){
        //Identificador es documento
        this.clienteFilter.Identificador =  this.nombre;
      }

       //Chequeamos si el filtro contiene solo letras
      else if(/\s/.test(this.nombre)){
         var inputFiltro = this.nombre.split(" ");
         this.clienteFilter.Nombre =inputFiltro[0];
         this.clienteFilter.Apellido =inputFiltro[1];
       }

       else if(!/[^a-z]/i.test(this.nombre.toLowerCase()) )
       {
         this.clienteFilter.Nombre = this.nombre;
       }
       else{
        this.clienteFilter.Nombre = this.nombre;
       }
    }
    emitEventChild(avisosPago : any){
     

    }
    imprimirAvisodepago(cliente : ClienteModel ){
      this.loadingInfo = true;
      this.loadChild = true;
   
      var clienteId = cliente.Id;
      var lista: Number[] = [];

      lista.push(clienteId)

      this.getData(lista);

      setTimeout(() => {
        //this.printAvisoPago();
     }, 1500)
     this.loadingInfo = false;
    }

    imprimirTodosLosAvisos(){
    
      this.loadingInfo = true;

      var lista : Number[];
      this.filter(true);
      setTimeout(() => {
 



       // this.printAvisoPago();
     }, 5000)
     
     this.loadingInfo = false;
    }
 

    printAvisoPago(){
      this.loadingInfo = true;

      let printContents, popupWin;
      printContents = document.getElementById('print-section').innerHTML;
      popupWin = window.open('', '_blank', 'top=0,left=0,height=auto,width=auto');
      popupWin.document.open();

      popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
          <style>

          </style>
        </head>
      <body onload="window.print();window.close()">${printContents}</body>
      </html>`
      );
      popupWin.document.close();
      popupWin.focus();
      popupWin.print();
      popupWin.close();
      this.loadingInfo = false;
    }

  public getData(clientesIds : Number[]){
        this.loadingInfo = true;
        var request ={
          ClientesIds : clientesIds,
          GenerarAvisoPago : true
        }
        
        this.clientesService.getProductosAvisoPago( request ).subscribe((res : any )=> {

      
          this.avisosPago = res;
          this.imprimir = true;

          this.uploadSuccess.emit(res );

        
      });
      this.loadingInfo = false;
    }

    openModalAdd() {
      const modalRef = this.modalService.open(AddEditClienteComponent, { backdrop: 'static'});
      modalRef.componentInstance.modalHeader = 'Agregar cliente';
      modalRef.componentInstance.clientes = this.clientes;
      modalRef.result.then(
        (result : any) => {
          this.filter();
        },
        ()=> {
      });
    }

    checkRouter(){
    
      if (this.router.children.find(d => d.component == ProductosClienteComponent)) {
        return false;
      } else {

      } return true;
    }

    openModalDelete( cliente : ClienteModel ) {

      const modalRef = this.modalService.open(DeleteClienteComponent, { backdrop: 'static'});
      modalRef.componentInstance.modalHeader = 'Cambiar estado de cliente';
      modalRef.componentInstance.cliente = cliente;
      modalRef.componentInstance.clientes = this.clientes;
      modalRef.result.then(
        (result : any) => {
          this.filter();
        },
        ()=> {
      });
    }

    loadCuotas(pagoId : number){
      this.cuotasPagas= []
  
      this.loadingCuotas=true;
      setTimeout(() => {
      this.pagosService.getCuotasPago(pagoId).subscribe( (res : any) => {
        this.cuotasPagas = res.Cuotas;
        this.loadingCuotas=false;
      });
      }, 800);
    }

    openModalInfo( content, cliente : ClienteModel ) {
      
      this.pagosCliente=[] 
      this.cuotasPagas= []
      const modalRef = this.modalService.open(content, { backdrop: 'static',size:'xl'});
  
        this.pagosService.getPagos(cliente.Id).subscribe( (res : any) => {
          this.pagosCliente = res.Pagos;
        });
      this.clienteInfo = cliente;
    }
    generateFecha(itemCuota : CuotaProductoModel){
      var desde =new Date(itemCuota.ClienteProducto.Desde);
  
      //Fecha de compra + mes de la cuota.
      //Para mostrar unicamanete las cuotas anterioras no pagas
      var month = new Date(desde.setMonth(desde.getMonth()+ (itemCuota.Cuota -1 ) ));
      return month;
    }
  

    openModalEdit(cliente:ClienteModel){

      var clienteAux = JSON.parse( JSON.stringify( cliente ) );

      const modalRef = this.modalService.open(AddEditClienteComponent, { backdrop: 'static'});
      modalRef.componentInstance.modalHeader = 'Editar cliente';
      modalRef.componentInstance.clientes = this.clientes;

      modalRef.componentInstance.cliente = clienteAux;
      modalRef.result.then(
        (result : any) => {
          this.filter();
        },
        ()=> {
      });
    }

    paymentDelete( pago: PagoClienteModel){
      var cliente = pago.Cliente;
      // borrar Pago
      this.pagosService.deletePagos(pago.Id).subscribe( (res : any) => {
        this.seBorro = res;

              //volver a cargar pagos
        this.pagosService.getPagos(cliente.Id).subscribe( (res : any) => {
        this.pagosCliente = res.Pagos;
        Swal.fire('', 'Estado de cliente actualizado correctamente','success' );
      });
      });

      this.clienteInfo = cliente;
    }

}
