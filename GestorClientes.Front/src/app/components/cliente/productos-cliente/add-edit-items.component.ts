import { Component, EventEmitter, Injectable, OnInit, Output } from '@angular/core';
import { NgbActiveModal, NgbDateParserFormatter, NgbDateStruct, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { ClienteModel } from 'src/app/models/cliente/cliente.model';
import Swal from 'sweetalert2';
import { Observable } from 'rxjs';
import { NgbDateCustomParserFormatter } from '../../helper/dateformat';
import { ItemModel } from '../../../models/item/item.model';
import { ProductoModel } from 'src/app/models/producto/producto.model';
import { AsignarProductoModel } from 'src/app/models/producto/asignarProducto';
import { ClienteProductoModel } from 'src/app/models/cliente/productoCliente.model';
import { ItemsService } from '../../../services/items.service';
import { ItemValorModel } from 'src/app/models/item/itemValor.model';
import { Console } from 'console';
import { ActivatedRoute, Router } from '@angular/router';
import { ClientesService } from 'src/app/services/clientes.service';
import { ClienteProductoItemModel } from '../../../models/cliente/productoItemCliente.model';
import { element } from 'protractor';
import { toString } from '../../helper/util';

declare function createToast(params: string, type: number);

@Component({
    selector: 'app-add-edit-items',
    template: `
    <div class="modal-header text-center">
        <h4 class="modal-title" id="modal-title">Editar Items</h4>
        <button type="button" class="close" aria-describedby="modal-title" (click)="activeModal.dismiss('Cross click')">
        <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
    <form autocomplete="off" (ngSubmit)="guardar(form)" #form="ngForm" class="form-horizontal style-form" method="get">
      <div class="d-flex flex-row   ">
      <div class="col-sm-6">
          <table class="animated fadeIn table table-striped table-advance table-hover">
            <thead>
              <tr>
                <th  class="hidden-phone" ><i class=" d-flex"></i> Items disponibles </th>
              </tr>
            </thead>
            <tbody>
              <tr  *ngFor="let item of AllItems" >
                    <td>
                      {{ item.Descripcion }}
                    </td>
                    <td>
                      <a type="button" (click)="agregarItem(item)"> <i class="fa-2x fas fa-plus-circle"></i> </a>
                    </td>
              </tr>
            </tbody>
          </table>
      </div>
      <div  class="col-sm-6">
          <table class="animated fadeIn table table-striped table-advance table-hover">
            <thead>
              <tr>
                <th  class="hidden-phone" ><i class=""></i> Items seleccionados </th>
              </tr>
            </thead>
            <tbody>
              <tr *ngFor="let itemLoaded of auxItemsSelected; let i = index" [attr.data-index]="i"  >
                  <td>
                     {{ itemLoaded.item.Descripcion }}
                  </td>
                  <td>
                  <input    type="text"
                            class="form-control "
                            name="valor"
                            required
                            [(value)]="itemLoaded.valor"

                            placeholder="Valor"
                            minlength="1"
                            (keyup)="asignarValue($event, i)"
                            (keyup.enter)="disable($event, i)"
                                                     
                           >
                           <small *ngIf ="itemLoaded.valor == ''" >Ingrese un valor</small>
                           
                  </td>
                  <td>
                    <a type="button" (click)="quitarItem(itemLoaded)"> <i class="fa-2x fas fa-minus-circle"></i> </a>
                  </td>
              </tr>
            </tbody>
          </table>
      </div>
      </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-success">Guardar</button>
        </div>
      </form>

    `,
    styles: [
    ],
  
    providers : [{provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter}]
  
  })
  @Injectable()
  export class AddEditItemsComponent implements OnInit   {
  
    public savingInfo : boolean;
    public modalHeader : string;
    public clienteProducto : any;
    public cliente : ClienteModel;

    public items : ItemModel [] = [];


    public AllItems : ItemModel[];
    public item : ItemModel;
    public loadItems: boolean;

    public listaResult : ClienteProductoItemModel []  = [];
    public selectedItems : ClienteProductoItemModel [] = [];
    public auxItemsSelected : Array<{clienteProItemId : number, valor : string, item : any}>;

  
    constructor( public activeModal: NgbActiveModal, 
                 private clientesService: ClientesService,
                 private itemsService: ItemsService,
                 private formBuilder: FormBuilder
                         ) {
            this.auxItemsSelected = new Array();
            this.selectedItems = [];

    }
  
    ngOnInit(): void {

      
     
      this.clienteProducto.Items.forEach(element => {
        
        var elementAux = {
            clienteProItemId : element.Id,
            valor : element.Valor.toString(),
            item : element.Item
        }
        this.auxItemsSelected.push(elementAux);
      })
      

      this.itemsService.getItems().subscribe( (data: any) => {this.AllItems = data.Items;});

    }
    
    
    agregarItem(itemToLoad : ItemModel) {
 
      var auxItemValor = { clienteProItemId: 0,valor : '', item : itemToLoad}
          this.auxItemsSelected.push(auxItemValor)

    }

    quitarItem (item : any) {

      var itemTo = this.auxItemsSelected.find(obj => obj.item.Id == item.item.Id);
     
      var indice = this.auxItemsSelected.indexOf(itemTo);
      this.auxItemsSelected.splice(indice, 1);
    }
    
      disable ($data){
        $data.target.disabled = true;
      }
    
      asignarValue ($data, i : number) {

 
        var objeto = this.auxItemsSelected[i];
        objeto.valor = $data.target.value;
        Object.assign(this.auxItemsSelected[i],  objeto);
      }
    



      guardar(form : NgForm ) {
        if(form.invalid) {
          
          return;
        }
        if(this.auxItemsSelected.some(d=> d.valor == '')){
          createToast("Item sin valor",1);

          return;
        }

        this.auxItemsSelected.forEach(element => {
          var result = new ClienteProductoItemModel();
          result.ClienteProductoId = this.clienteProducto.Id;
          result.Id = element.clienteProItemId;
          result.ItemValor = element.item;
          result.ItemValor.ItemId = element.item.Id;
          result.ItemValor.Valor = element.valor;
          this.listaResult.push(result);
        })
        
        this.clientesService.asignarItemsProducto(this.clienteProducto.Id, this.listaResult)
        .subscribe(res => {
          if(res){
            Swal.fire('Guardado', '¡Items Actualizados!', 'success')
          }
          else{
            Swal.fire('Error', '!', 'error')
          }
        });
         this.activeModal.close();
       }
}