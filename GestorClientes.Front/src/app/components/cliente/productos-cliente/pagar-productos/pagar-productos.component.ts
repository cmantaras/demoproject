import { FacturaPagoModel } from './../../../../models/cliente/facturaPago.model';
import { PagoClienteModel } from './../../../../models/cliente/pagoCliente.model';
import { Component, ContentChildren, Input, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router, RoutesRecognized } from '@angular/router';
import { NgbActiveModal, NgbModal, NgbModalModule, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ClienteModel } from 'src/app/models/cliente/cliente.model';
import { ClienteProdDeudaModel, CuotaProductoModel } from 'src/app/models/cliente/clienteProdDeuda.model';
import { ClienteRequestModel } from 'src/app/models/cliente/clienteRequest.model';
import { CierresService } from 'src/app/services/cierres.service';
import { ClientesService } from 'src/app/services/clientes.service';
import { PagosClienteService } from 'src/app/services/pagosCliente.service';
import Swal from 'sweetalert2';
import { ParametroModel } from 'src/app/models/parametro.model';
import { ParametrosService } from 'src/app/services/parametros.service';



@Component({
  selector: 'app-pago-cliente',
  templateUrl: './pagar-productos.component.html',
  styleUrls: ['./pagar-productos.component.css']
})
export class PagarProductosComponent implements OnInit {

  isLoading : boolean = false;
  clienteId :string;
  count : number;
  parametro : ParametroModel;
  cuotasProductos : Array< ClienteProdDeudaModel >;
  cuotasProductosTodos : Array< ClienteProdDeudaModel >;
  cuotasProductosToShow : Array< ClienteProdDeudaModel >;
  auxCuotasProductos : Array< ClienteProdDeudaModel >;
  facturaPago: FacturaPagoModel;
  total : number;
  today : Date;
  cliente : ClienteModel;
  clienteFiltro : ClienteRequestModel;
  listSelectedCuotas : CuotaProductoModel[];
  selectedItems : ClienteProdDeudaModel[];
  modalRef : NgbModalRef;
  idAvisoPago : number;

  tituloFactura: string ="TITULO_COMPROBANTE_PAGO";
  title : string;
  comentarioFactura: string ="COMENTARIO_COMPROBANTE_PAGO";
  comentary : string;



  totalDeuda : number = 0;
  mostrarCuotas : boolean;
  loadingData : boolean;
  cierres : Date[];


  constructor(
    public _parametroService : ParametrosService,
      public clientesService?: ClientesService,
      public pagosClienteService?: PagosClienteService,
      public router? : ActivatedRoute,
      public cierresService? : CierresService,
      private _route?: Router,
      public modalService?: NgbModal
  ) {
    this.loadingData = false;
    this.today = new Date();
    this.total = 0;
    this.clienteFiltro = new ClienteRequestModel();
    this.cliente = new ClienteModel();
    this.listSelectedCuotas = [];
    this.cuotasProductos = new Array< ClienteProdDeudaModel >();
    this.auxCuotasProductos = new Array< ClienteProdDeudaModel >();
    this.mostrarCuotas = false;

  }


  ngOnInit(): void {
    this.router.parent.params.subscribe(
      (params) =>
      {
          this.clienteFiltro.Id =params.id;

          this.getData(this.clienteFiltro);
      });

      this.mostrarCuotas= false;

      this._parametroService.get( this.tituloFactura).subscribe(( data: any )=>{

        var parametros : ParametroModel[] = data.Parametros
        var valorTitulo : any =parametros.find(ele => ele.Codigo ==  this.tituloFactura).Valor
        this.title = valorTitulo ;

      })
      this._parametroService.get( this.comentarioFactura).subscribe(( data: any )=>{

        var parametros : ParametroModel[] = data.Parametros
        var valorComentario : any =parametros.find(ele => ele.Codigo ==  this.comentarioFactura).Valor
        this.comentary = valorComentario ;


      })
  }


  public getData(clienteFiltro : ClienteRequestModel){
    this.cuotasProductos = new Array<ClienteProdDeudaModel>();
    this.loadingData = true;

    //Se obtienen las fechas de cierre para filtrar
    this.cierresService.getCierresMes().subscribe((data : any) => {

          this.cierres = data;

          this.clientesService.getClientes(    clienteFiltro ).subscribe((res : any )=> {
          var clientes : ClienteModel [] = res.Clientes
          this.cliente = clientes[0];

          this.clientesService.getProductosPendientesPago( { ClientesIds : [this.clienteFiltro.Id], GenerarAvisoPago : false  }).subscribe((res : any )=> {
            var clienteCuotas = res;
            this.cuotasProductosTodos = clienteCuotas[0].Productos;

            this.clientesService.getProductosPendientesPagoFecha( { ClientesIds : [this.clienteFiltro.Id], GenerarAvisoPago : false  }).subscribe((res : any )=> {
                var clienteCuotas = res;
                this.cuotasProductos = clienteCuotas[0].Productos;    
                this.count = clienteCuotas[0].Count;

                if(this.mostrarCuotas){ this.showAllCuotas();}
                else {this.checkIfshowCuota()}
                this.loadingData = false;
            });
          });
        });
      });
  }

  showAllCuotas(){

    this.mostrarCuotas=true;
    this.cuotasProductosToShow=[];
    this.cuotasProductosToShow = this.cuotasProductosTodos;
    
  }

  modelChanged(){
    this.total = 0;

  //Recalcular total de productos seleccionados
    for (let producto of Object.values(this.cuotasProductosToShow)) {
        for(let cuota of Object.values(producto)) {
          if(cuota.Pagar) {
          this.total =  this.total + ((cuota.MoraCuota) ? cuota.Importe + (cuota.Importe * (cuota.MoraCuota.Porcentaje/100)) : cuota.Importe);
        }
      }
    }
  }

  changeValue(cuota: CuotaProductoModel ){
    if(cuota.Pagar){
      cuota.Pagar = false;
    }else { cuota.Pagar = true; }
    this.modelChanged();
  }

  //Si la cuota es despues del presente -> no mostrar
 public checkIfshowCuota(){
   this.mostrarCuotas= false;
    this.cuotasProductosToShow= this.cuotasProductos;     
  }

  generateFecha(itemCuota : CuotaProductoModel){
    var desde =new Date(itemCuota.ClienteProducto.Desde);

    //Fecha de compra + mes de la cuota.
    //Para mostrar unicamanete las cuotas anterioras no pagas
    var month = new Date(desde.setMonth(desde.getMonth()+ (itemCuota.Cuota -1 ) ));
    return month;
  }

  pagar(){
    this.facturaPago = new FacturaPagoModel();
    this.listSelectedCuotas= [];

    for (let producto of Object.values(this.cuotasProductosToShow)) {
      for(let cuota of Object.values(producto)) {
        if(cuota.Pagar){
          this.listSelectedCuotas.push(cuota)
        }
      }
    }

    //Si el cliente paga el total de los productos
    const cuotas  = this.listSelectedCuotas.map(
      obj => ({
        CuotaId : obj.Id,
        ClienteProductoId : obj.ClienteProducto.Id,
        Cuota : obj.Cuota,
        Importe : obj.Importe
      })
    )
    var fecha = new Date();
    var formated = fecha.getMonth() +1 + "/" + fecha.getDate() + "/" + fecha.getFullYear() + ' '
                  + fecha.getHours() +':'+ fecha.getMinutes() + ':'+ fecha.getSeconds();
    const pagos = {
      Cuotas : cuotas,
      ClienteId : this.cliente.Id,
      Fecha : formated
    }

    this.pagosClienteService.realizarPago(pagos).subscribe((res: number) => {

      this.modalRef.close();
      this.idAvisoPago = res;
      var totalPago = 0;

         //Si el cliente paga el total de los productos
        const detalles  = this.listSelectedCuotas.map(
          obj => ({
            CuotaId : obj.Id,
            Producto : obj.ClienteProducto.ProductoPrecio.Producto.Descripcion,
            Cuota : obj.Cuota + '/'+obj.ClienteProducto.MetodoPago.Cantidad,
            MoraCuota : obj.MoraCuota,
            Importe : obj.MoraCuota ? (obj.Importe * (obj.MoraCuota.Porcentaje / 100) + obj.Importe) : obj.Importe
          })
        )
      
        detalles.forEach(obj => {
          totalPago += obj.Importe
        });

         var _aplicanMoras = detalles.some(element => element.MoraCuota != null);

          this.facturaPago.IdPago = res,
          this.facturaPago.Cliente = this.cliente,
          this.facturaPago.Detalles = detalles,
          this.facturaPago.Total = totalPago,
          this.facturaPago.AplicaMoras= _aplicanMoras;
          this.facturaPago.FechaHora = new Date()

      this.total = 0;
    });
    this.getData(this.clienteFiltro);

    setTimeout(() => {

      this.printImprimirPago()
      
      this.ngOnInit();
    }, 1000);

    

  }

  modalPagar(content){
      this.modalRef = this.modalService.open(content, { backdrop: 'static'});
  }

  printImprimirPago(){
    let printContents, popupWin;
    printContents = document.getElementById('print-recibo').innerHTML;

    popupWin = window.open('', '_blank', 'top=0,left=0,height=auto,width=auto');
    popupWin.document.open();

    popupWin.document.write(`
    <html>
      <head>
        <title>Print tab</title>
        <style>

        </style>
      </head>
    <body onload="window.print();window.close()">${printContents}</body>
    </html>`
    );
    popupWin.document.close();
    popupWin.focus();

  }

  setTextoFactura(){
    var titulo = this.title;
    var comentario = this.comentary;
    this._parametroService.setParamsFactura(titulo, comentario).subscribe(data=> {
      Swal.fire("", "Parametros guardados correctamente","success")
    })

  }

}

