import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';
import { ClientesService } from 'src/app/services/clientes.service';
import Swal from 'sweetalert2';
import { ClienteModel } from 'src/app/models/cliente/cliente.model';
import { ClienteProductoModel } from 'src/app/models/cliente/productoCliente.model';
import { ProductosService } from 'src/app/services/productos.service';



@Component({
  selector: 'app-eliminar-cliente',
  template: `
  <div class="modal-header">
    <h4 class="modal-title" id="modal-title">Eliminar producto</h4>
    <button type="button" class="close" aria-describedby="modal-title" (click)="activeModal.dismiss('Cross click')">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="modal-body">
    <p><strong>¿ Estás seguro que deseas eliminar el producto ? <span class="text-primary"> {{ " " + productoCliente.ProductoPrecio.Producto.Descripcion }}   </span> </strong></p>

  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-outline-secondary" (click)="activeModal.dismiss('cancel click')">Salir</button>
    <button type="button" class="btn btn-danger" (click)="eliminar()">Confirmar</button>
  </div>
  `,
  styles: [
  ]
})

export class DeleteProductoClienteComponent implements OnInit {

  public productoCliente : ClienteProductoModel = new ClienteProductoModel();
  public modalHeader : string;
  productos : ClienteProductoModel [] = [];



  constructor( public activeModal: NgbActiveModal,
               private clienteService: ClientesService, ) { }

  ngOnInit(): void {
    
   }

  eliminar(){

    this.clienteService.deleteProductoCliente(this.productoCliente).
      subscribe(resp => {

      Swal.fire('', 'Se eliminó cliente correctamente','success' );

      var indice = this.productos.indexOf(this.productoCliente);
      this.productos.splice(indice, 1);

      this.activeModal.close();

    }, (error) => {
      if(error.error)
      {
        let mensajeError = error.error.Message;
        Swal.fire('Error', mensajeError, 'error');
      }
      else { Swal.fire('Error', 'No se pudo eliminar cliente', 'error') }
    });

  }

}
