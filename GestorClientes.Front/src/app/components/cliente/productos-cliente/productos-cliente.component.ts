import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ClientesService } from 'src/app/services/clientes.service';
// import { AddEditprodutoClienteComponent } from 'src/app/components/configuracion/produtoCliente/add-edit-produtoCliente.component';
import { NgbModal, NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';

import { ProductoModel } from 'src/app/models/producto/producto.model';
import { ClienteModel } from 'src/app/models/cliente/cliente.model';
import { ClienteProductoModel } from 'src/app/models/cliente/productoCliente.model';
import { ClienteRequestModel } from 'src/app/models/cliente/clienteRequest.model';
import { DeleteProductoClienteComponent } from './delete-producto-cliente.component';
import { PagarProductosComponent } from './pagar-productos/pagar-productos.component'
import { ProductoComponent } from '../../configuracion/producto/producto.component';
import { AddEditItemsComponent } from './add-edit-items.component';
import { AsignarProductoModel } from 'src/app/models/producto/asignarProducto';
import { ItemModel } from '../../../models/item/item.model';
import { ItemsService } from '../../../services/items.service';


@Component({
  selector: 'app-produtoCliente',
  templateUrl: './productos-cliente.component.html',
  styleUrls: ['./productos-cliente.component.css'],
  providers: [NgbPaginationConfig]

})
export class ProductosClienteComponent implements OnInit {

  loading : boolean = true;
  productosCliente : ClienteProductoModel  [] = [];
  clienteId : string;
  showProductos : boolean;
  clienteFiltro : ClienteRequestModel = new ClienteRequestModel();

  allItems: ItemModel[];

  public cliente : ClienteModel = new ClienteModel();
  count : number;
  //Paginación
  page = 1;
  pageSize =4;
  items = [];
  totalItems : number;
  productoInfo: ProductoModel;
  fecha: Date;
  


  constructor(private productosClienteService: ClientesService,
              private itemsClienteService: ItemsService,
              public router:ActivatedRoute,
              public modalService: NgbModal,
              private config: NgbPaginationConfig,)
    {


    }

    ngOnInit(): void {
    this.page =1;

      this.router.paramMap.subscribe(params  => {
        this.clienteId = params.get('id');
            this.clienteFiltro.Id =  Number(this.clienteId);

            this.productosClienteService.getClientes(    this.clienteFiltro ).subscribe((res : any )=> {
              var clientes : ClienteModel [] = res.Clientes
              this.fillProductoscliente(   clientes[0] )
           });
      });

    }


    
    openModalInfo(producto :  ClienteProductoModel, cliente :  ClienteModel) {
      const modalRef = this.modalService.open(AddEditItemsComponent, { backdrop: 'static', size: 'lg'});
      modalRef.componentInstance.modalHeader = 'Agregar/Editar Items';
     
      modalRef.componentInstance.clienteProducto = producto;
      modalRef.componentInstance.cliente = cliente;
      modalRef.result.then(
        (result : any) => {
          this.ngOnInit();
        },
        ()=> {
      });

    }



    fillProductoscliente( cliente : ClienteModel ){

      this.cliente = cliente;
      this.loading = true;
      this.productosClienteService.getProductosByClienteId(cliente.Id)
        .subscribe( (data : any) => {

          this.showProductos = true;

          this.productosCliente = data.Productos;
          this.count = data.Count

          this.loading = false;

        },( errorServicio ) => {
          this.loading = false;
        });
    }

     openModalDelete( productoCliente : ClienteProductoModel ) {

       const modalRef = this.modalService.open(DeleteProductoClienteComponent, { backdrop: 'static'});
       modalRef.componentInstance.modalHeader = 'Eliminar produtoCliente';
       productoCliente.ClienteId = this.cliente.Id;
       modalRef.componentInstance.productoCliente = productoCliente;

       modalRef.componentInstance.produtoClientees = this.productosCliente;

       modalRef.result.then(
        (result : any) => {
          this.fillProductoscliente(this.cliente);
        },
        ()=> {
      });
     }
}


