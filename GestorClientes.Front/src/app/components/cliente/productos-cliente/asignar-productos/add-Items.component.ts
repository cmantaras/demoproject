import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';
import { ClientesService } from 'src/app/services/clientes.service';
import Swal from 'sweetalert2';
import { ClienteModel } from 'src/app/models/cliente/cliente.model';
import { ItemsService } from 'src/app/services/items.service';
import { ItemModel } from 'src/app/models/item/item.model';
import { ItemValorModel } from 'src/app/models/item/itemValor.model';
import { ProductoModel } from 'src/app/models/producto/producto.model';
import { MetodoPagoModel } from 'src/app/models/pagos-metodo/metodo-pago.model';

declare function createToast(params: string, type: number);

@Component({
  selector: 'app-eliminar-cliente',
  template: `
  <div class="modal-header text-center">
    <h4 class="modal-title" id="modal-title">Agregar items</h4>
    <button type="button" class="close" aria-describedby="modal-title" (click)="activeModal.dismiss('Cross click')">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="modal-body">
    <div class="text-center agregar-items" *ngIf="!loadItems && !showMetodosPago">
      <p class="h6"><strong>¿ Desea agregar items al producto seleccionado ? <span class="text-primary"> {{ cliente.Nombre }}   </span> </strong></p>
    </div>

    <div  *ngIf="showMetodosPago">
    <div  class="col text-center">
    <p class="h6"><strong> Seleccione un método de pago para el producto   </strong></p>
    <p  class="h4">  {{ producto.Descripcion  +"  -   "+producto.PrecioActual.Moneda.Descripcion +  " " + producto.PrecioActual.Precio  }} </p>
          <table class="animated fadeIn table table-striped table-advance table-hover">
            <thead>
            </thead>
            <tbody>
              <tr></tr>
              <tr></tr>
              <tr  style="cursor : pointer;" [ngStyle]="metodo.Color ?{'background-color': 'darkseagreen'} : null"
                    *ngFor="let metodo of producto.PrecioActual.Metodos "
                    (click)="selectMetodo(metodo)" >
                  <td *ngIf="producto.Servicio">
                    {{ producto.PrecioActual.Moneda.Descripcion + " " + (producto.PrecioActual.Precio / metodo.Cantidad).toFixed(0) + "   -  " + "Mensual"}}
                  </td>
                  <td *ngIf="!producto.Servicio">
                    {{ producto.PrecioActual.Moneda.Descripcion + " " + (producto.PrecioActual.Precio / metodo.Cantidad).toFixed(0) + "  x  " + metodo.Cantidad }}
                  </td>
              </tr>
            </tbody>
          </table>
      </div>
      </div>
    <div  *ngIf="loadItems">
    <form autocomplete="off"  #form="ngForm" class="form-horizontal style-form" method="get">
      <div class="d-flex flex-row   ">
      <div class="col-sm-6">
          <table class="animated fadeIn table table-striped table-advance table-hover">
            <thead>
              <tr>
                <th  class="hidden-phone" ><i class=" d-flex"></i> Items disponibles </th>
              </tr>
            </thead>
            <tbody>
              <tr  *ngFor="let item of items" >
                    <td>
                      {{ item.Descripcion }}
                    </td>
                    <td>
                      <a type="button" (click)="agregarItem(item)"> <i class="fa-2x fas fa-plus-circle"></i> </a>
                    </td>
              </tr>
            </tbody>
          </table>
      </div>
      <div  class="col-sm-6">
          <table class="animated fadeIn table table-striped table-advance table-hover">
            <thead>
              <tr>
                <th  class="hidden-phone" ><i class=""></i> Items seleccionados </th>
              </tr>
            </thead>
            <tbody>
              <tr *ngFor="let itemLoaded of selectedItems; let i = index" [attr.data-index]="i"  >
                  <td>
                     {{ itemLoaded.Item.Descripcion }}
                  </td>
                  <td>
                  <input type="text"
                            class="form-control "
                            pattern="/^[A-Za-z0-9\s]+$/g"
                            name="Nombre"
                            required
                            placeholder="Valor"
                            minlength="1"
                            (keyup)="asignarValue($event, i)"
                            (keyup.enter)="disable($event, i)"
                           >
                           <small *ngIf ="itemLoaded.Valor == ''" >Ingrese un valor</small>
                  </td>
                  <td>
                    <a type="button" (click)="quitarItem(itemLoaded)"> <i class="fa-2x fas fa-minus-circle"></i> </a>
                  </td>
              </tr>
            </tbody>
          </table>
      </div>
      </div>
      </form>

    </div>
  </div>
  <div class="modal-footer">

    <button *ngIf="!changeButtonSalir" type="button" class="btn btn-danger" (click)="loadProducto()">No</button>
    <button *ngIf="changeButtonSalir" type="button" class="btn btn-danger" (click)="activeModal.dismiss('Cross click')">Salir</button>


    <button *ngIf="!loadItems && !showMetodosPago" 
              type="button" class="btn btn-success" 
              (click)=" seleccionarItems()"> 
               Si </button>

    <button  *ngIf="loadItems" type="submit"  
            (click)="loadProducto()" 
            class="btn btn-outline-secondary" >
            Seleccionar método de pago<i 
            class="ml-1 fa fa-arrow-right"> </i>
          </button>
    <button  *ngIf="showMetodosPago" 
            [disabled]="!selectedMetodo"  
            type="button" 
            class="btn btn-success" 
            (click)="save()">
        Guardar</button>
  </div>
  `,
  styles: [
  ]
})

export class AddItemsComponent implements OnInit {

  public cliente : ClienteModel = new ClienteModel();
  public modalHeader : string;

  changeButtonSalir :boolean = false;
  clientes : ClienteModel [] = [];
  items : ItemModel [] = [];
  selectedItems : ItemValorModel [] = [];
  selectedMetodo : MetodoPagoModel;
  loadItems : boolean;
  showMetodosPago : boolean;
  producto : ProductoModel;
  valor : string;

  constructor( public activeModal: NgbActiveModal,
               private clientesService: ClientesService,
                private itemsService : ItemsService ) {

                this.loadItems = false;
                this.showMetodosPago = false;
  }

  ngOnInit(): void {

    this.itemsService.getItems()
      .subscribe( (data: any) => {
        this.items = data.Items;
    });
  }


  seleccionarItems() {
    this.loadItems = true;
  }

  selectMetodo(metodo : MetodoPagoModel){

    this.producto.PrecioActual.Metodos.forEach(x => {
      x.Color = false
    });
    metodo.Color = true;
    Object.assign(this.producto.PrecioActual.Metodos[this.producto.PrecioActual.Metodos.findIndex(el => el.Id === metodo.Id)],  metodo);
    metodo.Color = true;
    this.selectedMetodo = metodo;
  }

  agregarItem(itemToLoad : ItemModel) {
      var itemTo = new ItemValorModel();
      itemTo.Item = itemToLoad;
      itemTo.ItemId = itemToLoad.Id;
      itemTo.Valor = '';
      this.selectedItems.push(itemTo);
  }

  disable ($data){
    $data.target.disabled = true;
  }

  asignarValue ($data, i : number) {
    var objeto = this.selectedItems[i];
    objeto.Valor = $data.target.value;
    Object.assign(this.selectedItems[i],  objeto);
  }

  quitarItem (item : ItemValorModel) {
    var itemTo = this.selectedItems.find(obj => obj.Item.Id === item.Item.Id)
      var indice = this.selectedItems.findIndex(obj => obj.Item.Id === item.Item.Id);
      this.selectedItems.splice(indice, 1)
  }
  loadProducto(){
    if(this.selectedItems.some(d=> d.Valor == '')){
      createToast("Item sin valor",1);

      return;
    }
    this.loadItems = false;
    this.showMetodosPago = true;
    this.changeButtonSalir = true;
  }

  save(){
    var result = {
      items : this.selectedItems,
      metodo : this.selectedMetodo
    }
    this.activeModal.close( result);
  }

  eliminar(){

    this.clientesService.deleteCliente(this.cliente).
      subscribe(resp => {
      Swal.fire('', 'Se eliminó cliente correctamente','success' );
      var indice = this.clientes.indexOf(this.cliente);
      this.clientes.splice(indice, 1);
    }, (error) => {
      if(error.error)
      {
        let mensajeError = error.error.Message;
        Swal.fire('Error', mensajeError, 'error');
      }
      else { Swal.fire('Error', 'No se pudo eliminar cliente', 'error') }
    });
    this.activeModal.close();
  }

}
