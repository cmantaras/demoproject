import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { ClientesService } from 'src/app/services/clientes.service';
import { AddEditClienteComponent } from 'src/app/components/cliente/add-edit-cliente.component';
import { NgbModal, NgbPaginationConfig, NgbToast } from '@ng-bootstrap/ng-bootstrap';
import { ClienteModel } from '../../../../models/cliente/cliente.model';

import {CdkDragDrop, DropListRef, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import { ProductosService } from 'src/app/services/productos.service';
import { ProductoModel } from 'src/app/models/producto/producto.model';
import { AddItemsComponent } from './add-Items.component';
import { ItemModel } from 'src/app/models/item/item.model';
import { ProductosClienteComponent } from '../productos-cliente.component';
import { AsignarProductoModel } from 'src/app/models/producto/asignarProducto';
import { ItemValorModel } from 'src/app/models/item/itemValor.model';
import { FaLayersTextComponent } from '@fortawesome/angular-fontawesome';
import Swal from 'sweetalert2';
import { DatePipe } from '@angular/common';
import { Inject, Injectable, Optional } from '@angular/core';
import { Moment } from 'moment';
import { faWheelchair } from '@fortawesome/free-solid-svg-icons';
import { mergeAnalyzedFiles } from '@angular/compiler';



@Component({
  selector: 'app-cliente',
  templateUrl: './asignar-productos.component.html',
  styleUrls: ['./asignar-productos.component.css'],
  providers: [NgbPaginationConfig]
})
export class AsignarProductosComponent implements OnInit {

  loading : boolean = true;
  clientes : ClienteModel  [] = [];

  cliente : ClienteModel;

  //Paginación
  page = 1;
  pageSize =4;
  items = [];
  totalItems : number;
  clienteId : string;

  productos : ProductoModel[] = [];
  loadingItems : boolean;
  fecha : Date;
  selectedProductos : AsignarProductoModel [] = [];
  selectedItems : ItemModel [] = [];
  itemsValor : ItemValorModel[]=[];
  productosSinAsignar : AsignarProductoModel[]=[];
  currentProductDragged : ProductoModel;
  productoCliente : AsignarProductoModel;

  checkProductoCliente : AsignarProductoModel;
  total : number;
  constructor(private clientesService: ClientesService,
              public router:ActivatedRoute,
              public route : Router,
              public modalService: NgbModal,
              public productosService : ProductosService,
              private config: NgbPaginationConfig
              )
    {

      this.cliente = new ClienteModel();
      this.total = 0;
      this.loadingItems = false;

      this.router.parent.params.subscribe(params  => {
         this.clienteId = params['id'];
        this.loading = false;
      });

    }

    ngOnInit(): void {
      this.page =1;
      this.productosService.getProductos()
        .subscribe( (data : any) =>{
          this.productos = data.Productos;
      });
    }

    quitarItem (producto : AsignarProductoModel) {
      var itemTo = this.selectedProductos.find(obj => obj.Producto.Id === producto.Producto.Id)
        this.checkProductoCliente = new AsignarProductoModel();
        var indice = this.selectedProductos.findIndex(obj => obj.Producto.Id === producto.Producto.Id);
        this.selectedProductos.splice(indice, 1)
        this.total = this.total - producto.Producto.PrecioActual.Precio;
    }

    agregarProducto(producto: ProductoModel) {
     this.openModalAdd(producto);
    }

    checkProducto(productoCliente : AsignarProductoModel, i : number){
      this.selectedProductos.forEach(x => {
        x.Producto.Color = false
      });
      var producto = JSON.parse( JSON.stringify( productoCliente ) );
      producto.Producto.Color = true;
      Object.assign(this.selectedProductos[i],  producto);

      this.loadingItems = true;
      setTimeout(() => {
        this.checkProductoCliente = productoCliente;
        this.loadingItems = false;
      }, 700);

    }
    save(){
      this.clientesService.asignarProductos(this.selectedProductos)
        .subscribe(res => {
          if(res){
            Swal.fire('','Los productos se ingresaron correctamente', 'success');
            setTimeout(() => {

              this.route.navigateByUrl('/', {skipLocationChange: true}).then(() => {
                  this.route.navigate([`/cliente/${this.clienteId}/productos`]);
              });
            }, 1000)
          }
        });


    }
    openModalAdd(producto :  ProductoModel) {
      const modalRef = this.modalService.open(AddItemsComponent, { backdrop: 'static', size: 'lg'});
      modalRef.componentInstance.modalHeader = 'Agregar Items';

      producto.PrecioActual.Metodos.forEach(m => {
        m.Color = false
      });
      modalRef.componentInstance.producto = producto;

      // Luego de agregar items y seleccionar metodo de pago en modal se actualizan listan
      modalRef.result.then(
        (result? : any) => {

          this.fecha = new Date();
          var formated = this.fecha.getMonth() +1 + "/" + this.fecha.getDate() + "/" + this.fecha.getFullYear() + ' '
                        + this.fecha.getHours() +':'+ this.fecha.getMinutes() + ':'+ this.fecha.getSeconds();

          var productoCliente :  AsignarProductoModel = {
            ClienteId : this.clienteId,
            Producto : producto,
            Items : result['items'],
            Metodo : result['metodo'],
            Desde: formated,
            MetodoId : result['metodo'].Id,
            ProductoId : producto.Id.toString(),
            MetodoPagoId : result['metodo'].Id,
            Hasta: formated
          }
          //Se actualizan listas
          this.total = this.total + productoCliente.Producto.PrecioActual.Precio
          this.checkProductoCliente =  productoCliente;
          this.selectedProductos.push(productoCliente);
        },
        ()=> {
      });
    }
}

