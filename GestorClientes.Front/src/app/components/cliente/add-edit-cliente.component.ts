import { Component, EventEmitter, Injectable, OnInit, Output } from '@angular/core';
import { NgbActiveModal, NgbDateParserFormatter, NgbDateStruct, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { ClientesService } from 'src/app/services/clientes.service';
import { ClienteModel } from 'src/app/models/cliente/cliente.model';
import Swal from 'sweetalert2';
import { Observable } from 'rxjs';
import { ZonasService } from 'src/app/services/zona.service';
import { CiudadesService } from 'src/app/services/ciudades.service';
import { ZonaModel } from 'src/app/models/zona/zona.model';
import { CiudadModel } from 'src/app/models/ciudad/ciudad.model';
import { NgbDateCustomParserFormatter } from '../helper/dateformat';
import { NULL_EXPR } from '@angular/compiler/src/output/output_ast';




@Component({
  selector: 'app-add-edit-cliente',
  template: `
    <div class="modal-header">
      <h4 class="modal-title"> {{ modalHeader }} </h4>
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">

              <form autocomplete="off" (ngSubmit)="guardar( form )" #form="ngForm" class="form-horizontal style-form" method="get">
                <div class="form-group">

                  <label class="col control-label">Cédula de Identidad</label>
                  <div class="col-sm-10">
                  <input type="number"
                            class="form-control "
                            name="Identificador"
                            [ngClass]="{ 'is-invalid': form.submitted && identificador.invalid}"
                            [class.is-invalid]="identificador.invalid && identificador.touched"
                            [(ngModel)] = "cliente.Identificador"
                            required
                            placeholder="Cédula de Identidad"
                            minlength="1"
                            #identificador="ngModel"

                           >
                       <small   *ngIf="identificador.invalid && identificador.touched"
                                   class="form-text text-danger">Ingrese cédula de identidad válida</small>
                  </div>

                  <label class="col-sm-2 col-sm-2 control-label">HTE</label>
                  <div class="col-sm-10">
                  <input type="text"
                            class="form-control "
                            ng-pattern="/^[0-5]+$/"
                            name="hte"
                            [ngClass]="{ 'is-invalid': form.submitted && hte.invalid}"
                            [class.is-invalid]="hte.invalid && hte.touched"
                            [(ngModel)] = "cliente.Hte"
                            required
                            placeholder="HTE"
                            maxlength="4"
                            #hte="ngModel"

                           >
                           <small   *ngIf="hte.invalid && hte.touched"
                                   class="form-text text-danger">Ingrese un hte válido (4 caracteres)</small> 
                  </div>

                  <label class="col-sm-2 col-sm-2 control-label">Nombre</label>
                  <div class="col-sm-10">
                  <input type="text"
                            
                            class="form-control is-invalid"
                            name="Nombre"
                            [ngClass]="{ 'is-invalid': form.submitted && nombre.invalid}"
                            [class.is-invalid]="nombre.invalid && nombre.touched"
                            [(ngModel)] = "cliente.Nombre"
                            required
                            placeholder="Nombre"
                            minlength="1"
                            #nombre="ngModel"

                           >
                            <small   *ngIf="nombre.invalid && nombre.touched"
                                   class="form-text text-danger">Ingrese nombre del cliente válido</small>
                  </div>


                  <label class="col-sm-2 col-sm-2 control-label">Apellido</label>
                  <div class="col-sm-10">
                  <input type="text"
                            
                            class="form-control is-invalid"
                            name="Apellido"
                            [ngClass]="{ 'is-invalid': form.submitted && apellido.invalid }"
                            [class.is-invalid]="apellido.invalid && apellido.touched"
                            [(ngModel)] = "cliente.Apellido"
                            required
                            placeholder="Apellido"
                            minlength="1"
                            #apellido="ngModel"

                           >
                            <small   *ngIf="apellido.invalid && apellido.touched"
                                   class="form-text text-danger">Ingrese apellido del cliente válido</small>
                  </div>

                  <label class="col-sm-2 col-sm-2 control-label">Teléfono</label>
                  <div class="col-sm-10">
                  <input type="text"
                            pattern="[0-9]*"
                            class="form-control"
                            name="Telefono"

                            [(ngModel)] = "cliente.Telefono"
                            required
                            placeholder="Telefono"
                            minlength="7"
                            #telefono="ngModel"

                           >
                          <small   *ngIf="telefono.invalid && telefono.touched"
                                   class="form-text text-danger">Ingrese un número de telefono válido</small> 
                  </div>

                  <label class=" col-sm-10 control-label">Fecha de Nac.</label>
                 <div class="d-flex col-sm-8 flex-sw flex-row ">
                        <input class="form-control fecha "
                                placeholder="dd-mm-yyyy"
                                [minDate]="{year: 1940, month: 1, day: 1}"
                                name="dp"
                                required
                                [(ngModel)]="fechaNacimiento"
                                ngbDatepicker
                                readonly
                                #d1="ngbDatepicker"
                              >
                        <div class="input-group-append">
                          <button class="btn btn-outline-secondary btn-fecha calendar" (click)="d1.toggle()" type="button"><i class="fa fa-calendar"></i></button>
                        </div>
                    </div>

                    <label class="col-sm-2 col-sm-2 control-label">Dirección</label>
                  <div class="col-sm-10">
                  <input type="text"
                            class="form-control is-invalid"
                            name="Direccion"
                            [ngClass]="{ 'is-invalid': form.submitted && direccion.invalid }"
                            [class.is-invalid]="direccion.invalid && direccion.touched"
                            [(ngModel)] = "cliente.Direccion"
                            required
                            placeholder="Dirección"
                            minlength="1"
                            #direccion="ngModel"
                           >
                           <small   *ngIf="direccion.invalid && direccion.touched"
                                   class="form-text text-danger">Ingrese una dirección válida</small>
                  </div>
                  
    
                    <label class="col-sm-2 col-sm-2 control-label">Zona</label>
                    <div class=" col-sm-10 flex-sw ">
                      <ng-select [items]="zonas"
                          bindLabel="Descripcion"
                          bindValue="Id"
                          class="w-100"
                          [(ngModel)]="cliente.ZonaId"
                          name="zona"
                          required
                          #zona="ngModel">
                        
                      </ng-select>
                      <small   *ngIf="!cliente.ZonaId && zona.touched"
                                   class="form-text text-danger">Seleccione una zona</small>
                    </div>
                
                  


                  <label class="col-sm-2 col-sm-2 control-label">Ciudad</label>
                  <div class=" col-sm-10 flex-sw ">
                    <ng-select [items]="ciudades"
                        bindLabel="Descripcion"
                        bindValue="Id"
                        class="w-100"
                        [(ngModel)]="cliente.CiudadId"
                        required
                        name="ciudad"
                        #ciudad="ngModel">
                    </ng-select>
                    <small   *ngIf="!cliente.CiudadId && ciudad.touched"
                                   class="form-text text-danger">Seleccione una ciudad</small>
                  </div>
                  <label class=" col-sm-10 control-label">Fecha fin servicio</label>
                 <div class="d-flex col-sm-8 flex-sw flex-row ">
                        <input class="form-control fecha "
                                placeholder="dd-mm-yyyy"
                                required
                                name="dp1"
                                [(ngModel)]="fechaHasta"
                                ngbDatepicker
                                readonly
                                #d2="ngbDatepicker"
                              >
                        <div class="input-group-append">
                          <button class="btn btn-outline-secondary btn-fecha calendar" (click)="d2.toggle()" type="button"><i class="fa fa-calendar"></i></button>
                        </div>
                  </div>
                  <div class="d-flex col-sm-8 flex-sw flex-row ">
                    <div class="custom-control custom-switch">

                      <input
                             [(ngModel)]="cliente.ClienteViejo"
                              name="clienteViejo"
                             
                            style="cursor:pointer;"
                            type="checkbox"
                            class="custom-control-input" id="switch1">

                      <label class="custom-control-label" for="switch1"> Cliente sistema antiguo</label>
                     </div>
                  </div>

                  



                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-danger" (click)="activeModal.close('Close click')">Salir</button>
                  <button disabled="{{ form.$invalid }}" type="submit" class="btn btn-success">Guardar</button>
                </div>
              </form>

    </div>
  `,
  styles: [
  ],

  providers : [{provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter}]

})
@Injectable()
export class AddEditClienteComponent implements OnInit   {

  public savingInfo : boolean;
  public cliente : ClienteModel = new ClienteModel();
  public modalHeader : string;
  public clientes : ClienteModel [] = [];

  public zonas : ZonaModel [] = [];
  public ciudades : CiudadModel [] = [];

  public fechaNacimiento  : NgbDateStruct = {  year : null, month : null, day: null };
  public fechaHasta   = {  year : undefined, month : undefined, day: undefined };
  clienteForm: FormGroup;
  @Output() sendcliente= new EventEmitter();


  constructor( public activeModal: NgbActiveModal,
              private clientesService: ClientesService,
              private zonasService : ZonasService,
              private ciudadesService : CiudadesService,
              private formBuilder: FormBuilder
                       ) {


  }

  ngOnInit() {
    this.savingInfo= false;
    if(this.cliente.Id){
      var fecha = new Date(this.cliente.FechaNacimiento);
      var fechaHasta = new Date(this.cliente.FechaHasta);
      this.fechaNacimiento = { day : fecha.getDate(), month : fecha.getMonth() + 1, year : fecha.getFullYear() }
      this.fechaHasta = { day : fechaHasta.getDate(), month : fechaHasta.getMonth() + 1, year : fechaHasta.getFullYear() }

    }
    else{
      this.cliente.ClienteViejo == false;
    }

    this.zonasService.getZonas()
      .subscribe( (data : any) => {
        this.zonas = data.Zonas
      });


    this.ciudadesService.getCiudades()
      .subscribe( (data : any) => {
        this.ciudades = data.Ciudades
      });

  }

    ssssss


  guardar(form : NgForm ) {
    this.savingInfo = true;

    form.controls['dp1'].disable();

    if(form.invalid) {
      return;

    }

    
    this.cliente.FechaNacimiento = this.fechaNacimiento.month + "/" + this.fechaNacimiento.day + "/" + this.fechaNacimiento.year;
    this.cliente.FechaHasta = this.fechaHasta.month + "/" + this.fechaHasta.day + "/" + this.fechaHasta.year;
    if(this.cliente.ClienteViejo == null || this.cliente.ClienteViejo == undefined){
      this.cliente.ClienteViejo == false;
    }

    Swal.fire('cliente', 'El cliente se está ingresando', 'info');

    if ( this.cliente.Id )  {


        //Actualizar cliente
        this.clientesService.updateCliente(this.cliente).subscribe(resp => {
          Swal.fire(this.cliente.Nombre, 'Se actualizo correctamente','success' );

          Object.assign(this.clientes[this.clientes.findIndex(el => el.Id === this.cliente.Id)], this.cliente);
          this.activeModal.close();
          }, (error) => {
            

            if(error.error) {
            let mensajeError = error.error.Message;
            Swal.fire('Error', mensajeError, 'error');
            }
            else { Swal.fire('Error', 'No se pudo actualizar el cliente', 'error') }
            this.activeModal.close();
            
        });
        

    } else {

           //Agregar cliente
          this.clientesService.addCliente(this.cliente)
            .subscribe( (resp : any) => {

            this.cliente.Id = resp;

            this.clientes.push(this.cliente);
              Swal.fire(this.cliente.Nombre, 'Se agregó correctamente','success' );
              this.activeModal.close();
              }, (error) => {

                if(error.error)
                {
                  let mensajeError = error.error.Message;
                  Swal.fire('Error', mensajeError, 'error');
                }
                else { Swal.fire('Error', 'No se pudo agregar el cliente', 'error') }
                this.activeModal.close();
          });
          
       }

       Swal.hideLoading();
       this.savingInfo= false;

      
  }
}

