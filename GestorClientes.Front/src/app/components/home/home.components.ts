import { CierreMesModel } from './../../models/cliente/clienteProdDeuda.model';
import { ClienteModel } from 'src/app/models/cliente/cliente.model';
import { ParametroModel } from './../../models/parametro.model';
import { ParametrosService } from './../../services/parametros.service';
import { ReportesService } from './../../services/reportes.service';
import { EgresosComponent } from './../egresos/egresos.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ClientesService } from 'src/app/services/clientes.service';
import { Chart, registerables } from 'chart.js';
import { DatePipe } from '@angular/common';
import Swal from 'sweetalert2';
import { NgbActiveModal, NgbDateParserFormatter, NgbDateStruct, NgbModal, NgbPagination } from '@ng-bootstrap/ng-bootstrap';
import { CierresService } from 'src/app/services/cierres.service';
import { EgresosService } from 'src/app/services/egresos.service';
import { EgresoModel } from 'src/app/models/egreso/egreso.model';
import { PermisoModel } from '../../models/permiso.models';
import { NgbDateCustomParserFormatter } from '../helper/dateformat';


@Component({
  selector: 'app-visitante',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [
    {provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter}
   ]
})
export class HomeComponent implements OnInit {

   ingresos = new Map<Date, number>();
   egresos = new Map<Date, number>();
   fechaHoy : Date;
   chequeoFibra : string;
   fechaVencimiento : string;
   parametro : ParametroModel;
   birthdays : ClienteModel[];
   finContratos: ClienteModel[];
   cierres : CierreMesModel[];
   cheques : EgresoModel[];
   checkFibra  : NgbDateStruct = {  year : null, month : null, day: null };
   vencimientoMes : NgbDateStruct = {  year : null, month : null, day: null };
   loadingData : boolean;
   page: number;
   pageBirth: number;
   pageContrato: number;
   pageCheque: number;
   pageSize : number;
   count : number;

   private userPermisos: any = [];


  constructor( private reporteService : ReportesService,
                private _egresosServicios : EgresosService,
                private _cierresMes : CierresService,
                private datePipe: DatePipe,
                public modalService: NgbModal,
                public _parametroService : ParametrosService
               )
    {
      this.fechaHoy = new Date();
      this.parametro = new ParametroModel();
      this.cierres = [];
      this.cheques = [];
      this.chequeoFibra ="FECHA_CHECK_FIBRA"
      this.fechaVencimiento ="VENCIMIENTO_MES";
      this.finContratos = [];
      this.birthdays = [];
      this.page = 1;
      this.pageBirth = 1;
      this.pageContrato = 1;
      this.pageCheque = 1;
      this.pageSize = 5;
      this.loadingData = false;



      this._parametroService.get( this.fechaVencimiento).subscribe(( data: any )=>{

        var parametros : ParametroModel[] = data.Parametros

        var vencimiento : any =parametros.find(ele => ele.Codigo ==  this.fechaVencimiento).Valor
        var dateString = vencimiento;
        dateString = dateString.substr(3, 2)+"/"+dateString.substr(0, 2)+"/"+dateString.substr(6, 4);
        var date = new Date(dateString);

        this.vencimientoMes = { day : date.getDate(), month: date.getMonth() +1, year: date.getFullYear() }
      })

      this._parametroService.get( this.chequeoFibra).subscribe(( data: any )=>{

        var parametros : ParametroModel[] = data.Parametros
        var valorFechaCheck : any =parametros.find(ele => ele.Codigo ==  this.chequeoFibra).Valor
        var dateString = valorFechaCheck;
        dateString = dateString.substr(3, 2)+"/"+dateString.substr(0, 2)+"/"+dateString.substr(6, 4);
        var date = new Date(dateString);

        this.checkFibra = { day : date.getDate(), month: date.getMonth() +1, year: date.getFullYear() }
      })
    }

    ngOnInit() {
      this.userPermisos = localStorage.getItem("permisos");


      var word = "VER_REPORTES_INGRESOS-EGRESOS";

      var validate = this.userPermisos.includes(word);
      if(validate){
        this.reporteService.getIngresosByCierre().subscribe((res : Map<Date, number>) => {
          this.ingresos = res;

          this.fillChart();
        });
        this.reporteService.getEgresosByCierre().subscribe((res : Map<Date, number>) => {
          this.egresos = res;

          this.fillChart();
        });
        this.reporteService.getBirthdays().subscribe((res : any) => {
          this.birthdays = res.Clientes;


        });
        this.reporteService.getFinContratos().subscribe((res : any) => {
          this.finContratos = res.Clientes;


        });

        this.reporteService.getCheques().subscribe((res : any) => {
          this.cheques = res.Egresos;


        });

     }



    }

  setCheckFibra(){
    var valor = this.checkFibra.day +"/"+this.checkFibra.month+"/"+this.checkFibra.year;
    this._parametroService.setCheckFibra(valor).subscribe(data=> {
      Swal.fire("", "La fecha fue guardada correctamente","success")
    })
  }

  setVencimientoMes(){
    var valor = this.vencimientoMes.day +"/"+this.vencimientoMes.month+"/"+this.vencimientoMes.year;

    this._parametroService.setVencimientoMes(valor).subscribe(data=> {
      Swal.fire("", "La fecha fue guardada correctamente","success")
    })
  }

  fillChart(){
    var ctx = document.getElementById("myChart");
    var myChart = new Chart(ctx, {
      type: 'bar',
      data: {
          labels:Object.keys(this.ingresos).map( d =>  this.datePipe.transform(new Date(d), "dd/MM/yyyy")),
          datasets: [{
            label: 'Ingresos mes',
            data: Object.values(this.ingresos) ,
            fill: false,
            borderColor: 'rgb(255,0,0)',
            backgroundColor : ' rgb(0,128,0, 0.6)',
            tension: 0.1
          },
          {
            label: 'Egresos mes',
            data: Object.values(this.egresos) ,
            fill: false,
            borderColor: 'rgb(0,128,0)',
            backgroundColor :' rgb(255,0,0, 0.6)',
            tension: 0.1
          }]
      },
      options: {
          scales: {
              y: {
                  beginAtZero: true
              }
          }
      }
  });
  }


    async finalizarMes() {

      var fecha = new Date();
      var formated = fecha.getMonth() +1 + "/" + fecha.getDate() + "/" + fecha.getFullYear() + ' '
                    + fecha.getHours() +':'+ fecha.getMinutes() + ':'+ fecha.getSeconds();



      (await this._cierresMes.finalizarMes(formated)).subscribe(res =>{
        Swal.fire("Se aplicaron las moras correctamente","","success")
      });
    }

    openModalFinalizarMes(content){

      this._cierresMes.getCierresMes().subscribe((data : any) => {
        var cierres = data;
        var lastCierre = cierres[0];

        //"¿Desea continuar de todas maneras?", "question"
        var date = new Date(lastCierre);

        if(date.getMonth() == new Date().getMonth()){
          Swal.fire("No se pudo completar la operación", "Este mes ya fué finalizado el día : " +date.getDate() +"/" + (date.getMonth() +1),"warning")


        }
        else{
          Swal.fire({title : "¿Estás seguro que deseas finalizar el mes?",


          showCancelButton: true,
          confirmButtonText: `SI`,
          cancelButtonText: `NO`, })
            .then((result) => {
              if(result.isConfirmed){
                this.finalizarMes()
              }
              else if (result.isDenied){
                return;
              }
            })
        }


      });

    }
}
