import { ProveedoresService } from './../../../services/proveedores.service';
import { CiudadesService } from 'src/app/services/ciudades.service';
import { IngresoEgresoFilter } from './../../../models/Reportes/ingresosEgresosFilter.model';
import { AvisoPagoModel } from 'src/app/models/cliente/avisoPago.model';
import { ReportesService } from './../../../services/reportes.service';
import { EgresosService } from 'src/app/services/egresos.service';
import { PagosClienteService } from 'src/app/services/pagosCliente.service';
import { Component, OnInit } from '@angular/core';
import { EgresoModel } from 'src/app/models/egreso/egreso.model';
import { PagoClienteModel } from 'src/app/models/cliente/pagoCliente.model';
import { NgbDateParserFormatter, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { combineLatest, concat, forkJoin, Observable, of } from 'rxjs';
import  *  as XLSX from 'xlsx';
import 'jspdf-autotable';
import { DatePipe } from '@angular/common';
import { CiudadModel } from 'src/app/models/ciudad/ciudad.model';
import { CategoriaModel } from 'src/app/models/categoria/categoria.model';
import { CategoriaService } from 'src/app/services/categoria.service';
import { toString } from '../../helper/util';
import { NgbDateCustomParserFormatter } from '../../helper/dateformat';


declare var jsPDF: any;

@Component({
  selector: 'app-ingresos-egresos',
  templateUrl: './ingresos-egresos.component.html',
  styleUrls: ['./ingresos-egresos.component.css'],
  providers: [
    {provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter}
   ]
})
export class IngresosEgresosComponent implements OnInit {

  filterEgreso: IngresoEgresoFilter;
  filterIngreso : IngresoEgresoFilter;

  fechaDesde : NgbDateStruct;
  fechaHasta : NgbDateStruct;


  pagos : PagoClienteModel[];
  egresos : EgresoModel[];
  ciudades : CiudadModel[];
  categorias : CategoriaModel[];

  arrIngresos : PagoClienteModel[];
  arrEgresos : EgresoModel[];

  totalIngreso: number;
  totalEgreso : number;
  total: number;

  loading : boolean = false;
  loadingEgreso : boolean = false;
  loadingIngreso : boolean = false;
  pageIngreso : number = 1;
  pageEgreso : number = 1;
  pageSizeEgreso =8;
  pageSizeIngreso =8;
  countEgreso : number;
  countIngreso : number;

  items = [];
  totalItems : number;

  constructor( private reportesService : ReportesService,
                private ciudadesService : CiudadesService,
                private categoriasService : CategoriaService,
                public datepipe: DatePipe) {
                  var fechaHoy = new Date();

                  this.fechaHasta = { day : fechaHoy.getDate(), month: fechaHoy.getMonth() +1, year: fechaHoy.getFullYear()}
                  this.fechaDesde = { day : fechaHoy.getDate() -7 , month: fechaHoy.getMonth() +1, year: fechaHoy.getFullYear()}

                  this.filterIngreso = new IngresoEgresoFilter();
                  this.filterEgreso = new IngresoEgresoFilter();
                  this.arrEgresos = [];
                  this.arrIngresos =[];
                  this.pagos = [];
                  this.egresos = [];
                  this.total = 0;
                  this.totalIngreso = 0;
                  this.totalEgreso = 0;
                }


  ngOnInit(): void {
    this.getEgresos();
    this.getIngresos();
    this.getCiudades();
    this.getCategorias();
  }

  getCategorias(){
    this.categoriasService.getCategorias().subscribe((res : any) => {
      this.categorias = res.Categorias;
    });
  }

  getCiudades(){
    this.ciudadesService.getCiudades().subscribe((res : any) => {
      this.ciudades = res.Ciudades;
    });
  }

  getIngresos(){
    this.filterIngreso.Offset = this.pageIngreso;
    this.filterIngreso.Limit = this.pageSizeIngreso;
    this.filterIngreso.FechaDesde = this.fechaDesde.month + "/" + this.fechaDesde.day + "/" + this.fechaDesde.year
    this.filterIngreso.FechaHasta = this.fechaHasta.month +"/"+ this.fechaHasta.day +"/"+ this.fechaHasta.year

    this.loadingIngreso = true;
    this.pagos = [];
    setTimeout(()=>{
      this.reportesService.getAllPagos( this.filterIngreso).subscribe( (res : any) => {
        this.pagos = res.Pagos;
        this.countIngreso = res.Count;
        this.totalIngreso = res.TotalIngreso;
        this.loadingIngreso =false;

        this.calcularTotal();
      });
    }, 1000)
  }

  getEgresos(){
    this.filterEgreso.Offset = this.pageEgreso;
    this.filterEgreso.Limit = this.pageSizeEgreso;
    this.filterEgreso.FechaDesde = this.fechaDesde.month + "/" + this.fechaDesde.day + "/" + this.fechaDesde.year
    this.filterEgreso.FechaHasta = this.fechaHasta.month +"/"+ this.fechaHasta.day +"/"+ this.fechaHasta.year

    this.loadingEgreso = true;
    this.egresos = [];
    setTimeout(()=>{
      this.reportesService.getAllEgresos( this.filterEgreso).subscribe( (res : any) => {
        this.egresos = res.Egresos;
        this.countEgreso = res.Count;
        this.totalEgreso = res.TotalEgreso;
        this.loadingEgreso = false;

        this.calcularTotal();
      });
     }, 1000)
  }

  calcularTotal(){
    this.total = this.totalIngreso - this.totalEgreso
  }

  exportToFile(type : number): void
  {
    const LIMIT = 15000;
    this.filterEgreso.Offset = this.pageEgreso;
    this.filterEgreso.Limit = LIMIT;
    this.filterEgreso.FechaDesde = this.fechaDesde.month + "/" + this.fechaDesde.day + "/" + this.fechaDesde.year
    this.filterEgreso.FechaHasta = this.fechaHasta.month +"/"+ this.fechaHasta.day +"/"+ this.fechaHasta.year

    this.filterIngreso.Offset = this.pageIngreso;
    this.filterIngreso.Limit = LIMIT;
    this.filterIngreso.FechaDesde = this.fechaDesde.month + "/" + this.fechaDesde.day + "/" + this.fechaDesde.year
    this.filterIngreso.FechaHasta = this.fechaHasta.month +"/"+ this.fechaHasta.day +"/"+ this.fechaHasta.year

      //Esperar ambas solicitudes
      forkJoin([this.reportesService.getAllPagos( this.filterIngreso),
                this.reportesService.getAllEgresos( this.filterEgreso)])
         .subscribe(([dataIngreso, dataEgreso]: any) => {
          this.arrEgresos = dataEgreso.Egresos;
          this.arrIngresos = dataIngreso.Pagos;

          if(type == 1){
               this.convertToExcel();
             }
             else
           {  this.convertToPdf();}
         });
  }

  convertToExcel(){
    var totalEgresos =0;
    var totalIngresos =0

    this.arrEgresos.forEach(obj => {
      totalEgresos += obj.Importe;
    });
    this.arrIngresos.forEach(obj => {
      totalIngresos += obj.MontoTotal;
    });
    const arrEgresos = this.arrEgresos.map(
      x => ({
        Id: x.Id,
        Proveedor : x.Proveedor.Nombre,
        Fecha: this.datepipe.transform(x.FechaEgreso, 'dd-MM-yyyy'),
        Categoria: x.Categoria.Descripcion,
        Importe : x.Importe,
        Descripcion : x.Descripcion,
        EsCheque : x.EsCheque,
        FechaCheque : this.datepipe.transform(x.FechaCheque, 'dd-MM-yyyy')
      })
    )
    const arrIngresos = this.arrIngresos.map(
      x => ({
        Id : x.Id,
        Cliente : x.Cliente.Nombre +" "+ x.Cliente.Apellido,
        Ciudad: x.Cliente.Ciudad.Descripcion,
        Fecha: this.datepipe.transform(x.Fecha, 'dd-MM-yyyy'),
        Importe : x.MontoTotal,
        Detalle : x.Detalle
      })
    )

     const wb: XLSX.WorkBook = XLSX.utils.book_new();
     var ws1 = XLSX.utils.json_to_sheet([]);

    XLSX.utils.sheet_add_json(ws1,
      arrEgresos
    , {skipHeader: false, origin: "I3"});
     XLSX.utils.sheet_add_json(ws1,
      arrIngresos
    , {skipHeader: false, origin: "A3"});

    XLSX.utils.sheet_add_aoa(ws1, [['Egresos']], {origin: 'I1'});
    XLSX.utils.sheet_add_aoa(ws1, [['Ingresos']], {origin: 'A1'});
    XLSX.utils.sheet_add_aoa(ws1, [['Totales']], {origin: 'S1'});

    XLSX.utils.sheet_add_aoa(ws1, [['Egresos']], {origin: 'S5'});
    XLSX.utils.sheet_add_aoa(ws1, [['Ingresos']], {origin: 'S4'});
    XLSX.utils.sheet_add_aoa(ws1, [['Total']], {origin: 'S6'});
    XLSX.utils.sheet_add_aoa(ws1, [["$ " + totalEgresos.toFixed(0)]], {origin: 'T5'});
    XLSX.utils.sheet_add_aoa(ws1, [["$ " +totalIngresos.toFixed(0)]], {origin: 'T4'});
    XLSX.utils.sheet_add_aoa(ws1, [["$ " +(totalIngresos - totalEgresos).toFixed(0)]], {origin: 'T6'});

    XLSX.utils.book_append_sheet(wb, ws1, 'Reporte lecturas');
    XLSX.writeFile(wb, "Reporte-ingresos-egresos.xlsx");
  }

  convertToPdf(){
    var doc = new jsPDF();
    var colEgresos = ["IdEgreso","Proveedor","Fecha","Categoria", "Importe","Descripcion", "Es Cheque", "Fecha venc. cheque"];
    var rowsEgresos = [];

    var colIngresos = ["IdPago", "Cliente", "Ciudad","Fecha","Importe", "Detalle"];
    var rowsIngresos = [];

    var totalEgresos =0;
    var totalIngresos =0

    this.arrEgresos.forEach(obj => {
      totalEgresos += obj.Importe;
    });
    this.arrIngresos.forEach(obj => {
      totalIngresos += obj.MontoTotal;
    });
    const arrEgresos = this.arrEgresos.map(
      x => ({
        Id: x.Id,
        Proveedor : x.Proveedor.Nombre,
        Fecha: this.datepipe.transform(x.FechaEgreso, 'dd-MM-yyyy'),
        Categoria: x.Categoria.Descripcion,
        Monto : x.Importe,
        EsCheque : x.EsCheque,
        Descripcion : x.Descripcion,
        FechaCheque : this.datepipe.transform(x.FechaCheque, 'dd-MM-yyyy'),
      })
    )
    const arrIngresos = this.arrIngresos.map(
      x => ({
        Id : x.Id,
        Cliente : x.Cliente.Nombre,
        Ciudad: x.Cliente.Ciudad.Descripcion,
        Fecha: this.datepipe.transform(x.Fecha, 'dd-MM-yyyy'),
        Monto : x.MontoTotal,
        Detalle : x.Detalle
      })
    )

    arrEgresos.forEach(element => {

          var temp = [element.Id, element.Proveedor, element.Fecha, element.Categoria, element.Monto.toFixed(0), element.Descripcion, element.EsCheque, element.FechaCheque];
          rowsEgresos.push(temp);
    });
    arrIngresos.forEach(element => {
        var temp = [element.Id, element.Cliente, element.Ciudad, element.Fecha,element.Monto.toFixed(0), element.Detalle];

        rowsIngresos.push(temp);
    });
    


    //Ingresos
    doc.setFontSize(18);
    doc.text(15, 20, "Ingresos");

    doc.setFontSize(12);
    doc.text(`Desde: ${this.filterIngreso.FechaDesde} Hasta: ${this.filterIngreso.FechaHasta} `, 130,20);

    //doc.text(`Total = $ ${totalIngresos- totalEgresos}`, 150,70);

    // doc.autoTable(colIngresos, rowsIngresos, {startY: 30, startX : 10, 
    //                                                         styles: {/*overflow: 'linebreak' ,*/columnWidth: [700,100,100,100,100,1] } });

    doc.autoTable(colIngresos,rowsIngresos, {startY: 30, startX : 10, columnStyles: { 5 : { cellWidth: 50} },})
    doc.setFontSize(15);
    doc.text(`Total Ingresos = $ ${totalIngresos.toFixed(0)}`, 15,doc.lastAutoTable.finalY + 10);
    //Egresos
    doc.addPage();
    doc.setFontSize(18);
    doc.text(15, 20, "Egresos");

    doc.setFontSize(12);

    doc.text(`Desde: ${this.filterEgreso.FechaDesde} Hasta: ${this.filterEgreso.FechaHasta} `, 130,20);
    //{startY: doc.lastAutoTable.finalY , startX:100}
    doc.autoTable(colEgresos, rowsEgresos, { startY: 30, startX : 10, columnStyles: { 5 : { cellWidth: 30} },});

    doc.setFontSize(15);
    doc.text(`Total Egresos = $ ${totalEgresos.toFixed(0)}`, 15,doc.lastAutoTable.finalY + 10);
  

    //TOtal
    doc.setFontSize(20);
    doc.text(`Total= $ ${totalIngresos - totalEgresos}`, 70,doc.lastAutoTable.finalY + 30);
    //SAVE
    doc.save('Reporte-ingresos-egresos.pdf');

  }


  filter($event){
    this.ngOnInit();
  }

  filterGetIngreso($event){
    this.getIngresos();
  }

  filterGetEgreso($event){
    this.getEgresos();
  }

}
