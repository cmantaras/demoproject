import { Component, OnInit } from '@angular/core';
import { CiudadModel } from 'src/app/models/ciudad/ciudad.model';
import { ClienteModel, EstadoClienteModel } from 'src/app/models/cliente/cliente.model';
import { ClienteReporteFilterModel } from 'src/app/models/Reportes/clienteReporteFilter.model';
import { ZonaModel } from 'src/app/models/zona/zona.model';
import { CiudadesService } from 'src/app/services/ciudades.service';
import { ReportesService } from 'src/app/services/reportes.service';
import { ZonasService } from 'src/app/services/zona.service';
import { NgbModal, NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap'
import { ProductoPrecioModel} from 'src/app/models/producto/productoPrecio.model';
import { ClientesService } from 'src/app/services/clientes.service';
import { DeudorModel } from 'src/app/models/Reportes/deudor.model';
import {DeudoresFilter} from 'src/app/models/Reportes/deudoresFilter.model';
import  *  as XLSX from 'xlsx';
import 'jspdf-autotable';
import { element } from 'protractor';
import { ProductoModel } from '../../../models/producto/producto.model';
import { toString } from '../../helper/util';
import { DatePipe } from '@angular/common';
import { style } from '@angular/animations';
declare var jsPDF: any;


@Component({
  selector: 'app-clientes',
  templateUrl: './reporte-clientes.component.html',
  styleUrls: ['./reporte-clientes.component.css']
})
export class ReporteClientesComponent implements OnInit {

  clienteId : Number;
  clientes : ClienteModel[];
  clientesParaReporte : ClienteModel[];
  ciudades : CiudadModel[];
  zonas : ZonaModel[];
  estados : Array<any> = [];
  productosDeCliente : number;
  totalDeuda : number;

  clienteInfo : ClienteModel;
  productosClienteInfo : ProductoPrecioModel[];
  laodInfo : boolean;
  deudor : DeudorModel;
  deudores : DeudorModel[];
  arrDeudores : DeudorModel[];
  deudoresFilter : DeudoresFilter;

  count : number;
  pageSize : number;
  page : number;

  cargandoInfo: boolean =false;

  clientesFilter : ClienteReporteFilterModel;
  loading : boolean;
  clientesFilterNoActivos: ClienteReporteFilterModel;
  clientesParaReporteNoActivos: ClienteModel[];
  deudoresParaReporte: DeudorModel[] =[];
  IdsClientesDeudoresFiltrados: any[]= [];




  constructor( private _ciudadesService : CiudadesService,
                private _zonasService : ZonasService,
                public _modalService: NgbModal,
                private _clientesService : ClientesService,
                private datePipe : DatePipe,
                private _reporteServices : ReportesService) {
    this.loading = false;
    this.page = 1;
    this.count = 0;
    this.pageSize = 10;
    this.clientes = [];
    this.clientesFilter = new ClienteReporteFilterModel();
    this.clientesFilter.Estado= 2;
    this.clienteInfo = new ClienteModel();
    this.productosClienteInfo = [];
    this.deudores = [];
    this.deudoresFilter = new DeudoresFilter();
    this.deudor = new DeudorModel();
    this.clienteId =0;
    this.productosDeCliente =0;
    this.laodInfo = false;
    this.totalDeuda = 0;

    this.estados = [{
      "Descripcion" : "Inactivos",
      "Valor" : 0
    },{
      "Descripcion" : "Activos",
      "Valor" : 1
    },{
      "Descripcion" : "Todos",
      "Valor" : 2
    }
    ]

   }

  ngOnInit(): void {

    this.getCiudades();
    this.getClientes();
    this.getZonas();
  }


  getClientes(){
    this.clientes = [];
    this.clientesFilter.Limit = this.pageSize;
    this.clientesFilter.Offset = this.page;
    this.loading = true;

    setTimeout(() => {


      this._reporteServices.getClientes(this.clientesFilter).subscribe((data : any) => {

        this.count = data.Count
        this.clientes = data.Clientes;
        this.clientesParaReporte = data.Clientes;

        this.loading= false;

      });

    },800)
  }

  getCiudades(){
    this._ciudadesService.getCiudades().subscribe((data : any) => {

      this.ciudades = data.Ciudades
    });
  }


  getZonas(){
    this._zonasService.getZonas().subscribe((data : any) => {

      this.zonas = data.Zonas
    });
  }



  openModalInfo(content, cliente: ClienteModel){

    this.laodInfo = true;
    this.deudores = [];
    this.productosClienteInfo = [];
    this.totalDeuda =0;
    this.clienteInfo = cliente;

    if(this.clienteInfo.EstadoActualId ==8){
      this._clientesService.getProductosByClienteId (this.clienteInfo.Id).subscribe((res : any) => {
        if(res.Productos && res.Productos.length > 0){
          this.productosClienteInfo = res.Productos;
        }

      this.laodInfo = false;

      });


    }else{
      this.deudoresFilter.Offset = this.page;
      this.deudoresFilter.Limit = this.pageSize;
      this.deudoresFilter.idCliente = cliente.Id;


      this._reporteServices.getDeudores( this.deudoresFilter).subscribe((data : any) => {
      this.count = data.Count;
      this.deudores = data.Deudores;


      this.laodInfo = false;

      if(this.deudores.length ==0 || this.deudores[0].TotalDeuda == null || this.deudores[0].TotalDeuda == undefined){
        this.totalDeuda = 0;
      }else{
        this.totalDeuda = this.deudores[0].TotalDeuda;
      }



      });
    }
      const modalRef = this._modalService.open(content, { backdrop: 'static'});
    }

  exportToFile(type : number): void
  {

    this.cargandoInfo =true;
    


    const LIMIT = 15000;
    this.clientesFilter.Offset = this.page;
    this.clientesFilter.Limit = LIMIT;

    this.deudoresParaReporte=[];
    this.clientesParaReporte=[];
   
    //Todos los clientes segun filtro
    this._reporteServices.getClientes(this.clientesFilter).subscribe((data : any) => {
      this.clientesParaReporte = data.Clientes;
      var aux= this.clientesParaReporte.filter(x => x.EstadoActualId == 6).slice();

    //Ids de clientes filtrados que son deudores
      aux.forEach(client => {
        var tempId = client.Id;
        this.IdsClientesDeudoresFiltrados.push(tempId);   
      });

      
            //clientes menos deudores
            this.clientesParaReporte = this.clientesParaReporte.filter(obj => obj.EstadoActualId != 6).slice();

      //console.table(this.clientesParaReporte);

       //obtengo todos los deudores y muestro solo los q tienen id iguales a los clientes filtrados deudores
       this._reporteServices.getDeudores(this.deudoresFilter).subscribe((data : any) => {
        this.deudoresParaReporte = data.Deudores; 

        if(aux.length ==0){
          this.deudoresParaReporte =[]
        }
        else{
          this.deudoresParaReporte= this.deudoresParaReporte.filter(obj => this.IdsClientesDeudoresFiltrados.includes(obj.Cliente.Id)).slice();
        }

        if(type == 1){
          this.exportToExcel();
        }
        else
        { this.exportToPdf();
        }
      });


    });

   
  }

  exportToExcel(){
    var arrClientes = this.clientesParaReporte.map(
      x => ({
        Nombre : x.Nombre,
        Apellido : x.Apellido,
        Telefono : x.Telefono,
        Direccion : x.Direccion,
        Ciudad : x.Ciudad.Descripcion,
        Estado : x.Estado.Descripcion,
        Servicios : x.Servicios}))

      const arrDeudores = this.deudoresParaReporte.map(
        x => ({
          Nombre : x.Cliente.Nombre,
          Apellido: x.Cliente.Apellido,
          Telefono: x.Cliente.Telefono,
          Direccion : x.Cliente.Direccion,
          Ciudad : x.Cliente.Ciudad.Descripcion,
          Deuda : x.TotalDeuda.toFixed(0),
          Fecha: (this.datePipe.transform(x.FechaUltimoPago, 'dd-MM-yyyy')!= "01-01-0001")? this.datePipe.transform(x.FechaUltimoPago, 'dd-MM-yyyy') : "N/A",
          Dias : (x.DiasDesdePago != 0)? x.DiasDesdePago : "N/A"}))
          
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    var ws1 = XLSX.utils.json_to_sheet([0]);
    var ws2 = XLSX.utils.json_to_sheet([1]);

    XLSX.utils.sheet_add_json(ws1, arrClientes, {skipHeader: false, origin: "A3"});

    XLSX.utils.sheet_add_aoa(ws1, [['Clientes']], {origin: 'A1'});
    XLSX.utils.sheet_add_aoa(ws1, [[ "Nombre"]], {origin: 'A3'});
    XLSX.utils.sheet_add_aoa(ws1, [[ "Apellido"]], {origin: 'B3'});
    XLSX.utils.sheet_add_aoa(ws1, [[ "Telefono"]], {origin: 'C3'});
    XLSX.utils.sheet_add_aoa(ws1, [[ "Direccion"]], {origin: 'D3'});
    XLSX.utils.sheet_add_aoa(ws1, [[ "Ciudad"]], {origin: 'E3'});
    XLSX.utils.sheet_add_aoa(ws1, [[ "Estado"]], {origin: 'F3'});
    XLSX.utils.sheet_add_aoa(ws1, [[ "Servicios Activos"]], {origin: 'G3'});

    // var cell_ref = XLSX.utils.encode_cell({c:6, r:4 });
    


    XLSX.utils.sheet_add_json(ws2, arrDeudores, {skipHeader: false, origin: "A3"});

    XLSX.utils.sheet_add_aoa(ws2, [['Deudores']], {origin: 'A1'});
    XLSX.utils.sheet_add_aoa(ws2, [[ "Nombre"]], {origin: 'A3'});
    XLSX.utils.sheet_add_aoa(ws2, [[ "Apellido"]], {origin: 'B3'});
    XLSX.utils.sheet_add_aoa(ws2, [[ "Telefono"]], {origin: 'C3'});
    XLSX.utils.sheet_add_aoa(ws2, [[ "Direccion"]], {origin: 'D3'});
    XLSX.utils.sheet_add_aoa(ws2, [[ "Ciudad"]], {origin: 'E3'});
    XLSX.utils.sheet_add_aoa(ws2, [[ "Deuda"]], {origin: 'F3'});
    XLSX.utils.sheet_add_aoa(ws2, [[ "Fecha últ. pago"]], {origin: 'G3'});
    //XLSX.utils.sheet_add_aoa(ws2, [[ "Dias desde últ. pago"]], {origin: 'H3'});

    XLSX.utils.book_append_sheet(wb, ws1, 'Activos_Inactivos');
    XLSX.utils.book_append_sheet(wb, ws2, 'Deudores');
    var now = new Date();
    var fechaHoy = this.datePipe.transform(now.toDateString(), 'dd-MM-yyyy');
    XLSX.writeFile(wb, "Reporte-Clientes_"+fechaHoy+".xlsx");
    
    this.cargandoInfo =false;
  }

  exportToPdf(){
    console.table(this.deudoresParaReporte);
    var doc = new jsPDF();
    var colClientes = ["Nombre", "Apellido","Telefono", "Direccion", "Ciudad", "Estado", "Servicios Activos" ];
    var rowsClientes = [];



    const arrClientes = this.clientesParaReporte.map(
      x => ({
        Id : x.Id,
        Nombre : x.Nombre,
        Apellido: x.Apellido,
        Telefono: x.Telefono,
        Direccion : x.Direccion,
        Ciudad : x.Ciudad.Descripcion,
        Estado : x.Estado.Descripcion,
        Servicios : x.Servicios
      })
    )
    
     arrClientes.forEach(clienteReporte => {

          var temp = [clienteReporte.Nombre, clienteReporte.Apellido, clienteReporte.Telefono, clienteReporte.Direccion, clienteReporte.Ciudad, clienteReporte.Estado, clienteReporte.Servicios];

          rowsClientes.push(temp);
          
    });


    var colClientesDeudores = ["Nombre", "Apellido","Telefono", "Direccion", "Ciudad", "Estado", "Total deuda", "Fecha últ. pago", "Dias desde últ. pago" ];
    var rowsClientesDeudores = [];


    const arrDeudores = this.deudoresParaReporte.map(
      x => ({
        Nombre : x.Cliente.Nombre,
        Apellido: x.Cliente.Apellido,
        Telefono: x.Cliente.Telefono,
        Direccion : x.Cliente.Direccion,
        Ciudad : x.Cliente.Ciudad.Descripcion,
        Deuda : x.TotalDeuda.toFixed(0),
        Fecha: (this.datePipe.transform(x.FechaUltimoPago, 'dd-MM-yyyy')!= "01-01-0001")? this.datePipe.transform(x.FechaUltimoPago, 'dd-MM-yyyy') : "N/A",
        Dias : (x.DiasDesdePago != 0)? x.DiasDesdePago : "N/A"
        
      })
      //Estado : x.Cliente.Estado.Descripcion,
    )
    
     arrDeudores.forEach(element => {
       
        var temp0 = [element.Nombre, element.Apellido, element.Telefono, element.Direccion, element.Ciudad, "Deudor", element.Deuda, element.Fecha, element.Dias];
        
        rowsClientesDeudores.push(temp0);
          
    });

    if(this.clientesParaReporte.length !=0){
      doc.setFontSize(22);
      doc.text(15, 20, "Clientes");//TItulo general
      doc.autoTable(colClientes, rowsClientes, { startY: 30, startX : 10  });//Tabla clientes
    }
    
    if(this.deudoresParaReporte.length !=0 && this.clientesParaReporte.length !=0){
      doc.setFontSize(20);//subTitulo
      doc.text(15, doc.lastAutoTable.finalY + 30, "Deudores");//Tabla deudores
      doc.autoTable(colClientesDeudores, rowsClientesDeudores, {startY: doc.lastAutoTable.finalY + 40, startX : 10  });
    }
    if(this.deudoresParaReporte.length !=0 && this.clientesParaReporte.length ==0){
      doc.setFontSize(20);//subTitulo
      doc.text(15, 20, "Deudores");//Tabla deudores
      doc.autoTable(colClientesDeudores, rowsClientesDeudores, { startY: 30, startX : 10  });
    }
    if(this.deudoresParaReporte.length ==0 && this.clientesParaReporte.length ==0){
      doc.setFontSize(20);//subTitulo
      doc.text(15, 20, "No hay clientes correspondientes al filtro aplicado");//Tabla deudores
    }
    var now = new Date();
    var fechaHoy = this.datePipe.transform(now.toDateString(), 'dd-MM-yyyy');
    doc.save('Reporte_Clientes_'+fechaHoy+'.pdf');

    this.cargandoInfo =false;
  }






}
