import { DeudorModel } from './../../../models/Reportes/deudor.model';
import { ReportesService } from './../../../services/reportes.service';
import { Component, OnInit } from '@angular/core';
import { DeudoresFilter } from 'src/app/models/Reportes/deudoresFilter.model';
import  *  as XLSX from 'xlsx';
import 'jspdf-autotable';
import { DatePipe } from '@angular/common';
declare var jsPDF: any;
@Component({
  selector: 'app-deudores',
  templateUrl: './deudores.component.html',
  styleUrls: ['./deudores.component.css']
})
export class DeudoresComponent implements OnInit {

  deudores : DeudorModel[];
  arrDeudores : DeudorModel[];
  deudoresFilter : DeudoresFilter;
  loading : boolean;

  count : number;
  page : number;
  pageSize : number;
  constructor(private _reportesService : ReportesService,
              private datePipe : DatePipe) {
    this.loading = false;
    this.page = 1;
    this.pageSize = 7;

    this.deudores = [];
    this.deudoresFilter = new DeudoresFilter();
   }

  ngOnInit(): void {
    this.getDeudores();
  }

  getDeudores(){
    this.deudoresFilter.Offset = this.page;
    this.deudoresFilter.Limit = this.pageSize;

    this._reportesService.getDeudores( this.deudoresFilter).subscribe((data : any) => {
      this.count = data.Count;
      this.deudores = data.Deudores;

    })

  }

  filterGetDeudores($event){
      this.getDeudores();
  }

  exportToFile(type : number): void
  {
    const LIMIT = 15000;
    this.deudoresFilter.Offset = this.page;
    this.deudoresFilter.Limit = LIMIT;

        this._reportesService.getDeudores(this.deudoresFilter).subscribe((data : any) => {
          this.arrDeudores = data.Deudores;
          if(type == 1) this.convertToExcel();
          else this.convertToPdf();
         });
  }

  convertToExcel(){

    const arrDeudores = this.arrDeudores.map(
      x => ({
        Nombre : x.Cliente.Nombre +" "+ x.Cliente.Apellido,
        Cédula : x.Cliente.Identificador,
        Deuda: x.TotalDeuda,
        Fecha: (this.datePipe.transform(x.FechaUltimoPago, 'dd-MM-yyyy')!= "01-01-0001")? this.datePipe.transform(x.FechaUltimoPago, 'dd-MM-yyyy') : "N/A",
        CantDias : (x.DiasDesdePago != 0)? x.DiasDesdePago : "No se encontraron pagos"
      })
    )

    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    var ws1 = XLSX.utils.json_to_sheet([]);

    XLSX.utils.sheet_add_json(ws1, arrDeudores, {skipHeader: false, origin: "A3"});

    XLSX.utils.sheet_add_aoa(ws1, [['Deudores']], {origin: 'A1'});
    XLSX.utils.sheet_add_aoa(ws1, [[ "Nombre"]], {origin: 'A3'});
    XLSX.utils.sheet_add_aoa(ws1, [[ "Cédula"]], {origin: 'B3'});
    XLSX.utils.sheet_add_aoa(ws1, [[ "Deuda"]], {origin: 'C3'});
    XLSX.utils.sheet_add_aoa(ws1, [[ "Fecha último pago"]], {origin: 'D3'});
    XLSX.utils.sheet_add_aoa(ws1, [[ "Cant. dias sin pagar"]], {origin: 'E3'});

    XLSX.utils.book_append_sheet(wb, ws1, 'Reporte deudores');
    XLSX.writeFile(wb, "Reporte-deudores.xlsx");
  }

  convertToPdf(){
    var doc = new jsPDF();
    var colDeudores = ["Nombre", "Cédula", "Deuda","Fecha último pago", "Cant. días sin pagar" ];
    var rowsDeudores = [];


    const arrDeudores = this.arrDeudores.map(
      x => ({
        Nombre : x.Cliente.Nombre +" "+ x.Cliente.Apellido   ,
        Cédula : x.Cliente.Identificador ,
        Deuda: x.TotalDeuda,
        Fecha: (this.datePipe.transform(x.FechaUltimoPago, 'dd-MM-yyyy')!= "01-01-0001")? this.datePipe.transform(x.FechaUltimoPago, 'dd-MM-yyyy') : "N/A",
        CantDias : (x.DiasDesdePago != 0)? x.DiasDesdePago : "No se encontraron pagos"
      })
    )
    arrDeudores.forEach(element => {
          var temp = [element.Nombre, element.Cédula, element.Deuda, element.Fecha, element.CantDias];
          rowsDeudores.push(temp);
    });

    doc.text(15, 20, "Deudores");
    doc.text(70, 20,  this.datePipe.transform(new Date(), "dd/MM/yyyy"));
    doc.autoTable(colDeudores, rowsDeudores, { startY: 30, startX : 10  });

    doc.save('Reporte-.pdf');

  }
}
