import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ConsoleService } from '@ng-select/ng-select/lib/console.service';
import { AuthGuard } from 'src/app/auth/auth-guard.service';
import { AuthService } from 'src/app/auth/auth.service';
import { GlobalEventsManager } from '../../helper/global-events-manager';
import { LoginComponent } from '../../login/login.component';
import{ CheckPermissionsDirective  } from '../../../directives/check-permissions.directive'
@Component({
  selector: 'app-nvbar',
  templateUrl: './nvbar.component.html',
  styleUrls: ['./nvbar.component.css']
})
export class NvbarComponent implements OnInit {
  @Output() sidenavClose = new EventEmitter();
  public logged :boolean ;
  showNavBar: boolean = false;

  user : string;
  userName : string;
  userLastName : string;

  constructor(private auth : AuthGuard,
              private globalEventsManager: GlobalEventsManager) {

        var permisos = JSON.parse(localStorage.getItem("dataPermissions"));
        if(permisos){
            localStorage.setItem("permisos",  JSON.stringify(permisos.Permisos));
        }

        var userData = JSON.parse(localStorage.getItem("user"));
        if(userData){
          this.user = String(userData.UserName);
          this.userName = String(userData["Nombre"]);
          this.userLastName = String(userData["Apellido"]);
        }

        this.globalEventsManager.showNavBarEmitter.subscribe((mode)=>{
           this.showNavBar = mode;
        });

  }


  ngOnInit() {
    //this.logged = this.auth.canActivate();
  }

  public onSidenavClose = () => {
    this.sidenavClose.emit();
  }

  name = 'Angular';
  list_value1 = [];
   public menuArray: Array<any> = [];

   opened=-1;

  public list_value: Array<any> = [
    {
      "1": {
        "nombre" : "Cliente",
        "icono" : "fas fa-user",
        "router" : "cliente",
        "permisos" : ['VER_CLIENTES']
      },
      "2": {
        "nombre" : "Proveedor",
        "icono" : "fas fa-business-time",
        "router" : "proveedor",
        "permisos" : ['VER_PROVEEDORES']
      },

      "3": {
        "nombre" : "Egreso",
        "icono" : "fas fa-business-time",
        "router" : "egreso",
        "permisos" : ['VER_EGRESOS']
      },

       "5": {
         "nombre" : "Usuarios",
         "icono" : "fa fa-user",
         "router" : "usuario",
         "permisos" : ['VER_USUARIOS']
        },
        "6": {
          "nombre" : "Roles",
          "icono" : "fa fa-book",
          "router" : "roles",
          "permisos" : ['VER_ROLES']
         },
         "7": {
          "nombre" : "Reportes",
          "icono" : "fa fa-book",
          
          "permisos" : ['VER_CLIENTES'],
          "Submenu" : {

                "Ingresos/Egresos": {
                  "icono" : "",
                  "router" : "ingresosegresos",
                  "permisos" : ['VER_REPORTES_INGRESOS-EGRESOS']
                },
                "Deudores": {
                  "icono" : "",
                  "router" : "deudores",
                  "permisos" : ['VER_REPORTES_DEUDORES']
                },
                "Clientes": {
                  "icono" : "",
                  "router" : "reporteclientes",
                  "permisos" : ['VER_REPORTES_CLIENTES']
                }
              },
           },

      "4": {
        "nombre" : "Configuración",
        "icono" : "fas fa-wrench",

        "permisos" : ['VER_CLIENTES'],

          "Submenu" : {

          "Marca": {
            "icono" : "",
            "router" : "marca",
            "permisos" : ['VER_MARCAS'],
            
          },
          "Categoria": {
            "router" : "categoria",
            "permisos" : ['VER_CATEGORIAS'],
            
          },
          "Items": {
            "router" : "item",
            "permisos" : ['VER_ITEMS'],
            
          },
          "Ciudad": {
            "router" : "ciudad",
            "permisos" : ['VER_CIUDADES'],
            
          },
          "Impuesto": {
            "router" : "impuesto",
            "permisos" : ['VER_IMPUESTOS'],
            
          },
          "Moneda": {
            "router" : "moneda",
            "permisos" : ['MONEDAS'],
            
          },

          "Zonas": {
            "router" : "zona",
            "permisos" : ['VER_ZONAS'],
            
          },
          "Métodos de pago": {
            "router" : "metodopago",
            "permisos" : ['VER_METODOS_PAGO'],
            
          },
          "Productos": {
            "router" : "producto",
            "permisos" : ['VER_PRODUCTOS'],
                    
          },
        }

    },


    }

  ]

  sortAray(){
    this.menuArray = this.list_value.sort();

  }


}
