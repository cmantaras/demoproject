import { Directive, Input, ElementRef,HostListener,HostBinding ,Renderer2,Output,EventEmitter} from '@angular/core';

/*
 *
 */
@Directive({
  selector: '[checkToggle]'
})
export class SidebarLeftToggleDirective {
  @Input('checkToggle') partner;

  /**
   * @method constructor
   * @param elementRef [description]
   */
  constructor(
    public elementRef: ElementRef,
    public  renderer: Renderer2,
  ) {}


@HostBinding("class.active") isOpen = false;

@HostListener("click") toggleOpen($event){

  this.isOpen = !this.isOpen;
}

}
