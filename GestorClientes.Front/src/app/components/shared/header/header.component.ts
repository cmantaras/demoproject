import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthGuard } from 'src/app/auth/auth-guard.service';
import { AuthService } from 'src/app/auth/auth.service';
import { GlobalEventsManager } from '../../helper/global-events-manager';
import { LoginComponent } from '../../login/login.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  public logged : boolean;
  showNavBar: boolean = false;
  dataUser : any;


  user : string ="";
  userName : string = "";
  userLastName : string ="";
  usuarioRegistrado: boolean;


  constructor(private router : Router,
              private globalEventsManager : GlobalEventsManager,
              private authService : AuthService) {
                this.usuarioRegistrado = false;

                this.globalEventsManager.headerEmitter.subscribe((mode)=>{

                  this.dataUser = mode;
                  this.loggedIn(this.dataUser);
              });


              var permisos = JSON.parse(localStorage.getItem("dataPermissions"));

              if(permisos){
                  localStorage.setItem("permisos",  JSON.stringify(permisos.Permisos));
              }

        
              this.globalEventsManager.showNavBarEmitter.subscribe((mode)=>{
                  this.showNavBar = mode;

              });

              this.showNavBar = authService.isUserLoggedIn()
              globalEventsManager.showNavBar(this.showNavBar);
  }






  logOut() {
    localStorage.removeItem("jwt");
    this.globalEventsManager.showNavBar(false);
    this.router.navigate(["/login"]);
  }

  ngOnInit(): void {
    var data = JSON.parse(localStorage.getItem("user"));
    this.loggedIn(data);
  }

  loggedIn(userData : any){

    this.user = String(userData["Username"]);
    this.userName = String(userData["Nombre"]);
    this.userLastName = String(userData["Apellido"]);
    this.usuarioRegistrado = this.authService.isUserLoggedIn();
    

  }

}
