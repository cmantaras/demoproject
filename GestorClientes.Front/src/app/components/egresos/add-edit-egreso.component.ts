import {
  Component,
  EventEmitter,
  Injectable,
  OnInit,
  Output,
} from '@angular/core';
import { NgbActiveModal, NgbDateParserFormatter, NgbDateStruct, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgForm } from '@angular/forms';
import { EgresosService } from 'src/app/services/egresos.service';
import { EgresoModel } from 'src/app/models/egreso/egreso.model';
import Swal from 'sweetalert2';
import { Observable } from 'rxjs';
import { MonedasService } from 'src/app/services/monedas.service';
import { MonedaModel } from 'src/app/models/moneda/moneda.model';
import { CategoriaModel } from 'src/app/models/categoria/categoria.model';
import { ProveedoresService } from 'src/app/services/proveedores.service';
import { CategoriaService } from 'src/app/services/categoria.service';
import { ProveedorModel } from 'src/app/models/proveedor/proveedor.model';
import { ProveedorRequestModel } from '../../models/proveedor/proveedorRequest.model';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { NgbDateCustomParserFormatter } from '../helper/dateformat';

@Component({
  selector: 'app-add-edit-egreso',
   providers: [
    {provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter}
   ],

  template: `
    <div class="modal-header">
      <h4 class="modal-title"> {{ modalHeader }} </h4>
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>

    <div class="modal-body">
        <!-- BASIC FORM ELELEMNTS -->
              <form autocomplete="off" (ngSubmit)="guardar( form )" #form="ngForm" class="form-horizontal style-form" method="get">
                <div class="form-group">

                  <label class="col-sm-2 col-sm-2 control-label">Importe</label>
                  <div class="col-sm-10">
                  <input type="number"
                            class="form-control is-invalid"
                            name="importe"
                            [ngClass]="{ 'is-invalid': form.submitted && importe.invalid}"
                            [(ngModel)] = "egreso.Importe"
                            required
                            placeholder="Importe"
                            minlength="1"
                            #importe="ngModel"

                           >
                           <small   *ngIf="importe.invalid && importe.touched"
                                   class="form-text text-danger">Ingrese un importe válido</small>
                  </div>

                  <label class="col-sm-12 control-label">Moneda</label>
                 <div class="col-sm-9">
                   <!--Using items input-->
                    <ng-select [items]="monedas"
                        [disabled]="true"
                        [ngClass]="{ 'is-invalid': form.submitted && moneda.invalid }"
                        name="monedas"
                        required
                        bindLabel="Descripcion"
                        bindValue="Id"
                        [(ngModel)]="egreso.MonedaId"
                        #moneda="ngModel">
                    </ng-select>
                    <!--<small    *ngIf="form.submitted && moneda.invalid && !moneda"
                                        class="form-text text-danger">  Ingrese una moneda</small>-->

                  </div>

                  <label class="col-sm-12 control-label">Proveedor</label>
                 <div class=" col-sm-10 flex-sw ">
                   
                    <ng-select [items]="proveedores"
                        
                        name="proveedor"
                        required
                        bindLabel="Nombre"
                        bindValue="Id"
                        [(ngModel)]="egreso.ProveedorId"
                        #proveedor="ngModel">
                    </ng-select>
                    <small   *ngIf="!egreso.ProveedorId && proveedor.touched"
                                   class="form-text text-danger">Seleccione un proveedor</small>
                  </div>
                  <label class="col-sm-12 control-label">Categoría</label>
                 <div class="col-sm-10 flex-sw ">
                   <!--Using items input-->
                    <ng-select [items]="categorias"
                        
                        name="categoria"
                        required
                        bindLabel="Descripcion"
                        bindValue="Id"
                        [(ngModel)]="egreso.CategoriaId"
                        #categoria="ngModel">
                    </ng-select>
                    <small   *ngIf="!egreso.CategoriaId && categoria.touched"
                                   class="form-text text-danger">Seleccione una categoría</small>

                  </div>

                  <label class=" col-sm-12 control-label">Fecha</label>
                  <div class="input-group  col-sm-9">
                      <input class="form-control"
                            placeholder="dd-mm-yyyy"
                            name="dp"
                            required
                            [(ngModel)]="fechaEgreso"
                            ngbDatepicker
                            readonly 
                            #d1="ngbDatepicker"
                          >

                        <div class="input-group-append">
                          <button class="btn btn-outline-secondary btn-fecha calendar" (click)="d1.toggle()" type="button"><i class="fa fa-calendar"></i></button>
                        </div>
                  </div>

                  <div class="col-sm-9 mt-2">
                    <div class="custom-control custom-switch">

                      <input
                             [(ngModel)]="egreso.EsCheque"
                              name="esCheque"
                             (ngModelChange)="changeEsChequeField()"
                            style="cursor:pointer;"
                            type="checkbox"
                            class="custom-control-input" id="switch0">

                      <label class="custom-control-label" for="switch0"> Es un cheque</label>
                     </div>
                  </div>

                  <label class=" col-sm-10 control-label" *ngIf = "fechaHabilitada">Fecha vencimiento cheque </label>
                 <div class="d-flex col-sm-8 flex-sw flex-row " *ngIf = "fechaHabilitada">
                        <input class="form-control fecha "
                                placeholder="dd-mm-yyyy"
                                [minDate]="{year: 1980, month: 1, day: 1}"
                                name="dp2"
                                [(ngModel)]="fechaCheque"
                                ngbDatepicker
                                readonly
                                #d2="ngbDatepicker"
                              >
                        <div class="input-group-append">
                          <button class="btn btn-outline-secondary btn-fecha calendar" (click)="d2.toggle()" type="button"><i class="fa fa-calendar"></i></button>
                        </div>
                  </div>

                  <label class="col-sm-2 col-sm-2 control-label" *ngIf = "fechaHabilitada">Descripción</label>
                  <div class="col-sm-10" *ngIf = "fechaHabilitada">
                  <input type="text"
                            class="form-control"
                            name="descripcion"
                            [(ngModel)] = "egreso.Descripcion"
                            placeholder="Descripcion"
                            #descripcion="ngModel"
                           >
                  </div>

                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-danger" (click)="activeModal.close('Close click')">Salir</button>
                  <button type="submit" class="btn btn-success">Guardar</button>
                </div>
              </form>

    </div>


  `,
  styles: [],
})
@Injectable()
export class AddEditEgresoComponent implements OnInit {
  public egreso: EgresoModel = new EgresoModel();
  public modalHeader: string;
  public fechaEgreso: { day: number; month: number; year: number };
  public egresos: EgresoModel[];

  public monedas: MonedaModel[];
  public monedaId: number;
  public categorias: CategoriaModel[];
  public proveedores: ProveedorModel[];
  public requestProv : ProveedorRequestModel;

  public fechaHabilitada : boolean;
  public fechaCheque: NgbDateStruct = {  year : null, month : null, day: null };

  @Output() sendegreso = new EventEmitter();
  loadingInfo: boolean;

  constructor(
    public activeModal: NgbActiveModal,
    private egresosService: EgresosService,
    private monedasService: MonedasService,
    private categoriasService: CategoriaService,
    private proveedoresService: ProveedoresService
  ) {
    this.egresos = [];
    this.monedas = [];
    this.categorias = [];
    this.proveedores = [];
    this.requestProv = new ProveedorRequestModel();

  }

  ngOnInit() {
    this.monedasService.getMonedas().subscribe((res: any) => {
      this.monedas = res.Monedas;

      this.monedaId = this.monedas.filter(m => m.Descripcion == '$')[0].Id;

      this.egreso.MonedaId = this.monedaId;
    });



    this.categoriasService.getCategorias().subscribe((res: any) => {
      this.categorias = res.Categorias;

    });
    this.proveedoresService.getProveedores(this.requestProv).subscribe((res: any) => {
      this.proveedores = res.Proveedores;
    });

    //Si el modelo tiene datos asignar id de categoria y moneda
    if (this.egreso.Id) {


      this.egreso.CategoriaId = this.egreso.Categoria.Id;
      this.egreso.ProveedorId = this.egreso.Proveedor.Id;
      this.egreso.MonedaId = this.egreso.Moneda.Id;

      if(this.egreso.EsCheque){
        this.fechaHabilitada = true;
        
        var fechaCheque = new Date(this.egreso.FechaCheque);
        this.fechaCheque = { day : fechaCheque.getDate(), month : fechaCheque.getMonth() + 1, year : fechaCheque.getFullYear() };
        
      }

      var fecha = new Date(this.egreso.FechaEgreso);
      this.fechaEgreso = { day : fecha.getDate(), month : fecha.getMonth() + 1, year : fecha.getFullYear() }



    } else {
      //Si el objeto no se va a editar inicializar campo fecha al dia de hoy
      var fechaHoy = new Date(Date.now());

      var mes = fechaHoy.getMonth() + 1;
      this.fechaEgreso = {
        day: fechaHoy.getDate(),
        month: mes,
        year: fechaHoy.getFullYear(),
      };
    }
  }


  changeEsChequeField(){

    if (this.egreso.EsCheque) {
      this.fechaHabilitada = true;
    } else {
      this.fechaHabilitada = false;
    }


  }





  guardar(form: NgForm) {
    if (form.invalid) {
      return;
    }
  

    this.egreso.FechaEgreso = this.fechaEgreso.month.toString() + '/' + this.fechaEgreso.day.toString() + '/' + this.fechaEgreso.year.toString();
      if(this.egreso.EsCheque){
        this.egreso.FechaCheque = this.fechaCheque.month + "/" + this.fechaCheque.day + "/" + this.fechaCheque.year;
      }
      else{
        this.egreso.FechaCheque= "";
      }
  

    Swal.fire('egreso', 'El egreso se está ingresando', 'info');
    let peticion: Observable<any>;

    if (this.egreso.Id) {
      //Actualizar egreso

        peticion = this.egresosService.updateEgreso(this.egreso);

        peticion.subscribe
          (resp => {
            Swal.fire(this.egreso.Importe.toString(),'Se actualizó correctamente','success');

            Object.assign( this.egresos[this.egresos.findIndex((el) => el.Id === this.egreso.Id)],this.egreso );

            this.activeModal.close('Result ok');
          },
          (error) => {
            // console.log(error);

            if (error.error) {
              let mensajeError = error.error.Message;
              Swal.fire('Error', mensajeError, 'error');
            } else {
              Swal.fire('Error', 'No se pudo actualizar el egreso', 'error');
            }
          }
        );
      }else {
      //Agregar egreso
      peticion = this.egresosService.addEgreso(this.egreso);
      peticion.subscribe(
        (resp) => {
          this.egreso.Id = resp;

          this.egresos.push(this.egreso);
          Swal.fire(this.egreso.Categoria.Descripcion,'Se agregó correctamente','success' );

          this.activeModal.close();
        },
        (error) => {
          if (error.error) {
            let mensajeError = error.error.Message;
            Swal.fire('Error', mensajeError, 'error');
          } else {
            Swal.fire('Error', 'No se pudo agregar el egreso', 'error');
          }
        }
      );
    }

    Swal.hideLoading();
  }
}
