import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';
import { EgresosService } from 'src/app/services/egresos.service';
import Swal from 'sweetalert2';
import { EgresoModel } from 'src/app/models/egreso/egreso.model';



@Component({
  selector: 'app-eliminar-egreso',
  template: `
  <div class="modal-header">
    <h4 class="modal-title" id="modal-title">Eliminar egreso</h4>
    <button type="button" class="close" aria-describedby="modal-title" (click)="activeModal.dismiss('Cross click')">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="modal-body">
    <p><strong>¿ Estás seguro que deseas eliminar el egreso ? <span class="text-primary">  </span> </strong></p>

  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-outline-secondary" (click)="activeModal.dismiss('cancel click')">Salir</button>
    <button type="button" class="btn btn-danger" (click)="eliminar()">Confirmar</button>
  </div>
  `,
  styles: [
  ]
})

export class DeleteEgresoComponent implements OnInit {

  public egreso : EgresoModel = new EgresoModel();
  public modalHeader : string;
  egresos : EgresoModel [] = [];

  constructor( public activeModal: NgbActiveModal,
               private egresosService: EgresosService, ) { }

  ngOnInit(): void {  }

  eliminar(){

    this.egresosService.deleteEgreso(this.egreso).
      subscribe(resp => {

      Swal.fire('', 'Se eliminó egreso correctamente','success' );

      var indice = this.egresos.indexOf(this.egreso);
      this.egresos.splice(indice, 1);

    }, (error) => {
      if(error.error)
      {
        let mensajeError = error.error.Message;
        Swal.fire('Error', mensajeError, 'error');
      }
      else { Swal.fire('Error', 'No se pudo eliminar egreso', 'error') }
    });

    this.activeModal.close();
  }

}
