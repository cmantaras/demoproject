import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { EgresosService } from 'src/app/services/egresos.service';
import { NgbModal, NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';
import { EgresoModel } from '../../../app/models/egreso/egreso.model';
import { EgresosFilter} from '../../../app/models/egreso/egresosFilter.model';

import { DeleteEgresoComponent } from './delete-egreso.components';
import { AddEditEgresoComponent } from './add-edit-egreso.component';
import { CategoriaService } from '../../services/categoria.service';
import { ProveedoresService } from 'src/app/services/proveedores.service';
import { CategoriaModel } from 'src/app/models/categoria/categoria.model';
import { ProveedorModel } from 'src/app/models/proveedor/proveedor.model';
import { ProveedorRequestModel } from 'src/app/models/proveedor/proveedorRequest.model';


@Component({
  selector: 'app-egresos',
  templateUrl: './egresos.component.html',
  styleUrls: ['./egresos.component.css'],
  providers: [NgbPaginationConfig]
})
export class EgresosComponent implements OnInit {

  loading : boolean = false;
  egresos : EgresoModel  [] = [];
  categorias : CategoriaModel  [] = [];
  filterCheque: Array<any>;
  proveedores : ProveedorModel  [] = [];
  proveedorRequest : ProveedorRequestModel;
  loadData : boolean = false;
  egresosFilter : EgresosFilter;
  egresoInfo : EgresoModel;

  count : number;
  //Paginación
  page = 1;
  pageSize =8;
  items = [];
  totalItems : number;


  constructor(private egresosService: EgresosService,
              private _categoriaService : CategoriaService,
              private _proveedoresService : ProveedoresService,
              public router:ActivatedRoute,
              public modalService: NgbModal,
              private config: NgbPaginationConfig,
              )
    {
      this.egresosFilter = new EgresosFilter();
      this.egresosFilter.Offset =this.page;
      this.egresosFilter.Limit =this.pageSize;
      this.egresosFilter.IdCategoria =0;
      this.egresosFilter.IdProveedor =0;
      this.egresosFilter.EsCheque=0;
      this.filterCheque = [{
        "Descripcion" : "Cheques",
        "Valor" : 2
      },{
        "Descripcion" : "Contado",
        "Valor" : 1
      },{
        "Descripcion" : "Todos",
        "Valor" : 0
      }]

      this.proveedorRequest = new ProveedorRequestModel();
      this.proveedorRequest.Todos = true;
    }

    ngOnInit(): void {
      this.page =1;
      this.fillegresoss();
      this.getProveedores();
      this.getCategorias();
    }


    fillegresoss(){
      
      this.loadData = true;
      this.egresosService.getEgresos(this.egresosFilter)
        .subscribe( (data : any) => {

          this.egresos= data.Egresos;
          this.count = data.Count;
          this.loadData = false;

        },( errorServicio ) => {
          this.loading = false;
        });

    }

    filter($page){
      this.egresosFilter.Offset = $page;
      this.page = $page;

      this.fillegresoss();
    }


    openModalAdd() {
      const modalRef = this.modalService.open(AddEditEgresoComponent, { backdrop: 'static'});
      modalRef.componentInstance.modalHeader = 'Agregar egresos';
      modalRef.componentInstance.egresoss = this.egresos;
            modalRef.result.then(res => {

        this.ngOnInit()
      })

    }

    openModalDelete( egreso : EgresoModel ) {

      const modalRef = this.modalService.open(DeleteEgresoComponent, { backdrop: 'static'});
      modalRef.componentInstance.modalHeader = 'Eliminar egresos';
      modalRef.componentInstance.egreso = egreso;

      modalRef.componentInstance.egresos = this.egresos;
      modalRef.result.then(res => {

        this.ngOnInit()
      })
    }

    openModalEdit(egreso: EgresoModel){

      var egresoAux = JSON.parse( JSON.stringify( egreso ) );

      const modalRef = this.modalService.open(AddEditEgresoComponent, { backdrop: 'static'});
      modalRef.componentInstance.modalHeader = 'Editar egresos';
      modalRef.componentInstance.egresos = this.egresos;

      modalRef.componentInstance.egreso = egresoAux;

      modalRef.result.then(res => {

        this.ngOnInit()
      })

    }

    openModalInfo(content, egresoModel: EgresoModel){


      const modalRef = this.modalService.open(content, { backdrop: 'static'});

      this.egresoInfo = egresoModel;




    }


    getCategorias(){
      this._categoriaService.getCategoriasActivasInactivas().subscribe((data : any) => {

        this.categorias = data.Categorias
      });
    }

    getProveedores(){
      this._proveedoresService.getProveedores(this.proveedorRequest).subscribe((data : any) => {

        this.proveedores = data.Proveedores
      });
    }

    getEgresos(){
      this.egresos = [];
      this.egresosFilter.Limit = this.pageSize;
      this.egresosFilter.Offset = this.page;
      this.loadData = true;
      if (this.egresosFilter.IdCategoria == null) this.egresosFilter.IdCategoria=0;
      if (this.egresosFilter.IdProveedor == null) this.egresosFilter.IdProveedor=0;


      setTimeout(() => {


        this.egresosService.getEgresos(this.egresosFilter).subscribe((data : any) => {

          this.count = data.Count
          this.egresos = data.Egresos;
          this.loadData = false;

          //this.loading = false;

        });

      },800)

    }

}
