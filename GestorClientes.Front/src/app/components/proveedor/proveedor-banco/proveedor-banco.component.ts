
import { Component, EventEmitter, Injectable, OnInit, Output } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgForm } from '@angular/forms';
import { ProveedoresService } from 'src/app/services/proveedores.service';
import { ProveedorModel } from 'src/app/models/proveedor/proveedor.model';
import Swal from 'sweetalert2';
import { Observable } from 'rxjs';
import { ProveedorBancoModel } from 'src/app/models/proveedor/proveedorBanco.model';
import { MonedasService } from 'src/app/services/monedas.service';
import { MonedaModel } from 'src/app/models/moneda/moneda.model';
import { TipoCuentaModel } from '../../../models/proveedor/proveedorBanco.model';

@Component({
  selector: 'app-proveedor-banco',
  template: `
    <div class="modal-header">
      <h4 class="modal-title"> {{ modalHeader }} </h4>
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">

              <form autocomplete="off" (ngSubmit)="guardar()"class="form-horizontal style-form" method="get" #form="ngForm">
                <div class="form-group">

                            <label class=" col control-label">Nombre Banco</label>
                            <div class="col-sm-10">
                            <input type="text"
                                      class="form-control "
                                      name="Nombre"
                                      [disabled]="!editando"
                                      [(ngModel)] = "proveedorBanco.Nombre"
                                      placeholder="Nombre"
                                      minlength="1"
                                      #nombre="ngModel"
                                    >
          
                            </div>
                            <label class=" col control-label">Cuenta</label>
                            <div class="col-sm-10">
                            <input type="text"
                                      class="form-control "
                                      name="Cuenta"
                                      [disabled]="!editando"
                                      [(ngModel)] = "proveedorBanco.Cuenta"
                                      placeholder="Cuenta"
                                      #cuenta="ngModel"
                                    >

                            </div>

                            <label class="col-sm-12 control-label">Moneda</label>
                          <div class="col-sm-9">

                              <ng-select [items]="monedas"
                                  name="monedas"
                                  [disabled]="!editando"
                                  bindLabel="Descripcion"
                                  bindValue="Id"
                                  [(ngModel)]="proveedorBanco.MonedaId"
                                  #moneda="ngModel">
                              </ng-select>
                            </div>

                            
                            <label class="col-sm-12 control-label">Tipo Cuenta</label>
                          <div class="col-sm-9">

                              <ng-select [items]="tiposCuenta"
                                  name="tipos"
                                  [disabled]="!editando"
                                  bindLabel="Descripcion"
                                  bindValue="Id"
                                  [(ngModel)]="proveedorBanco.CuentaTipoId"
                                  #tipos="ngModel">
                              </ng-select>
                          </div>

                </div>



                <div class="modal-footer">
                  <button type="button" class="btn btn-danger"  (click)="activeModal.close('Close click')">Salir</button>
                  <button type="button" class="btn btn-primary" [hidden]="editando" (click)="changeEditandoProperty()">Editar</button>
                  <button [hidden]="!editando" [disabled]="loadingInfo" type="submit" class="btn btn-success">Guardar</button>
                </div>
              </form>

    </div>


  `,

})
@Injectable()
export class ProveedorBancoComponent implements OnInit {
  public proveedor : ProveedorModel = new ProveedorModel();
  public proveedorBanco : ProveedorBancoModel = new ProveedorBancoModel();
  public modalHeader : string;

  public tiposCuenta: TipoCuentaModel[]=[];
  public monedas: MonedaModel[]=[];

  public editando: boolean = false;
  public loadingInfo: boolean;


  constructor(public activeModal: NgbActiveModal,
              private proveedoresService: ProveedoresService,
              private monedaService: MonedasService,) { }




  ngOnInit(): void {

    this.monedaService.getAllMonedas().subscribe( (res : any) => {
      this.monedas = res.Monedas;
   


        this.proveedoresService.getTiposCuentas().subscribe( (resu : any) => {
            this.tiposCuenta = resu;
           
    
                this.proveedoresService.getInfoBancariaProveedor(this.proveedor.Id).subscribe( (result : any) => {
                  this.proveedorBanco = result;
             
                                                    });
                                                        });
                                                           });
  }


  changeEditandoProperty(){
    this.editando = true;
  }

  guardar() {
  
    this.loadingInfo = true;

    Swal.fire('Aguarde', 'La información bancaria se actualizará pronto', 'info');
    let peticion : Observable<any>;

      //Actualizar proveedorBAnco
      peticion = this.proveedoresService.updateProveedorBanco(this.proveedorBanco);
      peticion.subscribe(resp => {
        Swal.fire("Listo!", 'Información bancaria actualizada','success' );
        this.activeModal.close('Result ok');
        }, (error) => {
          console.log(error);
          if(error.error) {
          let mensajeError = error.error.Message;
          Swal.fire('Error', mensajeError, 'error');
          }
          else { Swal.fire('Error', 'No se pudo actualizar la información', 'error') }
      });


       Swal.hideLoading();

       this.loadingInfo = false;
  }



}


