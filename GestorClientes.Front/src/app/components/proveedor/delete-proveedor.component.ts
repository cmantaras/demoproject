import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';
import { ProveedoresService } from 'src/app/services/proveedores.service';
import Swal from 'sweetalert2';
import { ProveedorModel } from 'src/app/models/proveedor/proveedor.model';



@Component({
  selector: 'app-eliminar-proveedor',
  template: `
  <div class="modal-header">
    <h4 class="modal-title" id="modal-title">Eliminar proveedor</h4>
    <button type="button" class="close" aria-describedby="modal-title" (click)="activeModal.dismiss('Cross click')">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="modal-body">
    <p><strong>¿ Estás seguro que deseas eliminar proveedor ? <span class="text-primary"> {{ proveedor.RazonSocial }}   </span> </strong></p>

  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-outline-secondary" (click)="activeModal.dismiss('cancel click')">Salir</button>
    <button type="button" class="btn btn-danger" (click)="eliminar()">Confirmar</button>
  </div>
  `,
  styles: [
  ]
})

export class DeleteProveedorComponent implements OnInit {

  public proveedor : ProveedorModel = new ProveedorModel();
  public modalHeader : string;
  public proveedores : ProveedorModel [] = [];

  constructor( public activeModal: NgbActiveModal,
               private proveedorsService: ProveedoresService, ) { }

  ngOnInit(): void {  }

  eliminar(){

    this.proveedorsService.deleteProveedor(this.proveedor).
      subscribe(resp => {

      Swal.fire('', 'Se eliminó proveedor correctamente','success' );

      var indice = this.proveedores.indexOf(this.proveedor);
      this.proveedores.splice(indice, 1);
      this.activeModal.close();
    }, (error) => {
      if(error.error)
      {
        let mensajeError = error.error.Message;
        Swal.fire('Error', mensajeError, 'error');
      }
      else { Swal.fire('Error', 'No se pudo eliminar proveedor', 'error') }
    });


  }

}
