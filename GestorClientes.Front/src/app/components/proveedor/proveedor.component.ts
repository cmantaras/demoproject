import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { ProveedoresService } from 'src/app/services/proveedores.service';
import { AddEditProveedorComponent } from './add-edit-proveedor.components';
import { NgbModal, NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';
import { ProveedorModel } from '../../models/proveedor/proveedor.model';

import { DeleteProveedorComponent } from './delete-proveedor.component';
import { Subject } from 'rxjs';
import { ProveedorRequestModel } from 'src/app/models/proveedor/proveedorRequest.model';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { ProveedorBancoComponent } from './proveedor-banco/proveedor-banco.component';

@Component({
  selector: 'app-proveedor',
  templateUrl: './proveedor.component.html',
  styleUrls: ['./proveedor.component.css'],
  providers: [NgbPaginationConfig]
})
export class ProveedorComponent implements OnInit {

  loading : boolean = true;
  public proveedores : ProveedorModel  [] = [];
  loadingData : boolean = false;
  proveedorInfo : ProveedorModel;


  proveedorFilter : ProveedorRequestModel;
  filtro : string;
  filtroChanged : Subject<string> = new Subject<string>();

  //Paginación
  page : number = 1;
  pageSize =4;
  items = [];
  count : number;
  totalItems : number;


  constructor(private proveedoresService: ProveedoresService,
              public router:ActivatedRoute,
              public modalService: NgbModal,
              private config: NgbPaginationConfig,
              public route : Router,
              private changeDetectorRefs: ChangeDetectorRef)
    {


      this.proveedorFilter = new ProveedorRequestModel();
      this.proveedorFilter.Limit =this.pageSize;
      this.proveedorFilter.Offset = this.page;
      this.filtro = "";



      this.filtroChanged.pipe(debounceTime(800), distinctUntilChanged())
                .subscribe(model => {
                  // this.filtro = model;
                  this.filter();
                })

    }

    ngOnInit(): void {
      this.page =1;
      this.filter();
    }


    fillProveedores(){
      this.proveedoresService.getProveedores()
        .subscribe( (data : any) => {

          this.loading = true;
          this.proveedores = data.Proveedores;
          this.loading = false;
        },( errorServicio ) => {
              this.loading = false;
        });
    }

    onFieldChange(query:string){

      this.filtroChanged.next(query);
    }

    filter()  :void {

      this.proveedorFilter.Offset = this.page;
      this.proveedorFilter.Limit = this.pageSize;

      //Chequeamos si el filtro contiene solo numeros
       if(Number(this.filtro) ){
         //Identificador es documento
         this.proveedorFilter.RUT =  this.filtro;
       }
        else
        {
          this.proveedorFilter.Nombre = this.filtro;
        }


          this.loadingData = true;
          this.proveedoresService.getProveedores(this.proveedorFilter)
          .subscribe( (data : any) => {

           this.proveedores = data.Proveedores;
           this.count = data.Count;

          this.loading = false;
           this.loadingData = false;
           this.proveedorFilter = new ProveedorRequestModel();

         },( errorServicio ) => {
           this.loadingData = false;
         });
    }


    openModalAdd() {
      const modalRef = this.modalService.open(AddEditProveedorComponent, { backdrop: 'static'});
      modalRef.componentInstance.modalHeader = 'Agregar proveedor';
      modalRef.componentInstance.proveedores = this.proveedores;

      modalRef.result.then(res => {


        this.route.navigateByUrl('/', {skipLocationChange: true}).then(() => {
          this.route.navigate([`/proveedor`]);
      });

      });

    }

    openModalDelete( proveedor : ProveedorModel ) {

      const modalRef = this.modalService.open(DeleteProveedorComponent, { backdrop: 'static'});
      modalRef.componentInstance.modalHeader = 'Eliminar proveedor';
      modalRef.componentInstance.proveedor = proveedor;
      modalRef.componentInstance.proveedores = this.proveedores;
    
      modalRef.result.then(res => {
      this.proveedoresService.getProveedores({RUT:null, Nombre: null, Limit: this.pageSize, Offset: this.page}).subscribe((data : any) =>{

        this.proveedores= data.Proveedores;


        this.changeDetectorRefs.detectChanges()
      });

    });
    }

    openModalEdit(proveedor: ProveedorModel){

      var proveedorAux = JSON.parse( JSON.stringify( proveedor ) );

      const modalRef = this.modalService.open(AddEditProveedorComponent, { backdrop: 'static'});
      modalRef.componentInstance.modalHeader = 'Editar proveedor';
      modalRef.componentInstance.proveedores = this.proveedores;

      modalRef.componentInstance.proveedor = proveedorAux;
      modalRef.result.then(res => {


        this.route.navigateByUrl('/', {skipLocationChange: true}).then(() => {
          this.route.navigate([`/proveedor`]);
      });

      });


    }
    openModalInfo(content,proveedor: ProveedorModel){


      const modalRef = this.modalService.open(content, { backdrop: 'static'});

      this.proveedorInfo = proveedor;


    }

    openModalInfoBancaria(content, proveedor: ProveedorModel) {
      const modalRef = this.modalService.open(ProveedorBancoComponent, { backdrop: 'static'});
      modalRef.componentInstance.modalHeader = 'Información Bancaria';
      modalRef.componentInstance.proveedor = proveedor;
    }
}
