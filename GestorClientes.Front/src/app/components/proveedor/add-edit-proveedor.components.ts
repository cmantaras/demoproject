import { Component, EventEmitter, Injectable, OnInit, Output } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgForm } from '@angular/forms';
import { ProveedoresService } from 'src/app/services/proveedores.service';
import { ProveedorModel } from 'src/app/models/proveedor/proveedor.model';
import Swal from 'sweetalert2';
import { Observable } from 'rxjs';
import { CiudadesService } from 'src/app/services/ciudades.service';
import { CiudadModel } from 'src/app/models/ciudad/ciudad.model';
import { analyzeFile } from '@angular/compiler';
import { CategoriaModel } from 'src/app/models/categoria/categoria.model';


@Component({
  selector: 'app-add-edit-proveedor',
  template: `
    <div class="modal-header">
      <h4 class="modal-title"> {{ modalHeader }} </h4>
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">


        <!-- BASIC FORM ELELEMNTS -->
              <form autocomplete="off" (ngSubmit)="guardar( form )"class="form-horizontal style-form" method="get" #form="ngForm">
                <div class="form-group">

                <label class=" col control-label">Nombre</label>
                  <div class="col-sm-10">
                  <input type="text"
                            class="form-control is-invalid "
                            [ngClass]="{ 'is-invalid': form.submitted && nombre.invalid }"
                            [class.is-invalid]="nombre.invalid && nombre.touched"
                            name="Nombre"
                            [(ngModel)] = "proveedor.Nombre"
                            required
                            placeholder="Nombre"
                            minlength="1"
                            #nombre="ngModel"
                           >
                           <small   *ngIf="nombre.invalid && nombre.touched"
                                   class="form-text text-danger">Ingrese un nombre de proveedor válido</small>
                  </div>

                  <label class=" col control-label">Razón social</label>
                  <div class="col-sm-10">
                  <input type="text"
                            class="form-control is-invalid"
                            name="Razón Social"
                            [ngClass]="{ 'is-invalid': form.submitted && razonsocial.invalid }"
                            [class.is-invalid]="razonsocial.invalid && razonsocial.touched"
                            [(ngModel)] = "proveedor.RazonSocial"
                            
                            placeholder="Razón Social"
                            minlength="1"
                            #razonsocial="ngModel"
                           >
                           <small   *ngIf="razonsocial.invalid && razonsocial.touched"
                                   class="form-text text-danger">Ingrese una razón social válida</small>
                  </div>

                  <label class="col-sm-2 col-sm-2 control-label">RUT</label>
                  <div class="col-sm-10">
                  <input type="text"
                            class="form-control is-invalid"
                            name="Rut"
                            [ngClass]="{ 'is-invalid': form.submitted && rut.invalid }"
                            [class.is-invalid]="rut.invalid && rut.touched"
                            [(ngModel)] = "proveedor.RUT"
                            
                            placeholder="R.U.T"
                            minlength="1"
                            #rut="ngModel"
                           >
                           <small   *ngIf="rut.invalid && rut.touched"
                                   class="form-text text-danger">Ingrese número de rut válido</small>
                  </div>

                  <label class="col-sm-2 col-sm-2 control-label">Dirección</label>
                  <div class="col-sm-10">
                  <input type="text"
                            class="form-control is-invalid"
                            name="Direccion"
                            [ngClass]="{ 'is-invalid': form.submitted && direccion.invalid }"
                            [class.is-invalid]="direccion.invalid && direccion.touched"
                            [(ngModel)] = "proveedor.Direccion"

                            placeholder="Direccion"
                            minlength="1"
                            #direccion="ngModel"
                           >
                           <small   *ngIf="direccion.invalid && direccion.touched"
                                   class="form-text text-danger">Ingrese una dirección válida</small>
                  </div>


                   <!-- <label class="col-sm-2 col-sm-2 control-label">Ciudad</label>
                      <div class="col-sm-10 flex-sw ">

                          <ng-select [items]="ciudades"
                          name="ciudad"
                          [addTag]="true"
                          bindLabel="Descripcion"
                          bindValue="Descripcion"
                          required
                          [(ngModel)]="proveedor.Ciudad.Descripcion"
                          #ciudad="ngModel">
                      </ng-select>
                      <small   *ngIf="!proveedor.Ciudad.Descripcion && ciudad.touched"
                                   class="form-text text-danger">Seleccione una ciudad</small>
                  </div>-->

                  <label class="col-sm-2 col-sm-2 control-label">Ciudad</label>
                  <div class="col-sm-10">
                  <input type="text"
                            class="form-control is-invalid"
                            name="Ciudad"
                            [ngClass]="{ 'is-invalid': form.submitted && ciudad.invalid }"
                            [class.is-invalid]="ciudad.invalid && ciudad.touched"
                            [(ngModel)] = "proveedor.Ciudad"
                            required
                            placeholder="Ciudad"
                            minlength="1"
                            #ciudad="ngModel"
                           >
                           <small   *ngIf="ciudad.invalid && ciudad.touched"
                                   class="form-text text-danger">Ingrese una ciudad válido</small>
                  </div>


                  <label class="col-sm-2 col-sm-2 control-label">Teléfono</label>
                  <div class="col-sm-10">
                  <input type="text"
                            pattern="[0-9]*"
                            class="form-control is-invalid"
                            name="Telefono"
                            [ngClass]="{ 'is-invalid': form.submitted && telefono.invalid }"
                            [class.is-invalid]="telefono.invalid && telefono.touched"
                            [(ngModel)] = "proveedor.Telefono"
                            required
                            placeholder="Teléfono"
                            minlength="1"
                            #telefono="ngModel"
                           >
                           <small   *ngIf="telefono.invalid && telefono.touched"
                                   class="form-text text-danger">Ingrese un número de teléfono válido</small>
                  </div>

                  <label class="col-sm-2 col-sm-2 control-label">Correo</label>
                  <div class="col-sm-10">
                  <input type="text"
                            class="form-control is-invalid"
                            name="Correo"
                            [ngClass]="{ 'is-invalid': form.submitted && correo.invalid }"
                            [class.is-invalid]="correo.invalid && correo.touched"
                            [(ngModel)] = "proveedor.Correo"
                            
                            placeholder="Correo"
                            minlength="1"
                            #correo="ngModel"
                           >
                           <small   *ngIf="correo.invalid && correo.touched"
                                   class="form-text text-danger">Ingrese un correo electrónico válido</small>
                  </div>

                  <hr>
                    <!-- <div class="" style="border-top: 1px solid #8f8f8f;"> -->

                    <label class="col control-label">Nombre </label>
                    <div class="col-sm-10">
                    <input type="text"
                              class="form-control"
                              [ngClass]="{ 'is-invalid': form.submitted && contacto.invalid }"
                              [class.is-invalid]="contacto.invalid && contacto.touched"
                              name="Contacto"
                              [(ngModel)] = "proveedor.Contacto"
                              placeholder="Nombre de contacto"
                              minlength="1"
                              #contacto="ngModel"

                            >
                            <small   *ngIf="contacto.invalid && contacto.touched"
                                    class="form-text text-danger">Ingrese un nombre para contacto válido</small>
                    </div>

                    <label class="col control-label">Teléfono</label>
                    <div class="col-sm-10">
                    <input type="text"
                            pattern="[0-9]*"
                              class="form-control"
                              name="telContacto"
                              [(ngModel)] = "proveedor.ContactoTelefono"
                              placeholder="Contacto"
                              minlength="1"
                              #telContacto="ngModel"

                            >
                            <small   *ngIf="telContacto.invalid && telContacto.touched"
                                   class="form-text text-danger">Ingrese un número de teléfono para contacto válido</small>
                    </div>

                    <label class="col control-label">Correo</label>
                    <div class="col-sm-10">
                    <input type="text"
                              class="form-control"
                              name="CorreoContacto"
                              [ngClass]="{ 'is-invalid': form.submitted && correoContacto.invalid }"
                              [class.is-invalid]="correoContacto.invalid && correoContacto.touched"
                              [(ngModel)] = "proveedor.ContactoCorreo"
                              placeholder="Correo de contacto"
                              minlength="1"
                              #correoContacto="ngModel"
                            >
                            <small   *ngIf="correoContacto.invalid && correoContacto.touched"
                                   class="form-text text-danger">Ingrese un correo electrónico de contacto válido</small>
                    </div>
                  </div>


                <!-- </div> -->
                <div class="modal-footer">
                  <button type="button" class="btn btn-danger" [disabled]="loadingInfo" (click)="activeModal.close('Close click')">Salir</button>
                  <button type="submit" class="btn btn-success">Guardar</button>
                </div>
              </form>

    </div>


  `,
  styles: [
  ]
})
@Injectable()
export class AddEditProveedorComponent implements OnInit   {

  public proveedor : ProveedorModel = new ProveedorModel();
  public modalHeader : string;
  public proveedores : ProveedorModel[] = [];

  //public ciudades : CiudadModel [] =  [];
  public categorias : CategoriaModel [] = [];
  public loadingInfo :boolean;

  @Output() sendproveedor= new EventEmitter();


  constructor( public activeModal: NgbActiveModal,
              private proveedoresService: ProveedoresService,
              //private ciudadesService: CiudadesService
                       ) {
              //this.proveedor.Ciudad = new CiudadModel();
  }
  ngOnInit() {

    /*this.ciudadesService.getCiudades()
      .subscribe( (data : any)  => {
        this.ciudades = data.Ciudades;
      },( errorServicio ) => {

      });*/
  }
  guardar(form : NgForm ) {
    
    if(form.invalid) {
      return;
    }


    // if(this.proveedores.find(x => x.Descripcion.toLocaleLowerCase === this.proveedor.Descripcion.toLocaleLowerCase)){
    //   Swal.fire('proveedor', 'El proveedor ingresado ya existe', 'info');
    //   return;
    // }
    
    this.loadingInfo = true;

    

    //this.proveedor.CiudadDescripcion = this.proveedor.Ciudad.Descripcion;

    Swal.fire('proveedor', 'El proveedor se está ingresando', 'info');
    let peticion : Observable<any>;

    if ( this.proveedor.Id )  {


        //Actualizar proveedor
        peticion = this.proveedoresService.updateProveedor(this.proveedor);

        peticion.subscribe(resp => {
          Swal.fire(this.proveedor.RazonSocial, 'Se actualizó correctamente','success' );

          Object.assign(this.proveedores[this.proveedores.findIndex(el => el.Id === this.proveedor.Id)], this.proveedor);
          this.activeModal.close('Result ok');
          }, (error) => {
            // console.log(error);

            if(error.error) {
            let mensajeError = error.error.Message;
            Swal.fire('Error', mensajeError, 'error');
            }
            else { Swal.fire('Error', 'No se pudo actualizar el proveedor', 'error') }

        });

    } else {

           //Agregar proveedor

          peticion = this.proveedoresService.addProveedor(this.proveedor);
          peticion.subscribe((resp: any) => {

            this.proveedor.Id = resp;

            // this.proveedores.push(this.proveedor);
              Swal.fire(this.proveedor.RazonSocial, 'Se agregó correctamente','success' );
              if(resp){


              this.activeModal.close('Result ok');
            }
              }, (error) => {

                if(error.error)
                {
                  let mensajeError = error.error.Message;
                  Swal.fire('Error', mensajeError, 'error');
                }
                else { Swal.fire('Error', 'No se pudo agregar el proveedor', 'error') }
          });
       }



       Swal.hideLoading();

       this.loadingInfo = false;
  }
}

