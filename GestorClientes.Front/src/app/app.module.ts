import { BrowserModule } from '@angular/platform-browser';
import { LOCALE_ID, NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NvbarComponent } from './components/shared/nvbar/nvbar.component';
import { HeaderComponent } from './components/shared/header/header.component';

import { ROUTES } from './app.routes';
import { APP_ROUTING } from './app.routes';
import { RouterModule } from '@angular/router';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

import { MatListModule } from '@angular/material/list';
import { NgbModule, NgbTimepickerModule } from '@ng-bootstrap/ng-bootstrap';


import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { LoadingComponent } from './components/shared/loading/loading.component';

import { LoginComponent } from './components/login/login.component';
import { AuthGuard } from './auth/auth-guard.service';
import { AuthService } from './auth/auth.service';
import { JwtModule } from "@auth0/angular-jwt";
import { MatFormField } from '@angular/material/form-field';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { NgSelectModule} from '@ng-select/ng-select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import localeEs from '@angular/common/locales/es';
registerLocaleData(localeEs);
import { AuthInterceptor } from './auth/auth.interceptor';
import {NgPipesModule, OrderByPipe} from 'ngx-pipes';
import { MatIconModule } from '@angular/material/icon';

import { MatMenuModule } from '@angular/material/menu';

import {SidebarLeftToggleDirective} from './components/shared/nvbar/app-left.directive'

import { NgxMatDatetimePickerModule, NgxMatNativeDateModule, NgxMatTimepickerModule } from '@angular-material-components/datetime-picker';

import { DatePipe, registerLocaleData } from '@angular/common';

import { MatSidenavModule } from '@angular/material/sidenav';
import { UsuarioComponent } from './components/configuracion/usuario/usuario.component';
import { RolComponent } from './components/configuracion/rol/rol.component';
import { UsuariosService } from './services/usuarios.service';
import { DeleteUsuarioComponent } from './components/configuracion/usuario/delete-usuario.component';
import { AddEditUsuarioComponent } from './components/configuracion/usuario/add-edit-usuario.component';
import { AddEditRolComponent } from './components/configuracion/rol/add-edit-rol.component';
import { DeleteRolComponent } from './components/configuracion/rol/delete-rol.component';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input/lib/ngx-intl-tel-input.module';
import { EditPermisosRolComponent } from './components/configuracion/rol/edit-permisos-rol.component';
import { PermisosService } from './services/permisos.service';
import { GlobalEventsManager } from './components/helper/global-events-manager';
import { HomeComponent } from './components/home/home.components';

import { CheckPermissionsDirective } from './directives/check-permissions.directive';
import { CheckNoPermissionsDirective } from './directives/check-no-permission.directive';
import { PasswordValidator } from './directives/password-validator.directive';
import { MarcaComponent } from './components/configuracion/marca/marca.component';
import { MarcasService } from './services/marcas.service';
import { AddEditMarcaComponent } from './components/configuracion/marca/add-edit-marca.component';
import { DeleteMarcaComponent } from './components/configuracion/marca/delete-marca.component';
import { ImpuestoComponent } from './components/configuracion/impuesto/impuesto.component';
import { ImpuestosService } from './services/impuestos.service';
import { AddEditImpuestoComponent } from './components/configuracion/impuesto/add-edit-impuesto.component';
import { DeleteImpuestoComponent } from './components/configuracion/impuesto/delete-impuesto.component';
import { ItemComponent } from './components/configuracion/item/item.component';
import { ItemsService } from './services/items.service';
import { AddEditItemComponent } from './components/configuracion/item/add-edit-item.component';
import { DeleteItemComponent } from './components/configuracion/item/delete-item.components';
import { ProveedorComponent } from './components/proveedor/proveedor.component';
import { AddEditProveedorComponent } from './components/proveedor/add-edit-proveedor.components';
import { DeleteProveedorComponent } from './components/proveedor/delete-proveedor.component';
import { ProveedoresService } from './services/proveedores.service';
import { CategoriaComponent } from './components/configuracion/categorias/categoria.component';
import { DeleteCategoriaComponent } from './components/configuracion/categorias/delete-categoria.components';
import { AddEditCategoriaComponent } from './components/configuracion/categorias/add-edit-categoria.components';
import { CiudadComponent } from './components/configuracion/ciudad/ciudad.component';
import { DeleteCiudadComponent } from './components/configuracion/ciudad/delete-ciudad.components';
import { AddEditCiudadComponent } from './components/configuracion/ciudad/add-edit-ciudad.component';
import { ZonaComponent } from './components/configuracion/zona/zona.component';
import { DeleteZonaComponent } from './components/configuracion/zona/delete-zona.components';
import { AddEditzonaComponent } from './components/configuracion/zona/add-edit-zona.component';
import { MetodoPagoComponent } from './components/configuracion/metodo-pago/metodo-pago.component';
import { DeleteMetodoPagoComponent } from './components/configuracion/metodo-pago/delete-metodo-pago.component';
import { AddEditMetodoPagoComponent } from './components/configuracion/metodo-pago/add-edit-met-pago.component';
import { ClienteComponent } from './components/cliente/cliente.component';
import { DeleteClienteComponent } from './components/cliente/delete-cliente.compoent';
import { AddEditClienteComponent } from './components/cliente/add-edit-cliente.component';
import { ClientesService } from './services/clientes.service';
import { ProductoComponent } from './components/configuracion/producto/producto.component';
import { DeleteProductoComponent } from './components/configuracion/producto/delete-producto.component';
import { AddEditProductoComponent } from './components/configuracion/producto/add-edit-producto.component';

import { MonedasService } from './services/monedas.service';
import { MonedaComponent } from './components/configuracion/moneda/moneda.component';
import { DeleteMonedaComponent } from './components/configuracion/moneda/delete-moneda.component';
import { AddEditMonedaComponent } from './components/configuracion/moneda/add-edit-moneda.component';
import { LoadingDataComponent } from './components/shared/loading-data/loading-data.component'
import { AddEditProductoPrecioComponent } from './components/configuracion/producto/add-edit-precio-producto.component';
import { ProductosClienteComponent } from './components/cliente/productos-cliente/productos-cliente.component';
import { AsignarProductosComponent } from './components/cliente/productos-cliente/asignar-productos/asignar-productos.component';


import {DragDropModule} from '@angular/cdk/drag-drop';
import { AddItemsComponent } from './components/cliente/productos-cliente/asignar-productos/add-Items.component';
import { LoadingDataSmallComponent } from './components/shared/loading-data-small/loading-data-small.component';
import { PagarProductosComponent } from './components/cliente/productos-cliente/pagar-productos/pagar-productos.component';
import { EgresosComponent } from './components/egresos/egresos.component';
import { AddEditEgresoComponent } from './components/egresos/add-edit-egreso.component';
import { DeleteEgresoComponent } from './components/egresos/delete-egreso.components';
import { EgresosService } from './services/egresos.service';
import { AvisoPagoComponent } from './components/cliente/aviso-pago/aviso-pago.component';

import { IngresosEgresosComponent } from './components/reportes/ingresos-egresos/ingresos-egresos.component';
import { DeudoresComponent } from './components/reportes/deudores/deudores.component';
import { ReporteClientesComponent } from './components/reportes/clientes/reporte-clientes.component';
import { ThousandsPipe } from './components/helper/thousands-pipe.pipe';
import { ArraySortPipe } from './components/helper/orderBy.pipe';
import { AddEditItemsComponent } from './components/cliente/productos-cliente/add-edit-items.component';
import { ProveedorBancoComponent } from './components/proveedor/proveedor-banco/proveedor-banco.component';




export function tokenGetter() {
  return localStorage.getItem("jwt");
}


@NgModule({

  declarations: [
    PasswordValidator,
    LoginComponent,
    AppComponent,
    NvbarComponent,
    HeaderComponent,
    LoadingComponent,
    LoadingDataSmallComponent,
    LoadingDataComponent,
    SidebarLeftToggleDirective,



    UsuarioComponent,
    AddEditUsuarioComponent,
    DeleteUsuarioComponent,

    RolComponent,
    AddEditRolComponent,
    DeleteRolComponent,
    EditPermisosRolComponent,
    HomeComponent,
    RolComponent,
    CheckPermissionsDirective,
    CheckNoPermissionsDirective,


    MarcaComponent,
    AddEditMarcaComponent,
    DeleteMarcaComponent,

    ImpuestoComponent,
    AddEditImpuestoComponent,
    DeleteImpuestoComponent,

    ItemComponent,
    AddEditItemComponent,
    DeleteItemComponent,

    ProveedorComponent,
    AddEditProveedorComponent,
    DeleteProveedorComponent,

    CategoriaComponent,
    AddEditCategoriaComponent,
    DeleteCategoriaComponent,

    CiudadComponent,
    AddEditCiudadComponent,
    DeleteCiudadComponent,

    ZonaComponent,
    DeleteZonaComponent,
    AddEditzonaComponent,

    MetodoPagoComponent,
    DeleteMetodoPagoComponent,
    AddEditMetodoPagoComponent,

    ClienteComponent,
    DeleteClienteComponent,
    AddEditClienteComponent,

    ProductoComponent,
    DeleteProductoComponent,
    AddEditProductoComponent,

    MonedaComponent,
    DeleteMonedaComponent,
    AddEditMonedaComponent,

    AddEditProductoPrecioComponent,
    ProductosClienteComponent,
    AsignarProductosComponent,
    AddItemsComponent,

    PagarProductosComponent,

    EgresosComponent,
    AddEditEgresoComponent,
    DeleteEgresoComponent,
    AvisoPagoComponent,

    IngresosEgresosComponent,

    DeudoresComponent,

    ReporteClientesComponent,

    AddEditItemsComponent,

    ThousandsPipe,
    ArraySortPipe,
    ProveedorBancoComponent



  ],
  imports: [
    RouterModule.forRoot(ROUTES, {onSameUrlNavigation: 'reload'}),
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        whitelistedDomains: ["localhost:50481"],
        blacklistedRoutes: []
      }
    }),
    MatIconModule,
    MatListModule,
    NgPipesModule,
    MatMenuModule,
    NgSelectModule,
    BrowserModule,
    DragDropModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    NgbModalModule,
    MatSidenavModule,
    RouterModule.forRoot ( ROUTES, { useHash : true, onSameUrlNavigation: 'reload'}),
    APP_ROUTING,
    NgbModule,
    FontAwesomeModule,
    FormsModule,
    ReactiveFormsModule,
    NgbTimepickerModule,
    NgSelectModule
  ],
  providers: [
    DatePipe,

    UsuariosService,
    AuthService,
    AuthGuard,
    PermisosService,
    GlobalEventsManager,
    AuthService,

    MarcasService,
    ImpuestosService,
    ItemsService,
    ProveedoresService,
    ClientesService,
    MonedasService,
    EgresosService,
    { provide: LOCALE_ID, useValue: 'es-UY' },
     {
        provide: HTTP_INTERCEPTORS,
        useClass: AuthInterceptor,
        multi: true
     }
   ],
  bootstrap: [AppComponent],
  exports: [
    MatIconModule,
    MatListModule,
    RouterModule ]
})
export class AppModule { }
