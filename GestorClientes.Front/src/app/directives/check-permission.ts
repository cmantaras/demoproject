import { OnInit } from '@angular/core';
import { Directive, ElementRef, TemplateRef, ViewContainerRef } from '@angular/core';
import { Input } from '@angular/core';

@Directive({
  selector: '[hasPermission]'
})
export class HasPermissionDirective implements OnInit {
  private currentUser;
  private permissions = [];
  private userPermisos: any = [];

  constructor(
    private element: ElementRef,
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef,

  ) {
  }

  ngOnInit() {


    this.userPermisos = localStorage.getItem("permisos")
    // this.userService.currentUser.subscribe(user => {
    //   this.currentUser = user;
    //   // this.updateView();
    // });
  }

  @Input()
  set hasPermission(val) {
    this.permissions = val;
     this.updateView();
  }

   private updateView() {
     if (this.checkPermission()) {
        this.viewContainer.createEmbeddedView(this.templateRef);
     } else {
       this.viewContainer.clear();
     }
   }

  private checkPermission() {
    let hasPermission = false;

    if (this.userPermisos) {
      for (const checkPermission of this.permissions) {
        const permissionFound = this.userPermisos.find(x => x.toUpperCase() === checkPermission.toUpperCase());

      }
    }

    return hasPermission;
  }
}
