import { Directive, ElementRef, Input, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appCheckNoPermissions]'
})
export class CheckNoPermissionsDirective implements OnInit {

  private currentUser;
  private permissions : string [] = [];
  private userPermisos = [];

  constructor(
    private element: ElementRef,
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef,

  ) {}



  ngOnInit() {
    this.userPermisos = JSON.parse(localStorage.getItem('permisos'));

     this.updateView();

  }

  @Input()
  set appCheckNoPermissions(val) {
    this.permissions = val;
  }

   private updateView() {
     if (!this.checkPermission()) {
         this.viewContainer.createEmbeddedView(this.templateRef);
     } else {
         this.viewContainer.clear();
     }
   }

  private checkPermission() {
    let hasPermission = false;

    if (this.permissions) {

      for (const checkPermission of this.permissions) {


          const permissionFound : string = this.userPermisos.find(x => x.Key === checkPermission);

          if(permissionFound) return true;

      }

    }

    return hasPermission;
  }


}
