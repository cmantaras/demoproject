import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './components/login/login.component';

import { AuthGuard } from './auth/auth-guard.service';
import { AppComponent } from './app.component';
import { UsuarioComponent } from './components/configuracion/usuario/usuario.component';
import { RolComponent } from './components/configuracion/rol/rol.component';
import { HomeComponent } from './components/home/home.components';

import { AuthService } from './auth/auth.service';
import { MarcaComponent } from './components/configuracion/marca/marca.component';
import { ProveedorComponent } from './components/proveedor/proveedor.component';
import { ItemComponent } from './components/configuracion/item/item.component';
import { CategoriaComponent } from './components/configuracion/categorias/categoria.component';
import { CiudadComponent } from './components/configuracion/ciudad/ciudad.component';
import { ImpuestosService } from './services/impuestos.service';
import { ImpuestoComponent } from './components/configuracion/impuesto/impuesto.component';
import { ZonaComponent } from './components/configuracion/zona/zona.component';
import { MetodoPagoComponent } from './components/configuracion/metodo-pago/metodo-pago.component';
import { ClienteComponent } from './components/cliente/cliente.component';
import { ProductoComponent } from './components/configuracion/producto/producto.component';
import { MonedaComponent } from './components/configuracion/moneda/moneda.component';
import { ProductosClienteComponent } from './components/cliente/productos-cliente/productos-cliente.component';
import { AsignarProductosComponent } from './components/cliente/productos-cliente/asignar-productos/asignar-productos.component';
import { PagarProductosComponent } from './components/cliente/productos-cliente/pagar-productos/pagar-productos.component';
import { EgresosComponent } from './components/egresos/egresos.component';
import { AvisoPagoComponent } from './components/cliente/aviso-pago/aviso-pago.component';
import { IngresosEgresosComponent } from './components/reportes/ingresos-egresos/ingresos-egresos.component';
import { DeudoresComponent } from './components/reportes/deudores/deudores.component';
import { ReporteClientesComponent } from './components/reportes/clientes/reporte-clientes.component';

export const ROUTES: Routes = [



  { path: 'usuario',pathMatch : 'full', component: UsuarioComponent, canActivate : [AuthGuard] },
  { path: 'rol',pathMatch : 'full', component: RolComponent,  canActivate : [AuthGuard] },
  { path: 'login',pathMatch : 'full', component: LoginComponent},
  { path: 'main', pathMatch : 'full', component : AppComponent,  canActivate : [AuthGuard]},
  { path: 'home', pathMatch : 'full', component : HomeComponent,  canActivate : [AuthGuard]},
  {   path: 'cliente',
       component : ClienteComponent,
       canActivate : [AuthGuard],
            children :
            [
              { path: ':id/productos',
                component : ProductosClienteComponent,
                canActivate : [AuthGuard],
                    children : [
                      { path: 'asignar',
                        component : AsignarProductosComponent,
                        canActivate : [AuthGuard]},
                      { path: 'pago',
                        component : PagarProductosComponent,
                        canActivate : [AuthGuard],}  ]},
              { path: ':id/aviso',
              component : AvisoPagoComponent,
              canActivate : [AuthGuard]},
            ]
  },
  { path: 'proveedor',
    component : ProveedorComponent,
    canActivate : [AuthGuard],
  },
  { path: 'marca', pathMatch : 'full', component : MarcaComponent, canActivate : [AuthGuard]},
  { path: 'item', pathMatch : 'full', component : ItemComponent, canActivate : [AuthGuard]},
  { path: 'categoria', pathMatch : 'full', component : CategoriaComponent, canActivate : [AuthGuard]},
  { path: 'ciudad', pathMatch : 'full', component : CiudadComponent, canActivate : [AuthGuard]},
  { path: 'impuesto', pathMatch : 'full', component : ImpuestoComponent, canActivate : [AuthGuard]},
  { path: 'zona', pathMatch : 'full', component : ZonaComponent, canActivate : [AuthGuard]},
  { path: 'metodopago', pathMatch : 'full', component : MetodoPagoComponent, canActivate : [AuthGuard]},
  { path: 'producto', pathMatch : 'full', component : ProductoComponent, canActivate : [AuthGuard]},
  { path: 'moneda', pathMatch : 'full', component : MonedaComponent, canActivate : [AuthGuard]},
  { path: 'egreso', pathMatch : 'full', component : EgresosComponent, canActivate : [AuthGuard]},
  { path: 'usuario', pathMatch : 'full', component : UsuarioComponent, canActivate : [AuthGuard]},
  { path: 'roles', pathMatch : 'full', component : RolComponent, canActivate : [AuthGuard]},
  { path: 'ingresosegresos', pathMatch : 'full', component : IngresosEgresosComponent, canActivate : [AuthGuard]},
  { path: 'deudores', pathMatch : 'full', component : DeudoresComponent, canActivate : [AuthGuard]},
  { path: 'reporteclientes', pathMatch : 'full', component : ReporteClientesComponent, canActivate : [AuthGuard]},


  { path: 'resetpassword', component : LoginComponent},

  { path: '', pathMatch : 'full', redirectTo : 'funcionario' },
  { path: '**', pathMatch : 'full', redirectTo : 'home' },

];

export const APP_ROUTING = RouterModule.forRoot(ROUTES, {onSameUrlNavigation: 'reload'});
