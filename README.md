

## Script MySql version +8.0

Ejecutar script sql_22.10

## Proyecto frontend

Instalar Angular CLI [Angular CLI](https://github.com/angular/angular-cli)
Ejecutar en la terminal de visual studio

1) npm install
2) npm update
3) run serve

Configurar BASE_URL en el archivo global.component.ts

## Proyecto backend

Iniciar solucion en Visual Studio 2019
1) Click derecho en la solucion
2) Seleccionar set-up start project
3) Seleccionar GestorClientes.Api
4) Configurar connectionString en el archivo web.config