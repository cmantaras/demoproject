-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               8.0.25 - MySQL Community Server - GPL
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for dblocal
CREATE DATABASE IF NOT EXISTS `dblocal` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `dblocal`;

-- Dumping structure for table dblocal.auditoria
CREATE TABLE IF NOT EXISTS `auditoria` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `Fecha` datetime DEFAULT NULL,
  `UsuarioId` int DEFAULT NULL,
  `Accion` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `Detalle` text CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  `DatoTipo` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `DatoId` int DEFAULT NULL,
  `Origen` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE,
  KEY `FK_auditoria_usuario` (`UsuarioId`),
  CONSTRAINT `FK_auditoria_usuario` FOREIGN KEY (`UsuarioId`) REFERENCES `usuario` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=143 DEFAULT CHARSET=utf8mb3;

-- Dumping data for table dblocal.auditoria: ~85 rows (approximately)
/*!40000 ALTER TABLE `auditoria` DISABLE KEYS */;
INSERT INTO `auditoria` (`Id`, `Fecha`, `UsuarioId`, `Accion`, `Detalle`, `DatoTipo`, `DatoId`, `Origen`) VALUES
	(40, '2021-11-08 11:11:53', 1, 'Alta', 'Error al capturar.', 'ClienteProducto', 0, 'ClienteService.AsignarProducto'),
	(41, '2021-11-08 11:12:13', 1, 'Alta', 'Error al capturar.', 'PagoCliente', 246, 'PagoClienteService.AddPagoCliente'),
	(42, '2021-11-08 11:12:13', 1, 'Alta', 'Error al capturar.', 'List`1', 0, 'PagoClienteService.RealizarPago'),
	(43, '2021-11-08 17:18:28', 1, 'Alta', 'Error al capturar.', 'PagoCliente', 247, 'PagoClienteService.AddPagoCliente'),
	(44, '2021-11-08 17:18:28', 1, 'Alta', 'Error al capturar.', 'List`1', 0, 'PagoClienteService.RealizarPago'),
	(45, '2021-11-08 17:23:16', 1, 'Modificacion', '{"Categoria":{"Id":26,"Descripcion":"dfdf","Activo":true},"Moneda":{"Id":3,"Descripcion":"$","Activo":true},"Proveedor":{"Id":17,"Nombre":"sggssgsgsgs","RazonSocial":"gererer","RUT":"34343434","Direccion":"efdfddf 3554","Ciudad":" reaehadh","Telefono":"5555555555555555","Correo":"55555@gg.com","Contacto":null,"ContactoTelefono":null,"ContactoCorreo":"111111111@gg.com","Activo":true},"Id":39,"FechaEgreso":"2021-09-16T00:00:00","FechaCheque":"2021-09-24T00:00:00","Importe":7878787.0,"Descripcion":"cheque cambiado","MonedaId":3,"CategoriaId":26,"ProveedorId":17,"EsCheque":true,"Activo":true}', 'Egreso_7D88E44E69690D25416D58C3328C1E62F4DA58AC99F', 39, 'EgresoService.Edit'),
	(46, '2021-11-08 17:23:29', 1, 'Alta', '{"Id":40,"FechaEgreso":"2021-11-08T00:00:00","FechaCheque":null,"Importe":212343.0,"Descripcion":null,"MonedaId":3,"CategoriaId":24,"ProveedorId":18,"EsCheque":false,"Activo":true,"Moneda":null,"Categoria":null,"Proveedor":null}', 'Egreso', 40, 'EgresoService.Add'),
	(47, '2021-11-08 17:23:41', 1, 'Modificacion', '{"Categoria":{"Id":24,"Descripcion":"dfdff","Activo":true},"Moneda":{"Id":3,"Descripcion":"$","Activo":true},"Proveedor":{"Id":19,"Nombre":"kukukuuk","RazonSocial":"","RUT":null,"Direccion":"kffff","Ciudad":"kkkk","Telefono":"777777777777777777777","Correo":"kkhkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk","Contacto":null,"ContactoTelefono":null,"ContactoCorreo":null,"Activo":true},"Id":40,"FechaEgreso":"2021-11-08T00:00:00","FechaCheque":null,"Importe":21234.0,"Descripcion":"","MonedaId":3,"CategoriaId":24,"ProveedorId":19,"EsCheque":false,"Activo":true}', 'Egreso_7D88E44E69690D25416D58C3328C1E62F4DA58AC99F', 40, 'EgresoService.Edit'),
	(48, '2021-11-08 17:23:46', 1, 'Modificacion', '{"Categoria":{"Id":24,"Descripcion":"dfdff","Activo":true},"Moneda":{"Id":3,"Descripcion":"$","Activo":true},"Proveedor":{"Id":19,"Nombre":"kukukuuk","RazonSocial":"","RUT":null,"Direccion":"kffff","Ciudad":"kkkk","Telefono":"777777777777777777777","Correo":"kkhkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk","Contacto":null,"ContactoTelefono":null,"ContactoCorreo":null,"Activo":true},"Id":40,"FechaEgreso":"2021-11-08T00:00:00","FechaCheque":null,"Importe":21234.0,"Descripcion":"","MonedaId":3,"CategoriaId":24,"ProveedorId":19,"EsCheque":false,"Activo":true}', 'Egreso_7D88E44E69690D25416D58C3328C1E62F4DA58AC99F', 40, 'EgresoService.Edit'),
	(49, '2021-11-08 17:23:56', 1, 'Modificacion', '{"Categoria":{"Id":24,"Descripcion":"dfdff","Activo":true},"Moneda":{"Id":3,"Descripcion":"$","Activo":true},"Proveedor":{"Id":19,"Nombre":"kukukuuk","RazonSocial":"","RUT":null,"Direccion":"kffff","Ciudad":"kkkk","Telefono":"777777777777777777777","Correo":"kkhkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk","Contacto":null,"ContactoTelefono":null,"ContactoCorreo":null,"Activo":true},"Id":40,"FechaEgreso":"2021-11-08T00:00:00","FechaCheque":null,"Importe":21234.0,"Descripcion":"","MonedaId":3,"CategoriaId":24,"ProveedorId":19,"EsCheque":false,"Activo":true}', 'Egreso_7D88E44E69690D25416D58C3328C1E62F4DA58AC99F', 40, 'EgresoService.Edit'),
	(50, '2021-11-08 17:24:13', 1, 'Modificacion', '{"Categoria":{"Id":24,"Descripcion":"dfdff","Activo":true},"Moneda":{"Id":3,"Descripcion":"$","Activo":true},"Proveedor":{"Id":19,"Nombre":"kukukuuk","RazonSocial":"","RUT":null,"Direccion":"kffff","Ciudad":"kkkk","Telefono":"777777777777777777777","Correo":"kkhkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk","Contacto":null,"ContactoTelefono":null,"ContactoCorreo":null,"Activo":true},"Id":40,"FechaEgreso":"2021-11-08T00:00:00","FechaCheque":null,"Importe":21234.0,"Descripcion":"","MonedaId":3,"CategoriaId":24,"ProveedorId":19,"EsCheque":false,"Activo":true}', 'Egreso_7D88E44E69690D25416D58C3328C1E62F4DA58AC99F', 40, 'EgresoService.Edit'),
	(51, '2021-11-08 17:24:16', 1, 'Modificacion', '{"Categoria":{"Id":24,"Descripcion":"dfdff","Activo":true},"Moneda":{"Id":3,"Descripcion":"$","Activo":true},"Proveedor":{"Id":19,"Nombre":"kukukuuk","RazonSocial":"","RUT":null,"Direccion":"kffff","Ciudad":"kkkk","Telefono":"777777777777777777777","Correo":"kkhkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk","Contacto":null,"ContactoTelefono":null,"ContactoCorreo":null,"Activo":true},"Id":40,"FechaEgreso":"2021-11-08T00:00:00","FechaCheque":null,"Importe":21234.0,"Descripcion":"","MonedaId":3,"CategoriaId":24,"ProveedorId":19,"EsCheque":false,"Activo":true}', 'Egreso_7D88E44E69690D25416D58C3328C1E62F4DA58AC99F', 40, 'EgresoService.Edit'),
	(52, '2021-11-08 17:24:33', 1, 'Alta', '{"Id":41,"FechaEgreso":"2021-11-08T00:00:00","FechaCheque":null,"Importe":524646.0,"Descripcion":null,"MonedaId":3,"CategoriaId":25,"ProveedorId":19,"EsCheque":false,"Activo":true,"Moneda":null,"Categoria":null,"Proveedor":null}', 'Egreso', 41, 'EgresoService.Add'),
	(53, '2021-11-08 17:24:39', 1, 'Modificacion', '{"Categoria":{"Id":25,"Descripcion":"fdfd","Activo":true},"Moneda":{"Id":3,"Descripcion":"$","Activo":true},"Proveedor":{"Id":19,"Nombre":"kukukuuk","RazonSocial":"","RUT":null,"Direccion":"kffff","Ciudad":"kkkk","Telefono":"777777777777777777777","Correo":"kkhkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk","Contacto":null,"ContactoTelefono":null,"ContactoCorreo":null,"Activo":true},"Id":41,"FechaEgreso":"2021-11-08T00:00:00","FechaCheque":null,"Importe":52464.0,"Descripcion":"","MonedaId":3,"CategoriaId":25,"ProveedorId":19,"EsCheque":false,"Activo":true}', 'Egreso_7D88E44E69690D25416D58C3328C1E62F4DA58AC99F', 41, 'EgresoService.Edit'),
	(54, '2021-11-08 17:24:50', 1, 'Modificacion', '{"Categoria":{"Id":25,"Descripcion":"fdfd","Activo":true},"Moneda":{"Id":3,"Descripcion":"$","Activo":true},"Proveedor":{"Id":19,"Nombre":"kukukuuk","RazonSocial":"","RUT":null,"Direccion":"kffff","Ciudad":"kkkk","Telefono":"777777777777777777777","Correo":"kkhkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk","Contacto":null,"ContactoTelefono":null,"ContactoCorreo":null,"Activo":true},"Id":41,"FechaEgreso":"2021-11-08T00:00:00","FechaCheque":null,"Importe":5246445234.0,"Descripcion":"","MonedaId":3,"CategoriaId":25,"ProveedorId":19,"EsCheque":false,"Activo":true}', 'Egreso_7D88E44E69690D25416D58C3328C1E62F4DA58AC99F', 41, 'EgresoService.Edit'),
	(55, '2021-11-08 17:24:54', 1, 'Modificacion', '{"Categoria":{"Id":25,"Descripcion":"fdfd","Activo":true},"Moneda":{"Id":3,"Descripcion":"$","Activo":true},"Proveedor":{"Id":19,"Nombre":"kukukuuk","RazonSocial":"","RUT":null,"Direccion":"kffff","Ciudad":"kkkk","Telefono":"777777777777777777777","Correo":"kkhkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk","Contacto":null,"ContactoTelefono":null,"ContactoCorreo":null,"Activo":true},"Id":41,"FechaEgreso":"2021-11-08T00:00:00","FechaCheque":null,"Importe":52464452.0,"Descripcion":"","MonedaId":3,"CategoriaId":25,"ProveedorId":19,"EsCheque":false,"Activo":true}', 'Egreso_7D88E44E69690D25416D58C3328C1E62F4DA58AC99F', 41, 'EgresoService.Edit'),
	(56, '2021-11-08 17:25:17', 1, 'Alta', '{"Id":42,"FechaEgreso":"2021-11-08T00:00:00","FechaCheque":null,"Importe":233333.0,"Descripcion":null,"MonedaId":3,"CategoriaId":26,"ProveedorId":18,"EsCheque":false,"Activo":true,"Moneda":null,"Categoria":null,"Proveedor":null}', 'Egreso', 42, 'EgresoService.Add'),
	(57, '2021-11-08 17:26:04', 1, 'Alta', '{"Id":43,"FechaEgreso":"2021-11-08T00:00:00","FechaCheque":null,"Importe":2426246.0,"Descripcion":null,"MonedaId":3,"CategoriaId":27,"ProveedorId":19,"EsCheque":false,"Activo":true,"Moneda":null,"Categoria":null,"Proveedor":null}', 'Egreso', 43, 'EgresoService.Add'),
	(58, '2021-11-08 17:26:16', 1, 'Modificacion', '{"Categoria":{"Id":27,"Descripcion":"ffs","Activo":true},"Moneda":{"Id":3,"Descripcion":"$","Activo":true},"Proveedor":{"Id":19,"Nombre":"kukukuuk","RazonSocial":"","RUT":null,"Direccion":"kffff","Ciudad":"kkkk","Telefono":"777777777777777777777","Correo":"kkhkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk","Contacto":null,"ContactoTelefono":null,"ContactoCorreo":null,"Activo":true},"Id":43,"FechaEgreso":"2021-11-08T00:00:00","FechaCheque":null,"Importe":7000.0,"Descripcion":"","MonedaId":3,"CategoriaId":27,"ProveedorId":19,"EsCheque":false,"Activo":true}', 'Egreso_7D88E44E69690D25416D58C3328C1E62F4DA58AC99F', 43, 'EgresoService.Edit'),
	(59, '2021-11-08 17:28:34', 1, 'Alta', '{"Id":44,"FechaEgreso":"2021-11-08T00:00:00","FechaCheque":null,"Importe":447757.0,"Descripcion":null,"MonedaId":3,"CategoriaId":14,"ProveedorId":17,"EsCheque":false,"Activo":true,"Moneda":null,"Categoria":null,"Proveedor":null}', 'Egreso', 44, 'EgresoService.Add'),
	(60, '2021-11-08 17:29:32', 1, 'Modificacion', '{"Categoria":{"Id":14,"Descripcion":"Categoria 1234ABC","Activo":true},"Moneda":{"Id":3,"Descripcion":"$","Activo":true},"Proveedor":{"Id":17,"Nombre":"sggssgsgsgs","RazonSocial":"gererer","RUT":"34343434","Direccion":"efdfddf 3554","Ciudad":" reaehadh","Telefono":"5555555555555555","Correo":"55555@gg.com","Contacto":null,"ContactoTelefono":null,"ContactoCorreo":"111111111@gg.com","Activo":true},"Id":44,"FechaEgreso":"2021-11-08T00:00:00","FechaCheque":null,"Importe":5000000.0,"Descripcion":"","MonedaId":3,"CategoriaId":14,"ProveedorId":17,"EsCheque":false,"Activo":true}', 'Egreso_7D88E44E69690D25416D58C3328C1E62F4DA58AC99F', 44, 'EgresoService.Edit'),
	(61, '2021-11-08 17:33:06', 1, 'Alta', '{"Id":45,"FechaEgreso":"2021-11-08T00:00:00","FechaCheque":null,"Importe":2000.0,"Descripcion":null,"MonedaId":3,"CategoriaId":26,"ProveedorId":18,"EsCheque":false,"Activo":true,"Moneda":null,"Categoria":null,"Proveedor":null}', 'Egreso', 45, 'EgresoService.Add'),
	(62, '2021-11-08 17:33:12', 1, 'Modificacion', '{"Categoria":{"Id":26,"Descripcion":"dfdf","Activo":true},"Moneda":{"Id":3,"Descripcion":"$","Activo":true},"Proveedor":{"Id":18,"Nombre":"rrsfs","RazonSocial":"fsfssf","RUT":"2344234","Direccion":"Arequita 345","Ciudad":"Tacuarembó","Telefono":"43344334334434","Correo":"fsdsfsdf@dede.com","Contacto":null,"ContactoTelefono":null,"ContactoCorreo":null,"Activo":true},"Id":45,"FechaEgreso":"2021-11-08T00:00:00","FechaCheque":null,"Importe":55555.0,"Descripcion":"","MonedaId":3,"CategoriaId":26,"ProveedorId":18,"EsCheque":false,"Activo":true}', 'Egreso_7D88E44E69690D25416D58C3328C1E62F4DA58AC99F', 45, 'EgresoService.Edit'),
	(63, '2021-11-08 17:37:16', 1, 'Modificacion', '{"Categoria":{"Id":26,"Descripcion":"dfdf","Activo":true},"Moneda":{"Id":3,"Descripcion":"$","Activo":true},"Proveedor":{"Id":18,"Nombre":"rrsfs","RazonSocial":"fsfssf","RUT":"2344234","Direccion":"Arequita 345","Ciudad":"Tacuarembó","Telefono":"43344334334434","Correo":"fsdsfsdf@dede.com","Contacto":null,"ContactoTelefono":null,"ContactoCorreo":null,"Activo":true},"Id":45,"FechaEgreso":"2021-11-08T00:00:00","FechaCheque":null,"Importe":55555.0,"Descripcion":"","MonedaId":3,"CategoriaId":26,"ProveedorId":18,"EsCheque":false,"Activo":true}', 'Egreso_7D88E44E69690D25416D58C3328C1E62F4DA58AC99F', 45, 'EgresoService.Edit'),
	(64, '2021-11-08 17:37:45', 1, 'Alta', '{"Id":46,"FechaEgreso":"2021-11-08T00:00:00","FechaCheque":null,"Importe":30000.0,"Descripcion":null,"MonedaId":3,"CategoriaId":27,"ProveedorId":19,"EsCheque":false,"Activo":true,"Moneda":null,"Categoria":null,"Proveedor":null}', 'Egreso', 46, 'EgresoService.Add'),
	(65, '2021-11-08 17:38:18', 1, 'Modificacion', '{"Categoria":{"Id":24,"Descripcion":"dfdff","Activo":true},"Moneda":{"Id":3,"Descripcion":"$","Activo":true},"Proveedor":{"Id":18,"Nombre":"rrsfs","RazonSocial":"fsfssf","RUT":"2344234","Direccion":"Arequita 345","Ciudad":"Tacuarembó","Telefono":"43344334334434","Correo":"fsdsfsdf@dede.com","Contacto":null,"ContactoTelefono":null,"ContactoCorreo":null,"Activo":true},"Id":46,"FechaEgreso":"2021-11-08T00:00:00","FechaCheque":null,"Importe":222222.0,"Descripcion":"","MonedaId":3,"CategoriaId":24,"ProveedorId":18,"EsCheque":false,"Activo":true}', 'Egreso_7D88E44E69690D25416D58C3328C1E62F4DA58AC99F', 46, 'EgresoService.Edit'),
	(66, '2021-11-08 17:39:00', 1, 'Alta', '{"Id":47,"FechaEgreso":"2021-11-08T00:00:00","FechaCheque":null,"Importe":101010.0,"Descripcion":null,"MonedaId":3,"CategoriaId":27,"ProveedorId":19,"EsCheque":false,"Activo":true,"Moneda":null,"Categoria":null,"Proveedor":null}', 'Egreso', 47, 'EgresoService.Add'),
	(67, '2021-11-08 17:39:20', 1, 'Modificacion', '{"Categoria":{"Id":27,"Descripcion":"ffs","Activo":true},"Moneda":{"Id":3,"Descripcion":"$","Activo":true},"Proveedor":{"Id":19,"Nombre":"kukukuuk","RazonSocial":"","RUT":null,"Direccion":"kffff","Ciudad":"kkkk","Telefono":"777777777777777777777","Correo":"kkhkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk","Contacto":null,"ContactoTelefono":null,"ContactoCorreo":null,"Activo":true},"Id":47,"FechaEgreso":"2021-11-08T00:00:00","FechaCheque":null,"Importe":1000.0,"Descripcion":"","MonedaId":3,"CategoriaId":27,"ProveedorId":19,"EsCheque":false,"Activo":true}', 'Egreso_7D88E44E69690D25416D58C3328C1E62F4DA58AC99F', 47, 'EgresoService.Edit'),
	(68, '2021-11-08 17:39:44', 1, 'Alta', '{"Id":48,"FechaEgreso":"2021-11-08T00:00:00","FechaCheque":null,"Importe":8787.0,"Descripcion":null,"MonedaId":3,"CategoriaId":26,"ProveedorId":19,"EsCheque":false,"Activo":true,"Moneda":null,"Categoria":null,"Proveedor":null}', 'Egreso', 48, 'EgresoService.Add'),
	(69, '2021-11-08 17:40:03', 1, 'Modificacion', '{"Categoria":{"Id":20,"Descripcion":"sdffd","Activo":true},"Moneda":{"Id":3,"Descripcion":"$","Activo":true},"Proveedor":{"Id":18,"Nombre":"rrsfs","RazonSocial":"fsfssf","RUT":"2344234","Direccion":"Arequita 345","Ciudad":"Tacuarembó","Telefono":"43344334334434","Correo":"fsdsfsdf@dede.com","Contacto":null,"ContactoTelefono":null,"ContactoCorreo":null,"Activo":true},"Id":48,"FechaEgreso":"2021-11-08T00:00:00","FechaCheque":null,"Importe":4000.0,"Descripcion":"","MonedaId":3,"CategoriaId":20,"ProveedorId":18,"EsCheque":false,"Activo":true}', 'Egreso_7D88E44E69690D25416D58C3328C1E62F4DA58AC99F', 48, 'EgresoService.Edit'),
	(70, '2021-11-08 17:41:42', 1, 'Alta', '{"Id":49,"FechaEgreso":"2021-11-08T00:00:00","FechaCheque":null,"Importe":2323.0,"Descripcion":null,"MonedaId":3,"CategoriaId":16,"ProveedorId":19,"EsCheque":false,"Activo":true,"Moneda":null,"Categoria":null,"Proveedor":null}', 'Egreso', 49, 'EgresoService.Add'),
	(71, '2021-11-08 17:42:00', 1, 'Modificacion', '{"Categoria":{"Id":24,"Descripcion":"dfdff","Activo":true},"Moneda":{"Id":3,"Descripcion":"$","Activo":true},"Proveedor":{"Id":18,"Nombre":"rrsfs","RazonSocial":"fsfssf","RUT":"2344234","Direccion":"Arequita 345","Ciudad":"Tacuarembó","Telefono":"43344334334434","Correo":"fsdsfsdf@dede.com","Contacto":null,"ContactoTelefono":null,"ContactoCorreo":null,"Activo":true},"Id":49,"FechaEgreso":"2021-11-08T00:00:00","FechaCheque":null,"Importe":12121221.0,"Descripcion":"","MonedaId":3,"CategoriaId":24,"ProveedorId":18,"EsCheque":false,"Activo":true}', 'Egreso_7D88E44E69690D25416D58C3328C1E62F4DA58AC99F', 49, 'EgresoService.Edit'),
	(72, '2021-11-08 17:42:25', 1, 'Modificacion', '{"Categoria":{"Id":24,"Descripcion":"dfdff","Activo":true},"Moneda":{"Id":3,"Descripcion":"$","Activo":true},"Proveedor":{"Id":18,"Nombre":"rrsfs","RazonSocial":"fsfssf","RUT":"2344234","Direccion":"Arequita 345","Ciudad":"Tacuarembó","Telefono":"43344334334434","Correo":"fsdsfsdf@dede.com","Contacto":null,"ContactoTelefono":null,"ContactoCorreo":null,"Activo":true},"Id":49,"FechaEgreso":"2021-11-08T00:00:00","FechaCheque":null,"Importe":12121221.0,"Descripcion":"","MonedaId":3,"CategoriaId":24,"ProveedorId":18,"EsCheque":false,"Activo":true}', 'Egreso_7D88E44E69690D25416D58C3328C1E62F4DA58AC99F', 49, 'EgresoService.Edit'),
	(73, '2021-11-08 17:42:35', 1, 'Alta', '{"Id":50,"FechaEgreso":"2021-11-08T00:00:00","FechaCheque":null,"Importe":1212122.0,"Descripcion":null,"MonedaId":3,"CategoriaId":26,"ProveedorId":18,"EsCheque":false,"Activo":true,"Moneda":null,"Categoria":null,"Proveedor":null}', 'Egreso', 50, 'EgresoService.Add'),
	(74, '2021-11-08 17:42:53', 1, 'Modificacion', '{"Categoria":{"Id":16,"Descripcion":"1222","Activo":true},"Moneda":{"Id":3,"Descripcion":"$","Activo":true},"Proveedor":{"Id":17,"Nombre":"sggssgsgsgs","RazonSocial":"gererer","RUT":"34343434","Direccion":"efdfddf 3554","Ciudad":" reaehadh","Telefono":"5555555555555555","Correo":"55555@gg.com","Contacto":null,"ContactoTelefono":null,"ContactoCorreo":"111111111@gg.com","Activo":true},"Id":50,"FechaEgreso":"2021-11-08T00:00:00","FechaCheque":null,"Importe":144444.0,"Descripcion":"","MonedaId":3,"CategoriaId":16,"ProveedorId":17,"EsCheque":false,"Activo":true}', 'Egreso_7D88E44E69690D25416D58C3328C1E62F4DA58AC99F', 50, 'EgresoService.Edit'),
	(75, '2021-11-08 17:45:26', 1, 'Alta', '{"Id":51,"FechaEgreso":"2021-11-08T00:00:00","FechaCheque":null,"Importe":4000.0,"Descripcion":null,"MonedaId":3,"CategoriaId":24,"ProveedorId":18,"EsCheque":false,"Activo":true,"Moneda":null,"Categoria":null,"Proveedor":null}', 'Egreso', 51, 'EgresoService.Add'),
	(76, '2021-11-08 17:45:39', 1, 'Modificacion', '{"Categoria":{"Id":20,"Descripcion":"sdffd","Activo":true},"Moneda":{"Id":3,"Descripcion":"$","Activo":true},"Proveedor":{"Id":19,"Nombre":"kukukuuk","RazonSocial":"","RUT":null,"Direccion":"kffff","Ciudad":"kkkk","Telefono":"777777777777777777777","Correo":"kkhkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk","Contacto":null,"ContactoTelefono":null,"ContactoCorreo":null,"Activo":true},"Id":51,"FechaEgreso":"2021-11-08T00:00:00","FechaCheque":null,"Importe":88888.0,"Descripcion":"","MonedaId":3,"CategoriaId":20,"ProveedorId":19,"EsCheque":false,"Activo":true}', 'Egreso_7D88E44E69690D25416D58C3328C1E62F4DA58AC99F', 51, 'EgresoService.Edit'),
	(77, '2021-11-08 17:47:04', 1, 'Alta', '{"Id":52,"FechaEgreso":"2021-11-08T00:00:00","FechaCheque":null,"Importe":1000.0,"Descripcion":null,"MonedaId":3,"CategoriaId":27,"ProveedorId":18,"EsCheque":false,"Activo":true,"Moneda":null,"Categoria":null,"Proveedor":null}', 'Egreso', 52, 'EgresoService.Add'),
	(78, '2021-11-08 17:47:17', 1, 'Modificacion', '{"Categoria":{"Id":20,"Descripcion":"sdffd","Activo":true},"Moneda":{"Id":3,"Descripcion":"$","Activo":true},"Proveedor":{"Id":17,"Nombre":"sggssgsgsgs","RazonSocial":"gererer","RUT":"34343434","Direccion":"efdfddf 3554","Ciudad":" reaehadh","Telefono":"5555555555555555","Correo":"55555@gg.com","Contacto":null,"ContactoTelefono":null,"ContactoCorreo":"111111111@gg.com","Activo":true},"Id":52,"FechaEgreso":"2021-11-08T00:00:00","FechaCheque":null,"Importe":444444.0,"Descripcion":"","MonedaId":3,"CategoriaId":20,"ProveedorId":17,"EsCheque":false,"Activo":true}', 'Egreso_7D88E44E69690D25416D58C3328C1E62F4DA58AC99F', 52, 'EgresoService.Edit'),
	(79, '2021-11-08 17:48:22', 1, 'Alta', '{"Id":53,"FechaEgreso":"2021-11-08T00:00:00","FechaCheque":null,"Importe":23333.0,"Descripcion":null,"MonedaId":3,"CategoriaId":26,"ProveedorId":19,"EsCheque":false,"Activo":true,"Moneda":null,"Categoria":null,"Proveedor":null}', 'Egreso', 53, 'EgresoService.Add'),
	(80, '2021-11-08 17:48:34', 1, 'Modificacion', '{"Categoria":{"Id":26,"Descripcion":"dfdf","Activo":true},"Moneda":{"Id":3,"Descripcion":"$","Activo":true},"Proveedor":{"Id":19,"Nombre":"kukukuuk","RazonSocial":"","RUT":null,"Direccion":"kffff","Ciudad":"kkkk","Telefono":"777777777777777777777","Correo":"kkhkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk","Contacto":null,"ContactoTelefono":null,"ContactoCorreo":null,"Activo":true},"Id":53,"FechaEgreso":"2021-11-08T00:00:00","FechaCheque":null,"Importe":40000.0,"Descripcion":"","MonedaId":3,"CategoriaId":26,"ProveedorId":19,"EsCheque":false,"Activo":true}', 'Egreso_7D88E44E69690D25416D58C3328C1E62F4DA58AC99F', 53, 'EgresoService.Edit'),
	(81, '2021-11-08 17:49:10', 1, 'Alta', '{"Id":54,"FechaEgreso":"2021-11-08T00:00:00","FechaCheque":"2021-11-28T00:00:00","Importe":12212.0,"Descripcion":"wewewe","MonedaId":3,"CategoriaId":25,"ProveedorId":17,"EsCheque":true,"Activo":true,"Moneda":null,"Categoria":null,"Proveedor":null}', 'Egreso', 54, 'EgresoService.Add'),
	(82, '2021-11-08 17:49:26', 1, 'Modificacion', '{"Categoria":{"Id":25,"Descripcion":"fdfd","Activo":true},"Moneda":{"Id":3,"Descripcion":"$","Activo":true},"Proveedor":{"Id":17,"Nombre":"sggssgsgsgs","RazonSocial":"gererer","RUT":"34343434","Direccion":"efdfddf 3554","Ciudad":" reaehadh","Telefono":"5555555555555555","Correo":"55555@gg.com","Contacto":null,"ContactoTelefono":null,"ContactoCorreo":"111111111@gg.com","Activo":true},"Id":54,"FechaEgreso":"2021-11-08T00:00:00","FechaCheque":"2021-11-28T00:00:00","Importe":122222.0,"Descripcion":"wewewe","MonedaId":3,"CategoriaId":25,"ProveedorId":17,"EsCheque":true,"Activo":true}', 'Egreso_7D88E44E69690D25416D58C3328C1E62F4DA58AC99F', 54, 'EgresoService.Edit'),
	(83, '2021-11-08 17:49:51', 1, 'Alta', '{"Id":55,"FechaEgreso":"2021-11-08T00:00:00","FechaCheque":"2021-11-25T00:00:00","Importe":11111111.0,"Descripcion":"dfefg","MonedaId":3,"CategoriaId":26,"ProveedorId":18,"EsCheque":true,"Activo":true,"Moneda":null,"Categoria":null,"Proveedor":null}', 'Egreso', 55, 'EgresoService.Add'),
	(84, '2021-11-08 17:50:05', 1, 'Modificacion', '{"Categoria":{"Id":26,"Descripcion":"dfdf","Activo":true},"Moneda":{"Id":3,"Descripcion":"$","Activo":true},"Proveedor":{"Id":18,"Nombre":"rrsfs","RazonSocial":"fsfssf","RUT":"2344234","Direccion":"Arequita 345","Ciudad":"Tacuarembó","Telefono":"43344334334434","Correo":"fsdsfsdf@dede.com","Contacto":null,"ContactoTelefono":null,"ContactoCorreo":null,"Activo":true},"Id":55,"FechaEgreso":"2021-11-08T00:00:00","FechaCheque":null,"Importe":11111111.0,"Descripcion":"dfefg","MonedaId":3,"CategoriaId":26,"ProveedorId":18,"EsCheque":false,"Activo":true}', 'Egreso_7D88E44E69690D25416D58C3328C1E62F4DA58AC99F', 55, 'EgresoService.Edit'),
	(85, '2021-11-08 12:09:18', 1, 'Baja', '{"Cliente":null,"Cuotas":null,"Id":247,"ClienteId":107,"MontoTotal":500.0,"Fecha":"2021-11-08T17:18:27"}', 'PagoCliente_DEF9C166ACD92F1B5A1382FF6C2F328712B80A', 247, 'PagoClienteService.DeletePagoCliente'),
	(86, '2021-11-08 12:09:42', 1, 'Baja', '{"Cliente":null,"Cuotas":null,"Id":246,"ClienteId":107,"MontoTotal":999.99999999999989,"Fecha":"2021-11-08T11:12:12"}', 'PagoCliente_DEF9C166ACD92F1B5A1382FF6C2F328712B80A', 246, 'PagoClienteService.DeletePagoCliente'),
	(87, '2021-11-08 12:10:39', 1, 'Alta', 'Error al capturar.', 'PagoCliente', 248, 'PagoClienteService.AddPagoCliente'),
	(88, '2021-11-08 12:10:39', 1, 'Alta', 'Error al capturar.', 'List`1', 0, 'PagoClienteService.RealizarPago'),
	(89, '2021-11-08 12:10:52', 1, 'Alta', 'Error al capturar.', 'PagoCliente', 249, 'PagoClienteService.AddPagoCliente'),
	(90, '2021-11-08 12:10:52', 1, 'Alta', 'Error al capturar.', 'List`1', 0, 'PagoClienteService.RealizarPago'),
	(91, '2021-11-08 12:11:23', 1, 'Baja', '{"Cliente":null,"Cuotas":null,"Id":248,"ClienteId":107,"MontoTotal":333.33333333333331,"Fecha":"2021-11-08T12:10:38"}', 'PagoCliente_DEF9C166ACD92F1B5A1382FF6C2F328712B80A', 248, 'PagoClienteService.DeletePagoCliente'),
	(92, '2021-11-08 12:11:47', 1, 'Baja', '{"Cliente":null,"Cuotas":null,"Id":249,"ClienteId":107,"MontoTotal":166.66666666666666,"Fecha":"2021-11-08T12:10:51"}', 'PagoCliente_DEF9C166ACD92F1B5A1382FF6C2F328712B80A', 249, 'PagoClienteService.DeletePagoCliente'),
	(93, '2022-03-10 12:17:05', 1, 'Alta', 'Error al capturar.', 'PagoCliente', 250, 'PagoClienteService.AddPagoCliente'),
	(94, '2022-03-10 12:17:05', 1, 'Alta', 'Error al capturar.', 'List`1', 0, 'PagoClienteService.RealizarPago'),
	(95, '2021-12-28 12:19:22', 1, 'Baja', 'Error al capturar.', 'ClienteProducto_1C1268B00BE4577A700FABEF871AE6A3C9', 336, 'ClienteService.DeleteProductoCliente'),
	(96, '2021-11-28 12:20:02', 1, 'Alta', 'Error al capturar.', 'ClienteProducto', 0, 'ClienteService.AsignarProducto'),
	(97, '2021-12-28 12:31:06', 1, 'Alta', 'Error al capturar.', 'PagoCliente', 251, 'PagoClienteService.AddPagoCliente'),
	(98, '2021-12-28 12:31:06', 1, 'Alta', 'Error al capturar.', 'List`1', 0, 'PagoClienteService.RealizarPago'),
	(99, '2021-12-28 12:31:14', 1, 'Alta', 'Error al capturar.', 'PagoCliente', 252, 'PagoClienteService.AddPagoCliente'),
	(100, '2021-12-28 12:31:14', 1, 'Alta', 'Error al capturar.', 'List`1', 0, 'PagoClienteService.RealizarPago'),
	(101, '2022-03-28 12:33:11', 1, 'Alta', 'Error al capturar.', 'PagoCliente', 253, 'PagoClienteService.AddPagoCliente'),
	(102, '2022-03-28 12:33:11', 1, 'Alta', 'Error al capturar.', 'List`1', 0, 'PagoClienteService.RealizarPago'),
	(103, '2022-03-28 12:33:24', 1, 'Alta', 'Error al capturar.', 'PagoCliente', 254, 'PagoClienteService.AddPagoCliente'),
	(104, '2022-03-28 12:33:24', 1, 'Alta', 'Error al capturar.', 'List`1', 0, 'PagoClienteService.RealizarPago'),
	(105, '2022-03-28 12:33:36', 1, 'Alta', 'Error al capturar.', 'PagoCliente', 255, 'PagoClienteService.AddPagoCliente'),
	(106, '2022-03-28 12:33:37', 1, 'Alta', 'Error al capturar.', 'List`1', 0, 'PagoClienteService.RealizarPago'),
	(107, '2021-11-16 10:43:30', 1, 'Error', '{"ClassName":"System.NullReferenceException","Message":"Object reference not set to an instance of an object.","Data":null,"InnerException":null,"HelpURL":null,"StackTraceString":"   at GestorClientes.Logic.Dto.ProveedorBancoDto..ctor(ProveedorBanco proveedorBanco) in C:\\\\Users\\\\Francisco\\\\Documents\\\\GitHub\\\\gestorclientes\\\\GestorClientes\\\\GestorClientes\\\\Dto\\\\ProveedorBancoDto.cs:line 24\\r\\n   at GestorClientes.Logic.Services.ProveedorService.GetInfoBancariaProveedor(Int32 id) in C:\\\\Users\\\\Francisco\\\\Documents\\\\GitHub\\\\gestorclientes\\\\GestorClientes\\\\GestorClientes\\\\Services\\\\ProveedorService.cs:line 127","RemoteStackTraceString":null,"RemoteStackIndex":0,"ExceptionMethod":"1\\n.ctor\\nGestorClientes.Logic, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\\nGestorClientes.Logic.Dto.ProveedorBancoDto\\nVoid .ctor(GestorClientes.Dominio.Models.ProveedorBanco)","HResult":-2147467261,"Source":"GestorClientes.Logic","WatsonBuckets":null}', 'NullReferenceException', 0, 'ProveedorService.GetInfoBancariaProveedor'),
	(108, '2021-11-16 11:16:37', 1, 'Error', '{"ClassName":"System.Data.ConstraintException","Message":"The \'MonedaId\' property on \'ProveedorBanco\' could not be set to a \'null\' value. You must set this property to a non-null value of type \'System.Int32\'. ","Data":null,"InnerException":null,"HelpURL":null,"StackTraceString":"   at System.Data.Entity.Core.Common.Internal.Materialization.Shaper.ErrorHandlingValueReader`1.GetValue(DbDataReader reader, Int32 ordinal)\\r\\n   at lambda_method(Closure , Shaper )\\r\\n   at System.Data.Entity.Core.Common.Internal.Materialization.Shaper.HandleEntityAppendOnly[TEntity](Func`2 constructEntityDelegate, EntityKey entityKey, EntitySet entitySet)\\r\\n   at lambda_method(Closure , Shaper )\\r\\n   at System.Data.Entity.Core.Common.Internal.Materialization.Coordinator`1.ReadNextElement(Shaper shaper)\\r\\n   at System.Data.Entity.Core.Common.Internal.Materialization.Shaper`1.SimpleEnumerator.MoveNext()\\r\\n   at System.Data.Entity.Internal.LazyEnumerator`1.MoveNext()\\r\\n   at System.Linq.Enumerable.FirstOrDefault[TSource](IEnumerable`1 source)\\r\\n   at System.Data.Entity.Core.Objects.ELinq.ObjectQueryProvider.<>c__14`1.<GetElementFunction>b__14_1(IEnumerable`1 sequence)\\r\\n   at System.Data.Entity.Core.Objects.ELinq.ObjectQueryProvider.ExecuteSingle[TResult](IEnumerable`1 query, Expression queryRoot)\\r\\n   at System.Data.Entity.Core.Objects.ELinq.ObjectQueryProvider.System.Linq.IQueryProvider.Execute[TResult](Expression expression)\\r\\n   at System.Data.Entity.Internal.Linq.DbQueryProvider.Execute[TResult](Expression expression)\\r\\n   at System.Linq.Queryable.FirstOrDefault[TSource](IQueryable`1 source)\\r\\n   at GestorClientes.Logic.Services.ProveedorService.GetInfoBancariaProveedor(Int32 id) in C:\\\\Users\\\\Francisco\\\\Documents\\\\GitHub\\\\gestorclientes\\\\GestorClientes\\\\GestorClientes\\\\Services\\\\ProveedorService.cs:line 145","RemoteStackTraceString":null,"RemoteStackIndex":0,"ExceptionMethod":"8\\nGetValue\\nEntityFramework, Version=6.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\\nSystem.Data.Entity.Core.Common.Internal.Materialization.Shaper+ErrorHandlingValueReader`1\\nT GetValue(System.Data.Common.DbDataReader, Int32)","HResult":-2146232022,"Source":"EntityFramework","WatsonBuckets":null}', 'ConstraintException', 0, 'ProveedorService.GetInfoBancariaProveedor'),
	(109, '2021-11-16 11:33:48', 1, 'Error', '{"ClassName":"System.NullReferenceException","Message":"Object reference not set to an instance of an object.","Data":null,"InnerException":null,"HelpURL":null,"StackTraceString":"   at GestorClientes.Logic.Dto.ProveedorBancoDto..ctor(ProveedorBanco proveedorBanco) in C:\\\\Users\\\\Francisco\\\\Documents\\\\GitHub\\\\gestorclientes\\\\GestorClientes\\\\GestorClientes\\\\Dto\\\\ProveedorBancoDto.cs:line 24\\r\\n   at GestorClientes.Logic.Services.ProveedorService.GetInfoBancariaProveedor(Int32 id) in C:\\\\Users\\\\Francisco\\\\Documents\\\\GitHub\\\\gestorclientes\\\\GestorClientes\\\\GestorClientes\\\\Services\\\\ProveedorService.cs:line 147","RemoteStackTraceString":null,"RemoteStackIndex":0,"ExceptionMethod":"1\\n.ctor\\nGestorClientes.Logic, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\\nGestorClientes.Logic.Dto.ProveedorBancoDto\\nVoid .ctor(GestorClientes.Dominio.Models.ProveedorBanco)","HResult":-2147467261,"Source":"GestorClientes.Logic","WatsonBuckets":null}', 'NullReferenceException', 0, 'ProveedorService.GetInfoBancariaProveedor'),
	(110, '2021-11-16 12:33:53', 1, 'Error', '{"ClassName":"System.NullReferenceException","Message":"Object reference not set to an instance of an object.","Data":null,"InnerException":null,"HelpURL":null,"StackTraceString":"   at GestorClientes.Logic.Dto.ProveedorBancoDto..ctor(ProveedorBanco proveedorBanco) in C:\\\\Users\\\\Francisco\\\\Documents\\\\GitHub\\\\gestorclientes\\\\GestorClientes\\\\GestorClientes\\\\Dto\\\\ProveedorBancoDto.cs:line 24\\r\\n   at GestorClientes.Logic.Services.ProveedorService.GetInfoBancariaProveedor(Int32 id) in C:\\\\Users\\\\Francisco\\\\Documents\\\\GitHub\\\\gestorclientes\\\\GestorClientes\\\\GestorClientes\\\\Services\\\\ProveedorService.cs:line 147","RemoteStackTraceString":null,"RemoteStackIndex":0,"ExceptionMethod":"1\\n.ctor\\nGestorClientes.Logic, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\\nGestorClientes.Logic.Dto.ProveedorBancoDto\\nVoid .ctor(GestorClientes.Dominio.Models.ProveedorBanco)","HResult":-2147467261,"Source":"GestorClientes.Logic","WatsonBuckets":null}', 'NullReferenceException', 0, 'ProveedorService.GetInfoBancariaProveedor'),
	(111, '2021-11-16 12:34:12', 1, 'Error', '{"ClassName":"System.NullReferenceException","Message":"Object reference not set to an instance of an object.","Data":null,"InnerException":null,"HelpURL":null,"StackTraceString":"   at GestorClientes.Logic.Dto.ProveedorBancoDto..ctor(ProveedorBanco proveedorBanco) in C:\\\\Users\\\\Francisco\\\\Documents\\\\GitHub\\\\gestorclientes\\\\GestorClientes\\\\GestorClientes\\\\Dto\\\\ProveedorBancoDto.cs:line 24\\r\\n   at GestorClientes.Logic.Services.ProveedorService.GetInfoBancariaProveedor(Int32 id) in C:\\\\Users\\\\Francisco\\\\Documents\\\\GitHub\\\\gestorclientes\\\\GestorClientes\\\\GestorClientes\\\\Services\\\\ProveedorService.cs:line 147","RemoteStackTraceString":null,"RemoteStackIndex":0,"ExceptionMethod":"1\\n.ctor\\nGestorClientes.Logic, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\\nGestorClientes.Logic.Dto.ProveedorBancoDto\\nVoid .ctor(GestorClientes.Dominio.Models.ProveedorBanco)","HResult":-2147467261,"Source":"GestorClientes.Logic","WatsonBuckets":null}', 'NullReferenceException', 0, 'ProveedorService.GetInfoBancariaProveedor'),
	(127, '2021-11-16 12:49:26', 1, 'Alta', '{"Id":37,"Nombre":"tgttgjrftjrftjrt","RazonSocial":"jrtjrtjrtjr","RUT":"jrjrtrtjrt","Direccion":"rtjrttjrtjtjt","Ciudad":"jtjartjtjt","Telefono":"65445754755757","Correo":null,"Contacto":null,"ContactoTelefono":null,"ContactoCorreo":null,"Activo":true}', 'Proveedor', 37, 'ProveedorService.Add'),
	(128, '2021-11-16 14:00:38', 1, 'Alta', '{"CuentaTipo":{"Id":1,"Descripcion":"CA"},"Moneda":{"Id":2,"Descripcion":"UY","Activo":false},"Proveedor":{"Id":16,"Nombre":"Salus","RazonSocial":"Agua Salus","RUT":"1212212","Direccion":"ejido 3322","Ciudad":"Montevideo","Telefono":"26590478","Correo":"Salus@salus.com.uy","Contacto":"salus","ContactoTelefono":"26590478","ContactoCorreo":"Salus@salus.com.uy","Activo":true},"Id":1,"Nombre":"Santander","Cuenta":"09171624231-00001","ProveedorId":16,"CuentaTipoId":1,"MonedaId":2}', 'ProveedorBanco_663B26189ECD6C1302EA7B34F6B7C443E5F', 1, 'ProveedorService.UpdateProveedorBanco'),
	(129, '2021-11-16 14:00:59', 1, 'Alta', '{"CuentaTipo":{"Id":1,"Descripcion":"CA"},"Moneda":{"Id":2,"Descripcion":"UY","Activo":false},"Proveedor":{"Id":16,"Nombre":"Salus","RazonSocial":"Agua Salus","RUT":"1212212","Direccion":"ejido 3322","Ciudad":"Montevideo","Telefono":"26590478","Correo":"Salus@salus.com.uy","Contacto":"salus","ContactoTelefono":"26590478","ContactoCorreo":"Salus@salus.com.uy","Activo":true},"Id":1,"Nombre":"Brou","Cuenta":"09171624231-00002","ProveedorId":16,"CuentaTipoId":1,"MonedaId":2}', 'ProveedorBanco_663B26189ECD6C1302EA7B34F6B7C443E5F', 1, 'ProveedorService.UpdateProveedorBanco'),
	(130, '2021-11-16 14:01:42', 1, 'Alta', '{"CuentaTipo":{"Id":1,"Descripcion":"CA"},"Moneda":{"Id":1,"Descripcion":"USD","Activo":false},"Proveedor":{"Id":16,"Nombre":"Salus","RazonSocial":"Agua Salus","RUT":"1212212","Direccion":"ejido 3322","Ciudad":"Montevideo","Telefono":"26590478","Correo":"Salus@salus.com.uy","Contacto":"salus","ContactoTelefono":"26590478","ContactoCorreo":"Salus@salus.com.uy","Activo":true},"Id":1,"Nombre":"Santander","Cuenta":"09171624231-00001","ProveedorId":16,"CuentaTipoId":1,"MonedaId":1}', 'ProveedorBanco_663B26189ECD6C1302EA7B34F6B7C443E5F', 1, 'ProveedorService.UpdateProveedorBanco'),
	(134, '2021-11-16 14:33:14', 1, 'Alta', '{"Id":22,"Nombre":"City","Cuenta":"012002124-0004","ProveedorId":18,"CuentaTipoId":1,"MonedaId":4,"Proveedor":null,"CuentaTipo":null,"Moneda":null}', 'ProveedorBanco', 22, 'ProveedorService.UpdateProveedorBanco'),
	(135, '2021-11-16 14:44:00', 1, 'Alta', '{"Id":23,"Nombre":"","Cuenta":"","ProveedorId":17,"CuentaTipoId":1,"MonedaId":1,"Proveedor":null,"CuentaTipo":null,"Moneda":null}', 'ProveedorBanco', 23, 'ProveedorService.UpdateProveedorBanco'),
	(136, '2021-11-16 14:54:35', 1, 'Alta', '{"CuentaTipo":{"Id":1,"Descripcion":"CA"},"Moneda":{"Id":1,"Descripcion":"USD","Activo":false},"Proveedor":{"Id":37,"Nombre":"tgttgjrftjrftjrt","RazonSocial":"jrtjrtjrtjr","RUT":"jrjrtrtjrt","Direccion":"rtjrttjrtjtjt","Ciudad":"jtjartjtjt","Telefono":"65445754755757","Correo":null,"Contacto":null,"ContactoTelefono":null,"ContactoCorreo":null,"Activo":true},"Id":23,"Nombre":"","Cuenta":"","ProveedorId":37,"CuentaTipoId":1,"MonedaId":1}', 'ProveedorBanco_663B26189ECD6C1302EA7B34F6B7C443E5F', 23, 'ProveedorService.UpdateProveedorBanco'),
	(137, '2021-11-16 14:58:12', 1, 'Alta', '{"CuentaTipo":{"Id":1,"Descripcion":"CA"},"Moneda":{"Id":1,"Descripcion":"USD","Activo":false},"Proveedor":{"Id":37,"Nombre":"tgttgjrftjrftjrt","RazonSocial":"jrtjrtjrtjr","RUT":"jrjrtrtjrt","Direccion":"rtjrttjrtjtjt","Ciudad":"jtjartjtjt","Telefono":"65445754755757","Correo":null,"Contacto":null,"ContactoTelefono":null,"ContactoCorreo":null,"Activo":true},"Id":23,"Nombre":"Brou","Cuenta":"206516513203","ProveedorId":37,"CuentaTipoId":1,"MonedaId":1}', 'ProveedorBanco_663B26189ECD6C1302EA7B34F6B7C443E5F', 23, 'ProveedorService.UpdateProveedorBanco'),
	(138, '2021-11-16 14:58:26', 1, 'Alta', '{"CuentaTipo":{"Id":1,"Descripcion":"CA"},"Moneda":{"Id":2,"Descripcion":"UY","Activo":false},"Proveedor":{"Id":18,"Nombre":"rrsfs","RazonSocial":"fsfssf","RUT":"2344234","Direccion":"Arequita 345","Ciudad":"Tacuarembó","Telefono":"43344334334434","Correo":"fsdsfsdf@dede.com","Contacto":null,"ContactoTelefono":null,"ContactoCorreo":null,"Activo":true},"Id":22,"Nombre":"City","Cuenta":"012002124-0004","ProveedorId":18,"CuentaTipoId":1,"MonedaId":2}', 'ProveedorBanco_663B26189ECD6C1302EA7B34F6B7C443E5F', 22, 'ProveedorService.UpdateProveedorBanco'),
	(139, '2021-11-16 14:58:35', 1, 'Alta', '{"Id":24,"Nombre":"","Cuenta":"","ProveedorId":17,"CuentaTipoId":1,"MonedaId":2,"Proveedor":null,"CuentaTipo":null,"Moneda":null}', 'ProveedorBanco', 24, 'ProveedorService.UpdateProveedorBanco'),
	(140, '2021-11-16 14:59:29', 1, 'Alta', '{"Id":38,"Nombre":"yiyiyñ","RazonSocial":"yiñyiñyiñiñiñ","RUT":"ñiñiñiñi","Direccion":"rtuyrtu","Ciudad":"ryuruii","Telefono":"57775757575","Correo":null,"Contacto":null,"ContactoTelefono":null,"ContactoCorreo":null,"Activo":true}', 'Proveedor', 38, 'ProveedorService.Add'),
	(141, '2021-11-16 14:59:44', 1, 'Alta', '{"Id":25,"Nombre":"654684694","Cuenta":"56465416541","ProveedorId":38,"CuentaTipoId":2,"MonedaId":3,"Proveedor":null,"CuentaTipo":null,"Moneda":null}', 'ProveedorBanco', 25, 'ProveedorService.UpdateProveedorBanco'),
	(142, '2021-11-16 15:00:24', 1, 'Baja', '{"Cliente":null,"Cuotas":null,"Id":251,"ClienteId":107,"MontoTotal":166.66666666666666,"Fecha":"2021-12-28T12:31:05"}', 'PagoCliente_DEF9C166ACD92F1B5A1382FF6C2F328712B80A', 251, 'PagoClienteService.DeletePagoCliente');
/*!40000 ALTER TABLE `auditoria` ENABLE KEYS */;

-- Dumping structure for table dblocal.aviso_pago
CREATE TABLE IF NOT EXISTS `aviso_pago` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `FechaImpresion` datetime NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=1441 DEFAULT CHARSET=utf8mb3;

-- Dumping data for table dblocal.aviso_pago: ~0 rows (approximately)
/*!40000 ALTER TABLE `aviso_pago` DISABLE KEYS */;
/*!40000 ALTER TABLE `aviso_pago` ENABLE KEYS */;

-- Dumping structure for table dblocal.categoria
CREATE TABLE IF NOT EXISTS `categoria` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `Descripcion` varchar(100) NOT NULL,
  `Activo` tinyint NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Descripcion` (`Descripcion`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb3;

-- Dumping data for table dblocal.categoria: ~16 rows (approximately)
/*!40000 ALTER TABLE `categoria` DISABLE KEYS */;
INSERT INTO `categoria` (`Id`, `Descripcion`, `Activo`) VALUES
	(14, 'Categoria 1234ABC', 1);
/*!40000 ALTER TABLE `categoria` ENABLE KEYS */;

-- Dumping structure for table dblocal.cierre_mes
CREATE TABLE IF NOT EXISTS `cierre_mes` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `Fecha` datetime NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=330 DEFAULT CHARSET=utf8mb3;

-- Dumping data for table dblocal.cierre_mes: ~10 rows (approximately)
/*!40000 ALTER TABLE `cierre_mes` DISABLE KEYS */;
INSERT INTO `cierre_mes` (`Id`, `Fecha`) VALUES
	(207, '2021-06-30 00:00:00'),
	(208, '2021-07-30 14:40:43'),
	(209, '2021-08-30 14:40:43'),
	(326, '2021-12-28 12:20:24'),
	(327, '2022-01-28 12:31:45'),
	(328, '2022-02-28 12:32:00'),
	(329, '2022-03-28 12:32:45');
/*!40000 ALTER TABLE `cierre_mes` ENABLE KEYS */;

-- Dumping structure for table dblocal.ciudad
CREATE TABLE IF NOT EXISTS `ciudad` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `Descripcion` varchar(100) NOT NULL,
  `Activo` tinyint NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Descripcion` (`Descripcion`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb3;

-- Dumping data for table dblocal.ciudad: ~11 rows (approximately)
/*!40000 ALTER TABLE `ciudad` DISABLE KEYS */;
INSERT INTO `ciudad` (`Id`, `Descripcion`, `Activo`) VALUES
	(1, 'MONTEVIDEO', 1),
	(6, 'Tacuarembó', 1),
	(8, 'Canelones', 1),
	(9, 'Minas', 1),
	(10, 'Florida', 1);
/*!40000 ALTER TABLE `ciudad` ENABLE KEYS */;

-- Dumping structure for table dblocal.cliente
CREATE TABLE IF NOT EXISTS `cliente` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `Identificador` varchar(50) NOT NULL,
  `Nombre` varchar(50) NOT NULL,
  `Apellido` varchar(50) DEFAULT NULL,
  `Telefono` varchar(50) DEFAULT NULL,
  `FechaNacimiento` date DEFAULT NULL,
  `Direccion` varchar(100) DEFAULT NULL,
  `CiudadId` int DEFAULT NULL,
  `ZonaId` int DEFAULT NULL,
  `Activo` tinyint NOT NULL,
  `Hte` varchar(50) DEFAULT NULL,
  `EstadoId` int DEFAULT NULL,
  `Servicio` tinyint DEFAULT NULL,
  `FechaHasta` date DEFAULT NULL,
  `ClienteViejo` tinyint DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Identificador` (`Identificador`),
  KEY `FK_cliente_ciudad` (`CiudadId`),
  KEY `FK_cliente_zona` (`ZonaId`),
  KEY `FK_cliente_estado_actual_cliente` (`EstadoId`),
  CONSTRAINT `FK_cliente_ciudad` FOREIGN KEY (`CiudadId`) REFERENCES `ciudad` (`Id`),
  CONSTRAINT `FK_cliente_estado_actual_cliente` FOREIGN KEY (`EstadoId`) REFERENCES `estado_cliente` (`Id`),
  CONSTRAINT `FK_cliente_zona` FOREIGN KEY (`ZonaId`) REFERENCES `zona` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=utf8mb3;

-- Dumping data for table dblocal.cliente: ~0 rows (approximately)
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;
/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;

-- Dumping structure for table dblocal.cliente_estado
CREATE TABLE IF NOT EXISTS `cliente_estado` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `ClienteId` int NOT NULL,
  `EstadoClienteId` int NOT NULL,
  `Desde` datetime NOT NULL,
  `Activo` tinyint NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_cliente_estado_cliente` (`ClienteId`),
  KEY `FK_cliente_estado_estado_cliente` (`EstadoClienteId`),
  CONSTRAINT `FK_cliente_estado_cliente` FOREIGN KEY (`ClienteId`) REFERENCES `cliente` (`Id`),
  CONSTRAINT `FK_cliente_estado_estado_cliente` FOREIGN KEY (`EstadoClienteId`) REFERENCES `estado_cliente` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=244 DEFAULT CHARSET=utf8mb3;

-- Dumping data for table dblocal.cliente_estado: ~0 rows (approximately)
/*!40000 ALTER TABLE `cliente_estado` DISABLE KEYS */;
/*!40000 ALTER TABLE `cliente_estado` ENABLE KEYS */;

-- Dumping structure for table dblocal.cliente_producto
CREATE TABLE IF NOT EXISTS `cliente_producto` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `ClienteId` int NOT NULL,
  `Desde` datetime DEFAULT NULL,
  `Hasta` datetime DEFAULT NULL,
  `Activo` tinyint NOT NULL,
  `MetodoPagoId` int NOT NULL,
  `ProductoPrecioId` int DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_cliente_producto_cliente` (`ClienteId`),
  KEY `FK_cliente_producto_metodo_pago` (`MetodoPagoId`),
  KEY `FK_cliente_producto_producto_precio` (`ProductoPrecioId`),
  CONSTRAINT `FK_cliente_producto_cliente` FOREIGN KEY (`ClienteId`) REFERENCES `cliente` (`Id`),
  CONSTRAINT `FK_cliente_producto_metodo_pago` FOREIGN KEY (`MetodoPagoId`) REFERENCES `metodo_pago` (`Id`),
  CONSTRAINT `FK_cliente_producto_producto_precio` FOREIGN KEY (`ProductoPrecioId`) REFERENCES `producto_precio` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=338 DEFAULT CHARSET=utf8mb3;

-- Dumping data for table dblocal.cliente_producto: ~2 rows (approximately)
/*!40000 ALTER TABLE `cliente_producto` DISABLE KEYS */;
/*!40000 ALTER TABLE `cliente_producto` ENABLE KEYS */;

-- Dumping structure for table dblocal.cliente_producto_deuda
CREATE TABLE IF NOT EXISTS `cliente_producto_deuda` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `ClienteProductoId` int NOT NULL,
  `Cuota` int NOT NULL,
  `FechaPago` datetime DEFAULT NULL,
  `Importe` double DEFAULT NULL,
  `CierreMesId` int DEFAULT NULL,
  `MoraCuotaId` int DEFAULT NULL,
  `Activo` tinyint DEFAULT NULL,
  `PagoClienteId` int DEFAULT NULL,
  `AvisoPagoId` int DEFAULT NULL,
  `BajaManual` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_cliente_producto_deuda_cliente_producto` (`ClienteProductoId`),
  KEY `FK_cliente_producto_deuda_cierre_mes` (`CierreMesId`),
  KEY `FK_cliente_producto_deuda_mora_cuota` (`MoraCuotaId`),
  KEY `FK_cliente_producto_deuda_pago_cliente` (`PagoClienteId`),
  KEY `FK_cliente_producto_deuda_aviso_pago` (`AvisoPagoId`),
  CONSTRAINT `FK_cliente_producto_deuda_aviso_pago` FOREIGN KEY (`AvisoPagoId`) REFERENCES `aviso_pago` (`Id`),
  CONSTRAINT `FK_cliente_producto_deuda_cierre_mes` FOREIGN KEY (`CierreMesId`) REFERENCES `cierre_mes` (`Id`),
  CONSTRAINT `FK_cliente_producto_deuda_cliente_producto` FOREIGN KEY (`ClienteProductoId`) REFERENCES `cliente_producto` (`Id`),
  CONSTRAINT `FK_cliente_producto_deuda_mora_cuota` FOREIGN KEY (`MoraCuotaId`) REFERENCES `mora_cuota` (`Id`),
  CONSTRAINT `FK_cliente_producto_deuda_pago_cliente` FOREIGN KEY (`PagoClienteId`) REFERENCES `pago_cliente` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb3;

-- Dumping data for table dblocal.cliente_producto_deuda: ~12 rows (approximately)
/*!40000 ALTER TABLE `cliente_producto_deuda` DISABLE KEYS */;
/*!40000 ALTER TABLE `cliente_producto_deuda` ENABLE KEYS */;

-- Dumping structure for table dblocal.cliente_producto_item
CREATE TABLE IF NOT EXISTS `cliente_producto_item` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `ClienteProductoId` int NOT NULL,
  `ItemId` int NOT NULL,
  `Valor` varchar(100) NOT NULL,
  `Activo` tinyint NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK__cliente_producto` (`ClienteProductoId`),
  KEY `FK__item_categoria` (`ItemId`) USING BTREE,
  CONSTRAINT `FK__cliente_producto` FOREIGN KEY (`ClienteProductoId`) REFERENCES `cliente_producto` (`Id`),
  CONSTRAINT `FK__item` FOREIGN KEY (`ItemId`) REFERENCES `item` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=325 DEFAULT CHARSET=utf8mb3;

-- Dumping data for table dblocal.cliente_producto_item: ~0 rows (approximately)
/*!40000 ALTER TABLE `cliente_producto_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `cliente_producto_item` ENABLE KEYS */;

-- Dumping structure for table dblocal.cuenta_tipo
CREATE TABLE IF NOT EXISTS `cuenta_tipo` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `Descripcion` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table dblocal.cuenta_tipo: ~2 rows (approximately)
/*!40000 ALTER TABLE `cuenta_tipo` DISABLE KEYS */;
INSERT INTO `cuenta_tipo` (`Id`, `Descripcion`) VALUES
	(1, 'CA'),
	(2, 'CC');
/*!40000 ALTER TABLE `cuenta_tipo` ENABLE KEYS */;

-- Dumping structure for table dblocal.egreso
CREATE TABLE IF NOT EXISTS `egreso` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `MonedaId` int NOT NULL,
  `Importe` varchar(50) NOT NULL,
  `CategoriaId` int NOT NULL,
  `Activo` tinyint DEFAULT NULL,
  `ProveedorId` int NOT NULL,
  `EsCheque` tinyint DEFAULT NULL,
  `FechaCheque` datetime DEFAULT NULL,
  `FechaEgreso` datetime DEFAULT NULL,
  `Descripcion` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_egreso_moneda` (`MonedaId`),
  KEY `FK_egreso_proveedor_categoria` (`CategoriaId`),
  KEY `FK_egreso_proveedor` (`ProveedorId`),
  CONSTRAINT `FK_egreso_moneda` FOREIGN KEY (`MonedaId`) REFERENCES `moneda` (`Id`),
  CONSTRAINT `FK_egreso_proveedor` FOREIGN KEY (`ProveedorId`) REFERENCES `proveedor` (`Id`),
  CONSTRAINT `FK_egreso_proveedor_categoria` FOREIGN KEY (`CategoriaId`) REFERENCES `categoria` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8mb3;

-- Dumping data for table dblocal.egreso: ~18 rows (approximately)
/*!40000 ALTER TABLE `egreso` DISABLE KEYS */;
/*!40000 ALTER TABLE `egreso` ENABLE KEYS */;

-- Dumping structure for table dblocal.estado_cliente
CREATE TABLE IF NOT EXISTS `estado_cliente` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `Descripcion` varchar(50) NOT NULL DEFAULT '',
  `Tipo` tinyint NOT NULL,
  `Activo` tinyint NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Descripcion` (`Descripcion`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb3;

-- Dumping data for table dblocal.estado_cliente: ~11 rows (approximately)
/*!40000 ALTER TABLE `estado_cliente` DISABLE KEYS */;
INSERT INTO `estado_cliente` (`Id`, `Descripcion`, `Tipo`, `Activo`) VALUES
	(4, 'BAJA', 1, 1),
	(5, 'FALLECIMIENTO', 1, 1),
	(6, 'DEUDOR', 2, 1),
	(7, 'MUDANZA', 3, 1),
	(8, 'ACTIVO', 1, 1),
	(9, 'SIN JUSTIFICAR', 1, 1),
	(10, 'DTV', 1, 1),
	(11, 'PLATAFORMAS', 1, 1),
	(12, 'FALLA DEL SERVICIO', 1, 1),
	(13, 'DECISION TEVECA', 1, 1),
	(14, 'OTRA', 1, 1);
/*!40000 ALTER TABLE `estado_cliente` ENABLE KEYS */;

-- Dumping structure for table dblocal.impuesto
CREATE TABLE IF NOT EXISTS `impuesto` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `Descripcion` varchar(100) NOT NULL,
  `Porcentaje` double NOT NULL,
  `Activo` tinyint NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Descripcion` (`Descripcion`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb3;

-- Dumping data for table dblocal.impuesto: ~9 rows (approximately)
/*!40000 ALTER TABLE `impuesto` DISABLE KEYS */;
INSERT INTO `impuesto` (`Id`, `Descripcion`, `Porcentaje`, `Activo`) VALUES
	(1, 'IVA', 22, 1);
/*!40000 ALTER TABLE `impuesto` ENABLE KEYS */;

-- Dumping structure for table dblocal.item
CREATE TABLE IF NOT EXISTS `item` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `Descripcion` varchar(100) NOT NULL,
  `Activo` tinyint NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Descripcion` (`Descripcion`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb3;

-- Dumping data for table dblocal.item: ~12 rows (approximately)
/*!40000 ALTER TABLE `item` DISABLE KEYS */;
INSERT INTO `item` (`Id`, `Descripcion`, `Activo`) VALUES
	(1, 'Decodificadoes', 0);
/*!40000 ALTER TABLE `item` ENABLE KEYS */;

-- Dumping structure for table dblocal.marca
CREATE TABLE IF NOT EXISTS `marca` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `Descripcion` varchar(100) NOT NULL,
  `Activo` tinyint NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Descripcion` (`Descripcion`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb3;

-- Dumping data for table dblocal.marca: ~9 rows (approximately)
/*!40000 ALTER TABLE `marca` DISABLE KEYS */;
INSERT INTO `marca` (`Id`, `Descripcion`, `Activo`) VALUES
	(1, 'marca', 0),
	(2, 'marca3', 0),
	(3, 'marcas', 0),
	(4, 'Samsung', 0),
	(5, 'Xiaomi A', 0),
	(6, 'Mac1', 0),
	(7, 'asdasd', 0),
	(8, 'Panasonic', 1),
	(9, 'Salus', 1);
/*!40000 ALTER TABLE `marca` ENABLE KEYS */;

-- Dumping structure for table dblocal.metodo_pago
CREATE TABLE IF NOT EXISTS `metodo_pago` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `Descripcion` varchar(100) NOT NULL,
  `Cantidad` int NOT NULL,
  `Activo` tinyint NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb3;

-- Dumping data for table dblocal.metodo_pago: ~14 rows (approximately)
/*!40000 ALTER TABLE `metodo_pago` DISABLE KEYS */;
/*!40000 ALTER TABLE `metodo_pago` ENABLE KEYS */;

-- Dumping structure for table dblocal.moneda
CREATE TABLE IF NOT EXISTS `moneda` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `Descripcion` varchar(50) NOT NULL,
  `Activo` tinyint NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Descripcion` (`Descripcion`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3;

-- Dumping data for table dblocal.moneda: ~4 rows (approximately)
/*!40000 ALTER TABLE `moneda` DISABLE KEYS */;
INSERT INTO `moneda` (`Id`, `Descripcion`, `Activo`) VALUES
	(1, 'USD', 0),
	(2, 'UY', 0),
	(3, '$', 1),
	(4, 'EU', 0);
/*!40000 ALTER TABLE `moneda` ENABLE KEYS */;

-- Dumping structure for table dblocal.mora_cuota
CREATE TABLE IF NOT EXISTS `mora_cuota` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `Porcentaje` int DEFAULT NULL,
  `Descripcion` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3;

-- Dumping data for table dblocal.mora_cuota: ~3 rows (approximately)
/*!40000 ALTER TABLE `mora_cuota` DISABLE KEYS */;
INSERT INTO `mora_cuota` (`Id`, `Porcentaje`, `Descripcion`) VALUES
	(4, 10, 'Primer mes impago'),
	(5, 20, 'Segundo mes impago'),
	(6, 1, '3er mes impago');
/*!40000 ALTER TABLE `mora_cuota` ENABLE KEYS */;

-- Dumping structure for table dblocal.pago_cliente
CREATE TABLE IF NOT EXISTS `pago_cliente` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `ClienteId` int NOT NULL,
  `MontoTotal` double NOT NULL,
  `Fecha` datetime NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_pago_cliente_cliente` (`ClienteId`),
  CONSTRAINT `FK_pago_cliente_cliente` FOREIGN KEY (`ClienteId`) REFERENCES `cliente` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=256 DEFAULT CHARSET=utf8mb3;

-- Dumping data for table dblocal.pago_cliente: ~5 rows (approximately)
/*!40000 ALTER TABLE `pago_cliente` DISABLE KEYS */;
/*!40000 ALTER TABLE `pago_cliente` ENABLE KEYS */;

-- Dumping structure for table dblocal.parametro
CREATE TABLE IF NOT EXISTS `parametro` (
  `Codigo` varchar(50) NOT NULL,
  `Valor` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  PRIMARY KEY (`Codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- Dumping data for table dblocal.parametro: ~19 rows (approximately)
/*!40000 ALTER TABLE `parametro` DISABLE KEYS */;
INSERT INTO `parametro` (`Codigo`, `Valor`) VALUES
	('COMENTARIO_COMPROBANTE_PAGO', ''),
	('EMAIL_ACTIVE', 'True'),
	('EMAIL_ACTIVECOPY', 'False'),
	('EMAIL_HOST', ''),
	('EMAIL_NAME', ''),
	('EMAIL_PASSWORD', ''),
	('EMAIL_PORT', '465'),
	('EMAIL_SSL', 'true'),
	('EMAIL_TOEMAILCOPYADDRESS', ''),
	('EMAIL_TOEMAILCOPYNAME', ''),
	('EMAIL_USER', ''),
	('FECHA_CHECK_FIBRA', '22/10/2021'),
	('TITULO_COMPROBANTE_PAGO', ''),
	('URL_FRONT', 'http://localhost:4200/'),
	('USUARIOS_REGISTRO_EMAIL_BODY', '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">\r\n<html xmlns="http://www.w3.org/1999/xhtml">\r\n<head>\r\n  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />\r\n  <meta name="viewport" content="width=device-width, initial-scale=1" />\r\n  <title>Oxygen Welcome</title>\r\n\r\n  <style type="text/css">\r\n    /* Take care of image borders and formatting, client hacks */\r\n    img { max-width: 600px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;}\r\n    a img { border: none; }\r\n    table { border-collapse: collapse !important;}\r\n    #outlook a { padding:0; }\r\n    .ReadMsgBody { width: 100%; }\r\n    .ExternalClass { width: 100%; }\r\n    .backgroundTable { margin: 0 auto; padding: 0; width: 100% !important; }\r\n    table td { border-collapse: collapse; }\r\n    .ExternalClass * { line-height: 115%; }\r\n    .container-for-gmail-android { min-width: 600px; }\r\n\r\n\r\n    /* General styling */\r\n    * {\r\n      font-family: Helvetica, Arial, sans-serif;\r\n    }\r\n\r\n    body {\r\n      -webkit-font-smoothing: antialiased;\r\n      -webkit-text-size-adjust: none;\r\n      width: 100% !important;\r\n      margin: 0 !important;\r\n      height: 100%;\r\n      color: #676767;\r\n    }\r\n\r\n    td {\r\n      font-family: Helvetica, Arial, sans-serif;\r\n      font-size: 14px;\r\n      color: #777777;\r\n      text-align: center;\r\n      line-height: 21px;\r\n    }\r\n\r\n    a {\r\n      color: #676767;\r\n      text-decoration: none !important;\r\n    }\r\n\r\n    .pull-left {\r\n      text-align: left;\r\n    }\r\n\r\n    .pull-right {\r\n      text-align: right;\r\n    }\r\n\r\n    .header-lg,\r\n    .header-md,\r\n    .header-sm {\r\n      font-size: 28px;\r\n      font-weight: 700;\r\n      line-height: normal;\r\n      padding: 35px 0 0;\r\n      color: #4d4d4d;\r\n    }\r\n\r\n    .header-md {\r\n      font-size: 24px;\r\n    }\r\n\r\n    .header-sm {\r\n      padding: 5px 0;\r\n      font-size: 18px;\r\n      line-height: 1.3;\r\n    }\r\n\r\n    .content-padding {\r\n      padding: 20px 0 30px;\r\n    }\r\n\r\n    .mobile-header-padding-right {\r\n      width: 290px;\r\n      text-align: right;\r\n      padding-left: 10px;\r\n    }\r\n\r\n    .mobile-header-padding-left {\r\n      width: 290px;\r\n      text-align: left;\r\n      padding-left: 10px;\r\n    }\r\n\r\n    .free-text {\r\n      width: 100% !important;\r\n      padding: 10px 60px 0px;\r\n    }\r\n\r\n    .block-rounded {\r\n      border-radius: 5px;\r\n      border: 1px solid #e5e5e5;\r\n      vertical-align: top;\r\n    }\r\n\r\n    .button {\r\n      padding: 30px 0;\r\n    }\r\n\r\n    .info-block {\r\n      padding: 0 20px;\r\n      width: 260px;\r\n    }\r\n\r\n    .block-rounded {\r\n      width: 260px;\r\n    }\r\n\r\n    .info-img {\r\n      width: 258px;\r\n      border-radius: 5px 5px 0 0;\r\n    }\r\n\r\n    .force-width-gmail {\r\n      min-width:600px;\r\n      height: 0px !important;\r\n      line-height: 1px !important;\r\n      font-size: 1px !important;\r\n    }\r\n\r\n    .button-width {\r\n      width: 228px;\r\n    }\r\n\r\n  </style>\r\n\r\n  <style type="text/css" media="screen">\r\n    @import url(http://fonts.googleapis.com/css?family=Oxygen:400,700);\r\n  </style>\r\n\r\n  <style type="text/css" media="screen">\r\n    @media screen {\r\n      /* Thanks Outlook 2013! */\r\n      * {\r\n        font-family: \'Oxygen\', \'Helvetica Neue\', \'Arial\', \'sans-serif\' !important;\r\n      }\r\n    }\r\n  </style>\r\n\r\n  <style type="text/css" media="only screen and (max-width: 480px)">\r\n    /* Mobile styles */\r\n    @media only screen and (max-width: 480px) {\r\n\r\n      table[class*="container-for-gmail-android"] {\r\n        min-width: 290px !important;\r\n        width: 100% !important;\r\n      }\r\n\r\n      table[class="w320"] {\r\n        width: 320px !important;\r\n      }\r\n\r\n      img[class="force-width-gmail"] {\r\n        display: none !important;\r\n        width: 0 !important;\r\n        height: 0 !important;\r\n      }\r\n\r\n      a[class="button-width"],\r\n      a[class="button-mobile"] {\r\n        width: 248px !important;\r\n      }\r\n\r\n      td[class*="mobile-header-padding-left"] {\r\n        width: 160px !important;\r\n        padding-left: 0 !important;\r\n      }\r\n\r\n      td[class*="mobile-header-padding-right"] {\r\n        width: 160px !important;\r\n        padding-right: 0 !important;\r\n      }\r\n\r\n      td[class="header-lg"] {\r\n        font-size: 24px !important;\r\n        padding-bottom: 5px !important;\r\n      }\r\n\r\n      td[class="header-md"] {\r\n        font-size: 18px !important;\r\n        padding-bottom: 5px !important;\r\n      }\r\n\r\n      td[class="content-padding"] {\r\n        padding: 5px 0 30px !important;\r\n      }\r\n\r\n       td[class="button"] {\r\n        padding: 5px !important;\r\n      }\r\n\r\n      td[class*="free-text"] {\r\n        padding: 10px 18px 30px !important;\r\n      }\r\n\r\n      td[class="info-block"] {\r\n        display: block !important;\r\n        width: 280px !important;\r\n        padding-bottom: 40px !important;\r\n      }\r\n\r\n      td[class="info-img"],\r\n      img[class="info-img"] {\r\n        width: 278px !important;\r\n      }\r\n    }\r\n  </style>\r\n</head>\r\n\r\n<body bgcolor="#f7f7f7">\r\n<table align="center" cellpadding="0" cellspacing="0" class="container-for-gmail-android" width="100%">\r\n  <tr>\r\n    <td align="left" valign="top" width="100%" style="background:repeat-x url(http://s3.amazonaws.com/swu-filepicker/4E687TRe69Ld95IDWyEg_bg_top_02.jpg) #ffffff;">\r\n      <center>\r\n     \r\n        <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#ffffff" background="http://s3.amazonaws.com/swu-filepicker/4E687TRe69Ld95IDWyEg_bg_top_02.jpg" style="background-color:transparent">\r\n        \r\n  <tr>\r\n    <td align="center" valign="top" width="100%" style="background-color: #f7f7f7;" class="content-padding">\r\n      <center>\r\n        <table cellspacing="0" cellpadding="0" width="600" class="w320">\r\n          <tr>\r\n            <td class="header-lg">\r\n               \r\n            </td>\r\n          </tr>\r\n          <tr>\r\n            <td>\r\n              Estimado <strong> $Nombre </strong>,\r\n            </td>\r\n          </tr>\r\n          <tr>\r\n            <td class="free-text">\r\n                Bienvenid@ al sistema de gestión \r\n                Su nombre de usuario es: <strong> $Username .</strong>\r\n                A continuación podrá registrar su contraseña en el siguiente enlace\r\n                \r\n            </td>\r\n          </tr>\r\n          <tr>\r\n            <td class="button">\r\n              <div><!--[if mso]>\r\n                <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="$URL" style="height:45px;v-text-anchor:middle;width:155px;" arcsize="15%" strokecolor="#ffffff" fillcolor="#ff6f6f">\r\n                  <w:anchorlock/>\r\n                  <center style="color:#ffffff;font-family:Helvetica, Arial, sans-serif;font-size:14px;font-weight:regular;">Registrar contraseña</center>\r\n                </v:roundrect>\r\n              <![endif]--><a class="button-mobile" href="$URL"\r\n              style="background-color:#ff6f6f;border-radius:5px;color:#ffffff;display:inline-block;font-family:\'Cabin\', Helvetica, Arial, sans-serif;font-size:14px;font-weight:regular;line-height:45px;text-align:center;text-decoration:none;width:155px;-webkit-text-size-adjust:none;mso-hide:all;">Registrar contraseña</a></div>\r\n            </td>\r\n          </tr>\r\n        </table>\r\n      </center>\r\n    </td>\r\n  </tr>\r\n\r\n\r\n</table>\r\n</body>\r\n\r\n</html> \r\n'),
	('USUARIOS_REGISTRO_EMAIL_SUBJECT', ' Registro de usuario'),
	('USUARIOS_REINICIO_EMAIL_BODY', '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">\r\n<html xmlns="http://www.w3.org/1999/xhtml">\r\n<head>\r\n  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />\r\n  <meta name="viewport" content="width=device-width, initial-scale=1" />\r\n  <title>Oxygen Welcome</title>\r\n\r\n  <style type="text/css">\r\n    /* Take care of image borders and formatting, client hacks */\r\n    img { max-width: 600px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;}\r\n    a img { border: none; }\r\n    table { border-collapse: collapse !important;}\r\n    #outlook a { padding:0; }\r\n    .ReadMsgBody { width: 100%; }\r\n    .ExternalClass { width: 100%; }\r\n    .backgroundTable { margin: 0 auto; padding: 0; width: 100% !important; }\r\n    table td { border-collapse: collapse; }\r\n    .ExternalClass * { line-height: 115%; }\r\n    .container-for-gmail-android { min-width: 600px; }\r\n\r\n\r\n    /* General styling */\r\n    * {\r\n      font-family: Helvetica, Arial, sans-serif;\r\n    }\r\n\r\n    body {\r\n      -webkit-font-smoothing: antialiased;\r\n      -webkit-text-size-adjust: none;\r\n      width: 100% !important;\r\n      margin: 0 !important;\r\n      height: 100%;\r\n      color: #676767;\r\n    }\r\n\r\n    td {\r\n      font-family: Helvetica, Arial, sans-serif;\r\n      font-size: 14px;\r\n      color: #777777;\r\n      text-align: center;\r\n      line-height: 21px;\r\n    }\r\n\r\n    a {\r\n      color: #676767;\r\n      text-decoration: none !important;\r\n    }\r\n\r\n    .pull-left {\r\n      text-align: left;\r\n    }\r\n\r\n    .pull-right {\r\n      text-align: right;\r\n    }\r\n\r\n    .header-lg,\r\n    .header-md,\r\n    .header-sm {\r\n      font-size: 28px;\r\n      font-weight: 700;\r\n      line-height: normal;\r\n      padding: 35px 0 0;\r\n      color: #4d4d4d;\r\n    }\r\n\r\n    .header-md {\r\n      font-size: 24px;\r\n    }\r\n\r\n    .header-sm {\r\n      padding: 5px 0;\r\n      font-size: 18px;\r\n      line-height: 1.3;\r\n    }\r\n\r\n    .content-padding {\r\n      padding: 20px 0 30px;\r\n    }\r\n\r\n    .mobile-header-padding-right {\r\n      width: 290px;\r\n      text-align: right;\r\n      padding-left: 10px;\r\n    }\r\n\r\n    .mobile-header-padding-left {\r\n      width: 290px;\r\n      text-align: left;\r\n      padding-left: 10px;\r\n    }\r\n\r\n    .free-text {\r\n      width: 100% !important;\r\n      padding: 10px 60px 0px;\r\n    }\r\n\r\n    .block-rounded {\r\n      border-radius: 5px;\r\n      border: 1px solid #e5e5e5;\r\n      vertical-align: top;\r\n    }\r\n\r\n    .button {\r\n      padding: 30px 0;\r\n    }\r\n\r\n    .info-block {\r\n      padding: 0 20px;\r\n      width: 260px;\r\n    }\r\n\r\n    .block-rounded {\r\n      width: 260px;\r\n    }\r\n\r\n    .info-img {\r\n      width: 258px;\r\n      border-radius: 5px 5px 0 0;\r\n    }\r\n\r\n    .force-width-gmail {\r\n      min-width:600px;\r\n      height: 0px !important;\r\n      line-height: 1px !important;\r\n      font-size: 1px !important;\r\n    }\r\n\r\n    .button-width {\r\n      width: 228px;\r\n    }\r\n\r\n  </style>\r\n\r\n  <style type="text/css" media="screen">\r\n    @import url(http://fonts.googleapis.com/css?family=Oxygen:400,700);\r\n  </style>\r\n\r\n  <style type="text/css" media="screen">\r\n    @media screen {\r\n      /* Thanks Outlook 2013! */\r\n      * {\r\n        font-family: \'Oxygen\', \'Helvetica Neue\', \'Arial\', \'sans-serif\' !important;\r\n      }\r\n    }\r\n  </style>\r\n\r\n  <style type="text/css" media="only screen and (max-width: 480px)">\r\n    /* Mobile styles */\r\n    @media only screen and (max-width: 480px) {\r\n\r\n      table[class*="container-for-gmail-android"] {\r\n        min-width: 290px !important;\r\n        width: 100% !important;\r\n      }\r\n\r\n      table[class="w320"] {\r\n        width: 320px !important;\r\n      }\r\n\r\n      img[class="force-width-gmail"] {\r\n        display: none !important;\r\n        width: 0 !important;\r\n        height: 0 !important;\r\n      }\r\n\r\n      a[class="button-width"],\r\n      a[class="button-mobile"] {\r\n        width: 248px !important;\r\n      }\r\n\r\n      td[class*="mobile-header-padding-left"] {\r\n        width: 160px !important;\r\n        padding-left: 0 !important;\r\n      }\r\n\r\n      td[class*="mobile-header-padding-right"] {\r\n        width: 160px !important;\r\n        padding-right: 0 !important;\r\n      }\r\n\r\n      td[class="header-lg"] {\r\n        font-size: 24px !important;\r\n        padding-bottom: 5px !important;\r\n      }\r\n\r\n      td[class="header-md"] {\r\n        font-size: 18px !important;\r\n        padding-bottom: 5px !important;\r\n      }\r\n\r\n      td[class="content-padding"] {\r\n        padding: 5px 0 30px !important;\r\n      }\r\n\r\n       td[class="button"] {\r\n        padding: 5px !important;\r\n      }\r\n\r\n      td[class*="free-text"] {\r\n        padding: 10px 18px 30px !important;\r\n      }\r\n\r\n      td[class="info-block"] {\r\n        display: block !important;\r\n        width: 280px !important;\r\n        padding-bottom: 40px !important;\r\n      }\r\n\r\n      td[class="info-img"],\r\n      img[class="info-img"] {\r\n        width: 278px !important;\r\n      }\r\n    }\r\n  </style>\r\n</head>\r\n<div>\r\n<body bgcolor="#f7f7f7">\r\n<table align="center" cellpadding="0" cellspacing="0" class="container-for-gmail-android" width="100%">\r\n  <tr>\r\n    <td align="left" valign="top" width="100%" style="background:repeat-x url(http://s3.amazonaws.com/swu-filepicker/4E687TRe69Ld95IDWyEg_bg_top_02.jpg) #ffffff;">\r\n      <center>\r\n     \r\n        <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#ffffff" background="http://s3.amazonaws.com/swu-filepicker/4E687TRe69Ld95IDWyEg_bg_top_02.jpg" style="background-color:transparent">\r\n        \r\n  <tr>\r\n    <td align="center" valign="top" width="100%" style="background-color: #f7f7f7;" class="content-padding">\r\n      <center>\r\n        <table cellspacing="0" cellpadding="0" width="600" class="w320">\r\n          <tr>\r\n            <td class="header-lg">\r\n               Teveca\r\n            </td>\r\n          </tr>\r\n          <tr>\r\n            <td>\r\n              Estimado $Nombre ,\r\n            </td>\r\n          </tr>\r\n          <tr>\r\n            <td class="free-text">\r\n                Bienvenid@ al sistema de gestión .\r\n                Su nombre de usuario es: <strong> $Username . </strong>\r\n                A continuación podrá reiniciar su contraseña en el siguiente enlace\r\n                \r\n            </td>\r\n          </tr>\r\n          <tr>\r\n            <td class="button">\r\n              <div><!--[if mso]>\r\n                <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="$URL" style="height:45px;v-text-anchor:middle;width:155px;" arcsize="15%" strokecolor="#ffffff" fillcolor="#ff6f6f">\r\n                  <w:anchorlock/>\r\n                  <center style="color:#ffffff;font-family:Helvetica, Arial, sans-serif;font-size:14px;font-weight:regular;">Reiniciar contraseña</center>\r\n                </v:roundrect>\r\n              <![endif]--><a class="button-mobile" href="$URL"\r\n              style="background-color:#ff6f6f;border-radius:5px;color:#ffffff;display:inline-block;font-family:\'Cabin\', Helvetica, Arial, sans-serif;font-size:14px;font-weight:regular;line-height:45px;text-align:center;text-decoration:none;width:155px;-webkit-text-size-adjust:none;mso-hide:all;">Reiniciar contraseña</a></div>\r\n            </td>\r\n          </tr>\r\n        </table>\r\n      </center>\r\n    </td>\r\n  </tr>\r\n\r\n  \r\n</table>\r\n</body>\r\n</div>\r\n\r\n</html> \r\n'),
	('USUARIOS_REINICIO_EMAIL_SUBJECT', ' Reinicio de contraseña'),
	('VENCIMIENTO_MES', '28/04/2022');
/*!40000 ALTER TABLE `parametro` ENABLE KEYS */;

-- Dumping structure for table dblocal.permisorols
CREATE TABLE IF NOT EXISTS `permisorols` (
  `Permiso_Id` int NOT NULL,
  `Rol_Id` int NOT NULL,
  PRIMARY KEY (`Permiso_Id`,`Rol_Id`),
  KEY `Permiso_Roles_Target` (`Rol_Id`),
  CONSTRAINT `FK_permisorols_permisos` FOREIGN KEY (`Permiso_Id`) REFERENCES `permisos` (`Id`),
  CONSTRAINT `FK_permisorols_rol` FOREIGN KEY (`Rol_Id`) REFERENCES `rol` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- Dumping data for table dblocal.permisorols: ~117 rows (approximately)
/*!40000 ALTER TABLE `permisorols` DISABLE KEYS */;
INSERT INTO `permisorols` (`Permiso_Id`, `Rol_Id`) VALUES
	(2, 1),
	(3, 1),
	(4, 1),
	(15, 1),
	(16, 1),
	(1, 3),
	(2, 3),
	(3, 3),
	(4, 3),
	(5, 3),
	(6, 3),
	(7, 3),
	(8, 3),
	(9, 3),
	(10, 3),
	(11, 3),
	(12, 3),
	(13, 3),
	(14, 3),
	(15, 3),
	(16, 3),
	(17, 3),
	(18, 3),
	(19, 3),
	(20, 3),
	(21, 3),
	(22, 3),
	(23, 3),
	(24, 3),
	(25, 3),
	(26, 3),
	(27, 3),
	(28, 3),
	(29, 3),
	(30, 3),
	(31, 3),
	(32, 3),
	(33, 3),
	(34, 3),
	(35, 3),
	(36, 3),
	(37, 3),
	(38, 3),
	(39, 3),
	(40, 3),
	(41, 3),
	(42, 3),
	(43, 3),
	(44, 3),
	(45, 3),
	(46, 3),
	(47, 3),
	(48, 3),
	(49, 3),
	(50, 3),
	(51, 3),
	(52, 3),
	(53, 3),
	(54, 3),
	(55, 3),
	(56, 3),
	(57, 3),
	(58, 3),
	(59, 3),
	(60, 3),
	(61, 3),
	(62, 3),
	(63, 3),
	(64, 3),
	(65, 3),
	(66, 3),
	(67, 3),
	(68, 3),
	(69, 3),
	(70, 3),
	(71, 3),
	(72, 3),
	(1, 4),
	(2, 4),
	(3, 4),
	(4, 4),
	(5, 4),
	(8, 4),
	(14, 4),
	(15, 4),
	(16, 4),
	(17, 4),
	(18, 4),
	(19, 4),
	(20, 4),
	(21, 4),
	(22, 4),
	(23, 4),
	(24, 4),
	(25, 4),
	(26, 4),
	(27, 4),
	(28, 4),
	(29, 4),
	(30, 4),
	(31, 4),
	(32, 4),
	(33, 4),
	(34, 4),
	(35, 4),
	(36, 4),
	(37, 4),
	(38, 4),
	(39, 4),
	(40, 4),
	(41, 4),
	(42, 4),
	(43, 4),
	(44, 4),
	(45, 4),
	(46, 4),
	(71, 4);
/*!40000 ALTER TABLE `permisorols` ENABLE KEYS */;

-- Dumping structure for table dblocal.permisos
CREATE TABLE IF NOT EXISTS `permisos` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `Key` varchar(50) DEFAULT NULL,
  `Descripcion` varchar(50) NOT NULL,
  `GrupoId` int DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_permisos_permisos_grupos` (`GrupoId`),
  CONSTRAINT `FK_permisos_permisos_grupos` FOREIGN KEY (`GrupoId`) REFERENCES `permisos_grupos` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8mb3;

-- Dumping data for table dblocal.permisos: ~72 rows (approximately)
/*!40000 ALTER TABLE `permisos` DISABLE KEYS */;
INSERT INTO `permisos` (`Id`, `Key`, `Descripcion`, `GrupoId`) VALUES
	(1, 'VER_PRODUCTOS', 'VER PRODUCTOS', 1),
	(2, 'AGREGAR_PRODUCTOS', 'AGREGAR PRODUCTOS', 1),
	(3, 'EDITAR_PRODUCTOS', 'EDITAR PRODUCTOS', 1),
	(4, 'ELIMINAR_PRODUCTOS', 'ELIMINAR PRODUCTOS', 1),
	(5, 'VER_CLIENTES', 'VER CLIENTES', 2),
	(6, 'EDITAR_CLIENTES', 'EDITAR CLIENTES', 2),
	(7, 'AGREGAR_CLIENTES', 'AGREGAR CLIENTES', 2),
	(8, 'ELIMINAR_CLIENTES', 'ELIMINARCLIENTES', 2),
	(9, 'VER_PAGOS_CLIENTES', 'VER PAGOS CLIENTES', 2),
	(10, 'IMPRIMIR_AVISOS_CLIENTE', 'IMPRIMIR AVISOS CLIENTE', 2),
	(11, 'IMPRIMIR_AVISOS_TODOS_CLIENTES', 'IMPRIMIR AVISOS TODOS CLIENTES', 2),
	(12, 'AGREGAR_PROVEEDORES', 'AGREGAR PROVEEDORES', 3),
	(13, 'VER_PROVEEDORES', 'VER PROVEEDORES', 3),
	(14, 'ELIMINAR_PROVEEDORES', 'ELIMINAR PROVEEDORES', 3),
	(15, 'EDITAR_PROVEEDORES', 'EDITAR PROVEEDORES', 3),
	(16, 'VER_INFO_PROVEEDORES', 'VER INFO PROVEEDORES', 3),
	(17, 'AGREGAR_EGRESO', 'AGREGAR EGRESO', 4),
	(18, 'VER_EGRESOS', 'VER EGRESOS', 4),
	(19, 'EDITAR_EGRESO', 'EDITAR EGRESO', 4),
	(20, 'ELIMINAR_EGRESO', 'ELIMINAR EGRESO', 4),
	(21, 'VER_CATEGORIAS', 'VER CATEGORIAS', 5),
	(22, 'AGREGAR_CATEGORIAS', 'AGREGAR CATEGORIAS', 5),
	(23, 'EDITAR_CATEGORIAS', 'EDITAR CATEGORIAS', 5),
	(24, 'ELIMINAR_CATEGORIAS', 'ELIMINAR CATEGORIAS', 5),
	(25, 'VER_CIUDADES', 'VER CIUDADES', 6),
	(26, 'AGREGAR_CIUDADES', 'AGREGAR CIUDADES', 6),
	(27, 'EDITAR_CIUDADES', 'EDITAR CIUDADES', 6),
	(28, 'ELIMINAR_CIUDADES', 'ELIMINAR CIUDADES', 6),
	(29, 'VER_IMPUESTOS', 'VER IMPUESTOS', 7),
	(30, 'AGREGAR_IMPUESTOS', 'AGREGAR IMPUESTOS', 7),
	(31, 'EDITAR_IMPUESTOS', 'EDITAR IMPUESTOS', 7),
	(32, 'ELIMINAR_IMPUESTOS', 'ELIMINAR IMPUESTOS', 7),
	(33, 'VER_ITEMS', 'VER ITEMS', 8),
	(34, 'AGREGAR_ITEMS', 'AGREGAR ITEMS', 8),
	(35, 'EDITAR_ITEMS', 'EDITAR ITEMS', 8),
	(36, 'ELIMINAR_ITEMS', 'ELIMINAR ITEMS', 8),
	(37, 'VER_MARCAS', 'VER MARCAS', 9),
	(38, 'AGREGAR_MARCAS', 'AGREGAR MARCAS', 9),
	(39, 'EDITAR_MARCAS', 'EDITAR MARCAS', 9),
	(40, 'ELIMINAR_MARCAS', 'ELIMINAR MARCAS', 9),
	(41, 'VER_MONEDAS', 'VER MONEDAS', 10),
	(42, 'AGREGAR_MONEDAS', 'AGREGAR MONEDAS', 10),
	(43, 'EDITAR_MONEDAS', 'EDITAR MONEDAS', 10),
	(44, 'ELIMINAR_MONEDAS', 'ELIMINAR MONEDAS', 10),
	(45, 'VER_METODOS_PAGO', 'VER METODOS PAGO', 11),
	(46, 'AGREGAR_METODOS_PAGO', 'AGREGAR METODOS PAGO', 11),
	(47, 'EDITAR_METODOS_PAGO', 'EDITAR METODOS PAGO', 11),
	(48, 'ELIMINAR_METODOS_PAGO', 'ELIMINAR METODOS PAGO', 11),
	(49, 'VER_ZONAS', 'VER ZONAS', 12),
	(50, 'AGREGAR_ZONAS', 'AGREGAR ZONAS', 12),
	(51, 'EDITAR_ZONAS', 'EDITAR ZONAS', 12),
	(52, 'ELIMINAR_ZONAS', 'ELIMINAR ZONAS', 12),
	(53, 'VER_USUARIOS', 'VER USUARIOS', 13),
	(54, 'AGREGAR_USUARIOS', 'AGREGAR USUARIOS', 13),
	(55, 'EDITAR_USUARIOS', 'EDITAR USUARIOS', 13),
	(56, 'ELIMINAR_USUARIOS', 'ELIMINAR USUARIOS', 13),
	(57, 'VER_ROLES', 'VER ROLES', 14),
	(58, 'AGREGAR_ROLES', 'AGREGAR ROLES', 14),
	(59, 'EDITAR_ROLES', 'EDITAR ROLES', 14),
	(60, 'ELIMINAR_ROLES', 'ELIMINAR ROLES', 14),
	(61, 'VER_REPORTES_CLIENTES', 'VER REPORTES CLIENTES', 15),
	(62, 'VER_INFO_REPORTES_CLIENTES', 'VER INFO REPORTES CLIENTES', 15),
	(63, 'IMPRIMIR_PDF_REPORTES_CLIENTES', 'IMPRIMIR_PDF REPORTES CLIENTES', 15),
	(64, 'IMPRIMIR_EXCEL_REPORTES_CLIENTES', 'IMPRIMIR EXCEL REPORTES CLIENTES', 15),
	(65, 'VER_REPORTES_DEUDORES', 'VER REPORTES DEUDORES', 16),
	(66, 'IMPRIMIR_PDF_REPORTES_DEUDORES', 'IMPRIMIR PDF REPORTES DEUDORES', 16),
	(67, 'IMPRIMIR_EXCEL_REPORTES_DEUDORES', 'IMPRIMIR EXCEL REPORTES DEUDORES', 16),
	(68, 'VER_REPORTES_INGRESOS-EGRESOS', 'VER REPORTES INGRESOS-EGRESOS', 17),
	(69, 'IMPRIMIR_PDF_REPORTES_INGRESOS-EGRESOS', 'IMPRIMIR PDF REPORTES INGRESOS-EGRESOS', 17),
	(70, 'IMPRIMIR_EXCEL_REPORTES_INGRESOS-EGRESOS', 'IMPRIMIR EXCEL REPORTES INGRESOS-EGRESOS', 17),
	(71, 'CERRAR_MES', 'CERRAR MES', 18),
	(72, 'EDITAR_PARAMETROS', 'EDITAR PARAMETROS', 19);
/*!40000 ALTER TABLE `permisos` ENABLE KEYS */;

-- Dumping structure for table dblocal.permisos_grupos
CREATE TABLE IF NOT EXISTS `permisos_grupos` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb3;

-- Dumping data for table dblocal.permisos_grupos: ~19 rows (approximately)
/*!40000 ALTER TABLE `permisos_grupos` DISABLE KEYS */;
INSERT INTO `permisos_grupos` (`Id`, `Nombre`) VALUES
	(1, 'PRODUCTOS'),
	(2, 'CLIENTES'),
	(3, 'PROVEEDORES'),
	(4, 'EGRESOS'),
	(5, 'CATEGORIAS'),
	(6, 'CIUDADES'),
	(7, 'IMPUESTOS'),
	(8, 'ITEMS'),
	(9, 'MARCAS'),
	(10, 'MONEDAS'),
	(11, 'METODOS PAGO'),
	(12, 'ZONAS'),
	(13, 'USUARIOS'),
	(14, 'ROLES'),
	(15, 'REPORTES_CLIENTES'),
	(16, 'REPORTES_DEUDORES'),
	(17, 'REPORTES_INGRESOS-EGRESOS'),
	(18, 'CERRAR_MES'),
	(19, 'EDITAR_PARAMETROS');
/*!40000 ALTER TABLE `permisos_grupos` ENABLE KEYS */;

-- Dumping structure for table dblocal.producto
CREATE TABLE IF NOT EXISTS `producto` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `Codigo` varchar(50) NOT NULL,
  `Descripcion` varchar(100) DEFAULT NULL,
  `MarcaId` int DEFAULT NULL,
  `Activo` tinyint NOT NULL,
  `Servicio` tinyint DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Codigo` (`Codigo`),
  KEY `FK_producto_marca` (`MarcaId`),
  CONSTRAINT `FK_producto_marca` FOREIGN KEY (`MarcaId`) REFERENCES `marca` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=utf8mb3;

-- Dumping data for table dblocal.producto: ~10 rows (approximately)
/*!40000 ALTER TABLE `producto` DISABLE KEYS */;
/*!40000 ALTER TABLE `producto` ENABLE KEYS */;

-- Dumping structure for table dblocal.producto_precio
CREATE TABLE IF NOT EXISTS `producto_precio` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `ProductoId` int NOT NULL,
  `Precio` double NOT NULL,
  `Desde` datetime NOT NULL,
  `ImpuestoId` int DEFAULT NULL,
  `Activo` tinyint NOT NULL,
  `MonedaId` int DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_producto_precio_impuesto` (`ImpuestoId`),
  KEY `FK_producto_precio_producto_precio` (`ProductoId`),
  KEY `MonedaId` (`MonedaId`),
  CONSTRAINT `FK_producto_precio_impuesto` FOREIGN KEY (`ImpuestoId`) REFERENCES `impuesto` (`Id`),
  CONSTRAINT `FK_producto_precio_moneda` FOREIGN KEY (`MonedaId`) REFERENCES `moneda` (`Id`),
  CONSTRAINT `FK_producto_precio_producto` FOREIGN KEY (`ProductoId`) REFERENCES `producto` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=utf8mb3;

-- Dumping data for table dblocal.producto_precio: ~16 rows (approximately)
/*!40000 ALTER TABLE `producto_precio` DISABLE KEYS */;
/*!40000 ALTER TABLE `producto_precio` ENABLE KEYS */;

-- Dumping structure for table dblocal.producto_precio_metodo_pago
CREATE TABLE IF NOT EXISTS `producto_precio_metodo_pago` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `ProductoPrecioId` int NOT NULL,
  `MetodoPagoId` int NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `ProductoPrecioId_MetodoPagoId` (`ProductoPrecioId`,`MetodoPagoId`),
  KEY `FK_producto_precio_metodo_pago_metodo_pago` (`MetodoPagoId`),
  CONSTRAINT `FK_producto_precio_metodo_pago_metodo_pago` FOREIGN KEY (`MetodoPagoId`) REFERENCES `metodo_pago` (`Id`),
  CONSTRAINT `FK_producto_precio_metodo_pago_producto_precio` FOREIGN KEY (`ProductoPrecioId`) REFERENCES `producto_precio` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=206 DEFAULT CHARSET=utf8mb3;

-- Dumping data for table dblocal.producto_precio_metodo_pago: ~29 rows (approximately)
/*!40000 ALTER TABLE `producto_precio_metodo_pago` DISABLE KEYS */;
/*!40000 ALTER TABLE `producto_precio_metodo_pago` ENABLE KEYS */;

-- Dumping structure for table dblocal.proveedor
CREATE TABLE IF NOT EXISTS `proveedor` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `RazonSocial` varchar(50) DEFAULT NULL,
  `RUT` varchar(50) DEFAULT NULL,
  `Direccion` varchar(100) DEFAULT NULL,
  `Telefono` varchar(50) DEFAULT NULL,
  `Correo` varchar(100) DEFAULT NULL,
  `Contacto` varchar(100) DEFAULT NULL,
  `ContactoTelefono` varchar(100) DEFAULT NULL,
  `ContactoCorreo` varchar(100) DEFAULT NULL,
  `Activo` tinyint NOT NULL,
  `Nombre` varchar(50) DEFAULT NULL,
  `Ciudad` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `RUT` (`RUT`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb3;

-- Dumping data for table dblocal.proveedor: ~6 rows (approximately)
/*!40000 ALTER TABLE `proveedor` DISABLE KEYS */;
/*!40000 ALTER TABLE `proveedor` ENABLE KEYS */;

-- Dumping structure for table dblocal.proveedor_banco
CREATE TABLE IF NOT EXISTS `proveedor_banco` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `ProveedorId` int DEFAULT NULL,
  `Nombre` varchar(50) DEFAULT NULL,
  `Cuenta` varchar(50) DEFAULT NULL,
  `CuentaTipoId` int DEFAULT NULL,
  `MonedaId` int DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id` (`Id`),
  KEY `ProveedorId` (`ProveedorId`),
  KEY `FK_proveedor_banco_cuenta_tipo` (`CuentaTipoId`),
  KEY `FK_proveedor_banco_moneda` (`MonedaId`),
  CONSTRAINT `FK_proveedor_banco_cuenta_tipo` FOREIGN KEY (`CuentaTipoId`) REFERENCES `cuenta_tipo` (`Id`),
  CONSTRAINT `FK_proveedor_banco_moneda` FOREIGN KEY (`MonedaId`) REFERENCES `moneda` (`Id`),
  CONSTRAINT `FK_proveedor_banco_proveedor` FOREIGN KEY (`ProveedorId`) REFERENCES `proveedor` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table dblocal.proveedor_banco: ~5 rows (approximately)
/*!40000 ALTER TABLE `proveedor_banco` DISABLE KEYS */;
/*!40000 ALTER TABLE `proveedor_banco` ENABLE KEYS */;

-- Dumping structure for table dblocal.rol
CREATE TABLE IF NOT EXISTS `rol` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `Activo` tinyint NOT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3;

-- Dumping data for table dblocal.rol: ~4 rows (approximately)
/*!40000 ALTER TABLE `rol` DISABLE KEYS */;
INSERT INTO `rol` (`Id`, `Nombre`, `Activo`) VALUES
	(1, 'Operador1', 1),
	(2, 'no operador', 1),
	(3, 'Admin', 1),
	(4, 'Operador prueba', 1);
/*!40000 ALTER TABLE `rol` ENABLE KEYS */;

-- Dumping structure for table dblocal.usuario
CREATE TABLE IF NOT EXISTS `usuario` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `Apellido` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `Username` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `Password` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  `LastPasswordChange` datetime DEFAULT NULL,
  `LastLogin` datetime DEFAULT NULL,
  `Email` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `ChangePasswordToken` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  `Activo` tinyint NOT NULL,
  `IsAdmin` tinyint NOT NULL,
  `RolId` int DEFAULT NULL,
  `TokenDate` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE,
  KEY `FK_usuarios_roles` (`RolId`),
  CONSTRAINT `FK_usuarios_roles` FOREIGN KEY (`RolId`) REFERENCES `rol` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb3;

-- Dumping data for table dblocal.usuario: ~3 rows (approximately)
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` (`Id`, `Nombre`, `Apellido`, `Username`, `Password`, `LastPasswordChange`, `LastLogin`, `Email`, `ChangePasswordToken`, `Activo`, `IsAdmin`, `RolId`, `TokenDate`) VALUES
	(1, 'admin', 'admin', 'admin', 'wg7cd8O4gBPFuKP3Cha/EP97EgDEG/zYgdPnDIY5ZRr2NP5s', '2021-08-19 16:24:49', '2021-11-22 23:41:28', 'fhernandez@todosoft.com.uy', NULL, 1, 1, 3, NULL);
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;

-- Dumping structure for table dblocal.zona
CREATE TABLE IF NOT EXISTS `zona` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `Codigo` varchar(50) NOT NULL,
  `Descripcion` varchar(100) DEFAULT NULL,
  `Activo` tinyint NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Codigo` (`Codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb3;

-- Dumping data for table dblocal.zona: ~7 rows (approximately)
/*!40000 ALTER TABLE `zona` DISABLE KEYS */;
INSERT INTO `zona` (`Id`, `Codigo`, `Descripcion`, `Activo`) VALUES
	(12, 'C8', 'C8', 1);
/*!40000 ALTER TABLE `zona` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
